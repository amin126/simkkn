function _tab(a){
    var t452 = (a.tab !=undefined) ? a.tab: ".tab-menu";
    var b452 = (a.body !=undefined) ? a.body: ".body-tab";
    var d452 = (a.default !=undefined) ? a.default: "#upload";
    var u452 = document.URL;
    $('.tab-menu').click(function(event) {
        $('.tab-menu').removeClass('active');
        $(this).addClass('active');
        var i452 = $(this).attr('href');
        $(b452).hide(0);
        $(i452).show(0);
    });
    $(b452).hide(0);
    $(d452).show(0);
    u452 = u452.split('#');
    u452 = (u452[1]!=undefined) ? '#'+u452[1] : d452 ;
    $(t452+'[href="'+u452+'"]').trigger('click');
}

function _toTab(a,b){
    $(a).hide(0);
    $(b).show(0);
}

function _alert(a,b,c){
    if(a!=undefined && b!=undefined && c!=undefined){
        // success, warning, error
        swal(a, b, c);
    }else if(a!=undefined && b!=undefined && c==undefined){
        swal(a, b);
    }else if(a!=undefined && b==undefined && c==undefined){
        if(typeof(a)=='object'){
            if(a.mode=='confirm'){
                var conf = {   
                    title: (a.title !=undefined) ? a.title : "Question?",   
                    text: (a.msg !=undefined) ? a.msg : "It's a confirmation to next process!",   
                    type: (a.type !=undefined) ? a.type : "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: (a.bcolor !=undefined) ? a.bcolor : "#66BB6A",   
                    confirmButtonText: (a.yes !=undefined) ? a.yes : "Yes it",   
                    cancelButtonText: (a.no !=undefined) ? a.no : "No",   
                    closeOnConfirm: (a.closey !=undefined) ? a.closey : true,   
                    closeOnCancel: (a.closen !=undefined) ? a.closen : true 
                };
                swal(conf, function(isConfirm){   
                    if (isConfirm) {
                        if(typeof(a.isConfirm)=='function')
                            a.isConfirm();
                    } else {     
                        if(typeof(a.isCancel)=='function')
                            a.isCancel();
                    } 
                });
            }else{
                var conf = {   
                        title: (a.title !=undefined) ? a.title :"It is title",   
                        text: (a.msg !=undefined) ? a.msg : "It is a message",  
                        type: (a.type !=undefined) ? a.type : "warning",

                    };
                if(a.autoClose!=undefined){
                    conf.timer= a.autoClose,
                    conf.showConfirmButton= (a.close !=undefined) ? a.close : false
                }
                swal(conf,function(){
                    if(typeof(a.isClose)=='function')
                        a.isClose();
                });
            }
        }else{
            swal(a);
        }
    }
}

function _setValue(j,v,l){
    if(l!=false){
        l = (l==undefined) ? v : l;
        $('[name="'+j+'"]').val(v);
    }else{
        l = v;
        $('[name="'+j+'"]').val('');
    }
    $('[label="'+j+'"]').text(l);
}

function _form(n){
    for(a in n){
        $('[name="'+a+'"]').val(n[a]);
    }
}

var _ajaxProcess = false;
function _ajax(a){
    if(_ajaxProcess == false){
        _ajaxProcess = true;
        $.ajax({
            url:(a.url !=undefined) ? a.url: "",
            data:(a.data !=undefined) ? a.data : {},
            beforeSend:function(){
                if(a.append==true){
                    $(a.loading).append('<div class="progressMaterial">'+
                                      '<div class="indeterminate"></div>'+
                                  '</div>');
                    $(a.loading+' .progressMaterial').css('margin-top', '-4px');
                }else{
                    $(a.loading).prepend('<div class="progressMaterial">'+
                                      '<div class="indeterminate"></div>'+
                                  '</div>');
                    $(a.loading+' .progressMaterial').css('margin-bottom', '-4px');
                }
                $(a.loading+' .progressMaterial').css('background-color', ((a.colorback !=undefined) ? a.colorback : '#009efb36'));
                $(a.loading+' .progressMaterial .indeterminate').css('background-color', ((a.coloranim !=undefined) ? a.coloranim : '#f62d51'));
                if(typeof(a.before)=='function')
                    a.before();
            },
            type:'post',
            success(res){
                _ajaxProcess = false;
                $(a.loading+' .progressMaterial').remove();
                if(typeof(a.success)=='function')
                    a.success(res);
            }
        })
    }
}

// start progress
var _esProgress;
var _statusProgress = false;
function _progress(a) { //url,data,start,running,end,stop,error,append,loading
    if(_statusProgress == false){
        _statusProgress = true;
        var url = (a.url !=undefined) ? a.url : 'progress.php';
        if(typeof(a.data)=='object'){
            if(a.framework == true)
                url += '/';
            else
                url += '?';
          var iprc = 0;           
          for(var s in a.data){
            if(iprc>0)
                if(a.framework == true)
                    url += '/';
                else
                    url += '&';
            if(a.framework == true)
                url += a.data[s];
            else
                url += s + '=' + a.data[s];
            iprc++;
          }
        }

        _esProgress = new EventSource(encodeURI(url));

        if(a.append==true){
            $(a.loading).append('<div class="progressMaterial">'+
                              '<div class="indeterminate"></div>'+
                          '</div>');
            $(a.loading+' .progressMaterial').css('margin-top', '-4px');
        }else{
            $(a.loading).prepend('<div class="progressMaterial">'+
                              '<div class="indeterminate"></div>'+
                          '</div>');
            $(a.loading+' .progressMaterial').css('margin-bottom', '-4px');
        }
        $(a.loading+' .progressMaterial').css('background-color', ((a.colorback !=undefined) ? a.colorback : '#009efb36'));
        $(a.loading+' .progressMaterial .indeterminate').css('background-color', ((a.coloranim !=undefined) ? a.coloranim : '#f62d51'));
          
        //a message is received
        _esProgress.addEventListener('message', function(e) {
            var result = JSON.parse( e.data );


        
            if(e.lastEventId == 'START') {
                if(typeof(a.start)=='function')
                    a.start(result);
            }else if(e.lastEventId == 'CLOSE') {
                _statusProgress = false;
                $(a.loading+' .progressMaterial').remove();
                if(typeof(a.end)=='function')
                    a.end(result);
                _esProgress.close();
            }else{
                if(a.callback != undefined){
                    if(typeof(a.callback[e.lastEventId])=='function')
                        a.callback[e.lastEventId](result);
                    else
                        if(typeof(a.running)=='function')
                            a.running(result);
                }else{
                    if(typeof(a.running)=='function')
                            a.running(result);
                }
            }
        });
          
        _esProgress.addEventListener('error', function(e) {
            if(typeof(a.error)=='function')
                a.error(result);
            _esProgress.close();
        });
    }else{

    }
}
  
function _endProgress() {
    _statusProgress = false;
    _esProgress.close();
     if(typeof(a.stop)=='function')
         a.stop();
}
  
// end progress

function _cetak(t){
    //gunakan di dalam atribut form onsubmit="_cetak(this)"
    //gunakan di target onload="self.print();window.close();"
    var height = $(window).height()-100;
    var width = $(window).width()-100;
    var w = window.open('about:blank','Popup_Window',"toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width="+width+",height="+height+",top=50,left=50");
    t.target = 'Popup_Window';
}

// upload ajax

function _(el) {
  return document.getElementById(el);
}

var prgs235 = null;
var err235 = null;
var aborts235 = null;
var cmpl235 = null;

function _upload(a = {}) {
  prgs235 = (a.progress !=undefined) ? a.progress : null;
  cmpl235 = (a.complete !=undefined) ? a.complete : null;
  err235 = (a.error !=undefined) ? a.error : null;
  abort235 = (a.abort !=undefined) ? a.abort : null;
  var formdata = new FormData();
  if(typeof(a.name)=='object'){
    for (var inx in a.name) {
        var name =  null;
        var val = null;
        var dxf = null;
        if ($.isNumeric(inx)==true){
            dxf = _((a.name[inx] !=undefined) ? a.name[inx] : 'formdata');
            name = a.name[inx];
            val = dxf.value;
            if(dxf.files==null){
                formdata.append((name !=undefined) ? name : 'dataname', val);
            }
            else{
                formdata.append((name !=undefined) ? name : 'fileUpload', dxf.files[0]);
            }
        }else{
            formdata.append((inx !=undefined) ? inx : 'dataname', a.name[inx]);
        }
        
    }
  }else{
    var data = _((a.name !=undefined) ? a.name : 'fileUpload');
    if(data.files==null)
        formdata.append((a.name !=undefined) ? a.name : 'dataname', data.value);
    else
        formdata.append((a.name !=undefined) ? a.name : 'fileUpload', data.files[0]);
  }
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", (a.action !=undefined) ? a.action : 'upload_parse.php');
  ajax.send(formdata);
}

function progressHandler(event) {
  var percent = (event.loaded / event.total) * 100;
  if(typeof(prgs235)=='function')
    prgs235({
        'percent':Math.round(percent),
        'loaded':event.loaded,
        'total':event.total
    });
}

function completeHandler(event) {
    if(typeof(cmpl235)=='function')
        cmpl235(event.target.responseText);
}

function errorHandler(event) {
    if(typeof(err235)=='function')
        err235(false);
}

function abortHandler(event) {
   if(typeof(abort235)=='function')
        abort235(false);
}

// end upload ajax

// fungsi global
$(function () {
  $('[data-toggle="tooltip"]').tooltip();

  // number input
    $(".number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

  // currency
  $('.currency').on('keydown', function(e) {
    
      if (this.selectionStart || this.selectionStart == 0) {
        // selectionStart won't work in IE < 9
        
        var key = e.which;
        var prevDefault = true;
        
        var thouSep = ",";  // your seperator for thousands
        var deciSep = ".";  // your seperator for decimals
        var deciNumber = 2; // how many numbers after the comma
        
        var thouReg = new RegExp(thouSep,"g");
        var deciReg = new RegExp(deciSep,"g");
        
        function spaceCaretPos(val, cPos) {
            /// get the right caret position without the spaces
            
            if (cPos > 0 && val.substring((cPos-1),cPos) == thouSep)
              cPos = cPos-1;
            
            if (val.substring(0,cPos).indexOf(thouSep) >= 0) {
              cPos = cPos - val.substring(0,cPos).match(thouReg).length;
            }
            
            return cPos;
        }
        
        function spaceFormat(val, pos) {
            /// add spaces for thousands
            
            if (val.indexOf(deciSep) >= 0) {
                var comPos = val.indexOf(deciSep);
                var int = val.substring(0,comPos);
                var dec = val.substring(comPos);
            } else{
                var int = val;
                var dec = "";
            }
            var ret = [val, pos];
            
            if (int.length > 3) {
                
                var newInt = "";
                var spaceIndex = int.length;
                
                while (spaceIndex > 3) {
                    spaceIndex = spaceIndex - 3;
                    newInt = thouSep+int.substring(spaceIndex,spaceIndex+3)+newInt;
                    if (pos > spaceIndex) pos++;
                }
                ret = [int.substring(0,spaceIndex) + newInt + dec, pos];
            }
            return ret;
        }
        
        $(this).on('keyup', function(ev) {
            
            if (ev.which == 8) {
                // reformat the thousands after backspace keyup
                
                var value = this.value;
                var caretPos = this.selectionStart;
                
                caretPos = spaceCaretPos(value, caretPos);
                value = value.replace(thouReg, '');
                
                var newValues = spaceFormat(value, caretPos);
                this.value = newValues[0];
                this.selectionStart = newValues[1];
                this.selectionEnd   = newValues[1];
            }
        });
        
        if ((e.ctrlKey && (key == 65 || key == 67 || key == 86 || key == 88 || key == 89 || key == 90)) ||
           (e.shiftKey && key == 9)) // You don't want to disable your shortcuts!
            prevDefault = false;
        
        if ((key < 37 || key > 40) && key != 8 && key != 9 && prevDefault) {
            e.preventDefault();
            
            if (!e.altKey && !e.shiftKey && !e.ctrlKey) {
            
                var value = this.value;
                if ((key > 95 && key < 106)||(key > 47 && key < 58) ||
                  (deciNumber > 0 && (key == 110 || key == 188 || key == 190))) {
                    
                    var keys = { // reformat the keyCode
              48: 0, 49: 1, 50: 2, 51: 3,  52: 4,  53: 5,  54: 6,  55: 7,  56: 8,  57: 9,
              96: 0, 97: 1, 98: 2, 99: 3, 100: 4, 101: 5, 102: 6, 103: 7, 104: 8, 105: 9,
              110: deciSep, 188: deciSep, 190: deciSep
                    };
                    
                    var caretPos = this.selectionStart;
                    var caretEnd = this.selectionEnd;
                    
                    if (caretPos != caretEnd) // remove selected text
                    value = value.substring(0,caretPos) + value.substring(caretEnd);
                    
                    caretPos = spaceCaretPos(value, caretPos);
                    
                    value = value.replace(thouReg, '');
                    
                    var before = value.substring(0,caretPos);
                    var after = value.substring(caretPos);
                    var newPos = caretPos+1;
                    
                    if (keys[key] == deciSep && value.indexOf(deciSep) >= 0) {
                        if (before.indexOf(deciSep) >= 0) newPos--;
                        before = before.replace(deciReg, '');
                        after = after.replace(deciReg, '');
                    }
                    var newValue = before + keys[key] + after;
                    
                    if (newValue.substring(0,1) == deciSep) {
                        newValue = "0"+newValue;
                        newPos++;
                    }
                    
                    while (newValue.length > 1 && newValue.substring(0,1) == "0" && newValue.substring(1,2) != deciSep) {
                        newValue = newValue.substring(1);
                        newPos--;
                    }
                    
                    if (newValue.indexOf(deciSep) >= 0) {
                        var newLength = newValue.indexOf(deciSep)+deciNumber+1;
                        if (newValue.length > newLength) {
                          newValue = newValue.substring(0,newLength);
                        }
                    }
                    
                    newValues = spaceFormat(newValue, newPos);
                    
                    this.value = newValues[0];
                    this.selectionStart = newValues[1];
                    this.selectionEnd   = newValues[1];
                }
            }
        }
        
        $(this).on('blur', function(e) {
            
            if (deciNumber > 0) {
                var value = this.value;
                
                var noDec = "";
                for (var i = 0; i < deciNumber; i++) noDec += "0";
                
                if (value == "0" + deciSep + noDec) {
            this.value = ""; //<-- put your default value here
          } else if(value.length > 0) {
                    if (value.indexOf(deciSep) >= 0) {
                        var newLength = value.indexOf(deciSep) + deciNumber + 1;
                        if (value.length < newLength) {
                          while (value.length < newLength) value = value + "0";
                          this.value = value.substring(0,newLength);
                        }
                    }
                    else this.value = value + deciSep + noDec;
                }
            }
        });
      }
    });
})
// end global