$(document).ready(function() {

  notifikasi()

});

function notifikasi(){
	var urlnotiv = window.location.protocol+'//'+window.location.hostname+'/ta/lppm/admin/notif';
  var esprogressnotiv = new EventSource(encodeURI(urlnotiv));
    
  //a message is received
  esprogressnotiv.addEventListener('message', function(e) {
      var resultnotif = JSON.parse( e.data );      
      var fullHtml = ''; 
      if(e.lastEventId != 'START'){
      	$('#notifikasi .jmerah').html((resultnotif.baru>0)?'<span class="count">'+resultnotif.baru+'</span>':'');
      	$('#notifikasi .jpesan').text(resultnotif.baru);
      	$('#notifikasi .msgjumlah').text((resultnotif.baru==0)? 'Tidak ada notif baru':'Anda memiliki '+resultnotif.baru+' notif baru');
      	resultnotif.data.forEach(function(val, index){
      		fullHtml += htmlRow({
      			kelompok:val.kelompok,
      			keterangan:val.keterangan,
      			waktu:val.tgl,
      			id:val.id,
      			status:val.status,
      			url:val.url
      		});
      	});
      	$('#notifikasi .list-notifikasi').prepend(fullHtml);
      	esprogressnotiv.close();
        setTimeout(notifikasi, 5000);
      }
  });
    
  esprogressnotiv.addEventListener('error', function(e) {
		
  });
}

function htmlRow(data){
	if($('#notif-'+data.id).length==0){
		return '<a href="'+data.url+'" class="dropdown-item preview-item" style="white-space:unset !important;" id="notif-'+data.id+'">'+
              '<div class="preview-item-content">'+
                '<div class="preview-subject '+((data.status==1)?'font-weight-bold text-primary':'font-weight-medium')+' ">'+data.keterangan+'</div>'+
                '<p class="font-weight-light small-text pull-right '+((data.status==0)?'text-muted':'text-primary')+'  mt-2">'+
                  data.waktu+
                '</p>'+
              '</div>'+
            '</a>'+
            '<div class="dropdown-divider"></div>';
	}else{
  	return '';
  }
}