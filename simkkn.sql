-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 27 Jul 2018 pada 03.32
-- Versi server: 10.0.35-MariaDB
-- Versi PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `irandevi_simkkn`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `KDDPL` varchar(20) NOT NULL,
  `NAMADPL` varchar(50) DEFAULT NULL,
  `KONTAKDPL` varchar(25) DEFAULT NULL,
  `ALAMATDPL` varchar(255) DEFAULT NULL,
  `FOTODPL` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kapasitas_prodi`
--

CREATE TABLE `kapasitas_prodi` (
  `KDKAPRODI` int(11) NOT NULL,
  `KDPRODI` int(11) DEFAULT NULL,
  `KDKEL` int(11) DEFAULT NULL,
  `KAPRODI` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `KDKEL` int(11) NOT NULL,
  `KDTAHUN` int(11) DEFAULT NULL,
  `KDDPL` varchar(20) DEFAULT NULL,
  `ALAMATKEL` varchar(255) NOT NULL,
  `NAMAKEL` varchar(32) DEFAULT NULL,
  `KAPASITASKEL` int(11) DEFAULT NULL,
  `DEFKAPPRODI` int(1) DEFAULT NULL,
  `KORDXKEL` varchar(32) DEFAULT NULL,
  `KORDYKEL` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok_peserta`
--

CREATE TABLE `kelompok_peserta` (
  `KDKP` int(11) NOT NULL,
  `KDKEL` int(11) DEFAULT NULL,
  `NPM` varchar(12) NOT NULL,
  `NILAIMHS` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lembar_monitoring`
--

CREATE TABLE `lembar_monitoring` (
  `KDLM` int(11) NOT NULL,
  `KDKEL` int(11) DEFAULT NULL,
  `FILELM` varchar(255) DEFAULT NULL,
  `SIZELM` varchar(15) DEFAULT NULL,
  `TGLLM` datetime DEFAULT NULL,
  `HASHLM` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `NPM` varchar(12) NOT NULL,
  `TGLREGMHS` datetime DEFAULT NULL,
  `NAMAMHS` varchar(32) DEFAULT NULL,
  `ALAMATMHS` varchar(255) DEFAULT NULL,
  `KONTAKMHS` varchar(20) DEFAULT NULL,
  `SERAGAMMHS` enum('xl','l','m','s') DEFAULT NULL,
  `KDPRODI` int(11) DEFAULT NULL,
  `SKSMHS` int(3) DEFAULT NULL,
  `PEKERJAANMHS` varchar(255) DEFAULT NULL,
  `KORDXMHS` varchar(32) DEFAULT NULL,
  `KORDYMHS` varchar(32) DEFAULT NULL,
  `FOTOMHS` varchar(100) DEFAULT NULL,
  `STATUSMHS` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa_mendaftar`
--

CREATE TABLE `mahasiswa_mendaftar` (
  `NPM` varchar(12) NOT NULL,
  `TGLREGMHS` datetime DEFAULT NULL,
  `NAMAMHS` varchar(32) DEFAULT NULL,
  `ALAMATMHS` varchar(255) DEFAULT NULL,
  `KONTAKMHS` varchar(20) DEFAULT NULL,
  `SERAGAMMHS` enum('xl','l','m','s') DEFAULT NULL,
  `KDPRODI` int(11) DEFAULT NULL,
  `SKSMHS` int(3) DEFAULT NULL,
  `PEKERJAANMHS` varchar(255) DEFAULT NULL,
  `KORDXMHSD` varchar(32) DEFAULT NULL,
  `KORDYMHSD` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `KDNOTIF` varchar(20) NOT NULL,
  `TGLNOTIF` datetime DEFAULT NULL,
  `KETNOTIF` varchar(255) DEFAULT NULL,
  `URLNOTIF` varchar(255) DEFAULT NULL,
  `STATUSNOTIF` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `no_ordik`
--

CREATE TABLE `no_ordik` (
  `NPMORD` varchar(12) NOT NULL,
  `NAMAORD` varchar(32) DEFAULT NULL,
  `HPORD` varchar(15) DEFAULT NULL,
  `ALAMATORD` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `JENISSET` varchar(20) NOT NULL,
  `VALUESET` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`JENISSET`, `VALUESET`) VALUES
('api_key', 'AIzaSyB5NXz9eVnyJOA81wimI8WYE08kW_JMe8g'),
('auto_skedul', '0'),
('biro_pesan', '-'),
('edit_biodata_mhs', '1'),
('halaman_biro', '0'),
('halaman_camat', '0'),
('halaman_daftar', '0'),
('halaman_dpl', '0'),
('halaman_peserta', '1'),
('hal_pstkelompok', '0'),
('kabupaten_asal', 'sampang|pamekasan'),
('minimal_prodi', '3'),
('model_pembentukan', 'e'),
('tgl_skedul', '2018-07-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `KDPRODI` int(11) NOT NULL,
  `FAKPRODI` varchar(50) DEFAULT NULL,
  `NAMAPRODI` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `KDTAHUN` int(11) NOT NULL,
  `NAMATAHUN` varchar(15) DEFAULT NULL,
  `STATUSTAHUN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `upload`
--

CREATE TABLE `upload` (
  `KDUPL` int(11) NOT NULL,
  `FILEUPL` varchar(255) DEFAULT NULL,
  `SIZEUPL` varchar(15) DEFAULT NULL,
  `TGLUPL` datetime DEFAULT NULL,
  `NPM` varchar(12) DEFAULT NULL,
  `JENISUPL` varchar(32) DEFAULT NULL,
  `DESKUPL` varchar(255) DEFAULT NULL,
  `AJUKANUPL` int(1) DEFAULT NULL,
  `MSGUPL` text,
  `HASHUPL` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `USERN` varchar(32) NOT NULL,
  `NAMA` varchar(32) DEFAULT NULL,
  `PASSN` varchar(32) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`USERN`, `NAMA`, `PASSN`, `STATUS`) VALUES
('admin', 'LPPM', '158098e3af50e116215d869aa5559f13', 1),
('biro', 'BIRO KEMAHASISWAAN', 'd08411bef44274640c115f5f53301636', 3),
('camat', 'BAPAK CAMAT', 'd08411bef44274640c115f5f53301636', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`KDDPL`);

--
-- Indeks untuk tabel `kapasitas_prodi`
--
ALTER TABLE `kapasitas_prodi`
  ADD PRIMARY KEY (`KDKAPRODI`),
  ADD KEY `KDPRODI` (`KDPRODI`),
  ADD KEY `KDKEL` (`KDKEL`);

--
-- Indeks untuk tabel `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`KDKEL`),
  ADD KEY `FK_RELATIONSHIP_3` (`KDTAHUN`),
  ADD KEY `KDDPL` (`KDDPL`);

--
-- Indeks untuk tabel `kelompok_peserta`
--
ALTER TABLE `kelompok_peserta`
  ADD PRIMARY KEY (`KDKP`),
  ADD KEY `FK_RELATIONSHIP_11` (`KDKEL`),
  ADD KEY `NPM` (`NPM`);

--
-- Indeks untuk tabel `lembar_monitoring`
--
ALTER TABLE `lembar_monitoring`
  ADD PRIMARY KEY (`KDLM`),
  ADD KEY `KDKEL` (`KDKEL`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`NPM`),
  ADD KEY `KDPRODI` (`KDPRODI`);

--
-- Indeks untuk tabel `mahasiswa_mendaftar`
--
ALTER TABLE `mahasiswa_mendaftar`
  ADD PRIMARY KEY (`NPM`),
  ADD KEY `KDPRODI` (`KDPRODI`);

--
-- Indeks untuk tabel `no_ordik`
--
ALTER TABLE `no_ordik`
  ADD PRIMARY KEY (`NPMORD`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`JENISSET`);

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`KDPRODI`);

--
-- Indeks untuk tabel `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`KDTAHUN`);

--
-- Indeks untuk tabel `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`KDUPL`),
  ADD KEY `NPM` (`NPM`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`USERN`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kapasitas_prodi`
--
ALTER TABLE `kapasitas_prodi`
  MODIFY `KDKAPRODI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5261;

--
-- AUTO_INCREMENT untuk tabel `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `KDKEL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `kelompok_peserta`
--
ALTER TABLE `kelompok_peserta`
  MODIFY `KDKP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6740;

--
-- AUTO_INCREMENT untuk tabel `prodi`
--
ALTER TABLE `prodi`
  MODIFY `KDPRODI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT untuk tabel `tahun`
--
ALTER TABLE `tahun`
  MODIFY `KDTAHUN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `upload`
--
ALTER TABLE `upload`
  MODIFY `KDUPL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kapasitas_prodi`
--
ALTER TABLE `kapasitas_prodi`
  ADD CONSTRAINT `FK_RELATIONSHIP_KELOMPOK` FOREIGN KEY (`KDKEL`) REFERENCES `kelompok` (`KDKEL`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_RELATIONSHIP_PRODI` FOREIGN KEY (`KDPRODI`) REFERENCES `prodi` (`KDPRODI`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelompok`
--
ALTER TABLE `kelompok`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`KDTAHUN`) REFERENCES `tahun` (`KDTAHUN`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_RELATIONSHIP_DOSEN` FOREIGN KEY (`KDDPL`) REFERENCES `dosen` (`KDDPL`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelompok_peserta`
--
ALTER TABLE `kelompok_peserta`
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`KDKEL`) REFERENCES `kelompok` (`KDKEL`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_RELATIONSHIP_MAHASISWA` FOREIGN KEY (`NPM`) REFERENCES `mahasiswa` (`NPM`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `lembar_monitoring`
--
ALTER TABLE `lembar_monitoring`
  ADD CONSTRAINT `lembar_monitoring_ibfk_1` FOREIGN KEY (`KDKEL`) REFERENCES `kelompok` (`KDKEL`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
