<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/profilModel');
		$this->load->model('dosen/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->profilModel->get();
		$agt = $this->profilModel->anggota();
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('dosen/profil',array(
			'data'=>$data,
			'anggota'=>$agt
		));
		$this->load->view('dosen/footer');
	}

	public function biodata($npm){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->profilModel->biodata($npm);
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		if($data->FOTOMHS!=NULL){
			if (strpos($data->FOTOMHS, 'unira') !== false) {
			    $foto = $data->FOTOMHS;
			}else{
				$foto = base_url($data->FOTOMHS);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$data->NPM));
			$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$this->load->view('dosen/profil-mhs',array(
			'alamat'=>$data->ALAMATMHS,
			'telp'=>$data->KONTAKMHS,
			'kerja'=>$data->PEKERJAANMHS,
			'sks'=>$data->SKSMHS,
			'seragam'=>$data->SERAGAMMHS,
			'nama'=>$data->NAMAMHS,
			'prodi'=>$data->FAKPRODI.' / '.$data->NAMAPRODI,
			'npm'=>$data->NPM,
			'foto'=>$foto
		));
		$this->load->view('mhs/v/footer');
	}
}

/* End of file profil.php */
/* Location: ./application/controllers/dosen/profil.php */