<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_dpl'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('dosen/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
		$this->load->model('dosen/nilaiModel');
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->nilaiModel->get($this->kknaktif);
		$this->load->view('dosen/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('dosen/nilai',array(
			'data'=>$data
		));
		$this->load->view('dosen/footer');
	}

	public function set(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->nilaiModel->setnilai($_POST,$this->kknaktif);
	}

	public function cetak(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->nilaiModel->get($this->kknaktif);
		$this->load->view('dosen/print/header');
		$this->load->view('dosen/print/nilai',array('data'=>$data));
		$this->load->view('dosen/print/footer');
	}

}

/* End of file nilai.php */
/* Location: ./application/controllers/dosen/nilai.php */