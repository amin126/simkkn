<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mapModel');
	}
	public function index($params=null)
	{
			$this->cari($params);
	}

	public function cari($params=null){
		if($params==null){
			echo $this->mfungsi->viewJson(array(
				'status'=>false,
				'data'=>'Data tidak ditemukan'
			));
		}else{
      $dataa = $this->mapModel->get($params,false);
      $datab = array();
      if(count($dataa)==0){
      	echo $this->mfungsi->viewJson(array(
					'status'=>false,
					'data'=>'Data tidak ditemukan'
				));
      }else{
	      foreach ($dataa as $key => $value) {
	        if(!array_key_exists($value->KDKEL, $datab)){
	            $datab[$value->KDKEL] = new stdClass();
	            //$datab[$value->KDKEL]->kode = $value->KDKP;
	            $datab[$value->KDKEL]->iddpl = $value->KDDPL;
	            $datab[$value->KDKEL]->dpl = $value->NAMADPL;
	            $datab[$value->KDKEL]->kontak = $value->KONTAKDPL;
	            $datab[$value->KDKEL]->kelompok = (intval($this->mfungsi->setting('hal_pstkelompok'))==1) ? NULL : $value->NAMAKEL;
	            $datab[$value->KDKEL]->akademik = $value->NAMATAHUN;
	            $datab[$value->KDKEL]->lokasi = (intval($this->mfungsi->setting('hal_pstkelompok'))==1) ? NULL : $value->ALAMATKEL;
	            //$datab[$value->KDKEL]->lat = floatval($value->KORDXKEL);
	            //$datab[$value->KDKEL]->lng = floatval($value->KORDYKEL);
	            $datab[$value->KDKEL]->peserta = array();
	        }
	        if(intval($this->mfungsi->setting('hal_pstkelompok'))==0){
		        array_push($datab[$value->KDKEL]->peserta, array(
		            'npm'=>$value->NPM,
		            'nama'=>$value->NAMAMHS,
		            'prodi'=>$value->FAKPRODI.' / '.$value->NAMAPRODI,
		            'alamat'=>$value->ALAMATMHS,
		            //'lat'=>floatval($value->KORDXMHS),
		            //'lng'=>floatval($value->KORDYMHS)
		        ));
		      }
	      }
	      $dataa = array();
	      foreach ($datab as $key => $value) {
	      	array_push($dataa, $value);
	      }
	      echo $this->mfungsi->viewJson(array(
					'status'=>true,
					'data'=>$dataa
				));
	    }
    }
  }

}

/* End of file kelompok.php */
/* Location: ./application/controllers/kelompok.php */