<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->load->view('map/map');
	}

	public function cari(){
		$data = $this->mfungsi->kordinat($_POST['alamat']);
		echo json_encode($data);
	}

}

/* End of file Map.php */
/* Location: ./application/controllers/Map.php */