<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_camat'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('camat/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
		$this->load->model('camat/uploadModel');
	}
	public function index()
	{
		$this->find();
	}

	public function find(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->uploadModel->view();
		$this->load->view('camat/header');
		$this->load->view('camat/upload',array(
			'data'=>$data,
			'cari'=>$cari =  (isset($_POST['cari'])) ? $_POST['cari'] : ''
		));
		$this->load->view('camat/footer');
	}

	public function view($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function download($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}
}

/* End of file upload.php */
/* Location: ./application/controllers/camat/upload.php */