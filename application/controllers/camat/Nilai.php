<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_camat'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('camat/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
		$this->load->model('camat/nilaiModel');
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->nilaiModel->get($this->kknaktif);
		$this->load->view('camat/header');
		$this->load->view('camat/nilai',array(
			'data'=>$data
		));
		$this->load->view('camat/footer');
	}

	public function set(){
		// if(isset($this->kunci)){
		// 	$this->load->view('locked2');	
		// 	return true;
		// }
		// $this->nilaiModel->setnilai($_POST,$this->kknaktif);
	}

	public function cetak(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->nilaiModel->get($this->kknaktif);
		$this->load->view('camat/print/header');
		$this->load->view('camat/print/nilai',array('data'=>$data));
		$this->load->view('camat/print/footer');
	}

}

/* End of file nilai.php */
/* Location: ./application/controllers/camat/nilai.php */