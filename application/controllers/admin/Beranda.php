<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/berandaModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->load->view('admin/header');
		$data['kkn'] = $this->berandaModel->totalkkn();
		$data['peserta'] = $this->berandaModel->totalpeserta();
		$this->load->view('admin/beranda',$data);
		$this->load->view('admin/footer');
	}

	public function export(){
	$data = $this->berandaModel->loadMhs();
		//echo json_encode($data);
		$this->mexcel->export(array(
			'data'=>$data,
			'header'=>array('NPM','NAMA','KODE PRODI','ALAMAT'),
			'filename'=>'dataMhs'
		));
	}

	public function verifikasi(){

		$this->load->view('admin/verifikasi', array());
	}


	public function alamat(){
		$alamat = 'jln rangperang pamekasan';
		$this->load->model('admin/wilayahModel');
		$cek = $this->wilayahModel->find($alamat,'google');
		echo console($cek);
	}

}

/* End of file beranda.php */
/* Location: ./application/controllers/admin/beranda.php */