<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/kelompokModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$data['data'] = $this->kelompokModel->view();
		$this->load->view('admin/header');
		$this->load->view('admin/kelompok',$data);
		$this->load->view('admin/footer');
	}

	public function dosen($kode){
		$this->load->model('admin/dosenModel');
		$dsn = $this->dosenModel->load($kode);

		if($dsn->FOTODPL!=NULL){
			if (strpos($dsn->FOTODPL, 'unira') !== false) {
			    $foto = $dsn->FOTODPL;
			}else{
				$foto = base_url($dsn->FOTODPL);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?filter[nis]='.$dsn->KDDPL));
			if(isset($dmhs->errors) || !isset($dmhs->data))
				$foto = base_url('assets/images/faces/user.png');
			else
				if($dmhs->data[0]->attributes->thumbnail==null)
					$foto = base_url('assets/images/faces/user.png');
				else
					$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$dsn->FOTODPL = $foto;
		$this->load->view('admin/header');
		$this->load->view('admin/profil-dosen',array(
			'data'=>$dsn
		));
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$prodi = $this->kelompokModel->prodi();
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['nama'] = '';
		$data['dosen'] = $this->kelompokModel->dosen_add();
		$data['kapasitas'] = '';
		$data['dpl'] = '';
		$data['kordx'] = '';
		$data['kordy'] = '';
		$data['alamat'] = '';
		$data['default'] = 'true';
		$data['prodidata'] = $prodi;
		$vprodi = $this->kelompokModel->getdefkaprodi();
		$vpr = (count($vprodi)>0) ? $vprodi : $prodi;

		foreach ($vpr as $key => $value) {
  		$data['prodi'][$value->KDPRODI] = (isset($value->KAPRODI)) ? $value->KAPRODI : 1;
		}
		$this->load->view('admin/formKelompok',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$prodi = $this->kelompokModel->prodi();
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->kelompokModel->load($kode);
		$data['nama'] = $load->NAMAKEL;
		$data['kordx'] = $load->KORDXKEL;
		$data['kordy'] = $load->KORDYKEL;
		$data['kapasitas'] = $load->KAPASITASKEL;
		$data['dpl'] = $load->KDDPL;
		$data['default'] = ($load->DEFKAPPRODI==0) ? '' : 'true';
		$data['dosen'] = $this->kelompokModel->dosen_edit($kode);
		$data['alamat'] = $load->ALAMATKEL;
		$data['prodidata'] = $prodi;
		$vprodi = $this->kelompokModel->getkaprodi($kode);
		$pr = array();
		foreach ($vprodi as $key => $value) {
  		$pr[$value->KDPRODI] = $value->KAPRODI;
		}
		foreach ($prodi as $key => $value) {
			if(isset($pr[$value->KDPRODI]))
  			$data['prodi'][$value->KDPRODI] = $pr[$value->KDPRODI];
  		else
  			$data['prodi'][$value->KDPRODI] = 0;
		}
		$this->load->view('admin/formKelompok',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$prodi = $this->kelompokModel->prodi();
		// foreach ($prodi as $key => $value) {
		// 	$this->form_validation->set_rules('prodi['.$value->KDPRODI.']', 's', 'trim|required|numeric',array(
		// 		'required'=>'Kapasitas harus diisi',
		// 		'numeric'=>'Format pengisian nilai kapasitas tidak benar'
		// 	));
		// }
		$this->form_validation->set_rules('nama', 's', 'trim|required|callback_cek_nama['.$_POST['nama'].']',array(
			'required'=>'Nama kelompok harus diisi',
			'cek_nama'=>'Nama kelompok "'.$_POST['nama'].'" sudah digunakan'
		));

		// $this->form_validation->set_rules('kapasitas', 's', 'trim|required|numeric',array(
		// 	'required'=>'Kapasitas harus diisi',
		// 	'numeric'=>'Format pengisian nilai kapasitas tidak benar'
		// ));

		$this->form_validation->set_rules('dpl', 's', 'trim|required',array(
			'required'=>'Dosen pendamping lapangan harus dipilih',
		));

		$this->form_validation->set_rules('kordx', 's', 'trim|required',array(
			'required'=>'Kordinat X tidak diperoleh',
		));

		$this->form_validation->set_rules('kordy', 's', 'trim|required',array(
			'required'=>'Kordinat Y tidak diperoleh',
		));

		$this->form_validation->set_rules('alamat', 's', 'trim|required',array(
			'required'=>'Alamat harus diisi'
		));

		if ($this->form_validation->run() == FALSE)
    {
    	foreach ($prodi as $key => $value) {
    		$data['prodi'][$value->KDPRODI] = set_value('prodi['.$value->KDPRODI.']');
			}
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['nama'] = set_value('nama');
			$data['kordx'] = set_value('kordx');
			$data['kordy'] = set_value('kordy');
			$data['kapasitas'] = set_value('kapasitas');
			$data['dosen'] = $this->kelompokModel->dosen_add();
			$data['dpl'] = set_value('dpl');
			$data['alamat'] = set_value('alamat');
			$data['prodidata'] = $this->kelompokModel->prodi();
			$data['default'] = (isset($_POST['default'])) ? 'true' : '';
			$this->load->view('admin/formKelompok',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$id =  $this->kelompokModel->maxkode();
     	$_POST['kode'] = $id;
     	$_POST['kapasitas'] = 0;
     	$this->kelompokModel->simpan($_POST);
     	foreach ($prodi as $key => $value) {
				$this->kelompokModel->tambahkaprodi($value->KDPRODI,0,$id);
			}
   //  	foreach ($_POST['prodi'] as $key => $value) {
   //  		$this->kelompokModel->tambahkaprodi($key,$value,$id);
			// }
      redirect('admin/kelompok');
    }
	}

	public function update($kode){
		$prodi = $this->kelompokModel->prodi();
		// foreach ($prodi as $key => $value) {
		// 	$this->form_validation->set_rules('prodi['.$value->KDPRODI.']', 's', 'trim|required|numeric',array(
		// 		'required'=>'Kapasitas harus diisi',
		// 		'numeric'=>'Format pengisian nilai kapasitas tidak benar'
		// 	));
		// }

		$this->form_validation->set_rules('nama', 's', 'trim|required|callback_cek_nama_update['.$_POST['nama'].']',array(
			'required'=>'Nama kelompok harus diisi',
			'cek_nama_update'=>'Nama kelompok "'.$_POST['nama'].'" sudah digunakan'
		));

		// $this->form_validation->set_rules('kapasitas', 's', 'trim|required|numeric',array(
		// 	'required'=>'Kapasitas harus diisi',
		// 	'numeric'=>'Format pengisian nilai kapasitas tidak benar'
		// ));

		$this->form_validation->set_rules('dpl', 's', 'trim|required',array(
			'required'=>'Dosen pendamping lapangan harus dipilih',
		));

		$this->form_validation->set_rules('alamat', 's', 'trim|required',array(
			'required'=>'Alamat harus diisi'
		));

		$this->form_validation->set_rules('kordx', 's', 'trim|required',array(
			'required'=>'Kordinat X tidak diperoleh',
		));

		$this->form_validation->set_rules('kordy', 's', 'trim|required',array(
			'required'=>'Kordinat Y tidak diperoleh',
		));

		if ($this->form_validation->run() == FALSE)
    {
    	foreach ($prodi as $key => $value) {
    		$data['prodi'][$value->KDPRODI] = set_value('prodi['.$value->KDPRODI.']');
			}
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['kordx'] = set_value('kordx');
			$data['kordy'] = set_value('kordy');
			$data['nama'] = set_value('nama');
			$data['kapasitas'] = set_value('kapasitas');
			$data['dosen'] = $this->kelompokModel->dosen_edit($kode);
			$data['dpl'] = set_value('dpl');
			$data['alamat'] = set_value('alamat');
			$data['prodidata'] = $this->kelompokModel->prodi();
			$data['default'] = (isset($_POST['default'])) ? 'true' : '';
			$this->load->view('admin/formKelompok',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->kelompokModel->update($_POST,$kode);
   //   	foreach ($_POST['prodi'] as $key => $value) {
   //  		$this->kelompokModel->updatekaprodi($key,$value,$kode);
			// }
      redirect($this->mhistory->back());
    }
	}

	public function hapus($kode){
		$this->kelompokModel->hapus($kode);
    redirect($this->mhistory->back());
	}

	public function cek_nama($n){
		$k = $this->mfungsi->tahun()->kode;
		$this->db->where('NAMAKEL', $n);
		$this->db->where('KDTAHUN', $k);
    $query = $this->db->get('kelompok');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update($n){
		$k = $this->mfungsi->tahun()->kode;
		$this->db->where('KDTAHUN', $k);
		$this->db->where('KDKEL!=', $this->uri->segment(4));
		$this->db->where('NAMAKEL', $n);
    $query = $this->db->get('kelompok');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cekKordinat(){
		echo json_encode($this->mfungsi->kordinat($_POST['alamat']));
	}

	public function addresscheck(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		if($csv->status==true)
			return TRUE;
		else
			return FALSE;
	}

	public function detail($kode){
		$url = base_url(uri_string());
		$this->session->set_userdata('subPageURL', $url);
		$this->load->view('admin/header');
		$data['kelompok'] = $this->kelompokModel->view($kode);
		$data['mahasiswa'] = $this->kelompokModel->anggota($kode);
		$this->load->view('admin/kelompokDetail',$data);
		$this->load->view('admin/footer');
	}

	public function cetak($kode){
		$kel= $this->kelompokModel->view($kode);
		$data['kelompok'][0]=$kel[0];
		$data['mahasiswa'] = $this->kelompokModel->anggota($kode);
		$this->load->view('admin/print/header');
		$this->load->view('admin/print/kelompokDetail',$data);
		$this->load->view('admin/print/footer');
	}

	public function arsipall(){
		$kel= $this->kelompokModel->view();
		foreach ($kel as $key => $value) {
			$this->arsip($value->KDKEL,$value->NAMAKEL.'/');
		}
		$this->zip->download('KKN_'.date('dmY_His').'.zip');
	}

	public function arsip($kode,$m=''){
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->load->helper('csv');
		$this->load->library('zip');
		$kel= $this->kelompokModel->view($kode);
		$data['kelompok'][0]=$kel[0];
		$data['mahasiswa'] = $this->kelompokModel->anggota($kode);
		$data['laporan'] = $this->kelompokModel->laporan($kode);
		$data['monitoring'] = $this->kelompokModel->monitoring($kode);
		$mhs = array();
		array_push($mhs, array(
			'No',
			'NPM',
			'Nama',
			'Fak / Prodi',
			'Nilai',
			'Alamat'
		));
		$i = 1;
		foreach ($data['mahasiswa'] as $key => $value) {
			array_push($mhs, array(
				$i++,
				$value->NPM,
				$value->NAMAMHS,
				$value->FAKPRODI.'/'.$value->NAMAPRODI,
				$value->NILAIMHS,
				$value->ALAMATMHS
			));
			if($value->FOTOMHS!=NULL && file_exists(FCPATH.'./'.$value->FOTOMHS)) {
		    $ft = explode('/', $value->FOTOMHS);
		    $this->zip->add_data($m.'FOTO_PESERTA/'.end($ft),$this->mfungsi->file_get('./'.$value->FOTOMHS));
			}
		}
		$this->zip->add_data($m.'peserta.csv', array_to_csv($mhs));

		$ugh = array();
		array_push($ugh, array(
			'No',
			'Tgl',
			'File',
			'Jenis',
			'Kb',
			'User',
			'Desk'
		));
		$i = 1;
		foreach ($data['laporan'] as $key => $value) {
			array_push($ugh, array(
				$i++,
				$this->mfungsi->tgl($value->TGLUPL,true),
				$value->FILEUPL,				
				$value->JENISUPL,
				$value->SIZEUPL,
				'('.$value->NPM.') '.$value->NAMAMHS,
				$value->DESKUPL
			));
			if(file_exists(FCPATH.'./dokumen/'.$value->KDKEL.'/'.$value->FILEUPL)) {
		    $this->zip->add_data($m.'DOKUMEN/'.$value->FILEUPL,$this->mfungsi->file_get('./dokumen/'.$value->KDKEL.'/'.$value->FILEUPL));
			}
		}
		$this->zip->add_data($m.'laporan.csv', array_to_csv($ugh));

		foreach ($data['monitoring'] as $key => $value) {
			if(file_exists(FCPATH.'./monitoring/'.$value->KDKEL.'/'.$value->FILELM)) {
		    $this->zip->add_data($m.'MONITORING_DPL/'.$value->FILELM,$this->mfungsi->file_get('./monitoring/'.$value->KDKEL.'/'.$value->FILELM));
			}
		}

		$kelompok = array();
		foreach ($data['kelompok'] as $key => $value) {
			array_push($kelompok, array(
				'TAHUN KEGIATAN',$value->NAMATAHUN
			));
			array_push($kelompok, array(
				'NAMA KELOMPOK',$value->NAMAKEL
			));
			array_push($kelompok, array(
				'ALAMAT KELOMPOK',$value->ALAMATKEL
			));
			array_push($kelompok, array(
				'KAPASITAS KELOMPOK',($value->KAPASITASKEL==NULL)?0:$value->KAPASITASKEL
			));
			array_push($kelompok, array(
				'ID DPL',$value->KDDPL
			));
			array_push($kelompok, array(
				'NAMA DPL',$value->NAMADPL
			));
			array_push($kelompok, array(
				'ALAMAT DPL',$value->ALAMATDPL
			));
			array_push($kelompok, array(
				'KONTAK DPL',$value->KONTAKDPL
			));
			if($value->FOTODPL!=NULL && file_exists(FCPATH.'./'.$value->FOTODPL)) {
		    $ft = explode('/', $value->FOTODPL);
		    if(end($ft)!='user.png')
		    	$this->zip->add_data($m.'FOTO_DOSEN/'.end($ft),$this->mfungsi->file_get('./'.$value->FOTODPL));
			}
			
		}
		$this->zip->add_data($m.'profil_KKN.csv', array_to_csv($kelompok));
		if($m=='')
			$this->zip->download('KKN_'.date('dmY_His').'.zip');
		//array_to_csv($kelompok, 'toto.csv');
		//echo $this->mfungsi->viewJson($data);
	}

	public function cetakall(){
		$kel= $this->kelompokModel->view();
		$html = '';
		foreach ($kel as $key => $value) {
			$data['kelompok'][0]=$value;
			$data['mahasiswa'] = $this->kelompokModel->anggota($value->KDKEL);
			$html .= $this->load->view('admin/print/header',null,true);
			$html .= $this->load->view('admin/print/kelompokDetail',$data,true);
			$html .= $this->load->view('admin/print/footer',null,true);
			$html .= '<div class="pagebreak"> </div>';
		}
		$this->load->view('admin/print/semuakelompok',array('html'=>$html));
		// $data['mahasiswa'] = $this->kelompokModel->anggota($kode);
		// $this->load->view('admin/print/header');
		// $this->load->view('admin/print/kelompokDetail',$data);
		// $this->load->view('admin/print/footer');
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */