<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kinerja extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/statistikaModel');
	}
	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->mhistory->go();
		$this->load->model('admin/duplikatModel');
		$kel = $this->statistikaModel->kelompok();
		$data = array();
		foreach ($kel as $key => $value) {
			array_push($data,array(
				'kode'=>$value->KDKEL,
				'nama'=>$value->NAMAKEL,
				'upload'=>$this->statistikaModel->count_upload($value->KDKEL),
				'monitor'=>$this->statistikaModel->count_monitoring($value->KDKEL),
				'size'=>$this->statistikaModel->count_size_upload($value->KDKEL),
				'pengajuan'=>$this->statistikaModel->count_pengajuan($value->KDKEL),
				'diterima'=>$this->statistikaModel->count_pengajuan_diterima($value->KDKEL),
				'anggota'=>$this->statistikaModel->count_anggota($value->KDKEL),
			));
		}
		$dplkt = $this->duplikatModel->upl_peserta();
		$this->load->view('admin/header');
		$this->load->view('admin/statistika-kinerja',array(
			'data'=>$this->statistikaModel->select(),
			'duplikat'=>$dplkt['hapus'],
			'diterima'=>$this->duplikatModel->upl_diterima(),
			'diajukan'=>$this->duplikatModel->upl_diajukan(),
			'belum'=>$this->duplikatModel->upl_belum(),
			'monitoring'=>$this->duplikatModel->upl_monitoring(),
			'hemat'=>number_format((float)$dplkt['hemat']/1000, 2, ',', ''),
			'prodi'=>$this->statistikaModel->prodi(),
			'kemajuan'=>$data
		));
		$this->load->view('admin/footer');
	}

	public function detail($kode){
		if(isset($_GET['notif'])){
			$this->mnotif->baca($_GET['notif']);
		}
		$this->load->view('admin/header');
		$this->load->view('admin/statistika-kinerja-detail',array(
			'cari'=>(isset($_POST['cari'])) ? $_POST['cari'] : '',
			'data'=>$this->statistikaModel->upload($kode),
			'kelompok'=>$this->statistikaModel->kelompok($kode),
			'pengajuan'=>$this->statistikaModel->pengajuan($kode),
			'diterima'=>$this->statistikaModel->diterima($kode),
			'monitoring'=>$this->statistikaModel->monitoring($kode),
			'kode'=>$kode,
			'nilai'=>$this->statistikaModel->nilai($kode)
		));
		$this->load->view('admin/footer');
	}

	public function ajuan($tipe,$kel,$kode){
		$t = ($tipe=='n') ? 3 : 4;
		$alasan = (strlen($_POST['alasan'])==0) ?  null : $_POST['alasan'];
		$this->statistikaModel->setAjuan($t,$kel,$kode,$alasan);
	}

	public function btlajuan($k,$i){
		$this->statistikaModel->cancelAjuan($k,$i);
		echo json_encode($_POST);
	}

	public function cetak($tipe,$kode){
		switch ($tipe) {
			case 'nilai':
				$data = $this->statistikaModel->nilai($kode);
				$this->load->view('admin/print/header');
				$this->load->view('admin/print/nilai',array('data'=>$data));
				$this->load->view('admin/print/footer');
				break;
		}
	}

	public function hapus($mode){
		$this->load->model('admin/duplikatModel');
		$this->mremaining->start(false);
		switch ($mode) {
			case 'duplikat':
				$dplkt = $this->duplikatModel->upl_peserta();
				$total = 100;
				$pers = $total/count($dplkt['hapus']);
				foreach ($dplkt['hapus'] as $key => $value) {
					$path = './dokumen/'.$value->KDKEL.'/'.$value->FILEUPL;
					$this->duplikatModel->hapus_file($value->KDUPL,$path);
					$this->mremaining->running(array(
						'proses'=>floor($pers*($key+1)),
						'total'=>$total
					));
				}
				$this->mremaining->finish();
				break;
			case 'diterima':
				$diterima = $this->duplikatModel->upl_diterima();
				$total = 100;
				$pers = $total/count($diterima);
				foreach ($diterima as $key => $value) {
					$path = './dokumen/'.$value->KDKEL.'/'.$value->FILEUPL;
					$this->duplikatModel->hapus_file($value->KDUPL,$path);
					$this->mremaining->running(array(
						'proses'=>floor($pers*($key+1)),
						'total'=>$total
					));
				}
				$this->mremaining->finish();
				break;
			case 'diajukan':
				$diajukan = $this->duplikatModel->upl_diajukan();
				$total = 100;
				$pers = $total/count($diajukan);
				foreach ($diajukan as $key => $value) {
					$path = './dokumen/'.$value->KDKEL.'/'.$value->FILEUPL;
					$this->duplikatModel->hapus_file($value->KDUPL,$path);
					$this->mremaining->running(array(
						'proses'=>floor($pers*($key+1)),
						'total'=>$total
					));
				}
				$this->mremaining->finish();
				break;
			case 'belum':
				$belum = $this->duplikatModel->upl_belum();
				$total = 100;
				$pers = $total/count($belum);
				foreach ($belum as $key => $value) {
					$path = './dokumen/'.$value->KDKEL.'/'.$value->FILEUPL;
					$this->duplikatModel->hapus_file($value->KDUPL,$path);
					$this->mremaining->running(array(
						'proses'=>floor($pers*($key+1)),
						'total'=>$total
					));
				}
				$this->mremaining->finish();
				break;
			case 'monitoring':
				$monitoring = $this->duplikatModel->upl_monitoring();
				$total = 100;
				$pers = $total/count($monitoring);
				foreach ($monitoring as $key => $value) {
					$path = './monitoring/'.$value->KDKEL.'/'.$value->FILELM;
					$this->duplikatModel->hapus_file_monitoring($value->KDLM,$path);
					$this->mremaining->running(array(
						'proses'=>floor($pers*($key+1)),
						'total'=>$total
					));
				}
				$this->mremaining->finish();
				break;
		}
	}

}

/* End of file kinerja.php */
/* Location: ./application/controllers/admin/statistika/kinerja.php */