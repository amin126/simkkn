<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftar extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/statistikaModel');
	}
	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->mhistory->go();
		//$kel = $this->statistikaModel->kelompok();
		$kapasitas = $this->statistikaModel->cek_kapasitasProdi();
		$data = $this->statistikaModel->select();
		$prodi = $this->statistikaModel->prodi();
		$this->load->view('admin/header');
		$this->load->view('admin/statistika-pendaftar',array(
			'data'=>$data,
			'prodi'=>$prodi,
			'kapasitas'=>$kapasitas->TOTAL,
			'valprodi'=>array(
				'21'=>157,
				'22'=>68,
				'31'=>159,
				'62'=>48,
				'61'=>56,
				'63'=>11,
				'11'=>25,
				'51'=>25,
				'52'=>37,
				'41'=>23
			)
		));
		$this->load->view('admin/footer');
	}

	public function hitung(){
		$status = true;
		$prodi=$this->statistikaModel->prodi();
		foreach ($prodi as $key => $value) {
			//$_POST['prodi'][$value->KDPRODI] = 99;
			if(empty($_POST['prodi'][$value->KDPRODI])){
				$status = false;
				echo '<p class="text-danger">'.ucfirst(strtolower($value->NAMAPRODI)).' harus diisi</p>';
			}else{
				if(!is_numeric($_POST['prodi'][$value->KDPRODI])){
					$status = false;
					echo '<p class="text-danger">'.ucfirst(strtolower($value->NAMAPRODI)).' format harus angka</p>';
				}
			}
		}

		if($status==true){
			$label = [];
			foreach ($prodi as $key => $value) {
				$label[$value->KDPRODI] = $value->NAMAPRODI;
			}
			$kelindex = [];
			$kellabel = [];
			$kelompok = $this->statistikaModel->kelompok();
			foreach ($kelompok as $x => $y) {
				array_push($kelindex, $y->KDKEL);
				$kellabel[$y->KDKEL]['nama'] = $y->NAMAKEL;
				$kellabel[$y->KDKEL]['alamat'] = $y->ALAMATKEL;
			}
			$this->load->view('admin/statistika-kalkulator',array(
				'kel'=>$this->hitungFix($_POST,$kelompok),
				'prodilabel'=>$label,
				'kellabel'=>$kellabel
			));
		}

	}

	public function hitungFix($prodi,$kelompok){
		$sisa = [];
		$kel = [];
		$tkel = count($kelompok);
		foreach ($prodi as $key => $value) {
			foreach ($value as $x => $y) {
				$hasil = floor($y/$tkel);
				$ss = $y - ($tkel*$hasil);
				$sisa[$x] = $ss;
				foreach ($kelompok as $m => $n) {
					$kel[$n->KDKEL][$x] = $hasil;
				}
			}
			
		}
		$kelindex = array();
		foreach ($kelompok as $x => $y) {
			array_push($kelindex, $y->KDKEL);
		}

		$j = 0;
		foreach ($sisa as $x => $y) {
			for($i=0; $i<$y; $i++){
				$kel[$kelindex[$j]][$x]++;
				$j++;
				if($j>=$tkel)
					$j = 0;
			}
		}		
		return $kel;
	}

	public function terapkan(){
		$this->terapkanFix($_POST['value']);
	}

	public function terapkanFix($form){
		foreach ($form as $key => $value) {
			$kap = 0;
			foreach ($value as $x => $y) {
				$this->statistikaModel->setKapasitasprodi($key,$x,$y);
				$kap += $y;
			}
			$this->statistikaModel->setKapasitas($key,$kap);
		}
		redirect($this->mhistory->back());
	}

}

/* End of file pendaftar.php */
/* Location: ./application/controllers/admin/statistika/pendaftar.php */
