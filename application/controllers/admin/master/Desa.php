<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/desaModel');
		$this->mlogin->cek();
	}


	// ajax
	public function loadkecamatan(){
		$htm = '';
		$kabupaten = 00;
		$dataKec = $this->desaModel->kecamatan($_POST['kabupaten']);
		foreach ($dataKec as $key => $value) {
      $htm .= '<option value="'.$value->KDKEC.'" '.(($value->KDKEC==$kabupaten) ? 'selected' : '').'>'.$value->NAMAKEC.'</option>';
    }
    echo $htm;
	}
	//end ajax

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['data'] = $this->desaModel->view();
		$this->load->view('admin/desa',$data);
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['proses'] = false;
		$data['kecamatan'] = '';
		$data['dataKab'] = $this->desaModel->kabupaten();
		$data['kabupaten'] = '';
		$data['desa'] = '';
		$this->load->view('admin/formDesa',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->desaModel->load($kode);
		$data['proses'] = true;
		$data['kecamatan'] = $load->KDKEC;
		$data['dataKab'] = $this->desaModel->kabupaten();
		$data['kabupaten'] = $this->desaModel->kodeKabupaten($load->KDKEC);
		$data['desa'] = $load->NAMADESA;
		$data['dataKec'] = $this->desaModel->kecamatan($data['kabupaten']);
		$this->load->view('admin/formDesa',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		if(!isset($_POST['kecamatan']))
			$_POST['kecamatan'] = '';
		$this->form_validation->set_rules('desa', 's', 'trim|required|callback_cek_nama',array(
			'required'=>'Nama desa harus diisi',
			'cek_nama'=>'Nama desa "'.$_POST['desa'].'" sudah digunakan'
		));

		$this->form_validation->set_rules('kabupaten', 's', 'trim|required',array(
			'required'=>'Kabupaten harus dipilih'
		));

		$this->form_validation->set_rules('kecamatan', 's', 'trim|required',array(
			'required'=>'Kecamatan harus dipilih'
		));



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['proses'] = true;
			$data['kecamatan'] = set_value('kecamatan');
			$data['dataKab'] = $this->desaModel->kabupaten();
			$data['kabupaten'] = set_value('kabupaten');
			$data['desa'] = set_value('desa');
			$data['dataKec'] = $this->desaModel->kecamatan($_POST['kabupaten']);
			$this->load->view('admin/formDesa',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->desaModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){

		if(!isset($_POST['kecamatan']))
			$_POST['kecamatan'] = '';
		$this->form_validation->set_rules('desa', 's', 'trim|required|callback_cek_nama_update',array(
			'required'=>'Nama desa harus diisi',
			'cek_nama_update'=>'Nama desa "'.$_POST['desa'].'" sudah digunakan'
		));

		$this->form_validation->set_rules('kabupaten', 's', 'trim|required',array(
			'required'=>'Kabupaten harus dipilih'
		));

		$this->form_validation->set_rules('kecamatan', 's', 'trim|required',array(
			'required'=>'Kecamatan harus dipilih'
		));

		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['proses'] = true;
			$data['kecamatan'] = set_value('kecamatan');
			$data['dataKab'] = $this->desaModel->kabupaten();
			$data['kabupaten'] = set_value('kabupaten');
			$data['desa'] = set_value('desa');
			$data['dataKec'] = $this->desaModel->kecamatan($_POST['kabupaten']);
			$this->load->view('admin/formDesa',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->desaModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}


	public function hapus($kode){
		$this->desaModel->hapus($kode);
    redirect($this->mhistory->back(),'refresh');
	}

	public function cek_nama(){
		$this->db->where('NAMADESA', $_POST['desa']);
		$this->db->where('KDKEC', $_POST['kecamatan']);
    $query = $this->db->get('desa');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update(){
		$this->db->where('NAMADESA', $_POST['desa']);
		$this->db->where('KDDESA!=', $this->uri->segment(5));
		$this->db->where('KDKEC', $_POST['kecamatan']);
    $query = $this->db->get('desa');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */