<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/prodiModel');
		$cek = $this->prodiModel->cek();
		if($cek==0){
			$data = [];
			$prodi = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/prodi'));
			foreach ($prodi->data as $key => $value) {
				if($value->id!=200){
					array_push($data,array(
						'KDPRODI'=>$value->id,
						'FAKPRODI'=>$value->relationship->fakultas->data->attributes->nama,
						'NAMAPRODI'=>$value->attributes->nama
					));
				}
			}
			$this->prodiModel->simpan($data);
		}
	}
	public function index()
	{
		$this->view();
	}

	public function view(){
		$data['prodi'] = $this->prodiModel->select();
		$this->load->view('admin/header');
		$this->load->view('admin/prodi', $data);
		$this->load->view('admin/footer');
	}

	public function update(){
		$prodi = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/prodi'));
		foreach ($prodi->data as $key => $value) {
			if($value->id!=200){
				$cek = $this->prodiModel->cek1($value->id);
				$data = array(
					'kode'=>$value->id,
					'fakultas'=>$value->relationship->fakultas->data->attributes->nama,
					'prodi'=>$value->attributes->nama
				);
				if($cek==0){
					$this->prodiModel->simpan1($data);
				}else{
					$this->prodiModel->update($data);
				}
			}
		}
		redirect('admin/master/prodi');
	}

}

/* End of file prodi.php */
/* Location: ./application/controllers/admin/master/prodi.php */