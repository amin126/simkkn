<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/kabupatenModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['data'] = $this->kabupatenModel->view();
		$this->load->view('admin/kabupaten',$data);
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['nama'] = '';
		$this->load->view('admin/formKabupaten',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->kabupatenModel->load($kode);
		$data['nama'] = $load->name;
		$this->load->view('admin/formKabupaten',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('nama', 's', 'trim|required|callback_cek_nama['.$_POST['nama'].']',array(
			'required'=>'Nama kabupaten harus diisi',
			'cek_nama'=>'Nama kabupaten "'.$_POST['nama'].'" sudah digunakan'
		));


		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['nama'] = set_value('nama');
			$data['kapasitas'] = set_value('kapasitas');
			$this->load->view('admin/formKabupaten',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->kabupatenModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){

		$this->form_validation->set_rules('nama', 's', 'trim|required|callback_cek_nama_update['.$_POST['nama'].']',array(
			'required'=>'Nama kabupaten harus diisi',
			'cek_nama_update'=>'Nama kabupaten "'.$_POST['nama'].'" sudah digunakan'
		));



		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['nama'] = set_value('nama');
			$this->load->view('admin/formKelompok',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->kabupatenModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function status($kode,$status){
		$this->tahunModel->status(array(
			'kode'=>$kode,
			'status'=>$status
		));
    redirect($this->mhistory->back(),'refresh');
	}

	public function hapus($kode){
		$this->kabupatenModel->hapus($kode);
    redirect($this->mhistory->back(),'refresh');
	}

	public function cek_nama($n){
		console($n);
		$this->db->where('name', $n);
    $query = $this->db->get('kabupaten');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update($n){
		$this->db->where('KDKAB!=', $this->uri->segment(5));
		$this->db->where('name', $n);
    $query = $this->db->get('kabupaten');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */