<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/tahunModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['data'] = $this->tahunModel->view();
		$this->load->view('admin/tahun',$data);
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['label'] = '';
		$this->load->view('admin/formTahun',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$data['label'] = $this->tahunModel->load($kode)->NAMATAHUN;
		$this->load->view('admin/formTahun',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('label', 's', 'trim|required|callback_cek_tahun['.$_POST['label'].']',array(
			'required'=>'Label harus diisi',
			'cek_tahun'=>'Penamaan label sudah ada'
		));

		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['label'] = set_value('label');
			$this->load->view('admin/formTahun',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->tahunModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){
		$this->form_validation->set_rules('label', 's', 'trim|required|callback_cek_tahun_update['.$_POST['label'].']',array(
			'required'=>'Label harus diisi',
			'cek_tahun_update'=>'Penamaan label sudah ada'
		));

		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['label'] = set_value('label');
			$this->load->view('admin/formTahun',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->tahunModel->update($_POST['label'],$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function status($kode,$status){
		$this->tahunModel->status(array(
			'kode'=>$kode,
			'status'=>$status
		));
    redirect($this->mhistory->back(),'refresh');
	}

	public function hapus($kode){
		$this->tahunModel->hapus($kode);
    redirect($this->mhistory->back(),'refresh');
	}

	public function cek_tahun($n){
		$this->db->where('NAMATAHUN', $n);
    $query = $this->db->get('tahun');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_tahun_update($n){
		$this->db->where('KDTAHUN!=', $this->uri->segment(5));
		$this->db->where('NAMATAHUN', $n);
    $query = $this->db->get('tahun');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */