<?php
// Model E
// mahasiswa yang memiliki jarak rata-rata terjauh akan di proses terlebih dahulu

defined('BASEPATH') OR exit('No direct script access allowed');

class Gen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/adminModel');
		$this->mlogin->cek();
	}
	
	public function ulang(){
		$this->load->model('metode/aModel');
		$this->aModel->delJarakKelompok();
		$this->aModel->delKelompokPeserta();
		$this->aModel->delKapasitasKkn();
		$this->aModel->setStatusMhsNol();
	}

	public function getkordinat(){
		$this->load->model('metode/aModel');
		$npm = $_POST['npm'];
		$alamat = $_POST['alamat'];
		$cek = $this->mfungsi->kordinat($alamat);
		if($cek->status==true){
			$this->aModel->setKordinat(array(
				'alamat'=>$cek->alamat,
				'lat'=>$cek->lat,
				'lng'=>$cek->lng,
				'npm'=>$npm
			));
		}
		echo json_encode($cek);
	}

	public function getkordinatAll(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$noKordinat = $this->aModel->mhsNokordinat();
		foreach ($noKordinat as $key => $value) {
			$cek = $this->mfungsi->kordinat($value->ALAMATMHS);
			if($cek->status==true){
				$this->aModel->setKordinat(array(
					'alamat'=>$cek->alamat,
					'lat'=>$cek->lat,
					'lng'=>$cek->lng,
					'npm'=>$value->NPM
				));
			}
			$this->mremaining->running(array(
				'data'=>$cek,
				'npm'=>$value->NPM
			));
		}
		$this->mremaining->finish();
	}

	public function find_distance_geometry()
	{
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$mahasiswa = $this->aModel->getMhsGeometry();
		$kelompok = $this->aModel->getKelompok();
		$total = count($mahasiswa)*count($kelompok);
		$persen = 100 / $total;
		$i = 0;
		$data = array();
		$thn = null;
		foreach ($mahasiswa as $m => $mhs) {
			$j = 1;
			foreach ($kelompok as $k => $kel) {
				$thn = $kel->KDTAHUN;
				//√((x_(1-) x_2 )^2+(y_(1-) y_2 )^2 )
				// $a = pow($mhs->KORDXMHS - $kel->KORDXKEL,2);
				// $b = pow($mhs->KORDYMHS - $kel->KORDYKEL,2);
				// $c = sqrt($a + $b);
				array_push($data,array(
					'NPM'=>$mhs->NPM,
					'KDKEL'=>$kel->KDKEL,
					'KDPRODI'=>$mhs->KDPRODI,
					'NILAIJARAK'=>$this->mfungsi->getDistance($mhs->KORDXMHS,$mhs->KORDYMHS,$kel->KORDXKEL,$kel->KORDYKEL)
				));
				$this->mremaining->running(floor($persen*$i));
				$j++;
				$i++;
			}
		}
		$this->mfile->write(array(
			'file'=>'./jarak/'.$thn.'.json',
			'data'=>json_encode($data, JSON_PRETTY_PRINT)
		));
		$this->mremaining->finish();
	}

	public function pemetaan(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$jarak = $this->aModel->jarak_kelompok();
		$prodi = $this->aModel->getProdi();
		$kelompok = $this->aModel->getKelompok();
		$data = array();
		$order1 = array();
		$order = array();
		$persen = count($jarak)+count($prodi)+count($kelompok);
		$persen1 = 100/$persen;
		$persen2 = 0;
		$p32c = array();
		$datajarak = array();
		foreach ($jarak as $key => $value) {
			$datajarak[$value->NPM][$value->KDKEL][$value->KDPRODI] = $value->NILAIJARAK;
		}
		$jmprd = array();
		$jmprdmax = array();
		$jmprdsisa = array();
		$totalmhs = count($jarak)/count($kelompok);
		$jarak = $this->order_asc($datajarak);
		foreach ($jarak as $npm => $value) {
			foreach ($value as $key => $value1) {
				foreach ($value1 as $prd => $value2) {
					if(!array_key_exists($prd, $jmprd))
						$jmprd[$prd] = 0;
					$jmprd[$prd]++;
					break;
				}
				break;
			}
		}
		$jper['max'] = floor($totalmhs/count($kelompok));
		$jper['sisa'] = $totalmhs-($jper['max']*count($kelompok));

						
		foreach ($jmprd as $key => $value) {
			$jmprdmax[$key] = floor($value/count($kelompok));
			$jmprdsisa[$key] = $value-($jmprdmax[$key]*count($kelompok));
		}

		$bentukKKN = array();
		$sisaKKN = array();
		foreach ($kelompok as $key => $value) {
			$bentukKKN[$value->KDKEL] = array();
		}
		$sisaJarak = $jarak;
		while (!empty($jarak)) {
			foreach ($jarak as $npm => $value) {
				foreach ($value as $kel => $kels) {
					$sts = false;
					foreach ($kels as $prd => $jrk) {
						if(!array_key_exists($prd, $bentukKKN[$kel]))
							$bentukKKN[$kel][$prd] = array();
						$lskel = array();
						foreach ($jarak[$npm] as $k1 => $v1) {
							$x = (isset($bentukKKN[$k1][$prd])) ? count($bentukKKN[$k1][$prd]) : 0;
							if($this->countKel($bentukKKN[$k1])<$jper['max']+1){
								if($x<$jmprdmax[$prd]){
									array_push($lskel, $k1);
								}else if($jmprdmax[$prd]==0){
									array_push($lskel, $k1);
								}else{
									array_push($lskel, $k1);
								}
							}
						}
						if(in_array($kel, $lskel)){
							array_push($bentukKKN[$kel][$prd], $npm);
						}else{
							$sts=true;
						}
						break;
					}
					if($sts == false)
						break;
				}
				if(isset($jarak[$npm]))
					unset($jarak[$npm]);
				break;
			}
		}
		// $valkkn = array();
		// $ttlmhs = 0;
		// foreach ($bentukKKN as $key => $value) {
		// 	$valkkn[$key] = 0;
		// 	foreach ($value as $key1 => $value1) {
		// 		$valkkn[$key] += count($value1);
		// 		$ttlmhs += count($value1);
		// 	}
		// }
		// echo $this->mfungsi->viewJson($bentukKKN);
		// exit();

		$kapasitas = array();
		$ttlpeserta = 0;
		foreach ($bentukKKN as $key => $value) {
			if(!array_key_exists($key, $kapasitas))
				$kapasitas[$key] = array();
			foreach ($prodi as $prd => $vprd) {
				if(!array_key_exists($vprd->KDPRODI, $kapasitas[$key]))
					$kapasitas[$key][$vprd->KDPRODI] = 0;
				$kapasitas[$key][$vprd->KDPRODI] += (isset($value[$vprd->KDPRODI]))? count($value[$vprd->KDPRODI]): 0;
				$ttlpeserta += $kapasitas[$key][$vprd->KDPRODI];
			}
		}
		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpang kapasitas kelompok KKN'));

		$this->aModel->delKapasitasKkn();
		$this->aModel->setKapasitasKkn($kapasitas);
		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Kalkulasi...'));
		$persen1 = 100/$ttlpeserta;
		$persen2 = 0;
		foreach ($bentukKKN as $kel => $value) {
			foreach ($value as $prd => $val) {
				foreach ($val as $key => $npm) {
					$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpan kelompok KKN yang sudah dibentuk'));
						$persen2++;
					$this->aModel->verifikasi($npm);
					$this->aModel->kelompok_peserta($kel,$npm);
				}
			}
		}
		$this->mremaining->finish();
	}

	public function countKel($data){
		$x = 0;
		foreach ($data as $prd) {
			$x+=count($prd);
		}
		return $x;
	}

	public function order_asc($datax){
		$res = array();
		foreach ($datax as $key => $value) {
			$jrk = 0;
			foreach ($value as $key1 => $value1) {
				foreach ($value1 as $key2 => $value2) {
					$jrk += $value2;
				}
			}
			array_push($res, array(
				'NPM'=>$key,
				'JARAK'=>$jrk
			));
		}
		for($i=0; $i<count($res)-1; $i++){
			for($j=$i+1; $j<count($res); $j++){
				if($res[$j]['JARAK']>$res[$i]['JARAK']){
					$tmp = $res[$i];
					$res[$i] = $res[$j];
					$res[$j] = $tmp;
				}
			}
		}
		$ax4 = array();
		foreach ($res as $key => $value) {
			$ax4[$value['NPM']] = $this->order_kel($datax[$value['NPM']]);
		}
		return $ax4;
	}

	public function order_kel($datax){
		$res = array();
		foreach ($datax as $key => $value) {
			foreach ($value as $key1 => $value1) {
				array_push($res, array(
					'KEL'=>$key,
					'JARAK'=>$value1
				));
			}
		}
		for($i=0; $i<count($res)-1; $i++){
			for($j=$i+1; $j<count($res); $j++){
				if($res[$j]['JARAK']<$res[$i]['JARAK']){
					$tmp = $res[$i];
					$res[$i] = $res[$j];
					$res[$j] = $tmp;
				}
			}
		}
		$ax4 = array();
		foreach ($res as $key => $value) {
			$ax4[$value['KEL']] = $datax[$value['KEL']];
		}
		return $ax4;
	}

	public function group_select($datax,$no_kel){
		$skor = array();
		foreach ($datax as $kel => $value) {
			if(!in_array($kel, $no_kel)){
				$total = 0;
				foreach ($value as $key => $value_a) {
					$total = $total + $value_a['jarak'];
				}
				array_push($skor, array(
					'kelompok'=>$kel,
					'jarak'=>$total
				));
			}
		}

		for ($i=0; $i < count($skor)-1 ; $i++) { 
			$tmp = null;
			for ($j=$i+1; $j < count($skor) ; $j++) { 
				$a = $skor[$i]['jarak'];
				$b = $skor[$j]['jarak'];
				if($b<$a){
					$tmp = $skor[$j];
					$skor[$j] = $skor[$i];
					$skor[$i] = $tmp;
				}
			}
		}

		return array(
			'anggota'=>$datax[$skor[0]['kelompok']],
			'rincian'=>$skor[0]
		);
	}

}

/* End of file Metode_a.php */
/* Location: ./application/controllers/admin/metode/Metode_a.php */