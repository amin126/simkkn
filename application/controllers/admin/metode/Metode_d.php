<?php

// Model D
// urut kelompok pada masing-masing mahasiswa dari jarak terdekat
// cari rata-rata jarak ke kelompok masing-masing mahasiswa
// urut mahasiswa dari hasil rata-rata jarak
// jumlah semua nilai jarak  rata mahasiswa dan ambil hasil rata-ratanya
// ambil jumlah terkecil dari kelompok yang telah dipilih

defined('BASEPATH') OR exit('No direct script access allowed');

class Gen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/adminModel');
		$this->mlogin->cek();
	}
	
	public function ulang(){
		$this->load->model('metode/aModel');
		$this->aModel->delJarakKelompok();
		$this->aModel->delKelompokPeserta();
		$this->aModel->delKapasitasKkn();
		$this->aModel->setStatusMhsNol();
	}

	public function getkordinat(){
		$this->load->model('metode/aModel');
		$npm = $_POST['npm'];
		$alamat = $_POST['alamat'];
		$cek = $this->mfungsi->kordinat($alamat);
		if($cek->status==true){
			$this->aModel->setKordinat(array(
				'alamat'=>$cek->alamat,
				'lat'=>$cek->lat,
				'lng'=>$cek->lng,
				'npm'=>$npm
			));
		}
	}

	public function getkordinatAll(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$noKordinat = $this->aModel->mhsNokordinat();
		foreach ($noKordinat as $key => $value) {
			$cek = $this->mfungsi->kordinat($value->ALAMATMHS);
			if($cek->status==true){
				$this->aModel->setKordinat(array(
					'alamat'=>$cek->alamat,
					'lat'=>$cek->lat,
					'lng'=>$cek->lng,
					'npm'=>$value->NPM
				));
			}
			$this->mremaining->running(array(
				'data'=>$cek,
				'npm'=>$value->NPM
			));
		}
		$this->mremaining->finish();
	}

	public function find_distance_geometry()
	{
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$mahasiswa = $this->aModel->getMhsGeometry();
		$kelompok = $this->aModel->getKelompok();
		$total = count($mahasiswa)*count($kelompok);
		$persen = 100 / $total;
		$i = 0;
		$data = array();
		$thn = null;
		foreach ($mahasiswa as $m => $mhs) {
			$j = 1;
			foreach ($kelompok as $k => $kel) {
				$thn = $kel->KDTAHUN;
				//√((x_(1-) x_2 )^2+(y_(1-) y_2 )^2 )
				// $a = pow($mhs->KORDXMHS - $kel->KORDXKEL,2);
				// $b = pow($mhs->KORDYMHS - $kel->KORDYKEL,2);
				// $c = sqrt($a + $b);
				array_push($data,array(
					'NPM'=>$mhs->NPM,
					'KDKEL'=>$kel->KDKEL,
					'KDPRODI'=>$mhs->KDPRODI,
					'NILAIJARAK'=>$this->mfungsi->getDistance($mhs->KORDXMHS,$mhs->KORDYMHS,$kel->KORDXKEL,$kel->KORDYKEL)
				));
				$this->mremaining->running(floor($persen*$i));
				$j++;
				$i++;
			}
		}
		$this->mfile->write(array(
			'file'=>'./jarak/'.$thn.'.json',
			'data'=>json_encode($data, JSON_PRETTY_PRINT)
		));
		$this->mremaining->finish();
	}

	public function pemetaan(){
		$this->load->model('metode/aModel');
		set_time_limit(0);
		$this->mremaining->start();
		$jarak = $this->aModel->jarak_kelompok();
		$prodi = $this->aModel->getProdi();
		$kelompok = $this->aModel->getKelompok();
		$data = array();
		$persen = count($jarak)+count($prodi)+count($kelompok);
		$persen1 = 100/$persen;
		$persen2 = 0;
		$p32c = array();
		foreach ($jarak as $key => $value) {
			if(!array_key_exists($value->KDPRODI, $data)){
				$data[$value->KDPRODI] = array();
			}
			if(!array_key_exists($value->NPM, $data[$value->KDPRODI])){
				$data[$value->KDPRODI][$value->NPM] = array();
			}
			if(!array_key_exists($value->KDKEL, $data[$value->KDPRODI][$value->NPM])){
				$data[$value->KDPRODI][$value->NPM][$value->KDKEL] = array();
			}
			$data[$value->KDPRODI][$value->NPM][$value->KDKEL] = $value->NILAIJARAK;
			$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Mengambil nilai jarak antara mahasiswa dan kelompok pada setiap peserta'));
			$persen2++;
		}
 		$mhsUrutkelompok = array();
 		$avrgMhs = array();
 		$totalAvgMhs = 0;
 		$mhsProdi = array();
 		foreach ($data as $prd => $value) {
 			foreach ($value as $npm => $valnpm) {
 				$mhsProdi[$npm] = $prd;
 				$result = $this->urut_kelompok($valnpm);
 				$mhsUrutkelompok[$npm] = $result['data'];
 				$avrgMhs[$npm] = $result['average'];
 				$totalAvgMhs += $result['average'];
 			}
 		}
 		$avrgMhs = $this->urutAverageMhs($avrgMhs);
 		$totalAvgMhs = $totalAvgMhs/count($avrgMhs);
 		$kapasitasPerKelompok = floor(count($avrgMhs)/count($kelompok));
 		$sisaKapasitas = count($avrgMhs)-($kapasitasPerKelompok*count($kelompok));
 		$isiKelompk = array();
 		$mhsKelompok = array();
 		foreach ($kelompok as $key => $value) {
 			$this->isiKelompok[$value->KDKEL] = 0;
 			$this->isiKelompokSisa[$value->KDKEL] = 0;
 			$mhsKelompok[$value->KDKEL] = array();
 		}
 		$bts = 1;
 		foreach ($avrgMhs as $key => $value) {
 			if($bts<$kapasitasPerKelompok*count($kelompok)){
	 			$pilih = $this->pilihKelompok($mhsUrutkelompok[$key],$totalAvgMhs,$kapasitasPerKelompok,false);
	 			array_push($mhsKelompok[$pilih], $key);
	 		}else{
	 			$pilih = $this->pilihKelompok($mhsUrutkelompok[$key],$totalAvgMhs,$kapasitasPerKelompok,true);
	 			array_push($mhsKelompok[$pilih], $key);
	 		}
 			$bts++;
 		}

		$mulai = 0;
		$total = 0;

		$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Tentukan kapasitas pada masing-masing kelompok KKN'));

		$kapprodi = array();
		foreach ($prodi as $key => $value) {
			$kapprodi[$value->KDPRODI] = 0;
		}
		$kapasitasProdiFix = array();
		foreach ($mhsKelompok as $kel => $mhss) {
			$kapasitasProdiFix[$kel] = $kapprodi;
			foreach ($mhss as $key => $npm) {
				$kapasitasProdiFix[$kel][$mhsProdi[$npm]]++;
			}
		}
		$kapasitas = array();
		foreach ($kapasitasProdiFix as $kel => $vprodi) {
			foreach ($vprodi as $prd => $value) {
				array_push($kapasitas, array(
					'KDPRODI'=>$prd,
					'KDKEL'=>$kel,
					'KAPRODI'=>$value
				));
			}
		}
		$this->mremaining->running(array('persen'=>0,'pesan'=>'Simpang kapasitas kelompok KKN'));
		
	  $this->aModel->delKapasitasKkn();
		$this->db->insert_batch('kapasitas_prodi', $kapasitas);

		$persen = 0;
		foreach ($mhsKelompok as $kel => $value) {
			$persen += count($value);
		}
		$persen1 = 100/$persen;
		$persen2 = 0;

		foreach ($mhsKelompok as $kel => $value) {
			foreach ($value as $key => $npm) {
				$this->aModel->verifikasi($npm);
				$this->aModel->kelompok_peserta($kel,$npm);
				$this->mremaining->running(array('persen'=>floor($persen1*$persen2),'pesan'=>'Simpan kelompok KKN yang sudah dibentuk'));
				$persen2++;
			}
		}
		$this->mremaining->finish();
	}

	public function pilihKelompok($jrk,$avg,$kap,$mode){
		//normal
		$nilai = array();
		$kel = array();
		foreach ($jrk as $key => $value) {
			if($mode==false){
		  	if($this->isiKelompok[$value['kelompok']]<$kap){
					array_push($nilai, $value['nilai']);
					array_push($kel, $value['kelompok']);
				}
			}else{
				if($this->isiKelompokSisa[$value['kelompok']]<1){
					array_push($nilai, $value['nilai']);
					array_push($kel, $value['kelompok']);
				}
			}
		}
		$closest = null;
		$idx = null;
		$avg = $avg/sqrt($jrk); //konfigurasi pendekatan jarak
	  foreach ($nilai as $index => $item) {
	    if ($closest === null || abs($avg - $closest) > abs($item - $avg)) {
	      $closest = $item;
	      $idx = $index;
	    }
	  }
	  if($mode==false)
	  	$this->isiKelompok[$kel[$idx]]++;
	  else
	  	$this->isiKelompokSisa[$kel[$idx]]++;
	  return $kel[$idx];
	}

	public function urut_kelompok($datax){
		$kels = array();
		$avg = 0;
		foreach ($datax as $kel => $value) {
			array_push($kels, array(
				'kelompok'=>$kel,
				'nilai'=>$value
			));
			$avg += $value;
		}
		$avg = $avg/count($datax);
		for($i=0; $i<count($kels)-1; $i++){
			$temp = null;
			for($j=$i+1; $j<count($kels); $j++){
				if($kels[$j]['nilai']<$kels[$i]['nilai']){
					$temp = $kels[$i];
					$kels[$i] = $kels[$j];
					$kels[$j]=$temp;
				}
			}
		}
		return array(
				'data'=>$kels,
				'average'=>$avg
			);
	}

	public function urutAverageMhs($data){
		$npm = array();
		foreach ($data as $key => $value) {
			array_push($npm, $key);
		}

		for ($i=0; $i < count($npm)-1 ; $i++) { 
			for ($j=$i+1; $j < count($npm) ; $j++) { 
				if($data[$npm[$j]]>$data[$npm[$i]]){
					$temp = $data[$npm[$j]];
					$data[$npm[$j]] = $data[$npm[$i]];
					$data[$npm[$i]] = $temp;
				}
			}
		}

		return $data;
	}

}

/* End of file Metode_a.php */
/* Location: ./application/controllers/admin/metode/Metode_a.php */