<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verified extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('admin/verifiedModel');
	}

	public function index(){
		$this->view();
	}

	public function view(){
		$this->mhistory->go();
		$data = array();
		$mahasiswa = $this->verifiedModel->getMhs();
		$prodidata = $this->verifiedModel->getProdi();
		$kelompokdata = $this->verifiedModel->getKelompok();
		$prodi = array();

		foreach ($prodidata as $key => $value) {
			$prodi[$value->KDPRODI] = $value->NAMAPRODI;
		}
		$kab = $this->mfungsi->setting('kabupaten_asal');
		$kabupaten = array_filter(explode('|', $kab));
		if(!in_array('other', $kabupaten))
			array_push($kabupaten, 'other');
		foreach ($kabupaten as $b1 => $b2) {
			if(!array_key_exists($b2, $data))
				$data[$b2] = array();
		}
		foreach ($mahasiswa as $a1 => $a2) {
			$status = false;
			foreach ($kabupaten as $b1 => $b2) {
				$a = strtolower($a2->ALAMAT);
				$b = strtolower($b2);
				$pos = strpos($a, $b);
				if($pos===false){
				}else{
					if(!array_key_exists($a2->KODE, $data[$b]))
						$data[$b][$a2->KODE] = array();
					array_push($data[$b][$a2->KODE], $a2);
					$status = true;
					break;
				}
			}
			if($status==false){
				if(!array_key_exists('other', $data))
					$data['other'] = array();
				if(!array_key_exists($a2->KODE, $data['other']))
					$data['other'][$a2->KODE] = array();
				array_push($data['other'][$a2->KODE], $a2);
			}
		}

		foreach ($data as $key => $value) {
			foreach ($prodidata as $key1 => $value1) {
				if(!array_key_exists($value1->KDPRODI, $data[$key]))
					$data[$key][$value1->KDPRODI] = array();
			}
		}

		$def = null;
		$ttl = count($mahasiswa);
		$cekbnt = $this->verifiedModel->cek_pembentukan();
		$noKordinat = $this->verifiedModel->mhsNokordinat();
		if($ttl==0){
			if($cekbnt==true){
				$def = '#verified-option';
			}else{
				$def = '#verified';
			}
		}else{
			if(count($noKordinat)==0){
				if($cekbnt==true)
					$def = '#verified-option';
				else
					$def = '#informasi';
			}else{
				$def = '#validasi-alamat';
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/verified',array(
			'data'=>$data,
			'prodi'=>$prodi,
			'default'=>$def,
			'ttl'=>$ttl,
			'cekbnt'=>$cekbnt,
			'noKordinat'=>$noKordinat,
			'cekKel'=>count($kelompokdata)
		));

		$this->load->view('admin/footer');
	}

}

/* End of file verified.php */
/* Location: ./application/controllers/admin/verified.php */