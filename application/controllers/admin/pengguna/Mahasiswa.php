<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		$this->load->model('admin/mahasiswaModel');
		$this->mlogin->cek();
	}

	public function cek_nilai(){
		$data = $this->mahasiswaModel->null_nilai();
		$kel = array();
		foreach ($data as $key => $val) {
			if(!array_key_exists($val->KDKEL, $kel)){
				$kel[$val->KDKEL] = array(
					'info'=>array(
						'kelompok'=>$val->NAMAKEL,
						'dpl'=>$val->NAMADPL
					),
					'peserta'=>array()
				);
				array_push($kel[$val->KDKEL]['peserta'],
				array(
					'nama'=>$val->NAMAMHS,
					'npm'=>$val->NPM
				));
			}else{
				array_push($kel[$val->KDKEL]['peserta'],
				array(
					'nama'=>$val->NAMAMHS,
					'npm'=>$val->NPM
				));
			}
		}
		$html = '<div class="alert alert-warning" role="alert">
  Pengarsipan tidak dapat dilakukan karena nilai peserta masih ada yang belum diisi. Berikut daftar peserta yang nilainya belum diisi
</div><table cellpadding="3" cellspacing="0" border="1" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>NPM</th>
						<th>Nama</th>
					</tr>
				</thead>
				<tbody>';
				$no = 1;
		foreach ($kel as $key => $value) {
			$html .='
			<tr>
				<th colspan="3" class="text-muted" >Kelompok '.$value['info']['kelompok'].' ('.$value['info']['dpl'].')</th>
			</tr>
			';
			foreach ($value['peserta'] as $k => $v) {
				$html .='
				<tr>
					<td>'.$no++.'</td>
					<td>'.$v['npm'].'</td>
					<td>'.$v['nama'].'</td>
				</tr>
				';
			}
		}
		$html .= '</tbody></table>';
		if(count($kel)>0){
			echo json_encode(array(
				'status'=>true,
				'html'=>$html
			));
		}else{
			echo json_encode(array(
				'status'=>false,
				'html'=>'Silahkan refresh halaman'
			));
		}
	}

	public function index()
	{
		$this->data();
	}

	public function keluar($npm){
		$this->mahasiswaModel->keluaranggota($npm);
		redirect('admin/pengguna/mahasiswa');
	}

	public function addresscheck(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		echo json_encode($csv);
	}

	public function addresscheck2(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		if($csv->status==true)
			return TRUE;
		else
			return FALSE;
	}

	public function verifikasi($npm){
		$this->load->model('admin/verifikasiModel');
		$data = $this->verifikasiModel->singleVerifikasi($npm);
		if($data['error']==true){
			$this->mremaining->running(array('val'=>100,'data'=>$data),'end');
			$this->mremaining->finish(array('error'=>true));
			exit();
		}
		$kel = $data['data']['data']['select']->kata_kunci;
		$this->mahasiswaModel->kelompok_peserta($kel,$npm);
		$this->mahasiswaModel->verifikasi($npm);
		$this->mremaining->finish($data['data']);
		//redirect($this->mhistory->back());
	}

	public function verifikasi2($npm){
		$this->load->model('admin/verifikasiModel');
		$data = $this->verifikasiModel->singleVerifikasi2($npm);
		if($data['status']==true){
			//berhasil;
			$this->db->insert_batch('jarak_kelompok', $data['data']);
		}
		return $data['status'];//redirect($this->mhistory->back());
	}

	public function batalverifikasi($npm){
		$this->mahasiswaModel->kelompok_peserta_hapus($npm);
		$this->mahasiswaModel->btlverifikasi($npm);
		redirect($this->mhistory->back());
	}

	public function biodata_ajax(){
		$data = $this->mahasiswaModel->load($_POST['npm']);
		if($data->FOTOMHS!=NULL){
			if (strpos($data->FOTOMHS, 'unira') !== false) {
			    $foto = $data->FOTOMHS;
			}else{
				$foto = base_url($data->FOTOMHS);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$data->NPM));
			$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$data->FOTOMHS = $foto;
		$data->NAMAPRODI = $data->FAKPRODI.' / '.$data->NAMAPRODI;
		echo json_encode($data);
	}

	public function biodata($npm){
		$data = $this->mahasiswaModel->load($npm);
		if($data->FOTOMHS!=NULL){
			if (strpos($data->FOTOMHS, 'unira') !== false) {
			    $foto = $data->FOTOMHS;
			}else{
				$foto = base_url($data->FOTOMHS);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$data->NPM));
			$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$this->load->view('admin/header');
		$this->load->view('admin/mahasiswabiodata',array(
			'alamat'=>$data->ALAMATMHS,
			'telp'=>$data->KONTAKMHS,
			'kerja'=>$data->PEKERJAANMHS,
			'sks'=>$data->SKSMHS,
			'seragam'=>$data->SERAGAMMHS,
			'nama'=>$data->NAMAMHS,
			'prodi'=>$data->FAKPRODI.' / '.$data->NAMAPRODI,
			'npm'=>$data->NPM,
			'foto'=>$foto
		));
		$this->load->view('admin/footer');
	}

	public function arsipkan(){
		$pass = $_POST['password'];
		if($this->session->pass!=md5($pass)){
			$respon = array(
				'status'=>false,
				'msg'=>'Password yang anda masukkan salah!'
			);
		}else{
			$this->mahasiswaModel->arsipkan();
			$respon = array(
				'status'=>true,
				'msg'=>'Pengarsipan berhasil!'
			);
		}
		echo json_encode($respon);
	}

	public function data($page = 1){
		$this->session->subPageURL = null;
		$this->mhistory->go();
		$cari = '';
		$status = '';
		// $_POST['status'] = "2";
		// $_POST['cari'] = "";
		if(isset($_POST['cari'])){
			$cari = $_POST['cari'];
			$status = $_POST['status'];
		}
		$page = (5*($page-1));
		$nomor = $page+1;
		$total = $this->mahasiswaModel->totaldata([$cari,$status]);
		$data['srg'] = array();
		foreach ($total as $key => $value) {
			if(!array_key_exists($value->SERAGAMMHS, $data['srg']))
				$data['srg'][$value->SERAGAMMHS] = 0;
			$data['srg'][$value->SERAGAMMHS]++;
		}
		$this->mhistory->go();
		if(!isset($_POST['ajax']))
			$this->load->view('admin/header');
		$data['data'] = $this->mahasiswaModel->view($page,[$cari,$status]);
		$data['nomor'] = $nomor;
		$data['total'] = count($total);
		$data['cari'] = $cari;
		$data['status'] = $status;
		// echo $this->mfungsi->viewJson($_POST);
		// echo $this->mfungsi->viewJson($data);
		$this->load->view('admin/mahasiswa',$data);
		if(!isset($_POST['ajax']))
			$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$this->load->view('admin/formMahasiswa',array(
			'alamat'=>'',
			'kontak'=>'',
			'nim'=>'',
			'nama'=>'',
			'kordx'=>'',
			'kordy'=>'',
			'prodi'=>'',
			'kerja'=>'-',
			'sks'=>'0',
			'dataprodi'=>$this->db->get('prodi')->result(),
			'tipe'=>'tambah',
			'seragam'=>'',
			'kembali'=>$this->mhistory->back()
		));
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data = $this->mahasiswaModel->load($kode);
		$this->load->view('admin/formMahasiswa',array(
			'alamat'=>$data->ALAMATMHS,
			'kode'=>$kode,
			'kontak'=>$data->KONTAKMHS,
			'nim'=>$data->NPM,
			'kordx'=>$data->KORDXMHS,
			'kordy'=>$data->KORDYMHS,
			'nama'=>$data->NAMAMHS,
			'prodi'=>$data->KDPRODI,
			'kerja'=>$data->PEKERJAANMHS,
			'sks'=>$data->SKSMHS,
			'dataprodi'=>$this->db->get('prodi')->result(),
			'tipe'=>'edit',
			'seragam'=>$data->SERAGAMMHS,
			'kembali'=>$this->mhistory->back()
		));
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('nim', 'nim', 'trim|required|callback_cek_nama|numeric',array(
			'required'=>'Nim tidak boleh kosong',
			'cek_nama'=>'Nim "'.$_POST['nim'].'" sudah digunakan',
			'numeric'=>'Format nim tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 'nama', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong'
		));

		$this->form_validation->set_rules('kontak', 'kontak', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('kordx', 'lat', 'trim|required',array(
			'required'=>'Kordinat X tidak diperoleh',
		));

		$this->form_validation->set_rules('kordy', 'kontak', 'trim|required',array(
			'required'=>'Kordinat Y tidak diperoleh',
		));

		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		$this->form_validation->set_rules('sks', 'sks', 'trim|required|numeric');

		$this->form_validation->set_rules('seragam', 'seragam', 'trim|required');

		$this->form_validation->set_rules('prodi', 'prodi', 'trim|required');



		if ($this->form_validation->run() == FALSE)
    {
			$this->load->view('admin/header');
			$this->load->view('admin/formMahasiswa',array(
				'alamat'=>set_value('alamat'),
				'kontak'=>set_value('kontak'),
				'nim'=>set_value('nim'),
				'nama'=>set_value('nama'),
				'prodi'=>set_value('prodi'),
				'kordx'=>set_value('kordx'),
				'kordy'=>set_value('kordy'),
				'kerja'=>set_value('kerja'),
				'sks'=>set_value('sks'),
				'tipe'=>'tambah',
				'dataprodi'=>$this->db->get('prodi')->result(),
				'seragam'=>set_value('seragam'),
				'kembali'=>$this->mhistory->back()
			));
			$this->load->view('admin/footer');
    }
    else
    {
    	$_POST['status'] = 0;
     	$this->mahasiswaModel->simpan($_POST);
      redirect($this->mhistory->back());
    }
	}

	public function update($kode){

		$this->form_validation->set_rules('nim', 'nim', 'trim|required|callback_cek_nama_update|numeric',array(
			'required'=>'Nim tidak boleh kosong',
			'cek_nama_update'=>'Nim "'.$_POST['nim'].'" sudah digunakan',
			'numeric'=>'Format nim tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 'nama', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong'
		));

		$this->form_validation->set_rules('kordx', 'lat', 'trim|required',array(
			'required'=>'Kordinat X tidak diperoleh',
		));

		$this->form_validation->set_rules('kordy', 'kontak', 'trim|required',array(
			'required'=>'Kordinat Y tidak diperoleh',
		));

		$this->form_validation->set_rules('kontak', 'kontak', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('alamat', 'alamat', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		$this->form_validation->set_rules('sks', 'sks', 'trim|required|numeric');

		$this->form_validation->set_rules('seragam', 'seragam', 'trim|required');

		$this->form_validation->set_rules('prodi', 'prodi', 'trim|required');



		if ($this->form_validation->run() == FALSE)
    {
			$this->load->view('admin/header');
			$this->load->view('admin/formMahasiswa',array(
				'alamat'=>set_value('alamat'),
				'kontak'=>set_value('kontak'),
				'nim'=>set_value('nim'),
				'nama'=>set_value('nama'),
				'prodi'=>set_value('prodi'),
				'kordx'=>set_value('kordx'),
				'kordy'=>set_value('kordy'),
				'kerja'=>set_value('kerja'),
				'sks'=>set_value('sks'),
				'tipe'=>'edit',
				'dataprodi'=>$this->db->get('prodi')->result(),
				'kode'=>$kode,
				'seragam'=>set_value('seragam'),
				'kembali'=>$this->mhistory->back()
			));
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->mahasiswaModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function cekkordinat(){
		echo json_encode($this->mfungsi->kordinat($_POST['alamat']));
	}

	public function realisirAlamat($n=''){

	}

	public function hapus($kode){
		$this->mahasiswaModel->hapus($kode);
    redirect($this->mhistory->back());
	}

	public function api($dari,$sampai){
		$this->mremaining->start();
		$data = [];
		$prodi = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/prodi'));
		$total = (($sampai-$dari)+1) * (count($prodi->data)-1);
		$l = 1;
		foreach ($prodi->data as $i => $iv) {
			if($iv->id!=200){
				for($j = $dari; $j<= $sampai; $j++){
					$mhs = json_decode($this->mfungsi->file_get("https://api.unira.ac.id/v1/mahasiswa?limit=50000000000&filter[prodi]=$iv->id&filter[nim]=$j"));
						$o = new stdClass();
						$o->angkatan = $j;
						$o->prodi = $iv->attributes->nama;
						$o->fakultas = $iv->relationship->fakultas->data->attributes->nama;
						$o->mhs = $mhs->data;
						array_push($data,$o);
					$this->mremaining->running(array('progress'=>$l,'total'=>$total));
					$l++;
				}
			}
		}
		$this->mfile->write(array(
			'file'=>$this->session->usern.'_mhsApi.json',
			'data'=> json_encode($data)
		));
		$dat['users'] = $this->mahasiswaModel->allusername();
		$dat['mahasiswa'] = $data;
		$htm = $this->load->view('admin/apiMahasiswa', $dat,true);
		$this->mremaining->finish(array('template'=>$htm));
	}

	public function cekangkatan($n){
		$k = substr($n, 0,2);
		if($k>=20){
			return intval(substr($n, 0,4));
		}else{
			return 2011;
		}
	}

	public function apiproses($jns='',$vrf='0',$kd=''){
		$_POST['jenis'] = $jns;
		$_POST['kode'] = $kd;
		$mhss = json_decode($this->mfungsi->file_get(base_url().'/'.$this->session->usern.'_mhsApi.json'));
		$total = 0;
		$jenis = $_POST['jenis'];
		$kode = ($_POST['jenis']=='one') ? $_POST['kode'] : null;
		$opsi = null;
		$this->mremaining->start();
		if($jenis=='all'){
			$users = $this->mahasiswaModel->allusername();
			$i = 0;
			$j = 0;
			foreach ($mhss as $mk => $vk) {
				$total += count($vk->mhs);
			}
			foreach ($mhss as $mk => $vk) {
				foreach ($vk->mhs as $key => $value) {
					if(!in_array($value->id, $users)){
						$this->mahasiswaModel->simpan(array(
							'nim'=>$value->id,
							'nama'=>$value->attributes->nama,
							'kontak'=>$value->attributes->hp,
							'alamat'=>$value->attributes->alamat,
							'seragam'=>null,
							'prodi'=>strtoupper($vk->fakultas.' / '.$vk->prodi),
							'sks'=>$value->attributes->totalSks,
							'kerja'=>null,
							'status'=>$vrf
						));
						$j++;
					}
						$i++;
						$this->mremaining->running(array('process'=>floor($i*(100/$total)).'%','total'=>'100%'));
				}
			}
			$this->mremaining->finish(array('disimpan'=>$j,'tidak'=>($total-$j),'dari'=>$total));
		}else{
			foreach ($mhss as $mk => $vk) {
				foreach ($vk->mhs as $key => $value) {
					if($value->id==$kode){
						$this->mahasiswaModel->simpan(array(
							'nim'=>$value->id,
							'nama'=>$value->attributes->nama,
							'kontak'=>$value->attributes->hp,
							'alamat'=>$value->attributes->alamat,
							'seragam'=>null,
							'prodi'=>strtoupper($vk->fakultas.' / '.$vk->prodi),
							'sks'=>$value->attributes->totalSks,
							'kerja'=>null,
							'status'=>1
						));
						
						$this->mremaining->finish();
						break;
					}
				}
			}
		}
	}

	public function upload1(){
		echo json_encode($this->mio->upload(array(
			'path'=>'assets/',
			'format'=>'xls',
			'filename'=>'datamhs',
			'name'=>'file',
			'openerr'=>'',
			'closeerr'=>'',
			'overwrite'=>true
		)));
	}

	public function upload2(){
		$this->mexcel->import(array(
			'filename'=>'assets/datamhs.xls',
			'field'=>array('NPM','NAMAMHS','KDPRODI','ALAMATMHS'),
			'table'=>'mahasiswa',
			'database'=>true,
			'process'=>true,
			'except'=>array('TGLREGMHS'=>"date('Y-m-d H:i:s')",'STATUSMHS'=>'0'),
			'skip'=>array('field'=>'NPM','list'=>$this->mahasiswaModel->getAllUsername())
		));
	}

	public function upload(){
		echo json_encode($this->mio->upload(array(
			'path'=>'assets/',
			'format'=>'xls',
			'filename'=>'datamhs',
			'name'=>'file',
			'openerr'=>'',
			'closeerr'=>'',
			'overwrite'=>true
		)));
	}

	public function verifikasiManualProses(){
		$this->load->model('admin/verifikasiModel');
		$this->mremaining->start();
		$dataform = json_decode($this->mfungsi->file_get('verifikasiManual.json'));
		$ttlMhs = 0;
		foreach ($dataform->prioritasdata as $key => $value) {
			if(array_key_exists($value,$dataform ))
				$ttlMhs += count($dataform->{$value});
		}
		$this->mremaining->running(array('pesan'=>'Mencari jarak terdekat','persen'=>0,'hasil'=>0,'total'=>$ttlMhs));
		$this->load->model('admin/verifikasiModel');

		$i=1;
		$j=0;
		$total = 0;
		foreach ($dataform->prioritasdata as $key => $value) {
			$total += count($dataform->{$value});
		}

		foreach ($dataform->prioritasdata as $key => $value) {
			if(array_key_exists($value,$dataform )){
				foreach ($dataform->{$value} as $a => $b) {
					$attr = $this->verifikasiModel->getMhs2($b);
					$mhs = new stdClass();
					$mhs->NPM = $attr->NPM;
					$mhs->NAMA = $attr->NAMAMHS;
					$mhs->KODE = $attr->KDPRODI;
					$mhs->ALAMAT = $attr->ALAMATMHS;
					$mhs->PRODI = $attr->FAKPRODI.' / '.$attr->NAMAPRODI;
					$mhs->STATUS = $attr->STATUSMHS;

					
					
					$sts = $this->verifikasiModel->getVerifikasi($mhs,$i,$total,$dataform->kodekelompok->{$value});
					$this->mremaining->running(array('out'=>$sts),'consol');
					if($sts==false){
						$j++;
						$this->mahasiswaModel->btlverifikasi($mhs->NPM);
						$this->mahasiswaModel->kelompok_peserta_hapus($mhs->NPM);
					}else{
						$this->mahasiswaModel->verifikasi($mhs->NPM);
						$this->mahasiswaModel->kelompok_peserta($sts->kata_kunci,$mhs->NPM);
					}
					$i++;
				}
			}
		}
		$this->mremaining->finish(array('msg'=>'<h3 class="text-success">Dari total '.$total.' mahasiswa yang berhasil diverifikasi sebanyak '.($total-$j).' mahasiswa</h3>'));
	}

	public function prosesverifikasi($vrf = 0){
		$this->load->model('admin/verifikasiModel');
		$this->mremaining->start();
		$prodidata = $this->verifikasiModel->getProdi();
		$kelompok = $this->verifikasiModel->getKelompok();
		$mahasiswa = $this->verifikasiModel->getMhsfromdb();
		$totalmhs = $this->verifikasiModel->getMhs($vrf);
		if($vrf!=0){
			$kapasitas = $this->verifikasiModel->getKapasitasKkn($totalmhs);
			$this->mremaining->running(array('pesan'=>'Kalkulasi untuk membagi rata kelompok kkn','persen'=>0,'hasil'=>0,'total'=>(count($kapasitas))));
			$i=0;
			$dataval = $this->verifikasiModel->hitungFix($kapasitas,$kelompok);
			foreach ($dataval as $key => $value) {
				$kap = 0;
				foreach ($value as $pkey => $pvalue) {
					$this->verifikasiModel->setKapasitasKkn($key,$pkey,$pvalue);
					$kap += $y;
					$this->mremaining->running(array('pesan'=>'Kalkulasi untuk membagi rata kelompok kkn','persen'=>100,'hasil'=>$i,'total'=>count($kelompok)));
				}
				$i++;
				$this->verifikasiModel->setKapasitas($key,$kap);
			}
		}
		$kelindex = [];
		$kellabel = [];
		$this->load->model('admin/wilayahModel');
		$i = 1;
		$kelstatus = false;
		// foreach ($kelompok as $x => $y) {
		// 	array_push($kelindex, $y->KDKEL);
		// 	$kellabel[$y->KDKEL]['nama'] = $y->NAMAKEL;
		// 	$kellabel[$y->KDKEL]['alamat'] = $y->ALAMATKEL;
		// 	$cek = $this->wilayahModel->find($y->ALAMATKEL,'google');
		// 	$this->mremaining->running(array('pesan'=>'Cek alamat kkn','persen'=>100,'hasil'=>$i,'total'=>count($kelompok)));
		// 	if($cek->status==false){
		// 		$this->mremaining->running(array('alamat'=>$y->ALAMATKEL,'kelompok'=>$y->NAMAKEL),'kelompok');
		// 		$kelstatus = true;
		// 	}
		// 	$i++;
		// }

		if($kelstatus==true){
			$this->mremaining->finish(array('msg'=>'<h3 class="text-danger">Periksa alamat kelompok kkn, pastikan terhubung ke internet</h3>'));
			exit();
		}

		//$this->mremaining->running(array('pesan'=>'Mencari jarak terdekat','persen'=>(floor($i/count($value))*0),'hasil'=>0,'total'=>count($data)+count($mahasiswa)));
		$this->load->model('admin/verifikasiModel');

		$i=1;
		$j=0;
		foreach ($mahasiswa as $key => $value) {
			$respon = $this->verifikasiModel->getVerifikasi($value,$i,count($mahasiswa));
			if(is_array($respon))
				$sts = $respon['data'];
			else
				$sts = $respon;
			$cek = $this->mahasiswaModel->cek($value->NPM);
			if($sts==false){
				$j++;
				if($cek>0){
					$this->mahasiswaModel->btlverifikasi($value->NPM);
				}else{
					$this->mahasiswaModel->simpan(array(
						'nim'=>$value->NPM,
						'nama'=>$value->NAMA,
						'alamat'=>$value->ALAMAT,
						'kontak'=>null,
						'seragam'=>null,
						'prodi'=>$value->KODE,
						'sks'=>null,
						'kerja'=>null,
						'status'=>0
					));
				}
				$this->mahasiswaModel->kelompok_peserta_hapus($value->NPM);
			}else{
				$this->db->insert_batch('jarak_kelompok', $respon['database']);
				if($cek>0){
					//$this->mahasiswaModel->verifikasi($value->NPM);
					//$this->db->insert_batch('jarak_kelompok', $respon['database']);
				}else{
					$this->mahasiswaModel->simpan(array(
						'nim'=>$value->NPM,
						'nama'=>$value->NAMA,
						'alamat'=>$value->ALAMAT,
						'kontak'=>null,
						'seragam'=>null,
						'prodi'=>$value->KODE,
						'sks'=>null,
						'kerja'=>null,
						'status'=>1
					));
				}
				//$this->mahasiswaModel->kelompok_peserta($sts->kata_kunci,$value->NPM);
			}
			$i++;
			//sleep(1);
		}
		$this->mremaining->finish(array('msg'=>'<h3 class="text-success">Dari total '.count($mahasiswa).' mahasiswa yang berhasil diverifikasi sebanyak '.(count($mahasiswa)-$j).' mahasiswa</h3>'));
	}

	public function ulangiverifikasi2(){
		$this->mremaining->start();
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_GET['alamat'],'google');

		if($csv->status==true){
			$this->db->set('ALAMATMHS',urldecode($_GET['alamat']));
			$this->db->where('NPM',$_GET['npm']);
			$this->db->where('STATUSMHS',0);
			$this->db->update('mahasiswa');
			$cek = $this->verifikasi2($_GET['npm']);
			if($cek!=true){
				$this->mremaining->running(array('msg'=>$cek),'error');
			}
			$this->mremaining->finish(true);
		}
		else{
			if($_GET['alamat']=='')
				$this->mremaining->running(array('msg'=>'Alamat harus diisi'),'error');
			else
				$this->mremaining->running(array('msg'=>'Alamat "'.$_GET['alamat'].'" tidak tersedia, gunakan nama alamat yang mencakupi nama desa, kecamatan, dan kabupaten'),'error');
			$this->mremaining->finish(false);
		}
	}

	public function ulangiverifikasi(){
		$this->mremaining->start();
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_GET['alamat'],'google');

		if($csv->status==true){
			$this->db->set('ALAMATMHS',urldecode($_GET['alamat']));
			$this->db->where('NPM',$_GET['npm']);
			$this->db->where('STATUSMHS',0);
			$this->db->update('mahasiswa');
			$this->verifikasi($_GET['npm']);
			$this->mremaining->finish(true);
		}
		else{
			if($_GET['alamat']=='')
				$this->mremaining->running(array('msg'=>'Alamat harus diisi'),'error');
			else
				$this->mremaining->running(array('msg'=>'Alamat "'.$_GET['alamat'].'" tidak tersedia, gunakan nama alamat yang mencakupi nama desa, kecamatan, dan kabupaten'),'error');
			$this->mremaining->finish(false);
		}
	}

	public function infoUload(){
		$vrf = 1;
		$this->load->model('admin/verifikasiModel');
		$mahasiswa = $this->verifikasiModel->getMhs($vrf,true);
		$prodidata = $this->verifikasiModel->getProdi();
		foreach ($prodidata as $key => $value) {
			$prodi[$value->KDPRODI] = $value->NAMAPRODI;
		}
		$prd = $this->verifikasiModel->getMhsProdi($mahasiswa);
		foreach ($this->verifikasiModel->getProdi() as $key => $value) {
			$kapasitas[$value->KDPRODI] = (isset($prd[$value->KDPRODI])) ? count($prd[$value->KDPRODI]) : 0;
		}
		$this->load->view('admin/header');
		$this->load->view('admin/verifikasi', array(
			'mahasiswa'=>$mahasiswa,
			'kapasitas'=>$kapasitas,
			'excel'=>($vrf!=0) ? count($this->verifikasiModel->getMhsExcel()) : 0,
			'database'=>count($this->verifikasiModel->getMhsStatus(0)),
			'prodi'=>$prodi,
			'tipe'=>$vrf
		));
		$this->load->view('admin/footer');
	}

	public function takeinfo(){
		$vrf = 0;
		$this->load->model('admin/verifikasiModel');
		//$mahasiswa = $this->verifikasiModel->getMhs($vrf,true);getMhsfromdb
		$mahasiswa = $this->verifikasiModel->getMhsfromdb();
		$prodidata = $this->verifikasiModel->getProdi();
		$kelompokdata = $this->verifikasiModel->getKelompok();
		$prodi = array();
		foreach ($prodidata as $key => $value) {
			$prodi[$value->KDPRODI] = $value->NAMAPRODI;
		}
		$kab = $this->mfungsi->setting('kabupaten_asal');
		$kabupaten = array_filter(explode('|', $kab));
		if(!in_array('other', $kabupaten))
			array_push($kabupaten, 'other');
		$data = array();
		foreach ($kabupaten as $b1 => $b2) {
			if(!array_key_exists($b2, $data))
				$data[$b2] = array();
		}
		foreach ($mahasiswa as $a1 => $a2) {
			$status = false;
			foreach ($kabupaten as $b1 => $b2) {
				$a = strtolower($a2->ALAMAT);
				$b = strtolower($b2);
				$pos = strpos($a, $b);
				if($pos===false){
				}else{
					if(!array_key_exists($a2->KODE, $data[$b]))
						$data[$b][$a2->KODE] = array();
					array_push($data[$b][$a2->KODE], $a2);
					$status = true;
					break;
				}
			}
			if($status==false){
				if(!array_key_exists('other', $data))
					$data['other'] = array();
				if(!array_key_exists($a2->KODE, $data['other']))
					$data['other'][$a2->KODE] = array();
				array_push($data['other'][$a2->KODE], $a2);
			}
		}

		foreach ($data as $key => $value) {
			foreach ($prodidata as $key1 => $value1) {
				if(!array_key_exists($value1->KDPRODI, $data[$key]))
					$data[$key][$value1->KDPRODI] = array();
			}
		}
		$mhs = $this->mahasiswaModel->getMahasiswa();
		$totalkap = $this->mahasiswaModel->getJmlKapasitas();
		$kapasitas = array();
		foreach ($prodidata as $key1 => $value1) {
			if(!array_key_exists($value1->KDPRODI, $kapasitas))
				$kapasitas[$value1->KDPRODI] = 0;
		}
		foreach ($mhs as $key => $value) {
			$kapasitas[$value->KDPRODI]++;
		}
		$kabkkn = array();
		$kodekel = array();
		$sample = array('bangkalan','sampang','pamekasan','sumenep');
		foreach ($kelompokdata as $key => $value) {
			foreach ($sample as $key1 => $value1) {
				$a = strtolower($value->ALAMATKEL);
				$b = strtolower($value1);
				$pos = strpos($a, $b);
				if($pos===false){
				}else{
					if(!array_key_exists($b, $kodekel))
						$kodekel[$b] = array();
					array_push($kodekel[$b], $value->KDKEL);
					if(!in_array($b, $kabkkn)){
						array_push($kabkkn, $b);
						break;
					}
				}
			}
		}
		$kapasitaskelkab = array();
		$kapasitaskelkabisi = array();
		foreach ($kodekel as $key => $value) {
			if(!array_key_exists($key, $kapasitaskelkab)){
				$kapasitaskelkab[$key] = $this->mahasiswaModel->getkapasistakelompokkabupaten($value);
				$kapasitaskelkabisi[$key] = $this->mahasiswaModel->getkapasistakelompokkabupatenisi($value);
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/info-verifikasi', array(
			'data'=>$data,
			'prodi'=>$prodi,
			'kapasitas'=>$kapasitas,
			'totalKapasitas'=>$totalkap,
			'kabupatenkkn'=>$kabkkn,
			'kapasitaskab'=>$kapasitaskelkab,
			'kapasitaskabisi'=>$kapasitaskelkabisi,
			'kodekel'=>$kodekel
		));
		$this->load->view('admin/footer');
	}

	public function verifikasiManual(){
		$this->mfile->write(array(
			'file'=>'./verifikasiManual.json',
			'data'=>json_encode($_POST)
		));
	}

	public function setKapasitas(){
		$this->load->model('admin/verifikasiModel');
		$kelompok = $this->verifikasiModel->getKelompok();
		$data = $this->verifikasiModel->hitungFix($_POST['prodi'],$kelompok);
		foreach ($data as $kel => $value) {
			$ttl = 0;
			foreach ($value as $prodi => $isi) {
				$ttl += $isi;
				$this->verifikasiModel->setKapasitasKkn($kel,$prodi,$isi);
			}
			$this->verifikasiModel->setKapasitas($kel,$ttl);
		}
		redirect($_POST['back']);
	}

	public function editverifikasi(){
		echo 'testing';
	}

	public function cek_nama(){
		$this->db->where('NPM', $_POST['nim']);
    $query = $this->db->get('mahasiswa');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update(){
		$this->db->where('NPM', $_POST['nim']);
		$this->db->where('NPM!=', $this->uri->segment(5));
    $query = $this->db->get('mahasiswa');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */