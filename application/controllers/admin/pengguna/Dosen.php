<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		set_time_limit(0);
		$this->load->model('admin/dosenModel');
		$this->mlogin->cek();
	}

	public function index()
	{
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data['total'] = $this->db->count_all('dosen');
		$data['data'] = $this->dosenModel->view();
		$this->load->view('admin/dosen',$data);
		$this->load->view('admin/footer');
	}

	public function bio($kode){
		$this->load->model('admin/dosenModel');
		$dsn = $this->dosenModel->load($kode);

		if($dsn->FOTODPL!=NULL){
			if (strpos($dsn->FOTODPL, 'unira') !== false) {
			    $foto = $dsn->FOTODPL;
			}else{
				$foto = base_url($dsn->FOTODPL);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?filter[nis]='.$dsn->KDDPL));
			if(isset($dmhs->errors) || !isset($dmhs->data))
				$foto = base_url('assets/images/faces/user.png');
			else
				if($dmhs->data[0]->attributes->thumbnail==null)
					$foto = base_url('assets/images/faces/user.png');
				else
					$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$dsn->FOTODPL = $foto;
		$this->load->view('admin/header');
		$this->load->view('admin/profil-dosen',array(
			'data'=>$dsn
		));
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'tambah';
		$data['username'] = '';
		$data['nama'] = '';
		$data['kontak'] = '';
		$data['alamat'] = '';
		$data['jk'] = 'l';
		$data['foto'] = '';
		$data['pass'] = '';
		$data['rpass'] = '';
		$this->load->view('admin/formDosen',$data);
		$this->load->view('admin/footer');
	}

	public function edit($kode){
		$this->load->view('admin/header');
		$data['kembali'] = $this->mhistory->back();
		$data['tipe'] = 'edit';
		$data['kode'] = $kode;
		$load = $this->dosenModel->load($kode);
		$data['username'] = $load->KDDPL;
		$data['nama'] = $load->NAMADPL;
		$data['kontak'] = $load->KONTAKDPL;
		$data['alamat'] = $load->ALAMATDPL;
		$data['foto'] = $load->FOTODPL;
		$this->load->view('admin/formDosen',$data);
		$this->load->view('admin/footer');
	}

	public function simpan(){
		$this->form_validation->set_rules('username', 's', 'trim|required|callback_cek_nama|alpha_numeric',array(
			'required'=>'Username tidak boleh kosong',
			'cek_nama'=>'Username "'.$_POST['username'].'" sudah digunakan',
			'alpha_numeric'=>'Format username tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 's', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong'
		));

		$this->form_validation->set_rules('kontak', 's', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('alamat', 's', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		if ($this->form_validation->run() == FALSE)
    {
      $this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'tambah';
			$data['username'] = set_value('username');
			$data['nama'] = set_value('nama');
			$data['kontak'] = set_value('kontak');
			$data['alamat'] = set_value('alamat');
			$this->load->view('admin/formDosen',$data);
			$this->load->view('admin/footer');
    }
    else
    {
    	$_POST['kode']=$_POST['username'];
     	$this->dosenModel->simpan($_POST);
      redirect($this->mhistory->back(),'refresh');
    }
	}

	public function update($kode){

		$this->form_validation->set_rules('username', 's', 'trim|required|callback_cek_nama_update|alpha_numeric',array(
			'required'=>'Username tidak boleh kosong',
			'cek_nama_update'=>'Username "'.$_POST['username'].'" sudah digunakan',
			'alpha_numeric'=>'Format username tidak dibolehkan'
		));

		$this->form_validation->set_rules('nama', 's', 'trim|required',array(
			'required'=>'Nama tidak boleh kosong'
		));

		$this->form_validation->set_rules('kontak', 's', 'trim|required',array(
			'required'=>'Kontak tidak boleh kosong',
		));

		$this->form_validation->set_rules('alamat', 's', 'trim|required',array(
			'required'=>'Alamat tidak boleh kosong'
		));

		if ($this->form_validation->run() == FALSE)
    {
			$this->load->view('admin/header');
			$data['kembali'] = $this->mhistory->back();
			$data['tipe'] = 'edit';
			$data['kode'] = $kode;
			$data['username'] = set_value('username');
			$data['nama'] = set_value('nama');
			$data['kontak'] = set_value('kontak');
			$data['alamat'] = set_value('alamat');
			$this->load->view('admin/formDosen',$data);
			$this->load->view('admin/footer');
    }
    else
    {
     	$this->dosenModel->update($_POST,$kode);
      redirect($this->mhistory->back(),'refresh');
    }
	}


	public function hapus($kode){
		$this->dosenModel->hapus($kode);
    redirect($this->mhistory->back());
	}

	public function api(){
		$res= json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?limit=500000'));
		$data['dosen'] = $res->data;
		$data['users'] = [];//$this->dosenModel->allusername();
		$this->mfile->write(array(
			'file'=>$this->session->usern.'_dosenApi.json',
			'data'=> json_encode($res->data)
		));
		$this->load->view('admin/apiDosen', $data);
	}

	public function apiproses($jns='',$kd=''){
		$_POST['jenis'] = $jns;
		$_POST['kode'] = $kd;
		$dosens = json_decode($this->mfungsi->file_get(base_url().'/'.$this->session->usern.'_dosenApi.json'));
		$jenis = $_POST['jenis'];
		$kode = ($_POST['jenis']=='one') ? $_POST['kode'] : null;
		$opsi = null;
		$total = count($dosens);
		$users = $this->dosenModel->allusername();
		$this->mremaining->start();
		if($jenis=='all'){
			$i = 0;
			$j = 0;
			foreach ($dosens as $key => $value) {
				if(!in_array($value->id, $users)){
					$this->dosenModel->simpan(array(
						'kode'=>$value->id,
						'nama'=>$value->attributes->nama,
						'kontak'=>($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email,
						'alamat'=>$value->attributes->alamat
					));
				}else{
					$this->dosenModel->update(array(
						'nama'=>$value->attributes->nama,
						'kontak'=>($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email,
						'alamat'=>$value->attributes->alamat
					),$value->id);
				}
				$j++;
				$i++;
				$this->mremaining->running(array('process'=>floor($i*(100/$total)).'%','total'=>'100%'));
			}
			$this->mremaining->finish(array('disimpan'=>$j,'tidak'=>($total-$j),'dari'=>$total));
		}else{
			foreach ($dosens as $key => $value) {
				if($value->id==$kode){
					if(!in_array($value->id, $users)){
						$kontak = ($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email;
						$this->dosenModel->simpan(array(
							'kode'=>$value->id,
							'nama'=>$value->attributes->nama,
							'kontak'=>($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email,
							'alamat'=>$value->attributes->alamat
						));
					}else{
						$kontak = ($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email;
						$this->dosenModel->update(array(
							'nama'=>$value->attributes->nama,
							'kontak'=>($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email,
							'alamat'=>$value->attributes->alamat
						),$value->id);
					}
						$this->mremaining->finish();
						break;
				}
			}
		}
	}

	public function upload(){
		echo json_encode($this->mio->upload(array(
			'path'=>'assets/',
			'format'=>'xls',
			'filename'=>'datadosen',
			'name'=>'file',
			'openerr'=>'',
			'closeerr'=>'',
			'overwrite'=>true
		)));
	}

	public function exceltodb(){
		$this->mexcel->import(array(
			'filename'=>'assets/datadosen.xls',
			'field'=>['USERNDPL','NAMADPL','KONTAKDPL','ALAMATDPL','JKDPL','PASSDPL'],
			'table'=>'dosen',
			'function'=>array(
				'PASSDPL'=>'MD5(%s)'
			)
		));
	}

	public function cek_nama(){
		$this->db->where('KDDPL', $_POST['username']);
    $query = $this->db->get('dosen');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

	public function cek_nama_update(){
		$this->db->where('KDDPL', $_POST['username']);
		$this->db->where('KDDPL!=', $this->uri->segment(5));
    $query = $this->db->get('dosen');
    $count_row = $query->num_rows();
    if ($count_row > 0) {
        return FALSE;
    } else {
        return TRUE;
    }
	}

}

/* End of file tahun.php */
/* Location: ./application/controllers/admin/master/tahun.php */