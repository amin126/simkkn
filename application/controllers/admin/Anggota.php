<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/anggotakknModel');
		$this->mlogin->cek();
	}

	public function getkelompok(){
		$data = $this->anggotakknModel->allmahasiswa($_POST['kode']);
		$this->load->view('admin/anggotaMhs',array(
			'data'=>$data,
			'tipe'=>$_POST['tipe'],
			'kelompok'=>$_POST['kode']
		));
	}

	public function pindah(){
		$this->mhistory->go();
		$temp = [];
		$temp['npm'] = '';
		$temp['status'] = 'a';
		$temp['kkn'] = [];
		$temp['mahasiswa'] = [];
		$cari = '';
		$temp['kelompok'] = '';
		if(isset($_POST['npm'])){
			$temp['mahasiswa'] = $this->anggotakknModel->mahasiswa($_POST['npm'],1);
			if(count($temp['mahasiswa'])>0){
				$cari = $_POST['npm'];
				$temp['npm'] = $temp['mahasiswa'][0]->NPM;
				$temp['attr'] = $this->anggotakknModel->atribut_mhs($temp['npm']);
				//$temp['kkn'] = $this->anggotakknModel->kelompok_tersedia($temp['attr']->kode);
				$temp['kelompok'] = $temp['attr']->NAMAKEL;
				$temp['kkn'] = $this->anggotakknModel->kelompok();
				$temp['status'] = 'b';
			}else{
				$temp['status'] = 'c';
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/anggotaPindah2',array( //admin/anggotaPindah
			'cari'=>$cari,
			'npm'=>$temp['npm'],
			'status'=>$temp['status'],
			'kkn'=>$temp['kkn'],
			'kelompok'=>$temp['kelompok'],
			'mahasiswa'=>$temp['mahasiswa']
		));
		$this->load->view('admin/footer');
	}

	public function tambah(){
		$this->mhistory->go();
		$temp = array();
		$temp['npm'] = '';
		$temp['status'] = 'a';
		$cari = '';
		$temp['kkn'] = array();
		$temp['mahasiswa'] = array();
		if(isset($_POST['npm'])){
			$temp['mahasiswa'] = $this->anggotakknModel->mahasiswa($_POST['npm'],0);
			if(count($temp['mahasiswa'])>0){
				$cari = $_POST['npm'];
				$temp['npm'] = $temp['mahasiswa'][0]->NPM;
				$temp['attr'] = $this->anggotakknModel->atribut_mhs($temp['npm']);
				$temp['kkn'] = $this->anggotakknModel->kelompok();
				$temp['status'] = 'b';
			}else{
				$temp['status'] = 'c';
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/anggotaTambah2',array(
			'cari'=>$cari,
			'npm'=>$temp['npm'],
			'status'=>$temp['status'],
			'kkn'=>$temp['kkn'],
			'mahasiswa'=>$temp['mahasiswa']
		));
		$this->load->view('admin/footer');
	}

	public function tukar(){
		$this->mhistory->go();
		$temp = [];
		$temp['kelompok'] = $this->anggotakknModel->kelompok();
		$this->load->view('admin/header');
		$this->load->view('admin/anggotaTukar',array(
			'kelompok'=>$temp['kelompok']
		));
		$this->load->view('admin/footer');
	}

	public function cari(){
		$this->mhistory->go();
		$temp = [];
		$temp['npm'] = '';
		$temp['status'] = 'a';
		$temp['kkn'] = [];
		$temp['kelompok'] = null;
		$temp['anggota'] = null;
		$temp['mahasiswa'] = [];
		$cari = '';
		$foto = '';
		if(isset($_POST['npm'])){
			$cari = $_POST['npm'];
			$temp['mahasiswa'] = $this->anggotakknModel->mahasiswa($_POST['npm'],'cari');
			if(count($temp['mahasiswa'])>0){
				if($temp['mahasiswa'][0]->FOTOMHS!=NULL){
					if (strpos($temp['mahasiswa'][0]->FOTOMHS, 'unira') !== false) {
					    $foto = $temp['mahasiswa'][0]->FOTOMHS;
					}else{
						$foto = base_url($temp['mahasiswa'][0]->FOTOMHS);
					}
				}else{
					$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$temp['mahasiswa'][0]->NPM));
					$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
				}
				$temp['npm'] = $temp['mahasiswa'][0]->NPM;
				$temp['attr'] = $this->anggotakknModel->atribut_mhs($temp['npm']);
				$this->load->model('admin/kelompokModel');
				$kode = $this->anggotakknModel->get_kodekel($temp['npm'])[0]->KDKEL;
				$temp['kelompok'] = $this->kelompokModel->view($kode);
				$temp['anggota'] = $this->kelompokModel->anggota($kode);
				$temp['status'] = 'b';
			}else{
				$temp['status'] = 'c';
			}
		}
		$this->load->view('admin/header');
		$this->load->view('admin/anggotaCari',array(
			'cari'=>$cari,
			'npm'=>$temp['npm'],
			'status'=>$temp['status'],
			'mahasiswa'=>$temp['mahasiswa'],
			'anggota'=>$temp['anggota'],
			'foto'=>$foto,
			'kelompok'=>$temp['kelompok']
		));
		$this->load->view('admin/footer');
	}

	public function prosestambah(){
		$npm = $_POST['npm'];
		$kel = $_POST['kelompok'];
		$prodi = $this->anggotakknModel->atribut_mhs($npm);
		$kkn = $this->anggotakknModel->kelompok_tersedia($prodi->kode,$kel);
		if(count($kkn)>0){
			 $this->anggotakknModel->tambah($npm,$kel);
		}
		redirect($this->mhistory->back());
	}

	public function prosestambah2(){
		$npm = $_POST['npm'];
		$kel = $_POST['kelompok'];
		$prodi = $this->anggotakknModel->atribut_mhs($npm);
		$this->anggotakknModel->tambah($npm,$kel);
		$this->anggotakknModel->setKapasitesKel($kel,1);
		$this->anggotakknModel->update_kapasitas($kel,$prodi->kode,1);
		redirect($this->mhistory->back());
	}

	public function prosespindah(){
		$npm = $_POST['npm'];
		$kel = $_POST['kelompok'];
		$prodi = $this->anggotakknModel->atribut_mhs($npm);
		$kkn = $this->anggotakknModel->kelompok_tersedia($prodi->kode,$kel);
		if(count($kkn)>0){
			 $this->anggotakknModel->pindah($npm,$kel);
		}
		redirect($this->mhistory->back());
	}

	public function prosespindah2(){
		$npm = $_POST['npm'];
		$kelke = $_POST['kelompok'];
		$keldari = $this->anggotakknModel->kelompokMhs($npm)->kode;
		$prodidari = $this->anggotakknModel->atribut_mhs($npm)->kode;
		$this->anggotakknModel->setKapasitesKel($keldari,0);
		$this->anggotakknModel->tukar($npm,$kelke);
		$prodike = $this->anggotakknModel->atribut_mhs($npm)->kode;
		$this->anggotakknModel->setKapasitesKel($kelke,1);
		$this->anggotakknModel->update_kapasitas($keldari,$prodidari,0);
		$this->anggotakknModel->update_kapasitas($kelke,$prodike,1);
	}

	public function prosestukar(){
		$npmke = $_POST['npmke'];
		$prodike = $this->anggotakknModel->atribut_mhs($npmke)->kode;
		$kelke = $_POST['kelompokke'];
		$npmdari = $_POST['npmdari'];
		$prodidari = $this->anggotakknModel->atribut_mhs($npmdari)->kode;
		$keldari = $_POST['kelompokdari'];
		$this->anggotakknModel->tukar($npmdari,$kelke);
		$this->anggotakknModel->tukar($npmke,$keldari);
		$this->anggotakknModel->update_kapasitas($keldari,$prodidari,0);
		$this->anggotakknModel->update_kapasitas($keldari,$prodike,1);
		$this->anggotakknModel->update_kapasitas($kelke,$prodidari,1);
		$this->anggotakknModel->update_kapasitas($kelke,$prodike,0);
	}


}

/* End of file anggotakkn.php */
/* Location: ./application/controllers/admin/anggotakkn.php */