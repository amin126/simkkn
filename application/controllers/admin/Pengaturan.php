<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/pengaturanModel');
		$this->mlogin->cek();
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$this->pengaturanModel->setNew('api_key');
		$this->pengaturanModel->setNew('kabupaten_asal','sampang|pamekasan');
		$this->pengaturanModel->setNew('edit_biodata_mhs');
		$this->pengaturanModel->setNew('minimal_prodi','3');
		$this->pengaturanModel->setNew('model_pembentukan','a');
		$this->pengaturanModel->setNew('halaman_dpl','0');
		$this->pengaturanModel->setNew('halaman_camat','0');
		$this->pengaturanModel->setNew('halaman_biro','0');
		$this->pengaturanModel->setNew('halaman_peserta','0');
		$this->pengaturanModel->setNew('halaman_daftar','0');
		$this->pengaturanModel->setNew('halaman_uploadp','0');
		$this->pengaturanModel->setNew('halaman_uploadd','0');
		$this->pengaturanModel->setNew('hal_pstkelompok','0');
		$this->pengaturanModel->setNew('tgl_skedul',date('Y-m-d'));
		$this->pengaturanModel->setNew('auto_skedul','0');
		$this->pengaturanModel->setNew('pimpinan_lppm','-');
		$this->pengaturanModel->setNew('nosertifikat','%nomor%/E-01/LPPM-UNIRA/VI/%tahun%');
	}

	public function index(){
		$this->view();
	}

	public function view(){
		$this->mhistory->go();
		$this->load->view('admin/header');
		$data = array();
		$data['key'] = $this->mfungsi->setting('api_key');
		$data['biodata'] = intval($this->mfungsi->setting('edit_biodata_mhs'));
		$data['model'] = $this->mfungsi->setting('model_pembentukan');
		$data['btsprodi'] = $this->mfungsi->setting('minimal_prodi');
		$data['kabupaten'] = $this->mfungsi->setting('kabupaten_asal');
		$data['verifikasi'] = $this->mfungsi->setting('batas_verifikasi');
		$data['pimpinan'] = $this->mfungsi->setting('pimpinan_lppm');
		$data['nosertifikat'] = $this->mfungsi->setting('nosertifikat');
		$data['halaman'] = array(
			'dpl'=>intval($this->mfungsi->setting('halaman_dpl')),
			'camat'=>intval($this->mfungsi->setting('halaman_camat')),
			'biro'=>intval($this->mfungsi->setting('halaman_biro')),
			'peserta'=>intval($this->mfungsi->setting('halaman_peserta')),
			'daftar'=>intval($this->mfungsi->setting('halaman_daftar')),
			'peserta_kelompok'=>intval($this->mfungsi->setting('hal_pstkelompok')),
			'tgl_skedul'=>$this->mfungsi->tglpicker($this->mfungsi->setting('tgl_skedul')),
			'auto_skedul'=>$this->mfungsi->setting('auto_skedul'),
			'uploadp'=>$this->mfungsi->setting('halaman_uploadp'),
			'uploadd'=>$this->mfungsi->setting('halaman_uploadd')
		);
		$this->load->view('admin/pengaturan',$data);
		$this->load->view('admin/footer');
	}

	public function setModel($m=null){
		if($m!=null)
			$_POST['model'] = $m;
		$res = $this->mfile->copy_file('./application/controllers/admin/metode/Metode_'.$_POST['model'].'.php','./application/controllers/admin/metode/Gen.php');
		if($res==true){
			if($_POST['model']=='c'){
				$this->pengaturanModel->minimal_prodi($_POST['minimal']);	
			}
			$this->pengaturanModel->model_pembentukan($_POST['model']);
		}
	}

	public function update(){
		if(isset($_POST['editbiodata']))
			$this->pengaturanModel->edit_biodata(1);
		else
			$this->pengaturanModel->edit_biodata(0);

		if(isset($_POST['halaman_dpl']))
			$this->pengaturanModel->edit_halaman_dpl(1);
		else
			$this->pengaturanModel->edit_halaman_dpl(0);

		if(isset($_POST['halaman_camat']))
			$this->pengaturanModel->edit_halaman_camat(1);
		else
			$this->pengaturanModel->edit_halaman_camat(0);

		if(isset($_POST['halaman_biro']))
			$this->pengaturanModel->edit_halaman_biro(1);
		else
			$this->pengaturanModel->edit_halaman_biro(0);

		if(isset($_POST['halaman_peserta']))
			$this->pengaturanModel->edit_halaman_peserta(1);
		else
			$this->pengaturanModel->edit_halaman_peserta(0);

		if(isset($_POST['halaman_daftar']))
			$this->pengaturanModel->edit_halaman_daftar(1);
		else
			$this->pengaturanModel->edit_halaman_daftar(0);

		if(isset($_POST['halaman_peserta_kelompok']))
			$this->pengaturanModel->edit_halaman_peserta_kelompok(1);
		else
			$this->pengaturanModel->edit_halaman_peserta_kelompok(0);

		if(isset($_POST['halaman_uploadp']))
			$this->pengaturanModel->edit_halaman_uploadp(1);
		else
			$this->pengaturanModel->edit_halaman_uploadp(0);

		if(isset($_POST['halaman_uploadd']))
			$this->pengaturanModel->edit_halaman_uploadd(1);
		else
			$this->pengaturanModel->edit_halaman_uploadd(0);

		if(isset($_POST['auto_skedul']))
			$this->pengaturanModel->auto_skedul(1);
		else
			$this->pengaturanModel->auto_skedul(0);

		$this->pengaturanModel->api_key($_POST['apikey']);
		$this->pengaturanModel->pimpinan_lppm($_POST['pimpinan']);
		$this->pengaturanModel->nomor_sertifikat($_POST['nosertifikat']);
		$this->pengaturanModel->filter_kab($_POST['kabupaten']);
		$this->pengaturanModel->tgl_skedul($this->mfungsi->tglsql($_POST['tgl_skedul']));
		$array = array(
			'api_key_distance' => $_POST['apikey']
		);
		$this->session->set_userdata( $array );
		redirect('admin/pengaturan');
	}

	public function cek(){
		echo json_encode($this->mfungsi->cekLayananGoogle($_POST['api'],$_POST['alamat']));
	}

	//backup
	public function backup(){
		$this->load->model('admin/backupModel');
		$this->backupModel->db();
	}

	public function restore($mode = null){
		$this->load->model('admin/backupModel');
		if($mode=='upload'){
			if($this->session->pass_update==md5($_GET['gen'])){
				$cek = $this->mio->upload(array(
					'format'=>'zip',
					'path'=>'./upload',
					'filename'=>'backupData.zip',
					'overwrite'=>true,
					'name'=>'filesql',
					'size'=>10000000
				));
				echo json_encode($cek);
			}
		}else{
			$this->mremaining->start();
			$this->mremaining->running(array('proses'=>0,'total'=>100));
			$bck = './upload/backupData.zip';
			$this->load->library('extractor');
      $this->extractor->extract($bck, FCPATH.'/upload/backup/');
      $this->backupModel->restoredb('./upload/backup/simkkn_backup.sql');
      $this->mremaining->finish();
		}
	}

	public function renew(){
		if($this->session->pass_update==md5($_GET['gen'])){
			$tabel = array('kapasitas_prodi','kelompok_peserta','kelompok','dosen','tahun','upload','mahasiswa','prodi','mahasiswa_mendaftar','pengaturan','no_ordik','notifikasi','lembar_monitoring');
			foreach ($tabel as $key => $value) {
				$this->db->empty_table($value);
			}
			$this->mfile->empty_dir('./dokumen');
			$this->mfile->empty_dir('./monitoring');
			$this->mfile->empty_dir('./jarak');
			$this->mfile->empty_dir('./assets/images/peserta');
			$this->mfile->empty_dir('./assets/images/dosen');
			$this->mfile->empty_dir('./upload');
			$this->pengaturanModel->baru();
			$this->setModel('a');
			redirect('admin/pengaturan','refresh');
		}
	}

	public function cekpass(){
		if($this->session->pass==md5($_POST['pass'])){
			if(!isset($this->session->pass_update)){
				$this->session->pass_update = md5($this->session->pass);
			}
			echo json_encode($this->session->pass_update);
			$this->session->pass_update = md5($this->session->pass_update);
		}
		else{
			echo json_encode(false);
		}
	}

	public function ganti_ttd(){
		$dt = $this->mio->upload(array(
			'path'=>'./assets/images',
			'format'=>'png',
			'name'=>'fotottd',
			'filename'=>'signature.png',
			'size'=>500,
			'overwrite'=>true
		));
		$dt['foto'] = base_url('assets/images/signature.png?').rand(111,999);
		echo json_encode($dt);
	}

}

/* End of file pengaturan.php */
/* Location: ./application/controllers/admin/pengaturan.php */