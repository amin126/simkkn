<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biodata extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('mhs/statusModel');
		if($this->session->mhs_baru_daftar!=true)
			$this->statusModel->cetStatus();
		$this->load->model('mhs/biodataModel');
	}
	public function index()
	{
		$this->data();
	}

	public function alamat(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		echo json_encode($csv);
	}

	public function alamat2(){
		$this->load->model('admin/wilayahModel');
		$csv = $this->wilayahModel->find($_POST['alamat'],'google');
		$this->alamattext = $csv;
		if($csv->status==true){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function sksMax(){
		if(intval($_POST['sks'])>100){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function data(){
		$data = $this->biodataModel->mahasiswa();
		if($this->session->enable_edit_biodata==true){
			$data[0]->KORDXMHSD = $data[0]->KORDXMHS;
			$data[0]->KORDYMHSD = $data[0]->KORDYMHS;
		}
		$this->load->view('mhs/header');
		$this->load->view('mhs/biodata',array(
			'alamat'=>$data[0]->ALAMATMHS,
			'telp'=>$data[0]->KONTAKMHS,
			'kerja'=>$data[0]->PEKERJAANMHS,
			'sks'=>$data[0]->SKSMHS,
			'kordx'=>$data[0]->KORDXMHSD,
			'kordy'=>$data[0]->KORDYMHSD,
			'seragam'=>$data[0]->SERAGAMMHS
		));
		$this->load->view('mhs/footer');
	}

	public function update(){
		$this->form_validation->set_rules('sks', 'sks', 'required|numeric|callback_sksMax',array('sksMax'=>'Sks yang ditempuh kurang dari target'));
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('seragam', 'seragam', 'required');
		$this->form_validation->set_rules('kordx', 'lat', 'required',array('required'=>'Lokasi atau alamat tidak benar'));
		$this->form_validation->set_rules('kordy', 'lng', 'required',array('required'=>'Lokasi atau alamat tidak benar'));
		$this->form_validation->set_rules('alamat', 'alamat', 'required');

		if ($this->form_validation->run() == FALSE)
        {
        	$this->load->view('mhs/header');
          $this->load->view('mhs/biodata',array(
    				'alamat'=>set_value('alamat'),
    				'telp'=>set_value('telp'),
    				'sks'=>set_value('sks'),
    				'seragam'=>set_value('seragam'),
    				'kerja'=>set_value('kerja'),
    				'kordx'=>set_value('kordx'),
    				'kordy'=>set_value('kordy')
    			));
    			$this->load->view('mhs/footer');
        }
        else
        {
        	$this->session->set_userdata('biodata_update',true);
         	$this->biodataModel->update($_POST);
          redirect('mhs/biodata','refresh');
        }
	}

	public function view(){
		$this->load->view('mhs/biodata-view',array(
			'bio'=>$this->biodataModel->mahasiswa()
		));
	}

}

/* End of file biodata.php */
/* Location: ./application/controllers/mhs/biodata.php */