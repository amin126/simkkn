<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelompok extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
		$this->load->model('mhs/v/profilModel');
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->profilModel->get();
		$agt = $this->profilModel->anggota($data->KDKEL);
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/kelompok',array(
			'data'=>$data,
			'anggota'=>$agt
		));
		$this->load->view('mhs/v/footer');
	}

	public function dosen(){
		$dsn = $this->profilModel->dosen()[0];
		if($dsn->FOTODPL!=NULL){
			if (strpos($dsn->FOTODPL, 'unira') !== false) {
			    $foto = $dsn->FOTODPL;
			}else{
				$foto = base_url($dsn->FOTODPL);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?filter[nis]='.$dsn->KDDPL));
			if(isset($dmhs->errors) || !isset($dmhs->data))
				$foto = base_url('assets/images/faces/user.png');
			else
				if($dmhs->data[0]->attributes->thumbnail==null)
					$foto = base_url('assets/images/faces/user.png');
				else
					$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$dsn->FOTODPL = $foto;
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/profil-dosen',array(
			'data'=>$dsn
		));
		$this->load->view('mhs/v/footer');
	}

	public function download($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$kode = $this->uploadModel->kodekel();
		$path1 = './dokumen';
		$path2 = $kode[0]->KDKEL;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function biodata($npm){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->profilModel->biodata($npm);
		$this->load->view('mhs/v/header');
		if($data->FOTOMHS!=NULL){
			if (strpos($data->FOTOMHS, 'unira') !== false) {
			    $foto = $data->FOTOMHS;
			}else{
				$foto = base_url($data->FOTOMHS);
			}
		}else{
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?filter[nim]='.$data->NPM));
			$foto = 'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail;
		}
		$this->load->view('mhs/v/profil',array(
			'alamat'=>$data->ALAMATMHS,
			'telp'=>$data->KONTAKMHS,
			'kerja'=>$data->PEKERJAANMHS,
			'sks'=>$data->SKSMHS,
			'seragam'=>$data->SERAGAMMHS,
			'nama'=>$data->NAMAMHS,
			'prodi'=>$data->FAKPRODI.' / '.$data->NAMAPRODI,
			'npm'=>$data->NPM,
			'foto'=>$foto
		));
		$this->load->view('mhs/v/footer');
	}

}

/* End of file kelompok.php */
/* Location: ./application/controllers/mhs/ver/kelompok.php */