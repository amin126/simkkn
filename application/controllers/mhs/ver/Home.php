<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/home');
		$this->load->view('mhs/v/footer');
	}

}

/* End of file mode.php */
/* Location: ./application/controllers/mhs/verified/mode.php */