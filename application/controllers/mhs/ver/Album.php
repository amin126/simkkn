<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_peserta'));
		if($cek==1){
			$this->kunci = true;
		}
		if($this->session->verified==false)
			redirect(base_url('keluar'));
		$this->periode = $this->mfungsi->periodeMHS($this->session->usern);
		$this->load->model('mhs/v/galleryModel');
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$cari =  (isset($_POST['cari'])) ? $_POST['cari'] : '';
		$data = $this->galleryModel->view();
		$this->load->view('mhs/v/header');
		$this->load->view('mhs/v/gallery',array(
			'cari'=>$cari,
			'data'=>$data,
			'periode'=>$this->periode
		));
		$this->load->view('mhs/v/footer');
	}

	public function deskripsi(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->galleryModel->deskripsi($_POST);
	}

	public function proses(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		if($this->periode==true){
			if(isset($_FILES['file'])){
				$kode = $this->galleryModel->kodekel();
				$path1 = 'gallery';
				$path2 = $kode[0]->KDKEL;
				$fullpath = "./$path1/$path2";
				if (!is_dir("$path1/$path2")) {
				    mkdir($fullpath, 0777, TRUE);
				}	
				$cek = $this->mio->upload(array(
					'name'=>'file',
					'path'=>$fullpath,
					'format'=>'jpg|png|gif',
					'size'=>50000,
					'openerr'=>'<p class="text-danger text-center"><i>',
					'closeerr'=>'</i></p>',
					'filename'=>$_FILES['file']['name']
				));
				$data = '';
				$status = false;
				if($cek['status']==false){
					$data = '<td colspan="5">'.$cek['data'].'</td>';
				}else{
					$id = $this->galleryModel->maxId();
					$tgl = $this->mfungsi->timeNow();
					$this->galleryModel->simpan(array(
						'file'=>$cek['data']['file_name'],
						'ukuran'=>$cek['data']['file_size'],
						'tgl'=>$tgl,
						'npm'=>$this->session->usern,
						'id'=>$id,
						'deskripsi'=>null
					));
					$status = true;
					$ext = explode('.', $cek['data']['file_name']);
	    		$ext = end($ext);
					$data = '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
		    			<td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('mhs/ver/upload/view/').$id.'" class="btn-link">'.$cek['data']['file_name'].'</a>&nbsp;.&nbsp;<a href="'.base_url('mhs/ver/upload/download/').$id.'">Download</a></td>
		    			<td><span title="'.$this->session->usern.'">'.$this->session->nama.'</span></td>
		    			<td>'.$cek['data']['file_size'].'Kb</td>
		    			<td><a href="'.base_url('mhs/ver/upload/delete/').$id.'" onclick="return hapus(this);" class="btn-link text-danger"><i class="fa fa-close"></i></a></td>
		    			</tr>';
				}
				echo json_encode(array(
					'data'=>$data,
					'status'=>$status,
					'id'=>$id
				));
			}else{
				echo json_encode(array(
					'data'=>'<td colspan="4"><p class="text-danger text-center"><i>Tida ada foto yang dipilih</i></p></td>',
					'status'=>false
				));
			}
		}else{
			redirect($this->mhistory->back());
		}
	}

}

/* End of file Gallery.php */
/* Location: ./application/controllers/mhs/ver/Gallery.php */