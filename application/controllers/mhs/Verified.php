<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verified extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$this->load->model('mhs/statusModel');
		if($this->session->mhs_baru_daftar!=true)
			$this->statusModel->cetStatus();
	}

	public function index()
	{
		$this->view();
	}

	public function view(){
		$this->load->view('mhs/header');
		$this->load->view('mhs/verifikasi');
		$this->load->view('mhs/footer');
	}
}

/* End of file verified.php */
/* Location: ./application/controllers/mhs/verified.php */