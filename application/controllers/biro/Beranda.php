<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->load->view('biro/header');
		$this->load->view('biro/beranda');
		$this->load->view('biro/footer');
	}

}

/* End of file beranda.php */
/* Location: ./application/controllers/biro/beranda.php */