<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/mahasiswaModel');
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->data();
	}

	public function data($p=1){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$npms = $this->mahasiswaModel->get();
		$cr = '';
		$flt = '&page='.$p;
		if(isset($_POST['cari'])){
			$flt = '&filter[nim]='.urlencode($_POST['cari']);
			$cr = $_POST['cari'];
		}
		$this->mhistory->go();
		$data = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/mahasiswa?limit=10'.$flt));
		if(!isset($data->data)){
			$this->load->view('biro/header');
			$this->load->view('biro/mahasiswa',array(
				'data'=>array(),
				'cari'=>$cr,
				'links'=>array(),
				'npms'=>array(),
				'page'=>array(
					'self'=>0,
					'awal'=>0,
					'akhir'=>0,
					'first'=>0,
					'last'=>0
				)
			));
			$this->load->view('biro/footer');
		}else{
			$mhs = $data->data;
			$links = $data->links;
			$a = explode('=', $links->first);
			$awal = intval(end($a));
			$a = explode('=',$links->last);
			$akhir = intval(end($a));
			$a = explode('=', $links->self);
			$page = intval(end($a));
			if($page<7){
				$awal1 = 1;
				$akhir1 = 11;
			}else if($page>$akhir-7){
				$awal1 = $akhir-11;
				$akhir1 = $akhir;
			}else{
				$awal1 = $page-5;
				$akhir1 = $page+5;
			}
			$this->load->view('biro/header');
			$this->load->view('biro/mahasiswa',array(
				'data'=>$mhs,
				'cari'=>$cr,
				'links'=>$links,
				'npms'=>$npms,
				'page'=>array(
					'self'=>$page,
					'awal'=>$awal1,
					'akhir'=>$akhir1,
					'first'=>$akhir,
					'last'=>$awal
				)
			));
			$this->load->view('biro/footer');
		}
	}

	public function set(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		if($_POST['tipe']==1)
			$this->mahasiswaModel->tambah($_POST);
		else
			$this->mahasiswaModel->hapus($_POST['npm']);
	}

	public function cetak(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data = $this->nilaiModel->get($this->kknaktif);
		$this->load->view('biro/print/header');
		$this->load->view('biro/print/nilai',array('data'=>$data));
		$this->load->view('biro/print/footer');
	}

}

/* End of file nilai.php */
/* Location: ./application/controllers/biro/nilai.php */