<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/profilModel');
		$this->load->model('biro/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
	}

	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->view();
	}

	public function view(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->profilModel->get();
		$agt = $this->profilModel->anggota();
		$this->load->view('biro/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('biro/profil',array(
			'data'=>$data,
			'anggota'=>$agt
		));
		$this->load->view('biro/footer');
	}
}

/* End of file profil.php */
/* Location: ./application/controllers/biro/profil.php */