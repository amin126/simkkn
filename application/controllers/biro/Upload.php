<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/kknModel');
		$this->kkn = $this->kknModel->get();
		$this->kknaktif = $this->kknModel->aktif();
		$this->load->model('biro/uploadModel');
	}
	public function index()
	{
		$this->find();
	}

	public function find(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->mhistory->go();
		$data = $this->uploadModel->view();
		$this->load->view('biro/header',array('kkn'=>$this->kkn,'kknaktif'=>$this->kknaktif));
		$this->load->view('biro/upload',array(
			'data'=>$data,
			'cari'=>$cari =  (isset($_POST['cari'])) ? $_POST['cari'] : ''
		));
		$this->load->view('biro/footer');
	}

	public function view($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$ext = explode('.', $path);
		if(end($ext)=='pdf'){
			$this->mpdf->view($path);
		}else{
			$this->mio->download($path);
		}
	}

	public function download($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$file = $this->uploadModel->file($kode);
		$path1 = './dokumen';
		$path2 = $this->kknaktif;
		$namafile = $file[0]->FILEUPL;
		$path = $path1.'/'.$path2.'/'.$namafile;
		$this->mio->download($path);
	}

	public function ajukan($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->uploadModel->ajukan($kode);
		$info = $this->uploadModel->infofile($kode);
		$id = $this->mfungsi->idTime();
		$this->mnotif->tambah(array(
			'id'=>$id,
			'keterangan'=>'Dosen '.$info->NAMADPL.' dari kelompok '.$info->NAMAKEL.' mengajukan file '.$info->FILEUPL,
			'url'=>base_url('admin/statistika/kinerja/detail/'.$info->KDKEL.'?notif='.$id.'#ajuan'),
			'tgl'=>$this->mfungsi->timeNow()
		));
		redirect($this->mhistory->back());
	}

	public function batalajukan($kode){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$info = $this->uploadModel->infofile($kode);
		$this->uploadModel->batalajukan($kode);
		$this->mnotif->batal('Dosen '.$info->NAMADPL.' dari kelompok '.$info->NAMAKEL.' mengajukan file '.$info->FILEUPL);
		redirect($this->mhistory->back());
	}

}

/* End of file upload.php */
/* Location: ./application/controllers/biro/upload.php */