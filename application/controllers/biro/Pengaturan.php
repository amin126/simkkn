<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/pengaturanModel');
		$this->pengaturanModel->setNew('biro_pesan','-');
		$this->pengaturanModel->setNew('biro_rektor','-');
		$this->pengaturanModel->setNew('biro_presma','-');
		$this->pengaturanModel->setNew('biro_panitia','-');
		$this->pengaturanModel->setNew('biro_login_mhs','1');
		$this->pengaturanModel->setNew('biro_nomor','%nomor%/A/Pan. Ordik/UNIRA/X/%tahun%');
	}

	public function index(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->data();
	}

	public function data()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data['pass'] = '';
		$data['rpass'] = '';
		$data['pesan'] = $this->mfungsi->setting('biro_pesan');
		$data['rektor'] = $this->mfungsi->setting('biro_rektor');
		$data['presma'] = $this->mfungsi->setting('biro_presma');
		$data['panitia'] = $this->mfungsi->setting('biro_panitia');
		$data['nomor'] = $this->mfungsi->setting('biro_nomor');
		$data['login'] = $this->mfungsi->setting('biro_login_mhs');

		$this->db->order_by('AKDODK, 	KDPELAKSANA');
		$data['pelaksana_ordik'] = $this->db->get('pelaksana_ordik')->result();
		$this->load->view('biro/header');
		$this->load->view('biro/pengaturan',$data);
		$this->load->view('biro/footer');
	}

	public function update(){
		$this->form_validation->set_rules('pesan', 's', 'trim|required',array(
			'required'=>'Pesan singkat harus diisi',
		));
		$this->form_validation->set_rules('rektor', 's', 'trim|required',array(
			'required'=>'Form rektor harus diisi',
		));
		$this->form_validation->set_rules('presma', 's', 'trim|required',array(
			'required'=>'Form presma harus diisi',
		));
		$this->form_validation->set_rules('panitia', 's', 'trim|required',array(
			'required'=>'Form panitia harus diisi',
		));
		$this->form_validation->set_rules('nomor', 's', 'trim|required',array(
			'required'=>'Form pola nomor harus diisi',
		));
		if($_POST['pass']!=''){
			$this->form_validation->set_rules('pass', 'ini', 'trim|required');
			$this->form_validation->set_rules('rpass', 'ini', 'trim|required|matches[pass]',array(
				'matches'=>'Ulangi password tidak sama'
			));
		}	

		if ($this->form_validation->run() == FALSE) {
			$data['pass'] = set_value('pass');
			$data['rpass'] = set_value('rpass');
			$data['pesan'] = set_value('pesan');
			$data['rektor'] = set_value('rektor');
			$data['presma'] = set_value('presma');
			$data['panitia'] = set_value('panitia');
			$data['nomor'] = set_value('nomor');
			$this->load->view('biro/header');
			$this->load->view('biro/pengaturan',$data);
			$this->load->view('biro/footer');
		} else {
			if($_POST['pass']!=''){
				$this->pengaturanModel->update_password($_POST['pass']);
			}
			if(isset($_POST['login']))
				$this->pengaturanModel->update_pengaturan('biro_login_mhs',1);
			else
				$this->pengaturanModel->update_pengaturan('biro_login_mhs',0);

			$this->pengaturanModel->update_pesan($_POST['pesan']);
			$this->pengaturanModel->update_pengaturan('biro_rektor',$_POST['rektor']);
			$this->pengaturanModel->update_pengaturan('biro_presma',$_POST['presma']);
			$this->pengaturanModel->update_pengaturan('biro_panitia',$_POST['panitia']);
			$this->pengaturanModel->update_pengaturan('biro_nomor',$_POST['nomor']);
			redirect('biro/pengaturan');
		}
		
	}

	public function ganti_ttd($user=null){
		$dt = $this->mio->upload(array(
			'path'=>'./assets/images',
			'format'=>'png',
			'name'=>$user,
			'filename'=>'signature-'.$user.'.png',
			'size'=>500,
			'overwrite'=>true
		));
		$dt['foto'] = base_url('assets/images/signature-'.$user.'.png?').rand(111,999);
		echo json_encode($dt);
	}

	public function hapuspelaksanaan(){
		$this->db->where('KDPELAKSANA',$_POST['kode']);
		$this->db->delete('pelaksana_ordik');
	}

	public function pelaksanaan(){
		if($_POST['pelaksana']==''){
			echo json_encode(array(
				'status'=>false,
				'data'=>'Tanggal pelaksanaan tidak diisi'
			));
		}else{
			$this->db->where('AKDODK',$_POST['akademik']);
			$cek = $this->db->get('pelaksana_ordik')->num_rows();
			if($cek>0){
				echo json_encode(array(
					'status'=>false,
					'data'=>'Tahun akademik sudah ada'
				));
			}else{
				$this->db->insert('pelaksana_ordik',array(
					'AKDODK'=>$_POST['akademik'],
					'MSGODK'=>$_POST['pelaksana']
				));

				$this->db->select_max('KDPELAKSANA');
				$cek = $this->db->get('pelaksana_ordik')->result_array();
				$id = $cek[0]['KDPELAKSANA'];

				echo json_encode(array(
					'status'=>true,
					'kode'=>$id,
					'pesan'=>$_POST['pelaksana'],
					'akademik'=>$_POST['akademik']
				));
			}
		}
	}

	public function updatepelaksanaandefault(){
		$this->db->set('MSGODK',$_POST['pesan']);
		$this->db->where('AKDODK','default');
		$this->db->update('pelaksana_ordik');
	}

	public function singkron(){
		set_time_limit(0);
		$url = 'https://api.unira.ac.id/v1/mahasiswa?limit=50000000';
		$data = $this->mfungsi->file_get($url);

		$this->mfile->write(array(
			'file'=>'./mahasiswa_api_unira_all.json',
			'data'=>$data
		));
	}

}

/* End of file setkkn.php */
/* Location: ./application/controllers/biro/setkkn.php */