<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nonordik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
		$cek = intval($this->mfungsi->setting('halaman_biro'));
		if($cek==1){
			$this->kunci = true;
		}
		$this->load->model('biro/nonordikModel');
	}
	public function index()
	{
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$this->data();
	}

	public function data(){
		if(isset($this->kunci)){
			$this->load->view('locked2');	
			return true;
		}
		$data['data'] = $this->nonordikModel->get()->result();
		$this->load->view('biro/header');
		$this->load->view('biro/nonordik',$data);
		$this->load->view('biro/footer');
	}

	public function batal(){
		$this->nonordikModel->hapus($_POST['kode']);
	}

}

/* End of file nilai.php */
/* Location: ./application/controllers/biro/nilai.php */