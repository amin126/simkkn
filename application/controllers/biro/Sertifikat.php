<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->mlogin->cek();
	}

	public function topdf(){
		$array = array(
				'sertifikat_data' => $_POST['img'],
				'sertifikat_npm' =>  $_POST['npm']
			);
			
			$this->session->set_userdata( $array );
	}

	public function pdf(){
		$this->load->library('pdf');
    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->filename = $this->session->sertifikat_npm.".pdf";
    $this->pdf->load_view('sertifikat');
	}

	public function index()
	{
		$this->peserta();
	}

	public function peserta(){
		$this->load->view('biro/header');
		$this->load->view('biro/sertifikat');
		$this->load->view('biro/footer');
	}

	public function create(){
		set_time_limit(0);
		$this->load->model('sertifikatBiro');
		$mhs = $this->sertifikatBiro->mahasiswa_api($_POST['npm']);
		if($mhs->status==false){
			echo json_encode(array('status'=>false,'data'=>$mhs->pesan));
			exit;
		}
		$key = 'ORDIKUNEWA';
		$this->load->model('encyDesc');
		$data = array();
		$data['dibuat'] = $this->mfungsi->tgl(date('Y-m-d H:i:s'),true);
		$data['angkatan'] = $this->sertifikatBiro->angkatan($mhs->data->id);
		$data['prodi'] = $this->sertifikatBiro->prodi($mhs->data->id);
		$data['prodi_nama'] = $this->sertifikatBiro->prodi_nama($data['prodi']);
		$a = $this->encyDesc->enc($mhs->data->id);
		$data['qrcode'] = $this->encyDesc->encrypt($key,$a);
		$b = $this->sertifikatBiro->ambilalfabeta($mhs->data->attributes->nama);
		$data['security_code'] = $this->encyDesc->encrypt($b,$data['qrcode']);
		$all = $this->sertifikatBiro->mahasiswa_api_all();
		$nomor = 1;
		foreach ($all as $key => $value) {
			if($data['angkatan'] == $this->sertifikatBiro->angkatan($value->id)){
				if($value->id==$mhs->data->id){
					break;
				}
				$nomor++;
			}
		}
		$pola = $this->mfungsi->setting('biro_nomor');
		$data['nomor'] = 'No. '.str_replace(array('%nomor%','%tahun%'), array($nomor,$data['angkatan']), $pola);
		$data['npm'] = $mhs->data->id;
		$data['nama'] = $mhs->data->attributes->nama;
		$data['akademik'] = $data['angkatan'].'/'.($data['angkatan']+1);
		$x = explode('|', $this->mfungsi->setting('biro_rektor'));
		$data['rektorLabel'] = $x[0];
		$data['rektor'] = (isset($x[1]))?$x[1]:'';
		$data['brektor'] = (isset($x[2]))?$x[2]:'';
		$x = explode('|', $this->mfungsi->setting('biro_presma'));
		$data['presmaLabel'] = $x[0];
		$data['presma'] = (isset($x[1]))?$x[1]:'';
		$data['bpresma'] = (isset($x[2]))?$x[2]:'';
		$x = explode('|', $this->mfungsi->setting('biro_panitia'));
		$data['panitiaLabel'] = $x[0];
		$data['panitia'] = (isset($x[1]))?$x[1]:'';
		$data['bpanitia'] = (isset($x[2]))?$x[2]:'';

		$this->db->where('AKDODK',$data['akademik']);
		$plksn = $this->db->get('pelaksana_ordik');
		if($plksn->num_rows()>0){
			$v4134 = $plksn->result();
			$data['pelaksanaan'] = 'Sebagai Peserta Pengenalan Kehidupan Kampus Mahasiswa Baru<br> Universitas Madura Tahun Akademik '.$data['akademik'].'<br>yang dilaksanakan pada tanggal '.$v4134[0]->MSGODK.' di kampus Universitas Madura';
		}else{
			$this->db->where('AKDODK','default');
			$plksn = $this->db->get('pelaksana_ordik')->result();
			$data['pelaksanaan'] = $plksn[0]->MSGODK;
		}
		
		$this->mfile->write(array(
			'file'=>'./biro_sertifikat/'.$mhs->data->id.'.json',
			'data'=>json_encode($data,JSON_PRETTY_PRINT)
		));
		echo json_encode(array('status'=>true,'data'=>$data));
	}

	public function cek(){
		$this->load->view('biro/header');
		$this->load->view('biro/sertifikat-cek');
		$this->load->view('biro/footer');
	}

	public function periksa(){
		$this->load->model('sertifikatBiro');
		$key = 'ORDIKUNEWA';
		$this->load->model('encyDesc');
		$a = $this->encyDesc->decrypt($key,$_POST['npm']);
		$npm = $this->encyDesc->dec($a);
		
		$mhs = $this->sertifikatBiro->mahasiswa_api($npm);
		if($mhs->status==false){
			echo json_encode(array('status'=>false,'data'=>$mhs->pesan));
			exit;
		}
		$this->load->model('encyDesc');
		$data = array();
		$data['angkatan'] = $this->sertifikatBiro->angkatan($mhs->data->id);
		$data['prodi'] = $this->sertifikatBiro->prodi($mhs->data->id);
		$data['prodi_nama'] = $this->sertifikatBiro->prodi_nama($data['prodi']);
		$a = $this->encyDesc->enc($mhs->data->id);
		$data['qrcode'] = $this->encyDesc->encrypt($key,$a);
		$b = $this->sertifikatBiro->ambilalfabeta($mhs->data->attributes->nama);
		$data['security_code'] = $this->encyDesc->encrypt($b,$data['qrcode']);
		

		$data['npm'] = $mhs->data->id;
		$data['nama'] = $mhs->data->attributes->nama;
		echo json_encode(array('status'=>true,'data'=>$data));
	}

	public function cetak($npm=''){
		$data = json_decode($this->mfile->read('biro_sertifikat/'.$npm.'.json'));
		$this->load->view('biro/sertifikat-cetak',array('data'=>$data));
	}

}

/* End of file sertifikat.php */
/* Location: ./application/controllers/admin/sertifikat.php */