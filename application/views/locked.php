<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
  <style type="text/css">
    .a {
        display: table;
        width: 100%;
        position: absolute;
        height: 100%;
        padding: 30px;
    }
    .b {
        display: table-cell;
        vertical-align: middle;
    }
    .icn {
        font-size: 6pc;
    }
  </style>
</head>

<body class="bg-warning">
<div class="a text-center">
  <div class="b">
    <div class="mb-3 text-danger">
      <i class="fa icn fa-lock"></i>
    </div>
    <div class="font-weight-bold text-white h1 mb-2">
      HALAMAN TERKUNCI
    </div>
    <div class="text-danger">
      Komputer ini telah mencoba masuk ke sistem, kami memberikan batas maksimal 5x login gagal. hal ini mencegah bahwa bukanlah sebuah robot yang mencoba masuk ke sistem, silahkan login kembali nanti
    </div>
    <div class="font-weight-bold h2 mt-3 text-info">
      Tunggu <span id="detik"><?php echo $detik; ?></span> detik
    </div>
  </div>
</div>
<script type="text/javascript">
  var detik = <?php echo $detik; ?>;
  function ubahdetik(){
    document.getElementById("detik").innerHTML = detik;
    detik--;
    if(detik<0){
      window.location.href="<?php echo base_url('login'); ?>";
    }else{
      setTimeout(ubahdetik, 1000);
    }
  }
  setTimeout(ubahdetik, 1000);
</script>
</body>
</html>