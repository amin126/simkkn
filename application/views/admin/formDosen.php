<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-md-5">
                  <h4 class="card-title">
                  	<?php
                  	if($tipe=='tambah')
                  		echo 'Tambah Data Dosen Baru';
                  	else if($tipe=='edit')
                  		echo 'Edit Data Dosen';
                  	?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                  	if($tipe=='tambah')
                  		$act = base_url('admin/pengguna/dosen/simpan');
                  	else if($tipe=='edit')
                  		$act = base_url('admin/pengguna/dosen/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                    <div class="form-group">
                      <label for="usern">* Username <?php echo form_error('username','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="usern" value="<?php echo $username; ?>"  placeholder="Username" name="username">
                    </div>

                    <div class="form-group">
                      <label for="nama">* Nama Lengkap <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="nama" value="<?php echo $nama; ?>"  placeholder="Nama Lengkap" name="nama">
                    </div>

                    <div class="form-group">
                      <label for="kontak">* Kontak <?php echo form_error('kontak','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="kontak" value="<?php echo $kontak; ?>"  placeholder="Kontak" name="kontak">
                    </div>

                    <div class="form-group" id="groupalamat">
                      <label for="alamat">* Alamat <?php echo form_error('alamat','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="alamat" value="<?php echo $alamat; ?>"  placeholder="Alamat" name="alamat">
                    </div>

                   <!--  <div class="form-group row justify-content-end">
                      <button type="button" onclick="validasialamat();" class="mr-3 btn pull btn-sm btn-primary">Validasi alamat!</button>
                    </div> -->
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('admin/pengguna/mahasiswa/addresscheck'); ?>',
      data:{
        alamat:alm
      },
      loading:'#groupalamat',
      append:true,
      success:function(res){
        res = JSON.parse(res);
        if(res.result.text==null){
          _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
        }else{
          $('#alamat').val(res.result.text);  
        }        
      }
    })
  }
</script>