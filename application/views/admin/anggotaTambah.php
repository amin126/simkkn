<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
	  	<div class="card-body">
	  		<div class="row">
		  		<div class=" col-md-4">
		  			<div class="alert alert-info" role="alert">
						  Masukkan NPM / Nama / Alamat mahasiswa yang akan ditambahkan ke kelompok kkn, mahasiswa yang akan ditambahkan harus mendaftar dan berstatus belum diverifikasi.
						</div>
						<form class="form-inline row" action="<?php echo base_url('admin/anggota/tambah');  ?>" method="post">
						  <div class="form-group col-md-8">
						    <input autocomplete="off"  type="text" class="form-control" id="npm" name="npm" value="<?php echo $cari; ?>" placeholder="Cari...">
						  </div>
						  <div class="form-group col-md-4">
						  <button type="submit" class="btn btn-outline-primary">Ok</button>
						</div>
						</form>
						<?php
		  			if($status=='b'){
		  				?>
						<div class="card">
						  <div class="card-body">
						    <h4 class="card-title"><?php echo $mahasiswa[0]->NAMAMHS;  ?></h4>
						    <h6 class="card-subtitle mb-2 text-muted"><?php echo $mahasiswa[0]->NPM;  ?></h6>
						    <p class="card-text"><b>Fak / Prodi</b><br><?php echo $mahasiswa[0]->FAKPRODI.' / '.$mahasiswa[0]->NAMAPRODI;  ?></p>
						    <p class="card-text"><b>Alamat</b><br><?php echo $mahasiswa[0]->ALAMATMHS;  ?></p>
						  </div>
						</div>
						<?php
						}
						?>
		  		</div>
		  		<div class=" col-md-8">
		  			<?php
		  			if($status=='b'){
		  				?>
			  			<div class="alert alert-info" role="alert">
							  Daftar KKN yang kapasitasnya masih bisa ditempati
							</div>
		  				<?php
		  				echo '<div class="row">';
		  				foreach ($kkn as $key => $value) {
		  					?>
		  					<div class="col-md-6">
		  						<div class="card" style="border: 1;">
									  <div class="card-body">
									    <h4 class="card-title"><?php echo $value->NAMAKEL; ?></h4>
									    <h6 class="card-subtitle mb-2 text-muted"><?php echo $value->NAMADPL; ?></h6>
									    <p class="card-text"><?php echo $value->ALAMATKEL; ?></p>
									    <form action="<?php echo base_url('admin/anggota/prosestambah'); ?>" method="post" accept-charset="utf-8">
									    	<input autocomplete="off"  type="hidden" name="npm" value="<?php echo $npm; ?>">
									    	<input autocomplete="off"  type="hidden" name="kelompok" value="<?php echo $value->KDKEL; ?>">
									    	<button type="submit" class="btn btn-outline-primary btn-sm">Masuk</button>
									    </form>
									  </div>
									</div>	
		  					</div>
		  					<?php
		  				}
		  				echo '</div>';
		  			}else if($status=='c'){
		  				console($status);
		  				echo '<div class="alert alert-danger" role="alert">Mahasiswa tidak ada!</div>';
		  			}
		  			?>
		  		</div>
		  	</div>
	  	</div>
	  </div>
	</div>
</div>