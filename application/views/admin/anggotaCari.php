<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
	  	<div class="card-body">
	  		<div class="row">
		  		<div class=" col-md-4">
		  			<div class="alert alert-info" role="alert">
						  Pencarian dapat dilakukan berdasarkan NPM / Nama / Alamat.
						</div>
						<form class="form-inline row" action="<?php echo base_url('admin/anggota/cari');  ?>" method="post">
						  <div class="form-group col-md-8">
						    <input autocomplete="off"  type="text" class="form-control" id="npm" name="npm" value="<?php echo $cari; ?>" placeholder="Cari...">
						  </div>
						  <div class="form-group col-md-4">
						  <button type="submit" class="btn btn-outline-primary">Ok</button>
						</div>
						</form>
						<?php
		  			if($status=='b'){
		  				?>
						<div class="card">
						  <div class="card-body">
						  	<img src="<?php echo $foto.'?'.rand(111,999); ?>" alt="NO FOTO" class="img-thumbnail w-100 mb-3">
						    <h4 class="card-title"><?php echo $mahasiswa[0]->NAMAMHS;  ?></h4>
						    <h6 class="card-subtitle mb-2 text-muted"><?php echo $mahasiswa[0]->NPM;  ?></h6>
						    <p class="card-text"><b>Fak / Prodi</b><br><?php echo $mahasiswa[0]->FAKPRODI.' / '.$mahasiswa[0]->NAMAPRODI;  ?></p>
						    <p class="card-text"><b>Alamat</b><br><?php echo $mahasiswa[0]->ALAMATMHS;  ?></p>
						    <p class="card-text"><b>Kontak</b><br><?php echo $mahasiswa[0]->KONTAKMHS;  ?></p>
						  </div>
						</div>
						<?php
						}
						?>
		  		</div>
		  		<div class=" col-md-8">
		  			<?php
		  			if($status=='b'){
		  			?>


<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<div class="alert alert-info">
	  			RINCIAN PESERTA KKN "<?php echo  $mahasiswa[0]->NAMAMHS; ?>"
	  		</div>
	  		<div class="row">
			    <div class="col col-12">
			    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('admin/kelompok/cetak/'.$kelompok[0]->KDKEL); ?>"><i class="fa fa-print"></i>Cetak</a>
			    </div>
	  			<div class="col-md-12 font-weight-bold">
	  				Nama Kelompok KKN
	  			</div>
	  			<div class="col-md-12">
	  				<?php echo $kelompok[0]->NAMAKEL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  			<div class="col-md-12 font-weight-bold">
	  				Alamat Kelompok KKN
	  			</div>
	  			<div class="col-md-12">
	  				<?php echo $kelompok[0]->ALAMATKEL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  			<div class="col-md-12 font-weight-bold">
	  				Dosen Pendamping Lapangan
	  			</div>
	  			<div class="col-md-12">
	  				<?php echo $kelompok[0]->NAMADPL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  		</div>
	  		<div class="row">
	  			<div class="col">
	  				<table class="table datatable table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Npm</th>
									<th>Nama</th>
									<th>Fak / Prodi</th>
									<th>Alamat</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 1;
									foreach ($anggota as $key => $value) {
										$bg = ($npm==$value->NPM) ? 'bg-primary text-white' : '';
										?>
										<tr class="<?php echo $bg; ?>">
											<td><?php echo $i++; ?></td>
											<td><?php echo $value->NPM; ?></td>
											<td><?php echo $value->NAMAMHS; ?></td>
											<td><?php echo $value->NAMAPRODI; ?></td>
											<td><?php echo $value->ALAMATMHS; ?></td>
										</tr>
										<?php
									}
								?>
							</tbody>
						</table>	  				
	  			</div>	  			
	  		</div>
	  	</div>
	  </div>
	</div>
</div>



		  			<?php
		  			}else if($status=='c'){
		  				console($status);
		  				echo '<div class="alert alert-danger" role="alert">Anggota tidak ditemukan!</div>';
		  			}
		  			?>
		  		</div>
		  	</div>
	  	</div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({paging:false,scrollX:true,info:false});
	});
</script>