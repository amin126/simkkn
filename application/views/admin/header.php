<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/admin.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mod.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>"><span style="
    color: #f6f8fa;
">SIM</span><span style="
    color:  red;
    font-weight:  bold;
">KKN</span></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'); ?>images/logobw.png" style="width: 40px;height: 40px;" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex" style="width: 70%;">
          <li class="nav-item">
            <?php
              $menu = $this->uri->segment(2);
              $submenu = $this->uri->segment(3);
              switch ($menu) {
                case 'master':
                  if($submenu=='tahun')
                    echo 'TAHUN AKADEMIK KKN';
                  else if($submenu=='kabupaten')
                    echo 'DATA KABUPATEN';
                  else if($submenu=='kecamatan')
                    echo 'DATA KECAMATAN';
                  else if($submenu=='desa')
                    echo 'DATA DESA';
                  else if($submenu=='prodi')
                    echo 'DATA PRODI';
                  else
                    echo profil('aplikasi');
                break;
                case 'statistika':
                  if($submenu=='kinerja')
                    echo 'KINERJA KELOMPOK KKN';
                  else if($submenu=='pendaftar')
                    echo 'PENDAFTAR ANGGOTA KKN ';
                  else if($submenu=='nilai')
                    echo 'NILAI PESERTA ';
                  else if($submenu=='map')
                    echo 'PETA MONITORING LOKASI DAN JARAK KKN';
                  else
                    echo profil('aplikasi');
                break;
                case 'pengguna':
                  if($submenu=='admins')
                    echo 'DATA ADMINISTRATOR';
                  else if($submenu=='dosen')
                    echo 'DATA DOSEN';
                  else if($submenu=='mahasiswa')
                    echo 'DATA MAHASISWA';
                  else if($submenu=='camat')
                    echo 'DATA CAMAT';
                  else
                    echo profil('aplikasi');
                break;
                case 'kelompok':
                  echo 'KELOMPOK KKN TAHUN AKADEMIK '.$this->mfungsi->tahun()->label;
                break;
                case 'notif':
                  echo 'NOTIFIKASI SIM-KKN';
                break;
                case 'nilai':
                  echo 'EDIT NILAI PESERTA';
                break;
                case 'anggota':
                  if($submenu=='tambah')
                    echo 'TAMBAH ANGGOTA KKN';
                  else if($submenu=='pindah')
                    echo 'PINDAH ANGGOTA KKN';
                  else if($submenu=='tukar')
                    echo 'TUKAR ANGGOTA KKN';
                  else if($submenu=='cari')
                    echo 'CARI ANGGOTA KKN';
                  else
                    echo profil('aplikasi');
                break;
                case 'statistika':
                  if($submenu=='detail')
                    echo 'STATIS KEMAJUAN KINERJA KELOMPOK KKN';
                  else
                    echo 'DATA STATISTIKA KKN TAHUN AKADEMIK '.$this->mfungsi->tahun()->label;
                break;
                case 'reg':
                  echo 'MAHASISWA YANG MENDAFTAR KKN';
                break;
                case 'anggota':
                  echo 'ANGGOTA KKN TAHUN AKADEMIK '.$this->mfungsi->tahun()->label;
                break;
                case 'pengaturan':
                  echo 'PENGATURAN SISTEM';
                break;
                case 'verified':
                  echo 'PLOTTING / PEMBENTUKAN KELOMPOK KKN';
                break;
                case 'sertifikat':
                  if($submenu=='peserta')
                    echo 'ELEKTRONIK SERTIFIKAT PERSERTA KKN';
                  else if($submenu=='dpl')
                    echo 'ELEKTRONIK SERTIFIKAT DOSEN PENDAMPING LAPANGAN KKN';
                  else
                    echo 'PERIKSA KODE SERTIFIKAT';
                  
                break;
                default:
                  echo profil('aplikasi');
                  break;
              }
            ?>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown" id="notifikasi">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell-ring"></i>
              <span class="jmerah"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown" style="max-height: 300px;overflow:  hidden;overflow-y:  auto;">
              <a class="dropdown-item" href="<?php echo base_url('admin/notif/all');  ?>">
                <p class="mb-0 font-weight-normal float-left msgjumlah">
                </p>
                <span class="badge badge-pill badge-warning float-right">Lihat Semua</span>
              </a>
              <div class="dropdown-divider"></div>
              <div class="list-notifikasi"></div>
              
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img class="img-xs rounded-circle" src="<?php echo base_url('assets/'); ?>images/faces/user.png" alt="">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <a class="dropdown-item preview-item" href="<?php echo base_url('admin/pengaturan'); ?>">
                Pengaturan
              </a>
              <a class="dropdown-item preview-item" href="<?php echo base_url('keluar'); ?>">
                Keluar
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="fa fa-bars"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">

          <li class="nav-item <?php echo ($menu=='statistika' || $menu=='nilai') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-statistika" aria-expanded="false" aria-controls="menu-master"> <i class="menu-icon fa fa-bar-chart"></i> <span class="menu-title">Statistika KKN</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='statistika' || $menu=='nilai') ? 'show' : ''; ?>" id="menu-statistika">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='pendaftar') ? 'active' : ''; ?>" href="<?php echo base_url('admin/statistika/pendaftar'); ?>">Tabel KKN</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='map') ? 'active' : ''; ?>" href="<?php echo base_url('admin/statistika/map'); ?>">Peta KKN</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='kinerja') ? 'active' : ''; ?>" href="<?php echo base_url('admin/statistika/kinerja'); ?>">Kinerja KKN</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='nilai' || $menu=='nilai') ? 'active' : ''; ?>" href="<?php echo base_url('admin/statistika/nilai'); ?>">Nilai Peserta</a></li>
              </ul>
            </div>
          </li>
          
          <li class="nav-item <?php echo ($menu=='master') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-master" aria-expanded="false" aria-controls="menu-master"> <i class="menu-icon fa fa-cubes"></i> <span class="menu-title">Data Master</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='master') ? 'show' : ''; ?>" id="menu-master">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='tahun') ? 'active' : ''; ?>" href="<?php echo base_url('admin/master/tahun'); ?>">Tahun Akademik KKN</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='prodi') ? 'active' : ''; ?>" href="<?php echo base_url('admin/master/prodi'); ?>">Program Studi</a></li0000>
              </ul>
            </div>
          </li>


          <li class="nav-item <?php echo ($menu=='pengguna' || $menu=='reg') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-pengguna" aria-expanded="false" aria-controls="menu-pengguna"> <i class="menu-icon fa fa-users"></i> <span class="menu-title">Data Pengguna</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='pengguna') ? 'show' : ''; ?>" id="menu-pengguna">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($menu=='reg') ? 'active' : ''; ?>" href="<?php echo base_url('admin/reg'); ?>">Pendaftaran</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='admins') ? 'active' : ''; ?>" href="<?php echo base_url('admin/pengguna/admins'); ?>">Admin</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='camat') ? 'active' : ''; ?>" href="<?php echo base_url('admin/pengguna/camat'); ?>">Camat</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='biro') ? 'active' : ''; ?>" href="<?php echo base_url('admin/pengguna/biro'); ?>">Kemahasiswaan</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='dosen') ? 'active' : ''; ?>" href="<?php echo base_url('admin/pengguna/dosen'); ?>">Dosen</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='mahasiswa') ? 'active' : ''; ?>" href="<?php echo base_url('admin/pengguna/mahasiswa'); ?>">Peserta</a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item <?php echo ($menu=='kelompok') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('admin/kelompok'); ?>"><i class="menu-icon fa fa-map"></i><span class="menu-title">Kelompok KKN</span></a></li>

          <li class="nav-item <?php echo ($menu=='verified') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('admin/verified'); ?>"><i class="menu-icon fa fa-check"></i><span class="menu-title">Plotting Peserta KKN</span></a></li>

          <li class="nav-item <?php echo ($menu=='anggota') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-anggota" aria-expanded="false" aria-controls="menu-anggota"> <i class="menu-icon fa fa-wrench"></i> <span class="menu-title">Operasi Anggota</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='anggota') ? 'show' : ''; ?>" id="menu-anggota">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='cari') ? 'active' : ''; ?>" href="<?php echo base_url('admin/anggota/cari'); ?>">Cari Anggota</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='tambah') ? 'active' : ''; ?>" href="<?php echo base_url('admin/anggota/tambah'); ?>">Tambah Anggota</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='pindah') ? 'active' : ''; ?>" href="<?php echo base_url('admin/anggota/pindah'); ?>">Pindah Anggota</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='tukar') ? 'active' : ''; ?>" href="<?php echo base_url('admin/anggota/tukar'); ?>">Tukar Anggota</a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item <?php echo ($menu=='sertifikat') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-sertifikat" aria-expanded="false" aria-controls="menu-sertifikat"> <i class="menu-icon fa fa-certificate"></i> <span class="menu-title">E-Sertifikat</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='sertifikat') ? 'show' : ''; ?>" id="menu-sertifikat">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='peserta') ? 'active' : ''; ?>" href="<?php echo base_url('admin/sertifikat/peserta'); ?>">Peserta</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='dpl') ? 'active' : ''; ?>" href="<?php echo base_url('admin/sertifikat/dpl'); ?>">DPL</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='cek') ? 'active' : ''; ?>" href="<?php echo base_url('admin/sertifikat/cek'); ?>">Cek E-Sertifikat</a></li>
              </ul>
            </div>
          </li>

        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
