<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Tahun Pelaksanaan KKN</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Aktifkan salah satu tahun akademik yang akan diselenggarakan</h6>
				    </div>
				    <div class="col-md-4">
				    	<a class="float-right btn btn-primary" href="<?php echo base_url('admin/master/tahun/tambah'); ?>"><i class="fa fa-plus"></i>Tambah</a>
				    </div>
				    <div class="col-12">
				    	<table id="table" class="table table-bordered">
		                    <thead>
			                    <tr>
			                        <th>
			                        	#
			                        </th>
			                        <th>
			                        	Label Tahun Akademik
			                        </th>
			                        <th>
			                        	Status
			                        </th>
			                        <th>
			                        	Opsi
			                        </th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    	$i = 1;
		                    		foreach ($data as $key => $value) {
		                    			?>
			                    			<tr>
					                     		<td><?php echo $i++; ?></td>
					                     		<td><?php echo $value->NAMATAHUN; ?></td>
					                     		<td>
					                     			<?php
					                     				if($value->STATUSTAHUN==1)
					                     					echo '<i class="fa fa-unlock text-primary"></i>';
					                     				else
					                     					echo '<i class="fa fa-lock text-danger"></i>';
					                     			?>
					                     		</td>
					                     		<td width="90px">
					                     			<a title="Edit" class="text-primary" href="<?php echo base_url('admin/master/tahun/edit/'.$value->KDTAHUN); ?>"><i class="fa fa-pencil"></i></a>
					                     			&nbsp;.&nbsp;
					                     			<?php if($value->STATUSTAHUN==1){ ?>
					                     					<a title="Nonaktifkan" class="text-warning" href="<?php echo base_url('admin/master/tahun/status/'.$value->KDTAHUN.'/0'); ?>"><i class="fa fa-lock"></i></a>
					                     			<?php }else{ ?>
					                     					<a title="Aktifkan" class="text-info" href="<?php echo base_url('admin/master/tahun/status/'.$value->KDTAHUN.'/1'); ?>"><i class="fa fa-unlock"></i></a>
					                     			<?php } ?>
					                     			&nbsp;.&nbsp;
					                     			<?php if($value->STATUSTAHUN!=1){ ?>
																	    <a title="Hapus" class="text-danger" onclick="hapus('<?php echo base_url('admin/master/tahun/hapus/'.$value->KDTAHUN); ?>')" href="#!"><i class="fa fa-close"></i></a>
															    	<?php }else{ ?>
															    		<a title="Tidak dapat dihapus" class="text-muted"><i class="fa fa-close"></i></a>
															    	<?php } ?>
					                     		</td>
					                     	</tr>
		                    			<?php
		                    		}
		                    	?>
		                    </tbody>
		                  </table>
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX: true});
	});

	function hapus(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}
</script>