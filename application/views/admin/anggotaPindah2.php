<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
	  	<div class="card-body">
	  		<div class="row">
		  		<div class=" col-md-4">
		  			<div class="alert alert-info" role="alert">
						  Masukkan NPM / Nama / Alamat mahasiswa yang akan dipindahkan ke kelompok kkn lain, mahasiswa yang akan dipindahkan harus sudah terverifikasi.
						</div>
						<form class="form-inline row" action="<?php echo base_url('admin/anggota/pindah');  ?>" method="post">
						  <div class="form-group col-md-8">
						    <input autocomplete="off"  type="text" class="form-control" id="npm" name="npm" value="<?php echo $cari; ?>" placeholder="Cari...">
						  </div>
						  <div class="form-group col-md-4">
						  <button type="submit" class="btn btn-outline-primary">Ok</button>
						</div>
						</form>
						<?php
		  			if($status=='b'){
		  				?>
						<div class="card">
						  <div class="card-body">
						    <h4 class="card-title"><?php echo $mahasiswa[0]->NAMAMHS;  ?></h4>
						    <h6 class="card-subtitle mb-2 text-muted"><?php echo $mahasiswa[0]->NPM;  ?></h6>
						    <p class="card-text"><b>Fak / Prodi</b><br><?php echo $mahasiswa[0]->NAMAPRODI;  ?></p>
						    <p class="card-text"><b>Kelompok</b><br><?php echo $kelompok;  ?></p>
						    <p class="card-text"><b>Alamat</b><br><?php echo $mahasiswa[0]->ALAMATMHS;  ?></p>
						  </div>
						</div>
						<?php
						}
						?>
		  		</div>
		  		<div class="loading-pindah col-md-8">
		  			<?php
		  			if($status=='b'){
		  				if(count($kkn)>0){
		  				?>
				  			<div class="alert alert-info" role="alert">
								  Daftar KKN yang kapasitasnya masih bisa ditempati
								</div>
								<table id="table" class="table table-bordered">
	                <thead>
	                  <tr>
	                      <th>
	                      	#
	                      </th>
	                      <th>
	                      	Nama Kelompok
	                      </th>
	                      <th>
	                      	Kapasitas
	                      </th>
	                      <th>
	                      	DPL
	                      </th>
	                      <th>
	                      	Lokasi
	                      </th>
	                      <th>
	                      	Opsi
	                      </th>
	                  </tr>
	                </thead>
	                <tbody>
	                	<?php
	                	$i = 1;
	                		foreach ($kkn as $key => $value) {
	                			?>
	                  			<tr>
		                     		<td><?php echo $i++; ?></td>
		                     		<td><?php echo $value->NAMAKEL; ?></td>
		                     		<td><?php echo $value->KAPASITAS; ?></td>
		                     		<td><?php echo $value->NAMADPL; ?></td>
		                     		<td><?php echo $value->ALAMATKEL; ?></td>
		                     		<td>
		                     			<a class="btn-sm btn btn-outline-primary" onclick="pindah('<?php echo $value->KDKEL; ?>','<?php echo $npm; ?>')" href="#!">Pindah</a>
		                     		</td>
		                     	</tr>
	                			<?php
	                		}
	                	?>
	                </tbody>
	              </table>
		  					<?php
	  					}else{
	  						?>
	  						<div class="alert alert-warning" role="alert">
								  Tidak ada kelompok KKN yang tersedia....
								</div>
	  						<?php
	  					}
		  			}else if($status=='c'){
		  				console($status);
		  				echo '<div class="alert alert-danger" role="alert">Mahasiswa tidak terdaftar pada kegiatan KKN!</div>';
		  			}
		  			?>
		  		</div>
		  	</div>
	  	</div>
	  </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX:true});
	});
	function pindah(kel,npm){
		_alert({
				mode:'confirm',
				title:'Apakah akan dipindahkan?',
				msg:'Peserta ini akan dipindahkan apabila anda melanjutkannya',
				yes:'Ya, lanjutkan!',
				no:'Tidak',
				isConfirm:function(){
					_ajax({
						url:'<?php echo base_url('admin/anggota/prosespindah2') ?>',
						data:{
							npm:npm,
							kelompok:kel
						},
						loading:'.loading-pindah',
						success:function(){
							location.reload();
						}
					})
				}
			})
		
	}
</script>