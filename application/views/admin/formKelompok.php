<div class="row">
  <div class="col">
    <div class="card">
        <div class="card-body">
                              
          <div class="row justify-content-md-center">
            <div class="col col-md-5">
                  <h4 class="card-title">
                    <?php
                    if($tipe=='tambah')
                      echo 'Tambah Kelompok KKN Tahun Akademik '.$this->mfungsi->tahun()->label;
                    else if($tipe=='edit')
                      echo 'Edit Kelompok KKN  Tahun Akademik '.$this->mfungsi->tahun()->label;
                    ?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                    if($tipe=='tambah')
                      $act = base_url('admin/kelompok/simpan');
                    else if($tipe=='edit')
                      $act = base_url('admin/kelompok/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                    <div class="form-group">
                      <label for="namakel">* Nama Kelompok <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="namakel" value="<?php echo $nama; ?>"  placeholder="Nama Kelompok" name="nama">
                    </div>

                    <!-- <div class="form-group"> 
                      <label for="kapasitas">* Kapasitas Kelompok <?php echo form_error('kapasitas','<small class="text-danger">','</small>'); ?></label>
                      <span class="form-inline">
                      <input autocomplete="off"  type="text" class="w-25 form-control" id="kapasitas" readonly="" value="<?php echo $kapasitas; ?>"  placeholder="Kapasitas Kelompok" name="kapasitas"> <a href="#!" data-toggle="tooltip" data-placement="right" title="Atur kapasitas per prodi" onclick="openmodal();" class="btn ml-2 btn-outline-primary"><i class="fa fa-gear"></i></a>

                      <div class="modal fade" id="kapasitasprodiset" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title">KAPASITAS SETIAP PRODI</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <div class="card">
                                <div class="card-body">
                                  <table class="table table-hover table-sm">
                                    <tbody>
                                     
                                  <?php
                                    $i=0;
                                    foreach ($prodidata as $key => $value) {
                                      if($i==0 || $i==5)
                                        echo '';//'<div class="col">';
                                      ?>
                                       <tr>
                                        <th class="text-right"><?php echo form_error('prodi['.$value->KDPRODI.']','<small class="text-danger">','</small><br>'); ?><?php echo $value->NAMAPRODI ?></th>
                                        <td width="80px" align="right"><input autocomplete="off"  type="text" onblur="setkapasitas()" class="kapasitasprodi text-center form-control w-75" name="prodi[<?php echo $value->KDPRODI;  ?>]" value="<?php echo $prodi[$value->KDPRODI];  ?>" placeholder="0"></td>
                                      </tr>-->
                                      <!-- <div class="dropdown-divider"></div> -->
                                      <!--<?php
                                      if($i==4 || $i==9)
                                        echo '';//'</div>';
                                      $i++;
                                    }
                                    ?> 

                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-check form-check-flat">
                                  <label data-toggle="tooltip" data-placement="left" title="Guunakan pada penambahan kelompok kkn selanjutnya" class="form-check-label">
                                    <input autocomplete="off"  type="checkbox" name="default" value="<?php echo $default; ?>" class="form-check-input" <?php echo ($default!='') ? 'checked' : ''; ?>>
                                    &emsp;Default
                                  <i class="input-helper"></i></label>
                                </div>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      </span>
                    </div>-->

                    <div class="form-group">
                      <label for="dpl">* Dosen Pendamping Lapangan <?php echo form_error('dpl','<small class="text-danger">','</small>'); ?></label>

                      <select class="form-control selectpicker" data-live-search="true" id="dpl" name="dpl">
                        <option disabled="" <?php echo ($dpl=='') ? 'selected="':""; ?> value=""> - DPL -</option>
                        <?php

                        foreach ($dosen as $key => $value) {
                          if($dpl==$value->KDDPL){
                            echo '<option data-tokens="'.$value->NAMADPL.'" selected value="'.$value->KDDPL.'">'.$value->KDDPL.' | '.$value->NAMADPL.'</option>';
                          }else{
                            echo '<option data-tokens="'.$value->NAMADPL.'" value="'.$value->KDDPL.'">'.$value->KDDPL.' | '.$value->NAMADPL.'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>
                    <div class="form-group" id="groupalamat">
                      <label for="alamat">* Alamat lokasi KKN <?php echo form_error('alamat','<small class="text-danger">','</small>'); ?><?php echo form_error('kordx','<br><small class="text-danger">','</small>'); ?><?php echo form_error('kordy','<br><small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  placeholder="Alamat" name="alamat" class="form-control" id="alamat" type="text" value="<?php echo $alamat; ?>">

                      <input autocomplete="off"  placeholder="Lat" name="kordx" id="kordx" type="hidden" value="<?php echo $kordx; ?>">
                      <input autocomplete="off"  placeholder="Lng" name="kordy" id="kordy" type="hidden" value="<?php echo $kordy; ?>">

                    </div>


 


                <a href="#!" class="btn-link float-right"  onclick="findaddress();">Buka Peta</a><br><br>

                  <!-- <div class="form-group row justify-content-end">

                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="    float: right;">
                      <button onclick="validasialamat();" type="button" class="btn btn-primary">Validasi Alamat</button>

                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <a class="dropdown-item" onclick="findaddress();" href="#">Buka Map</a>
                        </div>
                      </div>
                    </div>


                  </div> -->
                  <div class="form-group">
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </div>
                  </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

 <div class="modal fade" tabindex="-1" role="dialog" id="map-google">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cari Alamat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
</script>

<script type="text/javascript">
var _alamatValid = false;
$(document).ready(function() {
  $('.selectpicker').selectpicker({
    size: 8
  });
  setkapasitas();
});

  function openmodal(){
    $('#kapasitasprodiset').modal('toggle');
  }

  function validasialamat(){
    
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('admin/kelompok/cekKordinat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#groupalamat',
      append:true,
      success:function(res){
        _alamatValid = false;
        res = JSON.parse(res);
        if(res.status==false){
          if(res.message=='OVER_QUERY_LIMIT'){
             _alert('Oopz..','Layanan telah melebihi batas, silahkan dapatkan API Key Google yang baru');
          }else{
            _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
          }
        }else{
          _alamatValid = true;
          _form({
            alamat:res.alamat,
            kordx:res.lat,
            kordy:res.lng
          })
        }        
      }
    })
  }

  function setkapasitas(){
    var val = 0;
    for(i=0; i<$('.kapasitasprodi').length; i++){
      var n = parseInt($('.kapasitasprodi').eq(i).val());
      if(isNaN(n))
        val += 0;
      else 
        val += n;
    }
    $('#kapasitas').val(val);
  }
</script>