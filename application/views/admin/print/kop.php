<style type="text/css">
    .dropdown-divider{
        border-top: 1px solid #000000;
    }
</style>
<div class="row">
    <div class="col-2">
        <div class="row justify-content-center">
            <div class="col-10 text-center">
                <img class="img-fluid" src="<?php echo base_url('assets/images/logobw.png'); ?>" alt="">
            </div>
            <div class="col-12 mt-2 text-center">
                LPPM-UNIRA
            </div>
        </div>
    </div>
    <div class="col-10 text-center">
        <div class="h4 mt-4 font-weight-bold"><?php  echo strtoupper(profil('instansi')); ?></div>
        <div class="h4 font-weight-bold"><?php  echo strtoupper(profil('kampus')); ?></div>
        <div><?php  echo ucwords(profil('alamat').' Tlp '.profil('telp').' Fax '.profil('fax')); ?></div>
        <div><?php  echo ucwords(profil('website').'; Email : '.profil('email')); ?></div>
    </div>
    <div class="col-12 mt-2">
        <div class="dropdown-divider">
            
        </div>
    </div>
</div>