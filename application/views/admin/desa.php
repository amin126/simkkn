<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col col-8">
				  		<h4 class="card-title">Data Desa</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Digunakan sebagai penempatan KKN <?php echo $this->mfungsi->tahun()->label; ?></h6>
				    </div>
				    <div class="col col-4">
				    	<a class="float-right btn btn-primary" href="<?php echo base_url('admin/master/desa/tambah'); ?>"><i class="fa fa-plus"></i>Tambah</a>
				    </div>
				    <div class="col col-12">
				    	<table id="table" class="table table-bordered">
		                    <thead>
			                    <tr>
			                        <th>
			                        	#
			                        </th>
			                        <th>
			                        	Desa
			                        </th>
			                        <th>
			                        	Kecamatan
			                        </th>
			                        <th>
			                        	Kabupaten
			                        </th>
			                        <th>
			                        	Operasi
			                        </th>
			                    </tr>
		                    </thead>
		                    <tbody>
		                    	<?php
		                    	$i = 1;
		                    		foreach ($data as $key => $value) {
		                    			?>
			                    			<tr>
					                     		<td><?php echo $i++; ?></td>
					                     		<td><?php echo $value->NAMADESA; ?></td>
					                     		<td><?php echo $value->NAMAKEC; ?></td>
					                     		<td><?php echo $value->NAMAKAB; ?></td>
					                     		<td>
					                     			<div class="btn-group">
																		  <a class="btn btn-primary" href="<?php echo base_url('admin/master/desa/edit/'.$value->KDDESA); ?>">Edit</a>
																		  <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																		    <span class="sr-only">Toggle Dropdown</span>
																		  </button>
																		  <div class="dropdown-menu"><!-- 
																		  	<a class="dropdown-item" href="'.base_url('admin/master/tahun/status/'.$value->KDTAHUN.'/1').'">Aktifkan</a>
																		    <div class="dropdown-divider"></div> -->
																		    <a class="dropdown-item text-danger" onclick="hapus('<?php echo base_url('admin/master/kecamatan/hapus/'.$value->KDKEC); ?>')" href="#!">Hapus</a>
																		  </div>
																		</div>	
					                     		</td>
					                     	</tr>
		                    			<?php
		                    		}
		                    	?>
		                    </tbody>
		                  </table>
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable();
	});

	function hapus(url){
			_alert({
				mode:'confirm',
				title:'Apakah akan dihapus?',
				msg:'Semua data yang berhubungan dengan data ini akan ikut terhapus, apakah akan dilanjutkan?',
				yes:'Ya, lanjutkan!',
				no:'Tidak',
				isConfirm:function(){
					window.location = url;
				}
			})
	}
</script>