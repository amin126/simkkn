<style type="text/css">
.switch {
  position: relative;
  display: inline-block;
  width: 42px;
  height: 24px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(19px);
  -ms-transform: translateX(19px);
  transform: translateX(19px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="row">
	<div class="col">
		<div class="card xloading">
		  	<div class="card-body">
			  	<div class="row">
				    <div class="col-12">
				    	<table id="table" class="table table-bordered">
                <thead>
                  <tr>
                      <th>
                      	#
                      </th>
                      <th>
                      	Kode DPL
                      </th>
                      <th>
                      	Nama DPL
                      </th>
                      <th>
                      	Kontak DPL
                      </th>
                      <th>
                      	Periode KKN
                      </th>
                      <th>
                        KDPL Terbaik
                      </th>
                      <th>
                        DPL Terbaik
                      </th>
                      <th>
                      	Serifikat
                      </th>
                  </tr>
                </thead>
                <tbody>
                	<?php
                	$i = 1;
                		foreach ($data as $key => $value) {
                			?>
                  			<tr>
	                     		<td><?php echo $i++; ?></td>
	                     		<td><?php echo $value->KDDPL; ?></td>
	                     		<td><?php echo $value->NAMADPL; ?></td>
	                     		<td><?php echo $value->KONTAKDPL; ?></td>
	                     		<td><?php echo $value->NAMATAHUN; ?></td>
                          <td>
                            <label class="switch">
                              <input type="checkbox" value="<?php echo $value->KDDPL; ?>" onchange="terbaik(this,'kdpl_terbaik')" <?php echo ($value->KTERBAIK!=null) ? 'checked' : ''; ?>>
                              <span class="slider round"></span>
                            </label>
                          </td>
                          <td>
                            <label class="switch">
                              <input type="checkbox" value="<?php echo $value->KDDPL; ?>" onchange="terbaik(this,'dpl_terbaik')" <?php echo ($value->TERBAIK!=null) ? 'checked' : ''; ?>>
                              <span class="slider round"></span>
                            </label>
                          </td>
	                     		<td>
	                     			<a href="<?php echo base_url('admin/sertifikat/dpl/generate/').$value->KDDPL; ?>" class="btn btn-primary btn-sm" onclick="_cetak(this)"><i class="fa fa-print"></i></a>
	                     		</td>
	                     	</tr>
                			<?php
                		}
                	?>
                </tbody>
              </table>
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX: true});
	});
  function terbaik(t,j){
    _ajax({
      url:'<?php echo base_url('admin/sertifikat/terbaik'); ?>',
      data:{
        id:$(t).val(),
        status:$(t).prop('checked'),
        jenis:j
      },
      success:function(res){
        
      },
      loading:'.navbar',
      colorback:'white',
      coloranim:'blue'
    })
  }
</script>