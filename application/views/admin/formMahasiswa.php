<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-md-5">
                  <h4 class="card-title">
                  	<?php
                  	if($tipe=='tambah')
                  		echo 'Tambah Data Mahasiswa Baru';
                  	else if($tipe=='edit')
                  		echo 'Edit Data Mahasiswa';
                  	?>
                  </h4>
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <?php
                  	if($tipe=='tambah')
                  		$act = base_url('admin/pengguna/mahasiswa/simpan');
                  	else if($tipe=='edit')
                  		$act = base_url('admin/pengguna/mahasiswa/update/'.$kode);
                  ?>

                  <form class="forms-sample" action="<?php echo $act; ?>" method="post">

                    <div class="form-group">
                      <label for="nim">* NPM <?php echo form_error('nim','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="usern" value="<?php echo $nim; ?>"  placeholder="NPM" name="nim">
                    </div>

                    <div class="form-group">
                      <label for="nama">* Nama Lengkap <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="nama" value="<?php echo $nama; ?>"  placeholder="Nama Lengkap" name="nama">
                    </div>

                    <div class="form-group">
                      <label for="prodi">* Fakultas / Prodi <?php echo form_error('prodi','<small class="text-danger">','</small>'); ?></label>

                      <select class="form-control selectpicker" data-live-search="true" id="prodi" name="prodi">
                        <option disabled="" <?php echo ($prodi=='') ? 'selected="':""; ?> value=""> - PRODI -</option>
                        <?php
                        foreach ($dataprodi as $key => $value) {
                          if($prodi==$value->KDPRODI)
                            echo '<option data-tokens="'.$value->FAKPRODI.' / '.$value->NAMAPRODI.'" selected value="'.$value->KDPRODI.' / '.$value->NAMAPRODI.'">'.$value->FAKPRODI.' / '.$value->NAMAPRODI.'</option>';
                          else
                            echo '<option data-tokens="'.$value->FAKPRODI.' / '.$value->NAMAPRODI.'" value="'.$value->KDPRODI.' / '.$value->NAMAPRODI.'">'.$value->FAKPRODI.' / '.$value->NAMAPRODI.'</option>';
                        }
                        ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="kerja">Pekerjaan</label>
                      <input autocomplete="off"  type="text" class="form-control" id="kerja" value="<?php echo $kerja; ?>"  placeholder="kerja" name="kerja">
                    </div>

                    <div class="form-group">
                      <label for="kontak">* Kontak <?php echo form_error('kontak','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="kontak" value="<?php echo $kontak; ?>"  placeholder="Kontak" name="kontak">
                    </div>

                    <div class="form-group" id="groupalamat">
                      <label for="alamat">* Alamat <?php echo form_error('alamat','<small class="text-danger">','</small>'); ?>
                        <?php echo form_error('kordx','<br><small class="text-danger">','</small>'); ?>
                        <?php echo form_error('kordy','<br><small class="text-danger">','</small>'); ?>
                      </label>
                      <input autocomplete="off"  type="text" placeholder="Alamat" name="alamat" class="form-control" id="alamat" value="<?php echo $alamat; ?>">

                      <input autocomplete="off"  type="hidden" placeholder="Lat" name="kordx" id="kordx" value="<?php echo $kordx; ?>">
                      <input autocomplete="off"  type="hidden" placeholder="Lng" name="kordy" id="kordy" value="<?php echo $kordy; ?>">
                      
                    </div>


                <a href="#!" class="btn-link float-right"  onclick="findaddress();">Buka Peta</a><br><br>

                  <!-- <div class="form-group row justify-content-end">
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="    float: right;">
                      <button onclick="validasialamat();" type="button" class="btn btn-primary">Validasi Alamat</button>

                      <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          
                        </button>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                          <a class="dropdown-item" onclick="findaddress();" href="#">Buka Map</a>
                        </div>
                      </div>
                    </div>

                  </div> -->
                  
                    <div class="form-group">
                      <label for="sks">* Jumlah Kumulatif sks yang diperoleh <?php echo form_error('sks','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="text" class="form-control" id="sks" value="<?php echo $sks; ?>"  placeholder="Sks" name="sks">
                    </div>

                    <div class="form-group">
                    <label for="seragam">Ukuran seragam</label>
                    <?php  echo form_error('seragam','<br><small class="text-danger">','</small>'); ?>
                      <div class="col-md-6">
                          <div class="form-group">
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='xl') ? 'checked':''; ?> name="seragam" id="xl" value="xl">
                                XL
                              <i class="input-helper"></i></label>
                            </div>
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='l') ? 'checked':''; ?> name="seragam" id="l" value="l">
                                L
                              <i class="input-helper"></i></label>
                            </div>
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input autocomplete="off"  type="radio" class="form-check-input" name="seragam" <?php echo ($seragam=='m') ? 'checked':''; ?> id="m" value="m">
                                M
                              <i class="input-helper"></i></label>
                            </div>
                            <div class="form-radio">
                              <label class="form-check-label">
                                <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='s') ? 'checked':''; ?> name="seragam" id="s" value="s">
                                S
                              <i class="input-helper"></i></label>
                            </div>
                          </div>
                        </div>
                    </div>

                    
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="map-google">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cari Alamat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
</script>

<script type="text/javascript">
  var _alamatValid = false;
  $(document).ready(function() {
    $('.selectpicker').selectpicker({
      size: 8
    });
  });
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('admin/pengguna/mahasiswa/cekkordinat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#groupalamat',
      append:true,
      success:function(res){
        res = JSON.parse(res);
        if(res.status==false){
          var _alamatValid = false;
          if(res.message=='OVER_QUERY_LIMIT'){
             _alert('Oopz..','Layanan telah melebihi batas, silahkan dapatkan API Key Google yang baru');
          }else{
            _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
          }
        }else{
          var _alamatValid = true;
          _form({
            alamat:res.alamat,
            kordx:res.lat,
            kordy:res.lng
          })
        }      
      }
    })
  }
</script>