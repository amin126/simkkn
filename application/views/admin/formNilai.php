<div class="row">
	<div class="col">
		<div class="card">
		  	<div class="card-body">
		  		<div class="row justify-content-md-center">
				    <div class="col col-md-5">
                  <h4 class="card-title">Edit Nilai Peserta</h4>
                  
                  <p class="card-description">
                    Yang bertanda (*) harus diisi!
                  </p>
                  <form class="forms-sample" action="<?php echo base_url('admin/nilai/simpan/'.$npm) ?>" method="post">

                  	<div class="form-group">
                      <label for="labeltahun">NPM</label>
                      <input autocomplete="off"  type="text"readonly class="form-control" value="<?php echo $npm; ?>"  >
                    </div>

                    <div class="form-group">
                      <label for="labeltahun">NAMA</label>
                      <input autocomplete="off"  type="text"readonly class="form-control" value="<?php echo $nama; ?>"  >
                    </div>

                    <div class="form-group">
                      <label for="labeltahun">FAKULTAS</label>
                      <input autocomplete="off"  type="text"readonly class="form-control" value="<?php echo $fakultas; ?>"  >
                    </div>

                    <div class="form-group">
                      <label for="labeltahun">PRODI</label>
                      <input autocomplete="off"  type="text"readonly class="form-control" value="<?php echo $prodi; ?>"  >
                    </div>

                    <div class="form-group">
                      <label for="labeltahun">KELOMPOK KKN</label>
                      <input autocomplete="off"  type="text"readonly class="form-control" value="<?php echo $kkn; ?>"  >
                    </div>

                    <div class="form-group">
                      <label for="labelnilai">* NILAI <?php echo form_error('nilai','<small class="text-danger">','</small>'); ?></label>
                      <input autocomplete="off"  type="number" class="form-control" style="width: 120px;" id="labelnilai" value="<?php echo $nilai; ?>"  placeholder="Nilai"  name="nilai">
                    </div>
                    
                    <button type="submit" class="btn btn-success mr-2">Simpan</button>
                    <a class="btn btn-light" href="<?php echo $kembali; ?>">Kembali</a>
                  </form>
				    </div>
				  </div>
		  	</div>
		</div>
	</div>
</div>