<style type="text/css">
	.body-tab{
		display: none;
	}
</style>
<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
	  	<div class="card-body">
	  		<h4 class="card-title">Verifkasi Mahasiswa</h4>
				<div class="body-tab" id="informasi">
					<h6 class="card-subtitle mb-2 text-muted">Dibawah ini merupakan rincian data mahasiswa yang akan di verifikasi</h6><br>
					<table class="table table-bordered table-hover">
						<thead  class="text-center">
							<tr>
								<th rowspan="2">PRODI</th>
								<th colspan="<?php echo count($data); ?>">KABUPATEN ASAL (JML MAHASISWA)</th>
								<th rowspan="2">TOTAL</th>
							</tr>
							<tr>
								<?php
									foreach ($data as $key => $value) {
										echo '<th>'.strtoupper($key).'</th>';
									}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							$jumlah = array();
							foreach ($prodi as $key => $value) {
								?>
									<tr>
										<?php
											$total = 0;
											echo '<td>'.$value.'</td>';
											foreach ($data as $key1 => $value1) {
												$cnt = count($data[$key1][$key]);
												if(!array_key_exists($key1, $jumlah))
													$jumlah[$key1] = 0;
												$jumlah[$key1] += $cnt;
												$total += $cnt;
												echo '<td>'.$cnt.'</td>';
											}
											echo '<td>'.$total.'</td>';
										?>
									</tr>
								<?php
							}
							?>
							<tr>
								<td>JUMLAH</td>
								<?php
								$total = 0;
								foreach ($jumlah as $key => $value) {
									$total += $value;
									echo '<td>'.$value.'</td>';
								}
								?>
								<td><?php echo $total; ?></td>
							</tr>
						</tbody>
					</table><br>
					<a href="#hitung-kapasitas" class="tab-menu btn pull-right btn-primary">Lanjutkan</a>
				</div>
				<div class="body-tab" id="hitung-kapasitas">
					<h6 class="card-subtitle mb-2 text-muted">Membagi rata peserta kkn pada setiap kelompok secara keseluruhan</h6><br>
					
					<div class="jumbotron"><form id="sekapasitas-form" onsubmit="return setkapasitas(this)" action="<?php echo base_url('admin/pengguna/mahasiswa/setKapasitas') ?>" method="post" accept-charset="utf-8">
					
					  
					  <h3>Jumlah Mhs di masing-masing prodi</h3>
					  <table class="table">
					  	<thead>
					  		<tr>
					  			<th>Prodi</th>
					  			<th>Jml Mhs</th>
					  		</tr>
					  	</thead>
					  	<tbody>
					  		<?php
					  		$ttl = 0;
					  		foreach ($kapasitas as $key => $value) {
					  			echo '<tr>
									  			<td>'.$prodi[$key].'</td>
									  			<td>'.$value.'<input autocomplete="off"  type="hidden" value="'.$value.'" name="prodi['.$key.']"></td>
									  		</tr>';
									  		$ttl += $value;
									  		//totalKapasitas
					  		}
					  		?>
					  		<tr>
					  			<th>JUMLAH</th>
					  			<th><?php echo $ttl; ?></th>
					  		</tr>
					  		
					  	</tbody>
					  </table>
					  <input autocomplete="off"  type="hidden" name="back" id="urlback" value="">
					  <hr class="my-4">
					  <p>Terapkan agar kelompok kkn dapat dibagi secara merata!</p>
					  <p class="lead">
					    <button class="btn btn-primary btn-lg" type="submit">Terapkan</button>
					  </p>
						</form>
						<h6 class="card-subtitle mb-2 text-muted">Sisa kapasitas di kelompok kkn dengan masing-masing kabupaten </h6>
						<?php
						foreach ($kapasitaskab as $key => $value) {
							echo strtoupper($key).' : <b>'.($value-$kapasitaskabisi[$key]).'</b><br>';
						}
						?>
					</div>
					<br>
					<a href="#informasi" class="tab-menu btn pull-left btn-warning">Kembali</a>
					<?php
					if($ttl==$totalKapasitas){
						?>
						<a href="#verifikasi" class="tab-menu btn pull-right btn-primary">Lanjutkan</a>
						<?php
					}
					?>
				</div>
				<div class="body-tab" id="verifikasi">
					<h6 class="card-subtitle mb-2 text-muted">Pilih cara untuk memasukkan mahasiswa ke kelompok kkn</h6><br>

					<div class="row justify-content-center text-center">
						<div class="col">
							<div class="alert alert-info" role="alert">
							  "AUTO" semua mahasiswa yang belum terverifikasi akan otomatis mencari kelompok kkn dengan jarak terdekat. tidak peduli apabila kelompok sudah memunuhi kapasitas maka mahasiswa akan mencari kelompok yang masih tersedia walaupun jarak kelompok kkn jauh dari wilayah mahasiswa
							</div>
							<a href="#auto-verifikasi" onclick="verifikasi(this)" class="tab-menu btn pull-left btn-primary">AUTO VERIFIKASI</a>
						</div>
						<div class="col">
							<div class="alert alert-success" role="alert">
							  "MANUAL" menentukan terlebih dahulu sebelum diverifikasi, hal ini bertujuan agar masuknya mahasiswa ke kelompok kkn menjadi tererah.
							</div>
							<a href="#manual-verifikasi" class="tab-menu btn pull-right btn-success">MANUAL VERIFIKASI</a>
						</div>
					</div>

					<br>
					<a href="#hitung-kapasitas" class="tab-menu btn pull-left btn-warning">Kembali</a>
				</div>
				<div class="body-tab" id="manual-verifikasi">
					<h6 class="card-subtitle mb-2 text-muted">Kabupaten Kelompok yang tersedia, pilih kabupaten kelompok lalu tentukan pada tabel rincian peserta kkn, berikutnya yang mana yang akan digolongkan ke kabupaten kelompok</h6>
					<form action="" method="post" accept-charset="utf-8">
						<?php
						foreach ($kabupatenkkn as $key => $value) {
							$warna = $this->mfungsi->random_color();
							?>
							<div class="form-check">
							  <label style="background: <?php echo $warna; ?>;padding-right: 8px;" class="form-check-label d-table">
							    <input autocomplete="off"  class="form-check-input" onchange="pilihkabupaten(this)" warna="<?php echo $warna; ?>" type="radio" id="select<?php echo $key; ?>" name="kabupaten" value="<?php echo $value; ?>">
							     <?php echo strtoupper($value); ?>
							  </label>
							</div>
							<?php
						}
						?>
					</form><br>
					<h6 class="card-subtitle mb-2 text-muted">Tabel rincian peserta kkn</h6>
					<table class="table table-bordered table-hover">
						<thead  class="text-center">
							<tr>
								<th rowspan="2">PRODI</th>
								<th colspan="<?php echo count($data); ?>">KABUPATEN ASAL (JML MAHASISWA)</th>
								<th rowspan="2">TOTAL</th>
							</tr>
							<tr>
								<?php
									foreach ($data as $key => $value) {
										echo '<th>'.strtoupper($key).'</th>';
									}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							$jumlah = array();
							foreach ($prodi as $key => $value) {
								?>
									<tr>
										<?php
											$total = 0;
											echo '<td>'.$value.'</td>';
											foreach ($data as $key1 => $value1) {
												$cnt = count($data[$key1][$key]);
												if(!array_key_exists($key1, $jumlah))
													$jumlah[$key1] = 0;
												$jumlah[$key1] += $cnt;
												$total += $cnt;
												$oncl = '';
												if($cnt>0)
													$oncl = 'style="cursor:pointer;" onclick="pilihkolom(this)"';
												echo '<td '.$oncl.' kabupaten="'.$key1.'" prodi="'.$key.'" pilih="false" keykabupaten="">'.$cnt.'</td>';
											}
											echo '<td>'.$total.'</td>';
										?>
									</tr>
								<?php
							}
							?>
							<tr>
								<td>JUMLAH</td>
								<?php
								$total = 0;
								foreach ($jumlah as $key => $value) {
									$total += $value;
									echo '<td>'.$value.'</td>';
								}
								?>
								<td><?php echo $total; ?></td>
							</tr>
						</tbody>
					</table>
					<br>
					<a href="#verifikasi" class="tab-menu btn pull-left btn-warning">Kembali</a>
					<a href="#manual-verifikasi-proses" class="tab-menu btn pull-right btn-primary">Lanjutkan</a>
				</div>
				<div class="body-tab" id="auto-verifikasi">
					<h6 class="card-subtitle mb-2 text-muted">Monitoring proses verifikasi</h6><br>
					  <style type="text/css">
					    .mod-scroll{
					      overflow: hidden;
					      overflow-y: auto;
					    }
					    #list-error tr{
					      cursor: pointer;
					    }
					    
					  </style>

					  <div class="h2 d-none" id="info-proses">
			        <span class="h5 text-info"><span id="pesan" class="text-muted">Menghitung....</span> | <span id="persen">0</span>%</span> <span class="h2 font-weight-bold xprocess"><span id="hasil">0</span> / <span id="total"><?php echo $ttl; ?></span> <span class="text-muted">Total Error</span> | <span id="count_error">0</span></span>
			      </div>

			      <div class="text-muted mt-3 mb-3">
			        Pesan Kesalahan saat proses verifikasi <small>(<i>klik pada baris yang akan di edit</i>)</small>
			      </div>

			      <div class="mod-scroll">
			        <table class="table table-hover table-bordered table-sm">
			          <tbody id="list-error" class="ul">

			          </tbody>
			        </table>
			      </div><br>
			      <a href="<?php echo base_url('admin/pengguna/mahasiswa'); ?>" style="display: none;" class="btn-finish btn pull-right btn-primary">Selesai</a>
				</div>
				<div class="body-tab" id="manual-verifikasi-proses">
					<h6 class="card-subtitle mb-2 text-muted">Tentukan prioritas kabupaten kkn, hal ini bertujuan akan memverifikasi secara berurutan sesuai dengan prioritas yang ditentukan</h6>
					<form id="prt-form" method="post" action=""><table class="table">
						<tbody  class="ul">
							<?php
							$i=0;
							foreach ($kabupatenkkn as $key => $value) {
								?>
								<tr id="prioritas<?php echo $i; ?>">
									<td class="li"></td>
									<td><div class="form-prioritas"><?php echo strtoupper($value); ?><input autocomplete="off"  type="hidden" name="prioritas[]" value="<?php echo $value; ?>"></div></td>
									<td width="1">
										<div class="btn-group" role="group" aria-label="Basic example">
                      <button type="button" onclick="pindahprioritas(<?php echo $i; ?>,'down')" class="btn btn-outline-primary icon-btn"><i class="mdi mdi-chevron-down"></i></button>
                      <button type="button" onclick="pindahprioritas(<?php echo $i; ?>,'up')" class="btn btn-outline-primary icon-btn"><i class="mdi mdi-chevron-up"></i></button>
                    </div>
									</td>
								</tr>
								<?php
								$i++;
							}
							?>
						</tbody>
					</table></form><br>
					<a href="#manual-verifikasi" class="tab-menu btn pull-left btn-warning">Kembali</a>
					<button onclick="prosesverifikasimanual()" class="btn pull-right btn-primary">Proses Verifikasi</button>
					<script type="text/javascript">
						var prioritasData = null;
						function pindahprioritas(n,t){
							n = parseInt(n);
							var a3 = (t=='up') ? n-1 : n+1;
							var a1 = $('#prioritas'+n+' .form-prioritas').html();
							var a2 = $('#prioritas'+a3+' .form-prioritas').html();
							$('#prioritas'+a3+' .form-prioritas').html(a1);
							$('#prioritas'+n+' .form-prioritas').html(a2);
						}
						function prosesverifikasimanual(){
							prioritasData = [];
							var cnt = $('#prt-form').serializeArray()
							for(i=0; i<cnt.length; i++){
								prioritasData.push(cnt[i].value);
							}
							//datasendverified
							//prioritasData
							//kodekelompok
							data = new Object();
							for(i in datasendverified){
								data[i] = datasendverified[i];
							}
							data['prioritasdata'] = prioritasData;
							data['kodekelompok'] = kodekelompok;
							_ajax({
								url:'<?php echo base_url('admin/pengguna/mahasiswa/verifikasiManual') ?>',
								data:data,
								loading:'#prt-form',
								success:function(res){
									_toTab('.body-tab','#auto-verifikasi');
									_progress({
										url:'<?php echo base_url('admin/pengguna/mahasiswa/verifikasiManualProses') ?>',
										start:function(){
								      $('#form-proses').addClass('d-none');
								      $('#info-proses').removeClass('d-none');
								      $('#list-error').html('');
								    },
								    running:function(res){
								      $('#hasil').text(res.hasil);
								      $('#persen').text(res.persen);
								      $('#pesan').text(res.pesan);
								      $('#total').text(res.total);
								      var count_error = $('#list-error tr').length;
								      $('#count_error').text(count_error);
								    },
								    callback:{
								      error:function(res){
								        dataeror[(noError-1)] = res;
								        var limit = (res.keterangan=='OVER QUERY LIMIT') ? 'data-limit' : '';
								        var html = '<tr onclick="editverifikasi(\''+(noError-1)+'\',this)" '+limit+' class="text-danger">'+
								                      '<td class="li"></td>'+  
								                      '<td>'+res.npm+'</td>'+
								                      '<td>'+res.nama+'</td>'+
								                      '<td>'+res.prodi+'</td>'+
								                      '<td>'+res.keterangan+'</td>'+
								                    '</tr>';
								                    noError++;
								        $('#list-error').prepend(html);
								      },
								      kelompok:function(res){
								        var html = '<tr class="text-danger">'+
								                      '<td class="li"></td>'+  
								                      '<td>'+res.kelompok+'</td>'+
								                      '<td>'+res.alamat+'</td>'+
								                      '<td><i>Periksa alamat dan internet</i></td>'+
								                    '</tr>';
								        $('#list-error').prepend(html);
								      },
								      consol:function(res){
								      	console.log(res)
								      }
								    },
								    end:function(res){
								      endProcess = true;
								      $('#info-proses').html(res.msg);
								      $('.btn-finish').removeAttr('style');
								    },
								    colorback:'red',
								    coloranim:'yellow',
								    loading:'.navbar',
								    append:true
									})
								}
							})
						}
					</script>
				</div>
				<div class="body-tab" id="proses-manual-verifikasi">
					<h6 class="card-subtitle mb-2 text-muted">Proses </h6><br>



					<br>
					<a href="#hitung-kapasitas" class="tab-menu btn pull-left btn-warning">Kembali</a>
					<a href="#hasil" class="tab-menu btn pull-right btn-primary">Lanjutkan</a>
				</div>

					
	  	</div>
	  </div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-alamat-mhs">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Alamat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>NPM</th>
                <td id="edit-npm"></td>
              </tr>
              <tr>
                <th>Nama</th>
                <td id="edit-nama"></td>
              </tr>
              <tr>
                <th>Prodi</th>
                <td id="edit-prodi"></td>
              </tr>
              <tr>
                <th>Alamat</th>
                <td>
                  <input autocomplete="off"  type="hidden"  name="npm" value="">
                  <input autocomplete="off"  type="text" class="form-control" name="alamat" placeholder="Alamat">
                </td>
              </tr>
            </tbody>
          </table>
          <div id="edit-info-msg" class="text-danger text-center">
            
          </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="verifikasiUlang()" class="btn btn-primary">Verifikasi</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	var dataverifikasi = null;
	var kodekelompok = null;
	jQuery(document).ready(function($) {
		_tab({
			tab:'.tab-menu',
			body:'.body-tab',
			default:'#informasi'
		})
		dataverifikasi = <?php echo json_encode($data) ?>;
		kodekelompok = <?php echo json_encode($kodekel) ?>;
	});
	function setkapasitas(t){
		_alert({
			mode:'confirm',
			title:'Apakah akan diterapkan?',
			msg:'Kapasistas kelompok akan dibagi secara merata',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				$('#urlback').val(document.URL);
				$(t).removeAttr('onsubmit');
				$(t).submit();
			}
		})
		return false;
	}
 	$(document).ready(function() {
    flex();
    $(window).resize(function(event) {
      flex();
    });
  });
  var noError = 1;
  var endProcess = false;
  var dataeror = new Array();
  var _thisList = null;
  function flex(){
    var h = $(window).height();
    var t = $('.mod-scroll').position();
    var pls = 120;
    $('.mod-scroll').height(h-t.top-pls);
  }

	function verifikasi(t){
		_progress({
	    url:'<?php echo base_url('admin/pengguna/mahasiswa/prosesverifikasi/0') ?>',
	    start:function(){
	      $('#form-proses').addClass('d-none');
	      $('#info-proses').removeClass('d-none');
	      $('#list-error').html('');
	    },
	    running:function(res){
	      $('#hasil').text(res.hasil);
	      $('#persen').text(res.persen);
	      $('#pesan').text(res.pesan);
	      $('#total').text(res.total);
	      var count_error = $('#list-error tr').length;
	      $('#count_error').text(count_error);
	    },
	    callback:{
	      error:function(res){
	        dataeror[(noError-1)] = res;
	        var limit = (res.keterangan=='OVER QUERY LIMIT') ? 'data-limit' : '';
	        var html = '<tr onclick="editverifikasi(\''+(noError-1)+'\',this)" '+limit+' class="text-danger">'+
	                      '<td class="li"></td>'+  
	                      '<td>'+res.npm+'</td>'+
	                      '<td>'+res.nama+'</td>'+
	                      '<td>'+res.prodi+'</td>'+
	                      '<td>'+res.keterangan+'</td>'+
	                    '</tr>';
	                    noError++;
	        $('#list-error').prepend(html);
	      },
	      kelompok:function(res){
	        var html = '<tr class="text-danger">'+
	                      '<td class="li"></td>'+  
	                      '<td>'+res.kelompok+'</td>'+
	                      '<td>'+res.alamat+'</td>'+
	                      '<td><i>Periksa alamat dan internet</i></td>'+
	                    '</tr>';
	        $('#list-error').prepend(html);
	      }
	    },
	    end:function(res){
	      endProcess = true;
	      $('#info-proses').html(res.msg);
	      $('.btn-finish').removeAttr('style');
	    },
	    colorback:'red',
	    coloranim:'yellow',
	    loading:'.navbar',
	    append:true
	  })
	}

	function editverifikasi(index,t){
    // var cek = $(t).attr('data-limit').size();
    // if(cek==0){
      if(endProcess==false){
        _alert('Proses verifikasi sedang berlangsung, tunggu sampai selesai dulu');
      }else{
        var res = dataeror[index];
        $('#edit-info-msg').html('');
        $('#edit-npm').text(res.npm);
        $('#edit-nama').text(res.nama);
        $('[name="alamat"]').val(res.alamat);
        $('[name="npm"]').val(res.npm);
        $('#edit-prodi').text(res.prodi);
        $('#edit-alamat-mhs').modal('toggle');
        _thisList = t;
      }
    // }else{
    //    _alert('OVER QUERY LIMIT','Batas kuota untuk request ke layanan api telah habis. silahkan coba lagi nanti sampai kuota ter-reset ');
    // }
  }

  function verifikasiUlang(){
    $('#edit-info-msg').html('');
    var npm = $('[name="npm"]').val();
    var alamat = $('[name="alamat"]').val();
    _progress({
      url:'<?php echo base_url('admin/pengguna/mahasiswa/ulangiverifikasi2'); ?>',
      data:{
        npm:npm,
        alamat:alamat
      },
      loading:'#edit-info-msg',
      running:function(res){
        if(res.process!=undefined){
          $('#edit-info-msg').html(res.process+'% / '+res.total);
        }
      },
      end:function(res){
        if(res==true || res.error==false){
          $(_thisList).remove();
          $('#edit-alamat-mhs').modal('toggle');
        }
      },
      callback:{
        error:function(res){
          $('#edit-info-msg').html(res.msg);
        },
        end:function(res){
          $('#edit-info-msg').html('Gagal diverifikasi, kesalahan tidak ditemukan');
        }
      }
    })
  }

  var warna = null;
  var kabupaten = null;

  function pilihkabupaten(t){
  	warna = $(t).attr('warna');
  	kabupaten = $(t).val();
  }

  var datasendverified = [];

  function pilihkolom(t){
  	Array.prototype.remove = function() {
		    var what, a = arguments, L = a.length, ax;
		    while (L && this.length) {
		        what = a[--L];
		        while ((ax = this.indexOf(what)) !== -1) {
		            this.splice(ax, 1);
		        }
		    }
		    return this;
		};

  	if(warna==null){
  		_alert('Kabupaten di kelompok kkn belum dipilih!');
  	}else{
  		var pilih = ($(t).attr('pilih')=="true") ? true : false;
  		var kab = $(t).attr('kabupaten');
  		var keykab = $(t).attr('keykabupaten');
  		var prod = $(t).attr('prodi');
  		if(pilih==false){
	  		$(t).css('background', warna);
	  		$(t).attr('pilih','true');
	  		$(t).attr('keykabupaten',kabupaten);
	  		if(datasendverified[kabupaten]==undefined)
	  			datasendverified[kabupaten]=[];
	  		for(i=0; i<dataverifikasi[kab][prod].length; i++){
	  			datasendverified[kabupaten].push(dataverifikasi[kab][prod][i].NPM);
	  		}
	  	}else{
	  		for(i=0; i<dataverifikasi[kab][prod].length; i++){
	  			if(keykab!=kabupaten)
	  				datasendverified[keykab].remove(dataverifikasi[kab][prod][i].NPM);
	  			else
	  				datasendverified[kabupaten].remove(dataverifikasi[kab][prod][i].NPM);
	  		}
	  		$(t).attr('keykabupaten',kabupaten);
	  		$(t).css('background', 'unset');
	  		$(t).attr('pilih','false');
	  	}
  	}
  }

</script>