<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<h4 class="card-title mt-5">DATA NILAI PESERTA BERDASARKAN PRODI <?php echo $proaktif['nama']; ?></h4>

	  		<div class="alert alert-info">
          Apabila terdapat perubahan nilai, silahkan klik <a href="<?php echo base_url('admin/nilai/arsip'); ?>">disini</a> untuk memperbarui status mahasiswa
        </div>

	  		<div class="row ">
	  			<div class="col-sm-3">
	  				<select onchange="lihatdata(this)" class="form-control selectpicker" data-live-search="true" id="prodi" name="prodi">
		          <option disabled="" <?php echo ($proaktif['kode']=='') ? 'selected="':""; ?> value=""> - PRODI -</option>
		          <?php

		          foreach ($prodi as $key => $value) {
		            if($proaktif['kode']==$value->KDPRODI){
		              echo '<option data-tokens="'.$value->NAMAPRODI.'" selected value="'.$value->KDPRODI.'">'.$value->NAMAPRODI.'</option>';
		            }else{
		              echo '<option data-tokens="'.$value->NAMAPRODI.'" value="'.$value->KDPRODI.'">'.$value->NAMAPRODI.'</option>';
		            }
		          }
		          ?>
		        </select>
		        <script type="text/javascript">
		        	function lihatdata(t){
		        		window.location.href = '<?php echo base_url('admin/statistika/nilai/view/') ?>'+$(t).val();
		        	}
		        </script>
	  			</div>
	  			<div class="col-sm-9">
	  				<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('admin/statistika/nilai/cetak/'.$proaktif['kode']); ?>"><i class="fa fa-print"></i>Cetak</a>
	  			</div>
	  			<div class="col-12 mt-4">
	  				<table id="datatabel" class="table table-bordered table-hover">
	  					<thead class="text-center text-muted">
	  						<tr>
	  							<th>#</th>
	  							<th>NPM</th>
	  							<th>NAMA</th>
	  							<th>KELOMPOK KKN</th>
	  							<th>NILAI ANGKA</th>
	  							<th>NILAI HURUF</th>
	  						</tr>
	  					</thead>
	  					<tbody>
	  						<?php
	  						foreach ($data as $key => $value) {
	  							?>
	  							<tr>
	  								<td><?php echo $key+1; ?></td>
	  								<td><?php echo $value->NPM; ?></td>
	  								<td><?php echo $value->NAMAMHS; ?></td>
	  								<td align="center"><a href="<?php echo base_url('admin/kelompok/detail/'.$value->KDKEL); ?>"><?php echo $value->NAMAKEL; ?></a></td>
	  								<td align="center">
	  									<?php if($value->NILAIMHS!=null){ ?>
	  										<a href="<?php echo base_url('admin/nilai/edit/').$value->NPM; ?>"><?php echo $value->NILAIMHS; ?></a>
	  									<?php }else{	?>
	  										<small><a class="text-muted" href="<?php echo base_url('admin/nilai/edit/').$value->NPM; ?>">SET</a></small>
	  									<?php } ?>
	  								</td>
	  								<td align="center"><?php echo $this->mfungsi->nilaihuruf($value->NILAIMHS); ?></td>
	  							</tr>
	  							<?php
	  						}
	  						?>
	  					</tbody>
	  				</table>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>
</div>
<?php
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#datatabel').DataTable({scrollX: true});	
		$('.selectpicker').selectpicker({
	    size: 8
	  });
	});
</script>