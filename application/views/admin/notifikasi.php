<div class="row">
	<div class="col">
		<div class="card" id="containerdosen">
		  	<div class="card-body">
			  	<div class="row">
				  	<div class="col-md-8">
				  		<h4 class="card-title">Semua Notifikasi</h4>
				    	<h6 class="card-subtitle mb-2 text-muted">Notifikasi dapat dikosongkan agar tidak memakan disk</h6>
				    </div>
				    <div class="col-md-4 text-right">
				    	<div class="btn-group"style="margin-bottom: 15px;" role="group" aria-label="First group">
                <a href="#!" class="btn btn-danger"  onclick="hapus('<?php echo base_url('admin/notif/clear'); ?>')" data-toggle="tooltip" data-placement="bottom" title="Kosongkan Notifikasi"><i class="fa fa-trash"></i></a>
              </div>	
				    </div>
				    <div class="col col-12">
									<table id="table" class="table table-bordered">
	                  <thead>
	                    <tr>
	                        <th>
	                        	#
	                        </th>
	                        <th>
	                        	Tgl
	                        </th>
	                        <th>
	                        	Deskripsi
	                        </th>
	                        <th>
	                        	Hapus
	                        </th>
	                    </tr>
	                  </thead>
	                  <tbody>
	                  	<?php
	                  		$i = 1;
	                  		foreach ($data as $key => $value) {
	                  			$cek = ($value->STATUSNOTIF==1) ? 'font-weight-bold' : '';
	                  			?>
	                    			<tr class="<?php echo $cek; ?>">
			                     		<td><?php echo $i++; ?></td>
			                     		<td><?php echo $this->mfungsi->tgl($value->TGLNOTIF,true); ?></td>
			                     		<td><a href="<?php echo base_url($value->URLNOTIF); ?>" target="_blank"><?php echo $value->KETNOTIF; ?></a></td>
			                     		<td><a class="dropdown-item text-danger" onclick="hapus('<?php echo base_url('admin/notif/delete/'.$value->KDNOTIF); ?>')"  href="#!"><i class="fa fa-close text-red"></i></a></td>
			                     	</tr>
	                  			<?php
	                  		}
	                  	?>
	                  </tbody>
	                </table>
				    	
				    </div>
				</div>
		  	</div>
		</div>	
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable({scrollX:true});
	});

	function hapus(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Notifikasi ini akan dihapus apabila anda melanjutkan',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}

	function clear(url){
		_alert({
			mode:'confirm',
			title:'Apakah akan kosongkan?',
			msg:'Semua notifikasi ini akan dihapus apabila anda melanjutkan',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				window.location = url;
			}
		})
	}
</script>