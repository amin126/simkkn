<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<h4 class="card-title text-center"><u>Rincian Kelompok Kuliah Kerja Nyata</u></h4>
	  		<div class="row">
			    <div class="col text-right col-12">
			    	<a class="float-right btn btn-outline-primary" onclick="_cetak(this)" href="<?php echo base_url('admin/kelompok/cetak/'.$kelompok[0]->KDKEL); ?>"><i class="fa fa-print"></i>Cetak</a>
			    	<a class="float-right btn btn-outline-success mr-3" href="<?php echo base_url('admin/kelompok/arsip/'.$kelompok[0]->KDKEL); ?>"><i class="fa fa-save"></i>Simpan</a>
			    	<a class="float-right btn btn-outline-warning  mr-3" href="<?php echo $this->mhistory->back(); ?>">Kembali</a>
			    </div>
	  			<div class="col-md-3 font-weight-bold">
	  				Nama Kelompok KKN
	  			</div>
	  			<div class="col-md-9">
	  				<?php echo $kelompok[0]->NAMAKEL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  			<div class="col-md-3 font-weight-bold">
	  				Alamat Kelompok KKN
	  			</div>
	  			<div class="col-md-9">
	  				<?php echo $kelompok[0]->ALAMATKEL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  			<div class="col-md-3 font-weight-bold">
	  				Dosen Pendamping Lapangan
	  			</div>
	  			<div class="col-md-9">
	  				<?php echo $kelompok[0]->NAMADPL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  			<div class="col-md-3 font-weight-bold">
	  				Kontak DPL
	  			</div>
	  			<div class="col-md-9">
	  				<?php echo ($kelompok[0]->KONTAKDPL==NULL) ? '-' : $kelompok[0]->KONTAKDPL; ?>
	  			</div>
	  			<div class="col-md-12 dropdown-divider"></div>
	  		</div>
	  		<div class="row">
	  			<div class="col">
	  				<table class="table datatable table-bordered table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Npm</th>
									<th>Nama</th>
									<th>Fak / Prodi</th>
									<th>Alamat</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i = 1;
									foreach ($mahasiswa as $key => $value) {
										?>
										<tr>
											<td><?php echo $i++; ?></td>
											<td><a class="btn-link" href="<?php echo base_url('admin/pengguna/mahasiswa/biodata/'.$value->NPM); ?>"><?php echo $value->NPM; ?></a></td>
											<td><?php echo $value->NAMAMHS; ?></td>
											<td><?php echo $value->NAMAPRODI; ?></td>
											<td><?php echo $value->ALAMATMHS; ?></td>
										</tr>
										<?php
									}
								?>
							</tbody>
						</table>	  				
	  			</div>	  			
	  		</div>
	  	</div>
	  </div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({paging:false,scrollX:true,info:false});
	});
</script>