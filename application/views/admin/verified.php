<style type="text/css" media="screen">
	.body-tab{
		display: none;
	}
</style>
<?php if($cekKel>0){ ?>
<div class="row">
	<div class="col">
		<div class="card">
	  	<div class="card-body">
	  		<div class="body-tab" id="informasi">
					<h6 class="card-subtitle mb-2 text-muted">Dibawah ini merupakan rincian data mahasiswa yang akan di verifikasi</h6><br>
					<table class="table datatable table-bordered table-hover">
						<thead  class="text-center">
							<tr>
								<th rowspan="2">PRODI</th>
								<th colspan="<?php echo count($data); ?>">KABUPATEN ASAL (JML MAHASISWA)</th>
								<th rowspan="2">TOTAL</th>
							</tr>
							<tr>
								<?php
									foreach ($data as $key => $value) {
										echo '<th>'.strtoupper($key).'</th>';
									}
								?>
							</tr>
						</thead>
						<tbody>
							<?php
							$jumlah = array();
							foreach ($prodi as $key => $value) {
								?>
									<tr>
										<?php
											$total = 0;
											echo '<td>'.$value.'</td>';
											foreach ($data as $key1 => $value1) {
												$cnt = count($data[$key1][$key]);
												if(!array_key_exists($key1, $jumlah))
													$jumlah[$key1] = 0;
												$jumlah[$key1] += $cnt;
												$total += $cnt;
												echo '<td>'.$cnt.'</td>';
											}
											echo '<td>'.$total.'</td>';
										?>
									</tr>
								<?php
							}
							?>
							<tr>
								<td>JUMLAH</td>
								<?php
								$total = 0;
								foreach ($jumlah as $key => $value) {
									$total += $value;
									echo '<td>'.$value.'</td>';
								}
								?>
								<td><?php echo $total; ?></td>
							</tr>
						</tbody>
					</table><br>
					<a href="#konfirmasi" class="tab-menu btn pull-right btn-primary">Lanjutkan</a>
				</div>
				<div class="body-tab" id="konfirmasi">
					<h6 class="card-subtitle mb-2 text-muted">Proses Pengambilan nilai jarak antara lokasi mahasiswa dengan lokasi kelompok kkn</h6><br>
					<div class="row">
						<div class="col-md-4 offset-md-4">
							<div class="alert alert-info" role="alert">
							  Konfirmasi Proses!
							</div> 
							<a href="#!" onclick="proses_kordinat()" class="btn btn-success pull-right">Ok</a>
						</div>
						<div class="col-12 mt-5 text-center">
							<h1 class="font-weight-bold persentase">Klik <b>Ok</b> untuk memulai proses!</h1>
						</div>
					</div>
					<br>
					<a href="#informasi"  class="tab-menu btn pull-left btn-warning">Kembali</a>
				</div>
				<div class="body-tab" id="validasi-alamat">
					<h2 class="card-subtitle mb-2 text-muted">Perbaiki Alamat Mahasiswa Tersisa <span id="e325"><?php echo count($noKordinat); ?></span></h2><br>
					<div class="row">
						<div class="col-12">
							<div class="alert h6 alert-danger" role="alert">
							  Silahkan validasi alamat terlebih dahulu, hal ini dibutuhkan pada saat perhitungan sebagai bahan pembentukan KKN, klik list untuk langsung mengedit alamat mahasiswa atau <b>Perbaiki Semua</b> untuk mempercepat proses
							</div> 
							<a href="#!" onclick="perbaiki_semua()" class="btn btn-success pull-right mb-2">Perbaiki Semua</a>
						</div>
						<div class="col-12">
							<style type="text/css">
								.xlist-error{
									transition: background-color 0.5s ease;								
								}
								.xlist-error:hover{
									background: #e53935;
									color: white;
									cursor: pointer;
								}
							</style>
							<div class="row" style="font-size: 11px;color: red">
								<?php  
								foreach ($noKordinat as $key => $value) {
									?>
									<div class="col-6 xlist-error" onclick="editformalamat(this)" npm="<?php echo $value->NPM; ?>" id="npm<?php echo $value->NPM; ?>">
										<div class="row">
											<div class="col-2 p-0">
												<?php echo $value->NPM; ?>
											</div>
											<div class="col-4 p-0">
												<?php echo $value->NAMAMHS; ?>
											</div>
											<div class="col-5 p-0" id="input<?php echo $value->NPM; ?>">
												<input autocomplete="off"  onblur="updateformalamat('<?php echo $value->NPM; ?>')" style="width: 100%;" type="hidden" value="<?php echo $value->ALAMATMHS; ?>">
												<span><?php echo $value->ALAMATMHS; ?></span>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</div>
							<script type="text/javascript">
								var s3545 = true;
								function perbaiki_semua(){
									var sukses = 0;
									var totaldata = parseInt($('#e325').text());
									if(s3545==true){
										_progress({
											url:'<?php echo base_url('admin/metode/gen/getkordinatAll') ?>',
											start:function(){
												s3545 = false;
											},
											running:function(res){
												s3545 = false;
												var npm = res.npm;
												res = res.data;
												if(res.status==true){
													sukses++;
													$('#input'+npm+' input').val(res.alamat);
													var total = parseInt($('#e325').text())-1;
													$('#e325').text(total);
													$('#input'+npm+' span').text(res.alamat);
													$('#npm'+npm).removeAttr('onclick');
													$('#npm'+npm).removeClass('xlist-error');
													$('#npm'+npm).css({
														'color':'#388e3c',
														'font-weight':'bold'
													});
												}else{
													$('#input'+npm+' span').html('<b>'+res.message+'</b>');
												}
											},
											end:function(){
												s3545 = true;
												_alert('Informasi','Data berhasil divalidasi sebanyak '+sukses+' dari total data '+totaldata);
											}
										})
									}
								}
								function updateformalamat(npm){
									s3545 = false;
									var input = $('#input'+npm+' input');
									_ajax({
										url:'<?php echo base_url('admin/metode/gen/getkordinat') ?>',
										data:{
											npm:npm,
											alamat:input.val()
										},
										before:function(res){
											input.attr('type', 'hidden');
											$('#input'+npm+' span').html('<b><i>Tunggu...!</i></b>');
											$('#input'+npm+' span').css('display', 'block');
										},
										success:function(res){
											res = JSON.parse(res);
											s3545 = true;
											if(res.status==true){
												input.val(res.alamat);
												var total = parseInt($('#e325').text())-1;
												$('#e325').text(total);
												$('#input'+npm+' span').text(res.alamat);
												$('#npm'+npm).removeAttr('onclick');
												$('#npm'+npm).removeClass('xlist-error');
												$('#npm'+npm).css({
													'color':'#388e3c',
													'font-weight':'bold'
												});
											}else{
												$('#input'+npm+' span').html('<b>'+res.message+'</b>');
											}
										}
									})
									
								}

								function editformalamat(t){
									if(s3545==true){
										var npm =  $(t).attr('npm');
										var input = $('#input'+npm+' input');
										input.attr('type', 'text');
										input.focus();
										input.select();
										$('#input'+npm+' span').css('display', 'none');
									}
								}
							</script>
						</div>
					</div>
					<br>
					<!-- <a href="#informasi"  class="tab-menu btn pull-left btn-warning">Kembali</a> -->
					<?php //echo json_encode($noKordinat); ?>
				</div>
				<div class="body-tab" id="verified">
					<div class="row text-center">
						<div class="col">
							<div class="h1">Semua mahasiswa sudah mendapatkan nilai jarak masing-masing pada setiap kelompok kkn</div>
							<div class="h3 mt-5 text-info">
								Klik <font class="font-weight-bold">PROSES</font> untuk melanjutkan ke pembentukan kelompok KKN
							</div>

							<button type="button" class="btn btn-lg btn-primary mt-5" onclick="pemetaanMhs()">PROSES</button>
						</div>
					</div>
				</div>
				<div class="body-tab" id="verified-option">
					<div class="row text-center">
						<div class="col">
							<div class="h1">Semua mahasiswa sudah dibentuk sebagai peserta <br>kelompok KKN</div>
							<div class="h3 mt-5 text-info">
								Pilih opsi yang akan anda lakukan!
							</div>

							<button type="button" class="btn btn-lg btn-danger svfe43 mt-5" onclick="ulangipemetaanMhs(this)">ULANGI PEMBENTUKAN KKN</button> 
							<a href="<?php echo base_url('admin/statistika/pendaftar'); ?>" class="btn btn-lg btn-primary mt-5">LIHAT HASIL</a>
						</div>
					</div>
				</div>
				<div class="body-tab" id="verified-process">
					<div class="row">
						<div class="col">
							<img src="<?php echo base_url('assets/images/animated/loading.gif')  ?>" alt="..." class="rounded mx-auto d-block">
							<div class="h1 text-danger d-table m-auto persen" style="position:  relative;top: -150px;">
								0%
							</div>
							<div class="text-primary text-center msg">
								Tunggu, proses sedang berlangsung...
							</div>
						</div>
					</div>
				</div>
				<div class="body-tab" id="verified-success">
					<div class="row">
						<div class="col">
							<img src="<?php echo base_url('assets/images/success.png')  ?>" alt="..." class="rounded mx-auto d-block">
							<div class="text-primary text-center msg" style="margin-bottom: 45px;position:  relative;top: 45px;">
								Pembentukan Kelompok KKN selesai, lihat <a class="font-weight-bold" href="<?php echo base_url('admin/statistika/pendaftar'); ?>">disini</a>.
							</div>
						</div>
					</div>
				</div>
	  	</div>
	  </div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="edit-alamat-mhs">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Alamat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>NPM</th>
                <td id="edit-npm"></td>
              </tr>
              <tr>
                <th>Nama</th>
                <td id="edit-nama"></td>
              </tr>
              <tr>
                <th>Prodi</th>
                <td id="edit-prodi"></td>
              </tr>
              <tr>
                <th>Alamat</th>
                <td>
                  <input autocomplete="off"  type="hidden"  name="npm" value="">
                  <input autocomplete="off"  type="text" class="form-control" name="alamat" placeholder="Alamat">
                </td>
              </tr>
            </tbody>
          </table>
          <div id="edit-info-msg" class="text-danger text-center">
            
          </div>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="verifikasiUlang()" class="btn btn-primary">Verifikasi</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
<?php }else{ ?>
<div class="alert alert-warning">
	DATA KELOMPOK KKN TIDAK TERSEDIA
</div>
<?php } ?>

<script type="text/javascript">
	var noError = 1;
  var endProcess = false;
  var dataeror = new Array();
  var _thisList = null;
	$(document).ready(function() {
		_tab({
			tab:'.tab-menu',
			body:'.body-tab',
			default:'<?php echo $default; ?>'
		});
		$('.datatable').DataTable({
			paging:false,
			scrollX:true,
			searching:false,
			responsive:true,
			info:false,
			width:'100%'
		});
	});

  function ulangipemetaanMhs(t){
  	_alert({
			mode:'confirm',
			title:'Konfirmasi',
			msg:'Ulangi pembentukan KKN?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				_ajax({
					url:'<?php echo base_url('admin/metode/gen/ulang') ?>',
					loading:'#verified-option',
					success:function(){
						window.location.href = '<?php echo base_url('admin/verified'); ?>';
					}
				})
			}
		})
  }

  function pemetaanMhs(){
  	_alert({
			mode:'confirm',
			title:'Konfirmasi',
			msg:'Buat Pembentukan kelompok KKN?',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				_toTab('#verified','#verified-process');
				_progress({
			    url:'<?php echo base_url('admin/metode/gen/pemetaan') ?>',
			    start:function(res){
			    	$('#verified-process .persen').text('0%');
			    	$('#verified-process .msg').text('Tunggu, proses sedang berlangsung...');
			    },
			    running:function(res){
			    	$('#verified-process .persen').text(res.persen+'%');
			    	$('#verified-process .msg').text(res.pesan);
			    },
			    end:function(){
			    	$('#verified-process .persen').text('100%');
			    	$('#verified-process .msg').text('Pembentukan Selesai');
			    	_toTab('#verified-process','#verified-success');
			    }
			  });
			}
		})
  }

	function editverifikasi(index,t){
    // var cek = $(t).attr('data-limit').size();
    // if(cek==0){
      if(endProcess==false){
        _alert('Proses verifikasi sedang berlangsung, tunggu sampai selesai dulu');
      }else{
        var res = dataeror[index];
        $('#edit-info-msg').html('');
        $('#edit-npm').text(res.npm);
        $('#edit-nama').text(res.nama);
        $('[name="alamat"]').val(res.alamat);
        $('[name="npm"]').val(res.npm);
        $('#edit-prodi').text(res.prodi);
        $('#edit-alamat-mhs').modal('toggle');
        _thisList = t;
      }
    // }else{
    //    _alert('OVER QUERY LIMIT','Batas kuota untuk request ke layanan api telah habis. silahkan coba lagi nanti sampai kuota ter-reset ');
    // }
  }

  function proses_kordinat(){
  	_progress({
	    url:'<?php echo base_url('admin/metode/gen/find_distance_geometry') ?>',
	    start:function(){

	    },
	    running:function(res){
	    	$('#konfirmasi .persentase').text(res+'%');
	    },
	    end:function(){
	    	_toTab('#konfirmasi','#verified');
	    }
	  });
  }
</script>