    <style>
			.searching {
			    position: absolute;
			    margin-left: 0px;
			    z-index: 9;
			    width: 100%;
			    margin-top: 10px;
			    display: none;
			}
    	.controls {
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
      }

      #pac-input {
      		float: left;
			    background-color: #fff;
			    font-family: Roboto;
			    font-size: 12px;
			    font-weight: 300;
			    padding: 0 8px 0px 10px;
			    text-overflow: ellipsis;
			    height: 29px;
			}

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }

      #btn-reset {
			  border: none;
			  border-radius: 1px;
			  background: white;
			  color: #7a7a7a;
			  font-size: 12px;
			  height: 29px;
			  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
			  font-family: roboto;
			  cursor: pointer;
			  display:none;
			}

			#btn-reset:active{
				background: #e4e4e4;
			}

			#btn-reset:hover{
				box-shadow: 0px 0px 1px  #4d90fe inset;
			}
    </style>
    <div class="row searching col-lg-4">
    	<input autocomplete="off"  id="pac-input" class="controls w-50" type="text" onkeypress="return find(event,this)" placeholder="Cari Kelompok atau Anggota">	
    	<button id="btn-reset" class="w-25" onclick="resetDataMap()">Reset</button>
    </div>
    
<div class="row col-md-4" id="infodetailkkn" style="display:none; position: absolute;z-index: 9;margin-left: -10px;margin-top: 45px;">
	<div class="col-12">
		<button style="background:  #fff;color:  #5a5a5a;box-shadow:  0 0 5px #bbb;" onclick="collapseinfokkn()" id="btnCollapse" class="btn btn-default" type="button" data-toggle="collapse" data-target="#infokkn" aria-expanded="false" aria-controls="infokkn">Informasi KKN &nbsp;&nbsp;&nbsp;<i class="fa fa-sort-desc" id="collapseinfokkn"></i></button>
		<div class="row">
		  <div class="col-12" id="w-infokkn" style="overflow:  hidden;overflow-y:  auto;">
		    <div class="collapse multi-collapse" id="infokkn" style="width: 93%; overflow: hidden; overflow-x: auto;">
		      <div class="card card-body">
		      	<div style="font-weight: bold;font-size: 13px;" class="info-kel">KELOMPOK</div>
		      	<div style="font-size: 12px;" class="info-dosen">DOSEN</div>
		      	<div class="text-muted info-alamat"  style="font-size: 11px;">ALAMAT</div>
		      	<div class="row dropdown-divider"></div>
		        <table class="table table-sm table-hover table-striped">
						  <thead>
						    <tr>
						      <th scope="col" style="font-size:  12px;">#</th>
						      <th scope="col" style="font-size:  12px;">NPM</th>
						      <th scope="col" style="font-size:  12px;">NAMA</th>
						      <th scope="col" style="font-size:  12px;">PRODI</th>
						      <th scope="col" style="font-size:  12px;">ALAMAT</th>
						    </tr>
						  </thead>
						  <tbody id="list-mhs">
						   
						  </tbody>
						</table>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalprofil" tabindex="-1" role="dialog" aria-labelledby="modalprofil" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Biodata Mahasiswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card">
				  <div class="card-body">
				  	<div class="row">
				  		<div class="col-md-4 offset-md-8">
				  			<img src="3" alt="NO FOTO" class="img-thumbnail w-100 FOTOMHS">
				  		</div>
				  	</div>
				    <table class="mt-3 table-striped table" style="font-size: 14px;">
							  <tbody>
							  	<tr>
							  		<td>1.</td>
							  		<td>Nomor Pokok Mahasiswa</td>
							  		<td>:</td>
							  		<td class="NPM"></td>
							  	</tr>
							  	<tr>
							  		<td width="3">2.</td>
							  		<td width="350">Nama Mahasiswa</td>
							  		<td width="1">:</td>
							  		<td class="NAMAMHS"></td>
							  	</tr>
							  	<tr>
							  		<td>3.</td>
							  		<td>Fakultas / Prodi</td>
							  		<td>:</td>
							  		<td class="NAMAPRODI"></td>
							  	</tr>
							  	<tr>
							  		<td>4.</td>
							  		<td>Nomor Kontak</td>
							  		<td>:</td>
							  		<td class="KONTAKMHS"></td>
							  	</tr>
							  	<tr>
							  		<td>5.</td>
							  		<td>Pekerjaan</td>
							  		<td>:</td>
							  		<td class="PEKERJAANMHS"></td>
							  	</tr>
							  	<tr>
							  		<td>6.</td>
							  		<td>Alamat</td>
							  		<td>:</td>
							  		<td class="ALAMATMHS"></td>
							  	</tr>
							  	<tr>
							  		<td>7.</td>
							  		<td>Jumlah Kumulatif sks yang diperoleh</td>
							  		<td>:</td>
							  		<td class="SKSMHS"></td>
							  	</tr>
							  	<tr>
							  		<td>8.</td>
							  		<td>Ukuran Seragam</td>
							  		<td>:</td>
							  		<td class="SERAGAMMHS"></td>
							  	</tr>
							  </tbody>
							</table>
				  </div>
				</div>
      </div>
    </div>
  </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id-ID<?php echo $key; ?>" async defer></script>
<div style="display: none;" id="container-map"><?php //echo json_encode($data); ?></div>
<script type="text/javascript">
	function profilMhs(npm){
		_ajax({
			url:'<?php echo base_url('admin/pengguna/mahasiswa/biodata_ajax'); ?>',
			data:{
				npm:npm
			},
			loading:'#container-map',
			success:function(res){
				res = JSON.parse(res);
				$.each(res,function(i,v){
					if(i=='FOTOMHS')
						$('.FOTOMHS').attr('src', v);
					else
						$('.'+i).text(v);
				});
				$('#modalprofil').modal('show')
			}
		})
	}
	var map = null;
	var collapseinfkkn = false;
	var markerBiru = [];
	var markerMerah = [];
	var markerKuning = [];
	var markerHijau = [];
	var lines = [];
 	var data = <?php echo json_encode($data); ?>;
 	var dataall = data;
	$(document).ready(function() {
		$(window).resize(function(event) {
			setTimeout(responsif, 300);
		});
		responsif();
		initmap();
	});
	function resetdataNode(){
		 markerBiru = [];
		 markerMerah = [];
	   markerKuning = [];
	   markerHijau = [];
	   lines = [];
	}
	function resetDataMap(){
		$('#pac-input').val('');
		$('#btn-reset').css('display','none');
		$('#infodetailkkn').css('display', 'none');
		if(collapseinfkkn==true)
			$('#btnCollapse').trigger('click');
		hapusMarkerMerah();
		hapusMarkerBiru();
		hapusMarkerKuning();
		hapusMarkerHijau();
		hapusGarisMerah();
		resetdataNode();
		data = dataall;
		markerkelompok();
	}
	function find(e,t) {
    if (e.keyCode == 13) {
      _ajax({
      	url:'<?php echo base_url('admin/statistika/map/cari') ?>',
      	data:{
      		cari:$(t).val()
      	},
      	loading:'#container-map',
      	success:function(res){
      		hapusMarkerMerah();
      		hapusMarkerBiru();
      		hapusMarkerKuning();
      		hapusMarkerHijau();
      		hapusGarisMerah();
      		data = JSON.parse(res);
      		markerkelompok();
      		$('#infodetailkkn').css('display', 'none');
      		if(collapseinfkkn==true)
      			$('#btnCollapse').trigger('click');
      		$('#btn-reset').css('display','block');
      	}
      })
      $('#pac-input').blur();
      return false;
    }
	}
	function reloadInfoKKN(){
		if(collapseinfkkn==true){
			var hc = $('.content-wrapper').height()-88;
			var hd = $('#infokkn').height();
			if(hd>hc)
				$('#w-infokkn').height(hc);
			else
				$('#w-infokkn').height(hd);
		}
	}
	function collapseinfokkn(){
		if(collapseinfkkn==false){
			collapseinfkkn = true;
			var hc = $('.content-wrapper').height()-88;
			var hd = $('#infokkn').height();
			if(hd>hc)
				$('#w-infokkn').height(hc);
			else
				$('#w-infokkn').height(hd);
			$('#collapseinfokkn').removeClass('fa-sort-desc');
			$('#collapseinfokkn').addClass('fa-sort-asc');
		}else{
			collapseinfkkn = false;
			setTimeout(function(){
				$('#w-infokkn').height(0);
			}, 300);
			$('#collapseinfokkn').addClass('fa-sort-desc');
			$('#collapseinfokkn').removeClass('fa-sort-asc');
		}
	}
	function responsif(){
		var elm = $('#container-map');
		var h = $('.content-wrapper').height();
		var w = $('.content-wrapper').width();
		elm.width(w);
		elm.height(h);
		elm.css('display', 'block');
		if(map==null){
			initmap();
		}
	}

	function markerkelompok(){
		for(key in data){
			tambahMarkerHijau({
				lat:data[key].lat,
				lng:data[key].lng,
				nama:data[key].nama,
				alamat:data[key].alamat,
				kelompok:key
			})
		}
	}

	function initmap(){
		 	map = new google.maps.Map(document.getElementById('container-map'), {
	      zoom: 11,
	      center: {lat: -7.041253, lng: 113.453699},
	      mapTypeId: 'roadmap',
	      disableDefaultUI: true
	    });
      google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
      	$('.searching').css({'display':'block'});
      });
      markerkelompok();
	}

	function toinfokkn(kel){
		$('.info-kel').text(data[kel].nama);
		$('.info-dosen').text(data[kel].dosen);
		$('.info-alamat').text(data[kel].alamat);
		$('#list-mhs').html('');
		for(key in data[kel].mahasiswa){
			var mhs = data[kel].mahasiswa[key];
			$('#list-mhs').append(
				'<tr id="mhs'+mhs.npm+'" class="list-mhs-info" style="cursor:pointer;" onclick="toMahasiswa(\''+kel+'\',\''+key+'\')">'+
					'<td style="font-size:  10px;">'+(parseInt(key)+1)+'</td>'+
					'<td style="font-size:  10px;"><a href="#!" class="btn-link text-primary" onclick="profilMhs(\''+mhs.npm+'\')">'+mhs.npm+'</a></td>'+
					'<td style="font-size:  10px;">'+mhs.nama+'</td>'+
					'<td style="font-size:  10px;">'+mhs.prodi+'</td>'+
					'<td style="font-size:  10px;">'+mhs.alamat+'</td>'+
				'</tr>'
			);
		}
	}

	function toMahasiswa(kel,key){
		tambahMarkerMerah(data[kel].mahasiswa[key]);
	}

	function tambahMarkerMhs(kel){
		toinfokkn(kel);
		hapusMarkerBiru();
		hapusMarkerMerah();
		for(key in data[kel].mahasiswa){
			tambahMarkerBiru({
				lat:data[kel].lat,
				lng:data[kel].lng
			},{
				npm:data[kel].mahasiswa[key].npm,
				lat:data[kel].mahasiswa[key].lat,
				lng:data[kel].mahasiswa[key].lng,
				title:data[kel].mahasiswa[key].npm+' | '+data[kel].mahasiswa[key].nama
			})
		}
	}

	function hapusMarkerMerah(){
		for (var i = 0; i < markerMerah.length; i++) {
      markerMerah[i].setMap(null);
    }
    markerMerah = [];
	}

	function hapusMarkerKuning(){
		for (var i = 0; i < markerKuning.length; i++) {
      markerKuning[i].setMap(null);
    }
    markerKuning = [];
	}

	function hapusMarkerBiru(){
		for (var i = 0; i < markerBiru.length; i++) {
      markerBiru[i].setMap(null);
      lines[i].setMap(null);
    }
    lines = [];
    markerBiru = [];
	}


	function hapusMarkerHijau(){
		for (var i = 0; i < markerHijau.length; i++) {
      markerHijau[i].setMap(null);
    }
    markerHijau = [];
	}

	function hapusGarisMerah(){
		for (var i = 0; i < lines.length; i++) {
      lines[i].setMap(null);
    }
    lines = [];
	}


	function keinfolistmhs(npm){
		if(collapseinfkkn==false)
			$('#btnCollapse').trigger('click');
		var p = $('#mhs'+npm+' *').position() ;
		$('.list-mhs-info *').css({'color':'black','font-weight':'normal'});
		$('#mhs'+npm+' *').css({'color':'red','font-weight':'bold'});
		$('#w-infokkn').scrollTop(p.top);
	}

	function tambahMarkerKuning(kel){
		hapusMarkerMerah();
		var markeyel = new google.maps.Marker({
	    position: {lat:kel.lat,lng:kel.lng},
	    map: map,
	    icon: {
	        path: google.maps.SymbolPath.CIRCLE,
	        scale: 6,
	        fillColor: "#FFFF00",
	        fillOpacity: 1,
	        strokeWeight: 0.4
	    }
	  });
	  markerKuning.push(markeyel);
	}

	function tambahMarkerMerah(mhs,cntr=true){
		hapusMarkerMerah();
		var markermhs = new google.maps.Marker({
	    position: {lat:mhs.lat,lng:mhs.lng},
	    map: map,
	    icon: {
	        path: google.maps.SymbolPath.CIRCLE,
	        scale: 3.5,
	        fillColor: "#FF0000",
	        fillOpacity: 1,
	        strokeWeight: 0
	    },
	    title:mhs.npm+' | '+mhs.nama
	  });
	  markermhs.addListener('click', function() {
    	keinfolistmhs(mhs.npm);
		});
		keinfolistmhs(mhs.npm);
	  markerMerah.push(markermhs);
	  if(cntr==true){
		  map.setCenter({
		  	lat:mhs.lat,
		  	lng:mhs.lng
		  })
		}
	}

	function tambahMarkerBiru(kel,mhs){
		var markermhs = new google.maps.Marker({
	    position: {lat:mhs.lat,lng:mhs.lng},
	    map: map,
	    icon: {
	        path: google.maps.SymbolPath.CIRCLE,
	        scale: 4.5,
	        fillColor: "#0000FF",
	        fillOpacity: 0.4,
	        strokeWeight: 0.4
	    },
	    title:mhs.title
	  });

	  markermhs.addListener('click', function() {
    	keinfolistmhs(mhs.npm);
    	tambahMarkerMerah(mhs,false);
		});
	  garis(kel,mhs);
	  markerBiru.push(markermhs);
	}

	function tambahMarkerHijau(data){
		var marker = new google.maps.Marker({
	    position: {lat:data.lat,lng:data.lng},
	    map: map,
	    icon: {
	        path: google.maps.SymbolPath.CIRCLE,
	        scale: 6.5,
	        fillColor: "#388E3C",
	        fillOpacity: 0.4,
	        strokeWeight: 0.4
	    }
	  });
	  markerHijau.push(marker);
	  marker.addListener('click', function() {
    	tambahMarkerMhs(data.kelompok);
    	$('#infodetailkkn').css('display', 'block');
    	hapusMarkerKuning();
    	tambahMarkerKuning(data);
    	reloadInfoKKN();
		});
		marker.addListener('mouseover', function() {
    	//infowindow.open(map,marker);
		});
		marker.addListener('mouseout', function() {
    	//infowindow.close(map,marker);
		});
	}

	function garis(from,to){
	  var flightPlanCoordinates = [
      {lat: from.lat, lng: from.lng},
      {lat: to.lat, lng: to.lng}
    ];
    var flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 1
    });
    flightPath.setMap(map);
    lines.push(flightPath);
	}
</script>