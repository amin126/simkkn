<div class="card">
  <div class="card-header text-right">
    <a href="<?php echo $this->mhistory->back(); ?>" class="btn btn-outline-warning">Kembali</a>
  </div>
  <div class="card-body">
    <h4 class="card-title">Biodata Dosen</h4>
  	<div class="row">
  		<div class="col-md-4 offset-md-8">
  			<img src="<?php echo $data->FOTODPL.'?'.rand(111,999); ?>" alt="NO FOTO" class="img-thumbnail w-100">
  		</div>
  	</div>
    <table class="mt-3 table-striped table" style="font-size: 14px;">
			  <tbody>
			  	<tr>
			  		<td width="3">1.</td>
			  		<td width="350">Nama Dosen</td>
			  		<td width="1">:</td>
			  		<td><?php echo strtoupper($data->NAMADPL); ?></td>
			  	</tr>
			  	<tr>
			  		<td>2.</td>
			  		<td>Alamat Dosen</td>
			  		<td>:</td>
			  		<td><?php echo strtoupper($data->ALAMATDPL); ?></td>
			  	</tr>
			  	<tr>
			  		<td>3.</td>
			  		<td>Kontak Dosen</td>
			  		<td>:</td>
			  		<td><?php echo $data->KONTAKDPL; ?></td>
			  	</tr>
			  </tbody>
			</table>
  </div>
</div>