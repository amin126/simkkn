<div id="progressdata">
	<center><h4></h4></center>
</div>
<form id="submitproses" onsubmit="return submitproses(this)" action="<?php echo base_url('admin/pengguna/dosen/apiproses') ?>" method="post" accept-charset="utf-8">
	<input autocomplete="off"  type="hidden" name="jenis" value="one">
	<input autocomplete="off"  type="hidden" name="kode">
</form>
<div class="card" id="containerdosen1">
	<div class="card-body">
		<table id="apitable" class="table table-bordered">
		  <thead>
		    <tr>
		        <th>
		        	#
		        </th>
		        <th>
		        	Username
		        </th>
		        <th>
		        	Nama Lengkap
		        </th>
		        <th>
		        	Alamat
		        </th>
		        <th>
		        	Kontak
		        </th>
		        <th>
		        	<button type="button" onclick="saveall()" class="btn btn-outline-info btn-sm">Save All</button>
		        </th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php
		  		$i = 1;
		  		foreach ($dosen as $key => $value) {
		  			if(!in_array($value->id, $users)){
		  			?>
		    			<tr id="tr<?php echo $i; ?>" class="trlist">
		         		<td onclick="marklisttr('<?php echo $i; ?>')"><?php echo $i; ?></td>
		         		<td onclick="marklisttr('<?php echo $i; ?>')"><?php echo $value->id; ?></td>
		         		<td onclick="marklisttr('<?php echo $i; ?>')"><?php echo $value->attributes->nama; ?></td>
		         		<td onclick="marklisttr('<?php echo $i; ?>')"><?php echo $value->attributes->alamat; ?></td>
		         		<td onclick="marklisttr('<?php echo $i; ?>')"><?php echo ($value->attributes->telepon!=null) ? $value->attributes->telepon : $value->attributes->email; ?></td>
		         		<td>
				        	<button type="button" onclick="savesingle('<?php echo $value->id; ?>',this)" class="btn btn-outline-success btn-sm">Save</button>
				        </td>
		         	</tr>
		  			<?php
		  			}
		  			$i++;
		  		}
		  	?>
		  </tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#apitable').DataTable({
			scrollX:true
		});
	});
	function saveall(){
		$('#submitproses [name="jenis"]').val('all');
		$('#submitproses').submit();
	}
	var _this = null;
	function savesingle(i,t){
		_this = t;
		$('#submitproses [name="jenis"]').val('one');
		$('#submitproses [name="kode"]').val(i);
		$('#submitproses').submit();
	}
	function submitproses(t){
		var jns = $('#submitproses [name="jenis"]').val();
		var kd = $('#submitproses [name="kode"]').val();
		_progress({
			url:$(t).attr('action'),
			framework:true,
			data:{
				jenis:jns,
				kode:kd
			},
			loading:'#containerdosen1',
			start:function(){
				if(jns=='all')
					$('#progressdata center h4').html('Menghitung..!');
			},
			running:function(res){
				if(jns=='all')
					$('#progressdata center h4').html(res.process+' s/d '+res.total);
			},
			end:function(res){
				if(jns=='all'){
					_alert({
						autoClose:10000,
						close:true,
						title:'Berhasil',
						type:'success',
						msg:res.disimpan+' data berhasil ditambahkan dari '+res.dari+', '+res.tidak+' data tidak tersimpan',
						isClose:function(){
							window.location.href="<?php echo base_url('admin/pengguna/dosen') ?>";
						}
					})
					$('#progressdata center h4').html('');
				}else{
					
					$(_this).removeAttr('onclick');
					$(_this).text('Done');
					$(_this).attr('disabled', '');
					$(_this).addClass('btn-outline-danger');
					$(_this).removeClass('btn-outline-success');
				}
			}
		})

		return false;
		
	}
</script>
