<div class="row">
	<div class="col-12">
		<form class="form-inline" onsubmit="return periksa(this)">
		  <div class="form-group mx-sm-3 mb-2">
		    <label for="inputPassword2" class="sr-only">Kode Sertifikat</label>
		    <input autocomplete="off"  type="text" class="form-control" id="npm" name="npm" placeholder="Kode Sertifikat">
		  </div>
		  <button type="submit" class="btn btn-primary mb-2 mr-2">Periksa</button>
		</form>
	</div>
	<div class="dropdown-divider"></div>
	<div class="col-12 mt-3 xloading">
		<div class="sertifikat" style="display: none;
    padding: 19px;
    border: 1px solid #ded6d6;
    background: white;
    box-shadow: -7px -6px 15px #86868626;
">
			<div class="row">
				<div class="col-12">
					<div id="qrcode"></div>
				</div>
				<div class="col-12">
					<div class="kodeqr" style="
    font-size: 80%;
"></div>
				</div>
				<div class="col-12 text-muted" style="
    font-size: 80%;
">
					<div class="kodematch"></div>
				</div>
				<div class="col-12 text-center mt-md-0 mt-3">
					<h2><u>SERTIFIKAT</u></h2>
				</div>
				<div class="col-12 text-center">
					<div class="nomor text-primary"></div>
				</div>
				<div class="col-12 text-center mt-5">
					Diberikan kepada :
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="nama font-weight-bold"></div>
				</div>
				<div class="col-12 text-center">
					<div class="npm font-weight-bold text-muted" style="
    font-size:  80%;
"></div>
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="alert alert-info sebagai"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var wqr = 0;
	var urlcetak = '<?php echo base_url('admin/sertifikat/cetak/') ?>';
	function periksa(t){
		_ajax({
			url:'<?php echo base_url('admin/sertifikat/periksa') ?>',
			data:$(t).serialize(),
			loading:'.xloading',
			success:function(res){
				res = JSON.parse(res);
				if(res.status==true){
					var data = res.data[0];
					if(data.LEVEL == 'mahasiswa'){
						$('.sertifikat .nama').text(data.NAMAMHS);
						$('.sertifikat .npm').text(data.NPM);
					}else{
						$('.sertifikat .nama').text(data.NAMADPL);
					}
					$('.sertifikat .kodeqr').text(data.KODE);
					$('.sertifikat .kodematch').text(data.KUNCIKODE);
					$('.sertifikat .nomor').text(data.NOMOR);
					$('.sertifikat .sebagai').html(data.SEBAGAI);
					createQr(data);
				}else{
					$('.sertifikat').hide();
					_alert('Kesalaha',res.data,'error');
				}
			}
		})
		return false;
	}

	function createQr(data){
		var plaintext = '';
		plaintext += 'KODE SERTIFIKAT = '+data.KODE+'-'+data.KUNCIKODE+'\n';
		if(data.LEVEL == 'mahasiswa'){
			plaintext += 'NPM = '+data.NPM+'\n';
			plaintext += 'NAMA = '+data.NAMAMHS+'\n';
			plaintext += 'PRODI = '+data.FAKPRODI+' / '+data.NAMAPRODI+'\n';
			plaintext += 'TANGGAL = '+data.TANGGAL+'\n';
			plaintext += 'REGISTER = '+data.TGLREGMHS+'\n';
			plaintext += 'DIBUAT = '+data.TANGGAL+'\n';
			plaintext += 'TERDAFTAR = '+data.TGLREGMHS+'\n';
		}else{
			plaintext += 'KODE DOSEN = '+data.KDDPL+'\n';
			plaintext += 'NAMA = '+data.NAMADPL+'\n';
			plaintext += 'DIBUAT = '+data.TANGGAL+'\n';
			plaintext += 'PERIODE = '+data.AKADEMIK+'\n';
		}
		// plaintext += 'KELOMPOK = '+data.NAMAKEL+'\n';
		// plaintext += 'TAHUN KKN = '+data.NAMATAHUN+'\n';
		// plaintext += 'ALAMAT KELOMPOK = '+data.ALAMATKEL+'\n';
		// plaintext += 'KAMPUS = UNIVERSITAS MADURA';

		$('#qrcode').empty();
		$('#qrcode').qrcode({
	    render: 'canvas',
	    minVersion: 1,
	    maxVersion: 40,
	    ecLevel: 'L',
	    left: 0,
	    top: 0,
	    size: 80,
	    fill: '#000',
	    background: '#fff',
	    text: plaintext,
	    radius: 0,
	    quiet: 0,
	    mode: 0,
	    mSize: 0.1,
	    mPosX: 0.5,
	    mPosY: 0.5,
	    label: 'no label',
	    fontname: 'sans',
	    fontcolor: '#000',
	    image: null
		});
		$('.sertifikat').show();
	}
</script>