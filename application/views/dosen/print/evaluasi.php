<div class="container mt-2">
	<?php 
	$this->load->view('dosen/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary">LAPORAN HASIL MONITORING <br> DOSEN PENDAMPING LAPANGAN (DPL)<br> KULIAH KERJA NYATA (KKN) UNIVERSITAS MADURA<br> TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></div>
	*KELOMPOK <?php echo strtoupper($kelompok->NAMAKEL); ?>
			<table class="table table-striped table-bordered">
		    	<tdead>
		    		<tr>
		    			<th>#</th>
		    			<th>Tanggal</th>
		    			<th>Tanggapan Masyarakat</th>
		    			<th>Hasil Survei Mahasiswa</th>
		    			<th>Kendala Yang Dihapadi Masyarakat</th>
		    			<th>Saran</th>
		    		</tr>
		    	</tdead>
		    	<tbody>
		    		<?php
		    		$i=1;
		    		foreach ($data as $key => $value) {
		    			?>
		    			<tr>
		    				<td><?php echo $i++; ?></td>
		    				<td><?php echo $this->mfungsi->tgl($value->TGLEV); ?></td>
		    				<td><?php echo $value->TANGGAPANEV; ?></td>
		    				<td><?php echo $value->SURVEIEV; ?></td>
		    				<td><?php echo $value->KENDALAEV; ?></td>
		    				<td><?php echo $value->SARANEV; ?></td>
		    			</tr>
		    			<?php
		    		}
		    		?>
		    	</tbody>
		    </table>
<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-4">
		<div class="font-weight-bold">
			Kepala Desa,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . .)
		</div>
	</div>
	<div class="col-4">
		<div class="font-weight-bold">
			KDPL,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . .)
		</div>
	</div>
	<div class="col-4">
		<div class="font-weight-bold">
			Dosen Pendamping Lapangan,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>