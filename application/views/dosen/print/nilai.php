<div class="container mt-2">
	<?php 
	$this->load->view('dosen/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary"><u>DAFTAR NILAI MAHASISWA KULIAH KERJA NYATA<br>UNIVERSITAS MADURA TAHUN AKADEMIK TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></u></div>


	    <table class="table table-bordered table-striped">
	    	<tdead>
	    		<tr>
	    			<th>#</th>
	    			<th>NPM</th>
	    			<th>NAMA</th>
	    			<th>FAK / PRODI</th>
	    			<th>NILAI</th>
	    		</tr>
	    	</tdead>
	    	<tbody>
	    		<?php
	    		$i = 1;
	    		foreach ($data as $key => $value) {
	    		?>
	    			<tr>
		    			<td><?php echo $i++; ?></td>
		    			<td><?php echo $value->NPM; ?></td>
		    			<td><?php echo $value->NAMAMHS; ?></td>
		    			<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
		    			<td><?php echo $value->NILAIMHS; ?></td>
		    		</tr>
	    		<?php
	    		}
	    		?>
	    	</tbody>
	    </table>

<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-5 offset-7">
		<div class="font-weight-bold">
			Ketua / Koordinator Kegiatan,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>