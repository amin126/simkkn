<div class="container mt-2">
	<?php 
	$this->load->view('dosen/print/kop');
?>
	<div class="mt-2 mb-2 text-center card-outline-secondary"><u>JADWAL KEGIATAN<br>KULIAH KERJA NYATA KELOMPOK <?php echo $data[0]->NAMAKEL; ?> <br>TAHUN AKADEMIK <?php echo $this->mfungsi->tahun()->label; ?></u></div>

			<table class="mt-3 table table-striped table-bordered">
		    	<tdead>
		    		<tr>
		    			<th>#</th>
		    			<th>Tanggal</th>
		    			<th>Judul Kegiatan</th>
		    			<th>Tempat Kegiatan</th>
		    			<th>Anggaran (Rp)</th>
		    			<th>Pelaksana</th>
		    			<th>Penanggung Jawab</th>
		    		</tr>
		    	</tdead>
		    	<tbody>
		    		<?php
		    		$i=1;
		    		foreach ($data as $key => $value) {
		    			?>
		    			<tr>
			    			<td><?php echo $i++; ?></td>
			    			<td><?php echo $this->mfungsi->tgl($value->TGLRK); ?></td>
			    			<td><?php echo $value->NAMARK; ?></td>
			    			<td><?php echo $value->TEMPATRK; ?></td>
			    			<td><?php echo number_format($value->ANGGARANRK); ?></td>
			    			<td><?php echo $value->PELAKSANARK; ?></td>
			    			<td><?php echo $value->NPM.' / '.$value->NAMAMHS; ?></td>
			    		</tr>
		    			<?php
		    		}
		    		?>

		    	</tbody>
		    </table>
<div class="row mt-5">
	<div class="col-12">
		<div class="col-5 offset-7">
			Pamekasan, . . . . . . . . . . . . . . . . . . . . . . . . . .  <?php echo date('Y'); ?>
		</div>
	</div>
	<div class="col-7">
		<div class="font-weight-bold">
			Dosen Pendamping Lapangan,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
	<div class="col-5">
		<div class="font-weight-bold">
			Ketua / Koordinator Kegiatan,
		</div><br><br>
		<div class="mt-5">
			( . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .)
		</div>
	</div>
</div>