<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="card">
		  <div class="card-body">
		  		<form action="<?php echo base_url('dosen/bio/update'); ?>" class="row" method="post">
		      	<div class="col-md-4 fotoloading">
			    		<img src="<?php echo $this->session->foto.'?'.rand(111,999); ?>" alt="NO FOTO" class="img-thumbnail w-100 fotodisplay">
			    		<div class="w-100 progress-status mb-4 mt-2" style="display: none;">
			    			<div class="progress">
								  <div class="progress-bar" st role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
			    		</div>
			    		<div class="w-100 text-center opsi-foto mb-4 mt-2">
			    			<a href="#!" onclick="return defaultfoto()" class="text-danger">Default</a>&nbsp;.&nbsp;
			    			<label style="cursor:pointer;" class="btn-link">
								    <span class="text-primary">Ganti</span><input autocomplete="off"  type="file" onchange="gantifoto(this)" id="foto" hidden accept="image/jpeg">
								</label>
			    		</div>
			    	</div>
			    	<div class="col-md-8">
				     	<div class="form-group">
				        <label for="npm">USERNAME</label>
				        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $data[0]->KDDPL; ?>" placeholder="Username">
				      </div>

				      <div class="form-group">
				        <label for="npm">NAMA <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
				        <input autocomplete="off"  type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" placeholder="Nama">
				      </div>

				      <div class="form-group">
				        <label for="npm">KONTAK <?php echo form_error('kontak','<small class="text-danger">','</small>'); ?></label>
				        <input autocomplete="off"  type="text" class="form-control" name="kontak" value="<?php echo $kontak; ?>" placeholder="Kontak">
				      </div>

				      <div class="form-group">
				        <label for="npm">ALAMAT <?php echo form_error('alamat','<small class="text-danger">','</small>'); ?></label>
				        <input autocomplete="off"  type="text" class="form-control" name="alamat" value="<?php echo $alamat; ?>" placeholder="Alamat">
				      </div>
				    </div>
			      <div class="mt-5 xloading pull-right">
			        <button class="btn ml-2 btn-primary font-weight-medium" type="submit">Simpan</button>
			      </div>
			    </form>
		    
		  </div>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	function gantifoto(t){
  	_upload({
  		action:'<?php echo base_url('dosen/bio/foto/ganti') ?>',
  		name:'foto',
  		progress:function(res){
  			$('.opsi-foto').hide();
  			$('.progress-status').show();
  			$('.progress-status .progress-bar').css('width', res.percent+'%');
  		},
  		complete:function(res){
  			console.log(res);
  			res = JSON.parse(res);
  			$('.opsi-foto').show();
  			$('.progress-status').hide();
  			if(res.status==true){
	  			$('.fotodisplay').attr('src', res.foto);
	  			$('.progress-status .progress-bar').css('width', '0%');
	  		}else{
	  			alert(res.foto);
	  		}
  		}
  	})
  }
	function defaultfoto(){
  	_ajax({
  		url:'<?php echo base_url('dosen/bio/foto/default') ?>',
  		loading:'.fotoloading',
  		append:false,
  		success:function(res){
  			res = JSON.parse(res);
  			$('.fotodisplay').attr('src', res.foto);
  		}
  	})
  }
</script>