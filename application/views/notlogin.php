<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin-free/jquery/pages/samples/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 07:02:26 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Help Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/simple-line-icons/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <style type="text/css">
    .auth form .form-group i{
      position: unset;
    }
  </style>
</head>

<body class="bg-info">
  
  <div class="row justify-content-center">
    <div class="col-md-8 col-lg-5 mt-5">
      <center class="mb-3 h2 text-white font-weight-bold">Bantuan Masuk</center>
      <div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Admin
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <p>Apabila anda mengalami kesulitan untuk masuk karena lupa password atau username maka tidak ada cara lain selain menghubungi pengembang sistem atau server domain.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Dosen Pendamping Lapangan (DPL)
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <p>
          Dosen pendamping lapangan yang dikenan untuk bisa login ke sistem harus memenuhi syarat sebagai berikut
          <ul>
            <li>Harus terdaftar sebagai DPL Kuliah Kerja Nyata tahun akademik <?php echo $this->mfungsi->tahun()->label; ?></li>
            <li>Harus mengetahui username dan password akun <a href="http://simat.unira.ac.id" target="_blank" class="btn-link">SIMAT UNIRA</a> (username dan password yang digunakan pada saat login SIMAT)</li>
          </ul>
        </p>
        <p>Apabila anda tetap tidak dapat login ke sistem, silahkan menghubungi Tim LPPM Universitas Madura</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Mahasiswa
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <p>
          Mahasiswa yang dikenan untuk bisa login ke sistem harus memenuhi syarat sebagai berikut
          <ul>
            <li>Harus KRS/memprogram matakuliah Kuliah Kerja Nyata tahun akademik <?php echo $this->mfungsi->tahun()->label; ?></li>
            <li>Matakuliah Kuliah Kerja Nyata tahun <?php echo $this->mfungsi->tahun()->label; ?> yang sudah di KRS harus divalidasi oleh dosen wali</li>
            <li>Gunakan NPM dan password pada saat anda login ke <a href="http://simat.unira.ac.id" target="_blank" class="btn-link">SIMAT UNIRA</a>, apabila anda lupa dengan password anda, silahkan untuk menghubungi SIMAT UNIRA dengan tujuan untuk memperoleh password baru</li>
          </ul>
        </p>
        <p>Apabila anda tetap tidak bisa login silahkan menghubungi Tim LPPM Universitas Madura</p>
      </div>
    </div>
  </div>
</div>
    </div>
  </div>


  <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url('assets/'); ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/misc.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>
  <script type="text/javascript">
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('mhs/daftar/alamat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#alamatvalid',
      append:true,
      success:function(res){
        res = JSON.parse(res);
        if(res.result.text==null){
          _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
        }else{
          $('#alamat').val(res.result.text);  
        }        
      }
    })
  }
</script>
  <!-- endinject -->
</body>
</html>
