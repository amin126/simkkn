<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="card">
		  <div class="card-body">
		  		<form action="<?php echo base_url('camat/bio/update'); ?>" method="post">
		      
			     	<div class="form-group">
			        <label for="npm">USERNAME</label>
			        <input autocomplete="off"  type="text" class="form-control" readonly=""  value="<?php echo $usern; ?>" placeholder="Username">
			      </div>

			      <div class="form-group">
			        <label for="npm">NAMA <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
			        <input autocomplete="off"  type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" placeholder="Nama">
			      </div>

			      <div class="alert alert-warning" role="alert">
              Jika ingin mengganti password silahkan diisi dibawah ini
            </div>

			      <div class="form-group">
              <label for="pass">Password </label>
              <input autocomplete="off"  type="password" class="form-control" id="pass" value="<?php echo $pass; ?>"  placeholder="Password" name="pass">
            </div>

            <div class="form-group">
              <label for="rpass">Ulangi password <?php echo form_error('rpass','<small class="text-danger">','</small>'); ?></label>
              <input autocomplete="off"  type="password" class="form-control" id="rpass" value="<?php echo $rpass; ?>"  placeholder="Ulangi Password" name="rpass">
            </div>

			      <div class="mt-5 xloading pull-right">
			        <button class="btn btn-primary font-weight-medium" type="submit">Simpan</button>
			      </div>
			    </form>
		    
		  </div>
		</div>
		
	</div>
</div>
