<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/camat.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mod.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <style type="text/css">
    .xlistKKN{
      color:#555;
    }
    .xlistKKN:hover{
      color:#555;
      text-decoration: unset;
    }
  </style>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>

<body>

<div class="modal fade" id="kelompokpilih" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Kelompok</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="list-group">
          <?php
          $thn = $this->mfungsi->tahun()->kode;
          $this->db->where('KDTAHUN',$thn);
          $this->db->order_by('NAMAKEL ASC');
          $data = $this->db->get('kelompok')->result();
          foreach ($data as $key => $value) {
            if($value->KDKEL==$this->session->dpl_kkn_aktif)
              echo '<a class="list-group-item list-group-item-action active"><b class="text-white">'.$value->NAMAKEL.'</b> <span class="text-muted">'.$value->ALAMATKEL.'</span></a>';
            else
              echo '<a href="'.base_url('camat/set/kkn/').$value->KDKEL.'" class="list-group-item list-group-item-action"><b>'.$value->NAMAKEL.'</b> <span class="text-muted">'.$value->ALAMATKEL.'</span></a>';
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>


  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>"><span style="
    color: #f6f8fa;
">SIM</span><span style="
    color:  red;
    font-weight:  bold;
">KKN</span></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'); ?>images/logobw.png" style="width: 40px;height: 40px;" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex" style="width: 70%;">
          <li class="nav-item">
            <?php
              $menu = $this->uri->segment(2);
              $submenu = $this->uri->segment(3);
              switch ($menu) {
                case 'profil':
                    echo 'DATA PROFIL KELOMPOK KKN';
                break;
                case 'upload':
                    echo 'FILE UPLOAD MAHASISWA';
                break;
                case 'kegiatan':
                    echo 'DATA RENCANA KEGIATAN KKN';
                break;
                case 'evaluasi':
                  echo 'EVALUASI MINGGUAN';
                break;
                case 'nilai':
                  echo 'PENILAIAN MAHASISWA';
                break;
                case 'bio':
                  echo 'PROFIL DATA DIRI DOSEN';
                break;
                default:
                  echo profil('aplikasi');
                  break;
              }
            ?>
          </li>
        </ul>
        <?php
        $this->db->where('KDDPL',$this->session->usern);
        ?>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img class="img-xs rounded-circle" src="<?php echo base_url('assets/'); ?>images/faces/user.png" alt="">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <a class="dropdown-item preview-item" href="<?php echo base_url('camat/bio'); ?>" data-toggle="modal" data-target="#kelompokpilih">
                KKN&nbsp;&nbsp;<span class="text-muted">(<?php echo $this->session->dpl_kkn_nama; ?>)</span>
              </a>
              <a class="dropdown-item preview-item" href="<?php echo base_url('camat/bio'); ?>">
                Akun&nbsp;&nbsp;<span class="text-muted">(<?php echo $this->session->usern; ?>)</span>
              </a>
              <a class="dropdown-item preview-item" href="<?php echo base_url('keluar'); ?>">
                Keluar
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="fa fa-bars"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item <?php echo ($menu=='profil') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('camat/profil'); ?>"><i class="menu-icon fa fa-sticky-note"></i><span class="menu-title">Profil KKN</span></a></li>
         <!--  <li class="nav-item <?php echo ($menu=='nilai') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('camat/nilai'); ?>"><i class="menu-icon fa fa-thumbs-up"></i><span class="menu-title">Nilai Mahasiswa</span></a></li> -->
          <li class="nav-item <?php echo ($menu=='upload') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('camat/upload'); ?>"><i class="menu-icon fa fa-upload"></i><span class="menu-title">File Uploaded</span></a></li>
          <!-- <li class="nav-item <?php echo ($menu=='kegiatan') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('camat/kegiatan'); ?>"><i class="menu-icon fa fa-calendar"></i><span class="menu-title">Jadwal Kegiatan</span></a></li>
          <li class="nav-item <?php echo ($menu=='evaluasi') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('camat/evaluasi'); ?>"><i class="menu-icon fa fa-check"></i><span class="menu-title">Evaluasi Mingguan</span></a></li> -->
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
