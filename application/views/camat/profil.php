<div class="card">
  <div class="card-body">
    <center><h3>PROFIL KELOMPOK KKN</h3></center>
    <div class="row">
		<div class="col-md-3">
				<b>NAMA KELOMPOK KKN</b>
		</div>
		<div class="col-md-9">
			<?php echo $data->NAMAKEL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>ALAMAT KKN</b>
		</div>
		<div class="col-md-9">
			<?php echo $data->ALAMATKEL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>DPL KKN</b>
		</div>
		<div class="col-md-9">
			<?php echo $data->NAMADPL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>KONTAK DPL</b>
		</div>
		<div class="col-md-9">
			<?php echo $data->KONTAKDPL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>
		<div class="col-md-12">
			<center class="mt-3"><h3>DAFTAR ANGGOTA KKN (MAHASISWA)</h3></center>
			<table class="table datatable table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Npm</th>
						<th>Nama</th>
						<th>Fak / Prodi</th>
						<th>No HP</th>
						<th>Alamat</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
						foreach ($anggota as $key => $value) {
							?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><?php echo $value->NPM; ?></td>
								<td><?php echo $value->NAMAMHS; ?></td>
								<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
								<td><?php echo $value->KONTAKMHS; ?></td>
								<td><?php echo $value->ALAMATMHS; ?></td>
							</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			info:false,
			paging:false,
			scrollX:true
		})
	});
</script>