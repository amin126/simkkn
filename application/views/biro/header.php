<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/font-awesome/css/font-awesome.min.css" />

  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/bootstrapselect/css/bootstrap-select.min.css">

  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/biro.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mod.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <style type="text/css">
    .xlistKKN{
      color:#555;
    }
    .xlistKKN:hover{
      color:#555;
      text-decoration: unset;
    }
  </style>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>"><span style="
    color: #f6f8fa;
">SIM</span><span style="
    color:  red;
    font-weight:  bold;
">KKN</span></a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'); ?>images/logobw.png" style="width: 40px;height: 40px;" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex" style="width: 70%;">
          <li class="nav-item">
            <?php
              $menu = $this->uri->segment(2);
              $submenu = $this->uri->segment(3);
              switch ($menu) {
                case 'mahasiswa':
                    echo 'DATA MAHASISWA UNIVERSITAS MADURA';
                break;
                case 'nonordik':
                    echo 'DATA MAHASISWA TIDAK MENGIKUTI ORDIK';
                break;
                case 'pengaturan':
                    echo 'PENGATURAN SISTEM';
                break;
                case 'sertifikat':
                  if($submenu=='peserta')
                    echo 'ELEKTRONIK SERTIFIKAT ORDIK';
                  else
                    echo 'PERIKSA KODE SERTIFIKAT';
                break;
                default:
                  echo profil('aplikasi');
                  break;
              }
            ?>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <img class="img-xs rounded-circle" src="<?php echo base_url('assets/'); ?>images/faces/user.png" alt="">
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <a class="dropdown-item preview-item text-muted"><?php echo $this->session->nama.' ('.$this->session->usern.')'; ?></a>
              <a class="dropdown-item preview-item" href="<?php echo base_url('keluar'); ?>">
                Keluar
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="fa fa-bars"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item <?php echo ($menu=='mahasiswa') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('biro/mahasiswa'); ?>"><i class="menu-icon fa fa-sticky-note"></i><span class="menu-title">Mahasiswa</span></a></li>
          <li class="nav-item <?php echo ($menu=='nonordik') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('biro/nonordik'); ?>"><i class="menu-icon fa fa-ban"></i><span class="menu-title">Non Ordik</span></a></li>
<!--           <li class="nav-item <?php echo ($menu=='sertifikat') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('biro/sertifikat'); ?>"><i class="menu-icon fa fa-certificate"></i><span class="menu-title">E-Sertifikat</span></a></li> -->
          <li class="nav-item <?php echo ($menu=='sertifikat') ? 'active' : ''; ?>">
            <a class="nav-link" data-toggle="collapse" href="#menu-sertifikat" aria-expanded="false" aria-controls="menu-sertifikat"> <i class="menu-icon fa fa-certificate"></i> <span class="menu-title">E-Sertifikat</span><i class="menu-arrow"></i></a>
            <div class="collapse <?php echo ($menu=='sertifikat') ? 'show' : ''; ?>" id="menu-sertifikat">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='peserta') ? 'active' : ''; ?>" href="<?php echo base_url('biro/sertifikat/peserta'); ?>">Mahasiswa</a></li>
                <li class="nav-item"> <a class="nav-link <?php echo ($submenu=='cek') ? 'active' : ''; ?>" href="<?php echo base_url('biro/sertifikat/cek'); ?>">Cek E-Sertifikat</a></li>
              </ul>
            </div>
          </li>

          <li class="nav-item <?php echo ($menu=='pengaturan') ? 'active' : ''; ?>"><a class="nav-link" href="<?php echo base_url('biro/pengaturan'); ?>"><i class="menu-icon fa fa-wrench"></i><span class="menu-title">Pengaturan</span></a></li>
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
