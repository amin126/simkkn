<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Mahasiswa Non Ordik</h4>
    <h6 class="card-subtitle mb-2 text-muted">Data mahasiswa yang ada dibawah ini tidak dapat mengikuti KKN</h6>
    <div class="row">
	    <div class="col col-12 mt-3">
		    <table class="table datatable table-bordered table-striped .xloading">
		    	<thead>
		    		<tr>
		    			<th>#</th>
		    			<th>NPM</th>
		    			<th>NAMA</th>
		    			<th>TELP</th>
		    			<th>ALAMAT</th>
		    			<th>AKSI</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<?php
		    		$i = 1;
		    		foreach ($data as $key => $value) {
		    			$txtdgr = '';
		    		?>
		    			<tr class="<?php echo $txtdgr; ?> ls-mhs" id="mhs<?php echo $i; ?>">
			    			<td><?php echo $i++; ?></td>
			    			<td><?php echo $value->NPMORD; ?></td>
			    			<td><?php echo $value->NAMAORD; ?></td>
			    			<td><?php echo $value->HPORD; ?></td>
			    			<td><?php echo $value->ALAMATORD; ?></td>
			    			<td>
			    				<a href="#!" class="text-danger" onclick="hapus('<?php echo $value->NPMORD; ?>',this)">Hapus</a>
			    			</td>
			    		</tr>
		    		<?php
		    		}
		    		?>
		    	</tbody>
		    </table>
		  </div>
		</div>
  </div>
</div>
<script type="text/javascript">
	var tabel;
	$(document).ready(function() {
		tabel = $('.datatable').DataTable({
      scrollX:true
    });
	});
	function hapus(data,t){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'Mahasiswa dapat mengikuti KKN apabila data ini dihapus, klik Ya untuk melanjutkan',
			yes:'Ya, lanjutkan!',
			no:'Tidak',
			isConfirm:function(){
				//window.location = url;
				_ajax({
					url:'<?php echo base_url('biro/nonordik/batal'); ?>',
					data:{
						kode:data
					},
					loading:'.xloading',
					success:function(res){
						console.log(res);
						tabel
		        .row( $(t).parents('tr') )
		        .remove()
		        .draw();
					}
				})
			}
		})
	}
</script>