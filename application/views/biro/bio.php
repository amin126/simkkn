<div class="row justify-content-center">
	<div class="col-md-6">
		<div class="card">
		  <div class="card-body">
		  		<form action="<?php echo base_url('dosen/bio/update'); ?>" method="post">
		      
			     	<div class="form-group">
			        <label for="npm">USERNAME</label>
			        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $data[0]->KDDPL; ?>" placeholder="Username">
			      </div>

			      <div class="form-group">
			        <label for="npm">NAMA <?php echo form_error('nama','<small class="text-danger">','</small>'); ?></label>
			        <input autocomplete="off"  type="text" class="form-control" name="nama" value="<?php echo $nama; ?>" placeholder="Nama">
			      </div>

			      <div class="form-group">
			        <label for="npm">KONTAK <?php echo form_error('kontak','<small class="text-danger">','</small>'); ?></label>
			        <input autocomplete="off"  type="text" class="form-control" name="kontak" value="<?php echo $kontak; ?>" placeholder="Kontak">
			      </div>

			      <div class="form-group">
			        <label for="npm">ALAMAT <?php echo form_error('alamat','<small class="text-danger">','</small>'); ?></label>
			        <input autocomplete="off"  type="text" class="form-control" name="alamat" value="<?php echo $alamat; ?>" placeholder="Alamat">
			      </div>

			      <div class="mt-5 xloading pull-right">
			        <button class="btn btn-primary font-weight-medium" type="submit">Simpan</button>
			      </div>
			    </form>
		    
		  </div>
		</div>
		
	</div>
</div>
