<div class="row">
    <div class="col">
        <div class="card">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="<?php echo base_url('biro/pengaturan/update') ?>" method="post" id="settingForm">
                        <div class="alert alert-info xloading">
                            <div class="form-group">
                                <label>Singkron data mahasiswa dengan SIMAT UNIRA</label>
                                <a href="#!" onclick="singkronData()" class="btn btn-lg btn-primary btn-block">Singkron Data Mahasiswa</a>
                                <small  class="form-text text-muted">Hal ini dilakukan apabila data mahasiswa pada SIMAT UNIRA terdapat perubahan, data diperlukan pada saat pembuatan sertifikat PKKMB</small>
                            </div>
                        </div>

                        <div class="alert alert-info">
                            <div class="form-group">
                                <label>Pesan Singkat <?php echo form_error('pesan','<small class="text-danger">','</small>'); ?></label>
                                <textarea class="form-control" aria-describedby="username" placeholder="Pesan Singkat" name="pesan"><?php echo $pesan; ?></textarea>
                                <small  class="form-text text-muted">Pesan singkat ditujukan kepada mahasiswa yang tidak dapat mengikuti kegiatan KKN</small>
                            </div>
                        </div>

                        <div class="alert alert-info">
                            <p class="text-info">
                                <strong>Tips</strong> Untuk melengkapi tanda tangan, nama dan nomor pokok seperti "NIP", "NPM" pada masing-masing tanda tangan, isi form <b class="text-primary">REKTOR UNIVERSITAS MADURA|NAMA_REKTOR|NIP: 01234575</b> yang dipisah dengan simbol <b class="text-primary">|</b>
                            </p>


                            <div class="form-group">
                                <label>Pola Nomor Sertifikat <?php echo form_error('nomor','<small class="text-danger">','</small>'); ?></label>
                                <input autocomplete="off" class="form-control" placeholder="Pola Nomor" name="nomor" value="<?php echo $nomor; ?>">
                                <small  class="form-text text-muted">jangan membuang "%nomor%" dan "%tahun%". kode tersebut akan diisi secara otomatis</small>
                            </div>
                            <div class="form-group">
                                <label>Tgl Pelaksaan PKKMB</label>
                                <table class="table table-bordered table-sm bg-white">
                                    <tr>
                                        <th>Akademik</th>
                                        <th>Pelaksanaan</th>
                                        <th width="88px">Opsi</th>
                                    </tr>
                                    <?php 
                                    foreach ($pelaksana_ordik as $key => $value) 
                                        { 
                                            if($value->AKDODK!='default'){
                                    ?>
                                        <tr id="tr-<?php echo $value->KDPELAKSANA; ?>">
                                            <td><?php echo $value->AKDODK; ?></td>
                                            <td><?php echo $value->MSGODK; ?></td>
                                            <td><a href="#!" onclick="hapusPelaksana('<?php echo $value->KDPELAKSANA; ?>')" class="text-danger">Hapus</a></td>
                                        </tr>
                                    <?php 
                                            }else{
                                                ?>
                                        <tr id="tr-default">
                                            <td>DEFAULT</td>
                                            <td><textarea id="edit-default" class="form-control" style="display: none;"></textarea><span id="msg-default"><?php echo $value->MSGODK; ?></span></td>
                                            <td>
                                                <a href="#!" onclick="editDefault()" id="btn-edit-default" class="text-success">Edit</a>
                                                <a href="#!" onclick="simpanDefault()" id="btn-simpan-default" class="text-primari" style="display: none;">Simpan</a>
                                            </td>
                                        </tr>
                                                <?php
                                            }
                                        }
                                    ?>
                                    
                                    
                                </table>
                                <a href="#!" class="btn-link btn" onclick="tambahPelaksanaan()" id="btn-tambah-pelaksana">Tambah</a>
                                <script type="text/javascript">

                                    function hapusPelaksana(kode){
                                        var c = confirm('Apakah akan dihapus?');
                                        if(c!=0){
                                            
                                            _ajax({
                                                url:'<?php echo base_url('biro/pengaturan/hapuspelaksanaan') ?>',
                                                data:{
                                                    kode:kode
                                                },
                                                loading:'.navbar',
                                                success:function(res){
                                                    $('#tr-'+kode).remove();
                                                }
                                            })
                                        }
                                    }

                                    function editDefault(){
                                        $('#tambahPelaksanaan').remove();
                                        $('#btn-tambah-pelaksana').hide('fast');
                                        $('#edit-default').show('fast');
                                        $('#edit-default').val($('#msg-default').text());
                                        $('#msg-default').hide('fast');
                                        $('#btn-edit-default').hide('fast');
                                        $('#btn-simpan-default').show('fast');
                                    }

                                    function simpanDefault(){
                                        _ajax({
                                            url:'<?php echo base_url('biro/pengaturan/updatepelaksanaandefault') ?>',
                                            data:{
                                                pesan:$('#edit-default').val()
                                            },
                                            loading:'.navbar',
                                            success:function(res){
                                                $('#btn-tambah-pelaksana').show('fast');
                                                $('#edit-default').hide('fast');
                                                $('#msg-default').text($('#edit-default').val());
                                                $('#msg-default').show('fast');
                                                $('#btn-edit-default').show('fast');
                                                $('#btn-simpan-default').hide('fast');
                                            }
                                        })

                                    }
                                    function tambahPelaksanaan(t){
                                        var d = new Date();
                                        var n = parseInt(d.getFullYear());
                                        var html = '<tr id="tambahPelaksanaan" class="font-weight-bold">'+
                                                        '<td>'+
                                                         '<select id="akademik-dropdown">';
                                                                
                                                        for (var i = n; i >=n-7; i--) {
                                                            html +='<option>'+i+'/'+(i+1)+'</option>';
                                                        }

                                                            html +='</select>'+
                                                        '</td>'+
                                                        '<td><input class="form-control" id="msg-pelaksanaan" placeholder="21 Agustus s/d 04 September 2018"></td>'+
                                                        '<td><a href="#1" class="text-warning" onclick="batalTambah()">Batal</a>&nbsp;.&nbsp;<a href="#!" class="text-primary" onclick="simpanTambah()">Simpan</a></td>'+
                                                    '</tr>';
                                        $('#tr-default').before(html);
                                        $('#btn-tambah-pelaksana').hide('fast');
                                        $('#msg-pelaksanaan').focus();
                                    }

                                    function batalTambah(){
                                        $('#tambahPelaksanaan').remove();
                                        $('#btn-tambah-pelaksana').show('fast');
                                    }

                                    function simpanTambah(){
                                        _ajax({
                                            url:'<?php echo base_url('biro/pengaturan/pelaksanaan') ?>',
                                            data:{
                                                akademik:$('#akademik-dropdown').val(),
                                                pelaksana:$('#msg-pelaksanaan').val()
                                            },
                                            loading:'.navbar',
                                            success:function(res){
                                                res = JSON.parse(res);
                                                if(res.status==true){
                                                    var html = '<tr id="tr-'+res.kode+'">'+
                                                                '<td>'+res.akademik+'</td>'+
                                                                '<td>'+res.pesan+'</td>'+
                                                                '<td><a href="#!" onclick="hapusPelaksana(\''+res.kode+'\')" class="text-danger">Hapus</a></td>'+
                                                            '</tr>';
                                                    $('#tr-default').before(html);
                                                    $('#tambahPelaksanaan').remove();
                                                    $('#btn-tambah-pelaksana').show('fast');
                                                }else{
                                                    _alert('Kesalahan',res.data,'error');
                                                }
                                            }
                                        })
                                    }
                                </script>
                            </div>

                            <div class="form-group" >
                            <div class="alert alert-warning" role="alert">
                                 Login mahasiswa untuk pengambilan sertifikat PKKMB
                                      <div class="form-check form-check-flat">
                                      <label class="form-check-label">
                                        <input autocomplete="off"  type="checkbox" class="form-check-input" name="login" <?php echo ($login==1)? 'checked' : ''; ?>>
                                        Perbolehkan Login
                                      <i class="input-helper"></i></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Tanda Tangan I <?php echo form_error('rektor','<small class="text-danger">','</small>'); ?></label>
                                <input autocomplete="off" class="form-control" placeholder="Rektor" name="rektor" value="<?php echo $rektor; ?>">
                                
                                <img class="img-thumbnail w-100" id="view-ttd-rektor" src="<?php echo base_url('assets/images/signature-rektor.png?'.rand(111,999)) ?>">
                                <div id="proses-rektor" style="
                                    width:  100%;
                                    margin-top: 7px;
                                    display:  none;
                                    text-align: center;
                                ">
                                    0%
                                </div>
                                <label id="btn-ganti-rektor" class="btn-link mt-2">
                                <span>Ganti tanda tangan I (*.png, < 500Kb)</span><input autocomplete="off"  type="file" onchange="gantittd(this,'rektor')" name="file" id="rektor" hidden accept="image/png">
                                </label>
                            </div>

                            <div class="form-group">
                                <label>Tanda Tangan II <?php echo form_error('presma','<small class="text-danger">','</small>'); ?></label>
                                <input autocomplete="off" class="form-control" placeholder="Presma" name="presma" value="<?php echo $presma; ?>">
                                
                                <img class="img-thumbnail w-100" id="view-ttd-presma" src="<?php echo base_url('assets/images/signature-presma.png?'.rand(111,999)) ?>">
                                <div id="proses-presma" style="
                                    width:  100%;
                                    margin-top: 7px;
                                    display:  none;
                                    text-align: center;
                                ">
                                    0%
                                </div>
                                <label id="btn-ganti-presma" class="btn-link mt-2">
                                <span>Ganti tanda tangan II (*.png, < 500Kb)</span><input autocomplete="off"  type="file" onchange="gantittd(this,'presma')" name="file" id="presma" hidden accept="image/png">
                                </label>
                            </div>

                            <div class="form-group">
                                <label>Tanda Tangan III <?php echo form_error('panitia','<small class="text-danger">','</small>'); ?></label>
                                <input autocomplete="off" class="form-control" placeholder="Panitia" name="panitia" value="<?php echo $panitia; ?>">
                                
                                <img class="img-thumbnail w-100" id="view-ttd-panitia" src="<?php echo base_url('assets/images/signature-panitia.png?'.rand(111,999)) ?>">
                                <div id="proses-panitia" style="
                                    width:  100%;
                                    margin-top: 7px;
                                    display:  none;
                                    text-align: center;
                                ">
                                    0%
                                </div>
                                <label id="btn-ganti-panitia" class="btn-link mt-2">
                                <span>Ganti tanda tangan III (*.png, < 500Kb)</span><input autocomplete="off"  type="file" onchange="gantittd(this,'panitia')" name="file" id="panitia" hidden accept="image/png">
                                </label>
                            </div>

                            
                        </div>


                        <div class="dropdown-divider"></div>
                        <div class="alert alert-warning">
                          Password diisi apabila ingin mengganti password yang baru.
                        </div>
                        <div class="form-group" >
                            <label>Password <?php echo form_error('pass','<small class="text-danger">','</small>'); ?></label>
                            <input autocomplete="off" type="password" class="form-control" aria-describedby="Password" value="<?php echo $pass; ?>" name="pass" placeholder="Password">
                        </div>
                        <div class="form-group" >
                            <label>Ulangi Password <?php echo form_error('rpass','<small class="text-danger">','</small>'); ?></label>
                            <input autocomplete="off" type="password" name="rpass" class="form-control" aria-describedby="Ulangi Password" value="<?php echo $rpass; ?>" placeholder="Ulangi Password">
                        </div>
                        <div class="dropdown-divider"></div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function gantittd(t,e){
        $('#btn-ganti-'+e).hide('fast');
        $('#proses-'+e).text('0%');
        $('#proses-'+e).show('fast');
        _upload({
            progress:function(res){
                $('#proses-'+e).text(res.percent+'%');
            },
            complete:function(res){
                $('#btn-ganti-'+e).show('fast');
                $('#proses-'+e).hide('fast');
                res = JSON.parse(res);
                if(res.status==false){
                    _alert('Kesalahan',res.data,'error');
                }else{
                    $('#view-ttd-'+e).attr('src', res.foto);
                }
            },
            name:e,
            action:'<?php echo base_url('biro/pengaturan/ganti_ttd/'); ?>'+e
        })
    }

    function singkronData(){
        _alert({
            mode:'confirm',
            title:'Apakah akan dilanjutkan?',
            msg:'Data mahasiswa akan disingkron dari SIMAT UNIRA, hal ini memerlukan proses cukup lama, silahkan tunggu hingga selesai',
            yes:'Ya, lanjutkan!',
            no:'Tidak',
            isConfirm:function(){
               _ajax({
                url:'<?php echo base_url('biro/pengaturan/singkron'); ?>',
                success:function(res){
                    _alert('Info','Data berhasil disingkron','success');
                },
                loading:'.xloading'
               })
            }
        })
    }
</script>