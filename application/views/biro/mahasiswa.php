<div class="card">
  <div class="card-body">
    <h4 class="card-title">DATA MAHASISWA</h4>
    <h6 class="card-subtitle mb-2 text-muted">Data diperoleh dari Simat Unira</h6>
    <div class="row">
    	<div class="col-12">
    		<form class="form-inline float-right mb-2" action="<?php echo base_url('biro/mahasiswa/data/data') ?>" method="post" id="formcari">
					<div class="input-group  mr-sm-2">
				    <input autocomplete="off"  type="text" name="cari" value="<?php echo $cari; ?>" class="form-control" placeholder="NPM">
				  </div>
					<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</form>
    	</div>
    </div>
    <table class="table datatable table-bordered table-striped .xloading">
    	<thead>
    		<tr>
    			<th>#</th>
    			<th>NPM</th>
    			<th>NAMA</th>
    			<th>TELP</th>
    			<th>ALAMAT</th>
    			<th>AKSI</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		$i = (($page['self']-1)*10)+1;
    		foreach ($data as $key => $value) {
    			$txtdgr = '';
    			if(in_array($value->id, $npms)){
    				$txtdgr = 'font-wight-bold text-danger';
    			}
    		?>
    			<tr class="<?php echo $txtdgr; ?> ls-mhs" id="mhs<?php echo $i; ?>">
	    			<td><?php echo $i++; ?></td>
	    			<td><?php echo $value->id; ?></td>
	    			<td><?php echo $value->attributes->nama; ?></td>
	    			<td><?php echo $value->attributes->hp; ?></td>
	    			<td><?php echo $value->attributes->alamat; ?></td>
	    			<td>
	    				<?php
	    					if(in_array($value->id, $npms)){
	    						echo '<a class="text-danger" onclick="return status(this,0)" href="#!" data-npm="'.$value->id.'" data-nama="'.$value->attributes->nama.'"  data-id="'.$i.'" data-hp="'.$value->attributes->hp.'" data-alamat="'.$value->attributes->alamat.'">Buka</a>';
	    					}else{
	    						echo '<a class="text-primary" onclick="return status(this,1)" href="#!" data-npm="'.$value->id.'" data-nama="'.$value->attributes->nama.'" data-id="'.$i.'" data-hp="'.$value->attributes->hp.'" data-alamat="'.$value->attributes->alamat.'">Kunci</a>';
	    					}
	    				?>
	    			</td>
	    		</tr>
    		<?php
    		}
    		?>
    	</tbody>
    </table>
    <?php
     if($page['first']>10){ ?>
    <div class="justify-content-center row">
	    <nav aria-label="Page navigation example" class=" mt-3" style="margin: auto;">
			  <ul class="pagination">
			  	<?php
			  	echo '<li class="page-item"><a class="page-link" href="'.base_url('biro/mahasiswa/data/'.$page['last']).'"><<</a></li>';
			  	for($i=$page['awal']; $i<=$page['akhir']; $i++){
			  		if($i==$page['self']){
			  			echo '<li class="page-item active"><a class="page-link" href="#">'.$i.' <span class="sr-only">(current)</span></a></li>';
			  		}else{
			  			echo '<li class="page-item"><a class="page-link" href="'.base_url('biro/mahasiswa/data/'.$i).'">'.$i.'</a></li>';
			  		}
			  	}
			  	echo '<li class="page-item"><a class="page-link" href="'.base_url('biro/mahasiswa/data/'.$page['first']).'">>></a></li>';
			  	?>
			  </ul>
			</nav>
		</div>
		<?php } ?>
  </div>
</div>


<script type="text/javascript">
	function status(t,p){
		var npm = $(t).attr('data-npm');
		var nama = $(t).attr('data-nama');
		var hp = $(t).attr('data-hp');
		var alamat = $(t).attr('data-alamat');
		var id = parseInt($(t).attr('data-id'))-1;
		_ajax({
			url:'<?php echo base_url('biro/mahasiswa/set') ?>',
			data:{
				npm:npm,
				nama:nama,
				hp:hp,
				alamat:alamat,
				tipe:p
			},
			loading:'.xloading',
			success:function(res){
				if(p==1){
					$('#mhs'+id).attr('class', 'text-danger');
					$(t).attr('onclick', 'return status(this,0)');
					$(t).attr('class', 'text-danger');
					$(t).text('Buka');
				}else{
					$('#mhs'+id).removeClass('text-danger');
					$(t).attr('onclick', 'return status(this,1)');
					$(t).attr('class', 'text-primary');
					$(t).text('Kunci');
				}
			}
		})
		return false;
	}
</script>