<div class="row justify-content-center">
	<div class="col-12">
			<a onclick="_cetak(this)" href="<?php echo base_url('mhs/cetak/biodata'); ?>" class="btn btn-outline-info pull-right"><i class="fa fa-print"></i> Cetak</a>	
	</div>
	<div class="col-md-6">
		<div class="card">
		  <div class="card-body">
		  		<?php if($this->session->enable_edit_biodata==true){ ?>
		  		
				  	<div class="alert alert-info" role="alert">
				  		Anda masih diberi kesempatan untuk mengubah beberapa informasi data diri anda sebelum pembentukan kkn dilakukan!
				  	</div>
				  <?php  }else{ ?>
				  	<div class="alert alert-danger" role="alert">
				  	Biodata tidak dapat diubah setelah pendaftaran anda divalidasi oleh LPPM!				  
					</div>
				  <?php } ?>
		    <form xonsubmit="return submitform(this)" id="formreg" action="<?php echo base_url('mhs/biodata/update'); ?>" method="post">
		      <div class="form-group">
		        <label for="npm">NPM</label>
		        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->usern; ?>" id="npm" xplaceholder="NPM">
		      </div>
		      <div class="form-group">
		        <label for="nama">Nama</label>
		        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->nama; ?>" id="nama" xplaceholder="Nama">
		      </div>
		      <div class="alert alert-warning" role="alert">
									  <b>Penting..! </b>Silahkan cari alamat anda di <a href="#!" onclick="findaddress();" class="font-weight-bold">disini</a> untuk menentukan tempat tinggal anda saat ini! hal ini bertujuan agar kami dapat mempertimbangkan tempat kkn yang akan anda tempati
									</div>
		      <div class="form-group" id="alamatvalid">
		        <label for="alamat">Alamat <small class="font-italic text-warning">Desa, Kecamatan, Kabupaten</small></label>

		        <?php echo form_error('kordx','<br><small class="text-danger">','</small>'); ?>
            <?php echo form_error('kordy','<br><small class="text-danger">','</small>'); ?>
		        <input autocomplete="off"  type="text" class="form-control" value="<?php echo $alamat; ?>" id="alamat" name="alamat" xplaceholder="Alamat">

		         <input autocomplete="off"  type="hidden" required="" value="<?php echo $kordx; ?>" id="kordx" name="kordx" placeholder="lat">
             <input autocomplete="off"  type="hidden" required="" value="<?php echo $kordy; ?>" id="kordy" name="kordy" placeholder="lng">


		        <?php  echo form_error('alamat','<small class="text-danger xerralamat">','</small>'); ?>
		        <br>
		        <a href="#!" class="btn-link float-right"  onclick="findaddress();">Buka Peta</a>
		        <!-- <div class="btn-group" role="group" aria-label="Button group with nested dropdown" style="    float: right;">
              <button onclick="validasialamat();" type="button" class="btn btn-primary">Validasi Alamat</button>

              <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a class="dropdown-item" onclick="findaddress();" href="#">Buka Map</a>
                </div>
              </div>
            </div> -->


		      <br><br>
		      </div>
		      <div class="form-group">
		        <label for="prodi">Prodi</label>
		        <input autocomplete="off"  type="text" class="form-control" disabled value="<?php echo $this->session->prodi; ?>" id="prodi" name="prodi" xplaceholder="Prodi">
		      </div>
		      <div class="form-group">
		        <label for="telp">Telp <small class="font-italic text-warning">Gunakan nomor yang dapat kami hubungi</small></label>
		        <input autocomplete="off"  type="text" class="form-control" value="<?php echo $telp; ?>" id="telp" name="telp" xplaceholder="Telp">
		        <?php  echo form_error('telp','<small class="text-danger">','</small>'); ?>
		      </div>
		      <div class="form-group">
		        <label for="kerja">Pekerjaan <small class="font-italic text-warning">Biarkan apabila anda tidak bekerja</small></label>
		        <input autocomplete="off"  type="text" class="form-control" value="<?php echo $kerja; ?>" id="kerja" name="kerja" xplaceholder="Pekerjaan">
		      </div>
		      <div class="form-group">
		        <label for="sks">SKS yang sudah ditempuh <small class="font-italic text-warning">Sesuaikan dengan Transkip terbaru anda</small></label>
		        <input autocomplete="off"  type="text" class="form-control" value="<?php echo $sks; ?>" id="sks" name="sks" xplaceholder="sks">
		        <?php  echo form_error('sks','<small class="text-danger">','</small>'); ?>
		      </div>
		      <div class="form-group">
		        <label for="seragam">Ukuran seragam <small class="font-italic text-warning">Sesuaikan dengan ukuran tubuh anda</small></label>
		        <?php  echo form_error('seragam','<br><small class="text-danger">','</small>'); ?>
		        <div class="col-md-6">
		            <div class="form-group">
		              <div class="form-radio">
		                <label class="form-check-label">
		                  <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='xl') ? 'checked':''; ?> name="seragam" id="xl" value="xl">
		                  XL
		                <i class="input-helper"></i></label>
		              </div>
		              <div class="form-radio">
		                <label class="form-check-label">
		                  <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='l') ? 'checked':''; ?> name="seragam" id="l" value="l">
		                  L
		                <i class="input-helper"></i></label>
		              </div>
		              <div class="form-radio">
		                <label class="form-check-label">
		                  <input autocomplete="off"  type="radio" class="form-check-input" name="seragam" <?php echo ($seragam=='m') ? 'checked':''; ?> id="m" value="m">
		                  M
		                <i class="input-helper"></i></label>
		              </div>
		              <div class="form-radio">
		                <label class="form-check-label">
		                  <input autocomplete="off"  type="radio" class="form-check-input" <?php echo ($seragam=='s') ? 'checked':''; ?> name="seragam" id="s" value="s">
		                  S
		                <i class="input-helper"></i></label>
		              </div>
		            </div>
		          </div>
		      </div>
		      <div class="mt-5 xloading pull-right">
		        <button class="btn btn-primary font-weight-medium" type="submit">Simpan</button>
		      </div>
		    </form>
		  </div>
		</div>
		
	</div>
</div>

  <div class="modal fade" tabindex="-1" role="dialog" id="map-google">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Cari Alamat</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready(function() {
    _ajax({
      url:'<?php echo base_url('map'); ?>',
      success:function(res){
        $('#map-google .modal-body').html(res);
      }
    })
  });
  function findaddress(){
    $('#map-google').modal('show');
  }
</script>

<?php
if(isset($this->session->biodata_update)){
?>
<script type="text/javascript">
	$(document).ready(function() {
      _alert('Data berhasil di ubah');
  });
</script>
<?php
}
$this->session->unset_userdata('biodata_update');
?>
<script type="text/javascript">
		var _alamatValid = false;
  function validasialamat(){
    var alm = $('#alamat').val();
    _ajax({
      url:'<?php echo base_url('mhs/daftar/alamat'); ?>',
      data:{
        alamat:alm
      },
      loading:'#alamatvalid',
      append:true,
      success:function(res){
      	var _alamatValid = false;
        res = JSON.parse(res);
        if(res.status==false){
          _alert('Kesalahan','Alamat tidak tersedia, inputkan dalam cakupan nama kelurahan kecamatan dan kabupaten','error');
        }else{
        	_alamatValid = true; 
          _form({alamat:res.result.text})
          $('.xerralamat').text('');
        }        
      }
    })
  }
</script>