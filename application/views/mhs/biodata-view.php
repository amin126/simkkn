<table class="table table-striped table-bordered">
	<tbody>
		<tr>
			<th>Nama</th>
			<td><?php echo $bio[0]->NAMAMHS; ?></td>
		</tr>
		<tr>
			<th>NPM</th>
			<td><?php echo $bio[0]->NPM; ?></td>
		</tr>
		<tr>
			<th>Fakultas / Prodi</th>
			<td><?php echo $bio[0]->FAKPRODI.' / '.$bio[0]->NAMAPRODI; ?></td>
		</tr>
		<tr>
			<th>Nomor Kontak</th>
			<td><?php echo $bio[0]->KONTAKMHS; ?></td>
		</tr>
		<tr>
			<th>Pekerjaan</th>
			<td><?php echo ($bio[0]->PEKERJAANMHS=='') ? 'Tidak bekerja' : $bio[0]->PEKERJAANMHS; ?></td>
		</tr>
		<tr>
			<th>Alamat</th>
			<td><?php echo $bio[0]->ALAMATMHS; ?></td>
		</tr>
		<tr>
			<th>Sks yang diperoleh</th>
			<td><?php echo $bio[0]->SKSMHS; ?></td>
		</tr>
		<tr>
			<th>Ukuran seragam</th>
			<td><?php echo strtoupper($bio[0]->SERAGAMMHS); ?></td>
		</tr>
	</tbody>
</table>
<div class="alert alert-warning" role="alert">
  Apabila data ingin diubah silahkan menuju <a href="<?php echo base_url('mhs/biodata'); ?>">kesini</a>
</div>