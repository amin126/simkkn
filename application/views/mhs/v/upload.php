<?php
$cek = intval($this->mfungsi->setting('hal_pstkelompok'));
if($cek==0){
?>
<div class="card">
  <div class="card-body">
    <h4 class="card-title">Data Laporan</h4>
    
    <div class="row">
    	<?php if($periode == true){ ?>

    		<?php
        //if($this->session->tidak_lulus!=true && $this->session->mhs_lulus!=true){
        if($upload==0){
        	?>
         
    	<div class="col-12">
        <div class="alert alert-warning" role="alert">
            <ul>
                <li>Jenis file yang dapat di proses <b>".pdf"</b></li>
                <li>Ukuran file maksimal 5Mb</li>
            </ul>
        </div>
        <h4>Unggah Laporan</h4>
        <form action="<?php echo base_url('mhs/ver/upload/proses') ?>" method="post" onsubmit="return upload(this)" enctype="multipart/form-data" id="js-upload-form">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="inputEmail4">File</label>
              <input autocomplete="off"  type="file" required class="form-control" accept="application/pdf" name="file" id="file">
            </div>
            <div class="form-group col-md-2">
              <label for="inputPassword4">Jenis</label>
              <select required class="custom-select mb-2 mr-sm-2 mb-sm-0" id="jenis" name="jenis">
                <option selected value="proposal">Proposal</option>
                <option value="laporan">Laporan</option>
              </select>
            </div>
          </div>
          <div class="form-row">
              <div class="form-group col-md-7 mt-md-2">
                <label for="inputAddress">Nama File <small class="text-muted">Ganti nama file dengan sesingkat mungkin (maks 15 huruf)</small></label> 
                <input autocomplete="off" maxlength="15" style="width: 200px;" type="text" class="form-control" name="nama" id="nama" placeholder="Nama file">
              </div>
          </div>
          <div class="form-row">
              <div class="form-group col-md-7 mt-md-2">
                <label for="inputAddress">Deskripsi <small class="text-muted">Deskripsi tidak harus isi</small></label> 
                <input autocomplete="off"  type="text" class="form-control" name="deksripsi" id="deskripsi" placeholder="Deskripsi file">
              </div>
          </div>
          <div class="form-row">
              <button type="submit" oxnclick="upload()" class="btn btn-primary ml-1" id="js-upload-submit">Unggah</button>
          </div>
        </form>

    	</div>
        <?php  }
        } ?>
    	<div class="col-12 mt-2  mb-2">
            <form class="form-inline float-sm-right" action="<?php echo base_url('mhs/ver/upload/find') ?>" method="post" id="formcari">
              <input autocomplete="off"  type="text"  name="cari" class="form-control mb-2 mr-sm-2 mb-sm-0" value="<?php echo $cari; ?>" placeholder="Cari">
              <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
            </form>
    	</div>
    </div>

    <div class="row" style="overflow:  auto;">
        <div class="col-12">
            <table class="table datatable table-hover xloadfile">
                <thead>
                    <tr class="text-muted">
                        <th></th>
                        <th>File</th>
                        <th>Upload User</th>
                        <th>Size</th>
                        <th>Date</th>
                        <th>Jenis</th>
                        <th>Deskripsi</th>
                        <th>Status</th>
                        <th>Del</th>
                    </tr>
                </thead>
                <tbody>
                    
                <?php
                foreach ($data as $key => $value){ 
                    
                    $status = '';
                    if($value->NPM==$this->session->usern){
                        $del = '<a class="btn-link text-muted"><i class="fa fa-trash"></i></a>';
                        if($value->AJUKANUPL==0 && $periode == true){
                            $status = '<span class="badge badge-pill badge-info"><i>Evaluasi DPL</i></span>';
                            if($upload==0){
                                $del = '<a href="'.base_url('mhs/ver/upload/delete/').$value->KDUPL.'" onclick="return hapus(this);" class="btn-link text-danger"><i class="fa fa-trash"></i></a>';
                            }else{
                                $del = '<a class="btn-link text-muted"><i class="fa fa-trash"></i></a>';
                            }
                        }else if($value->AJUKANUPL==1){
                            $status = '<span class="badge badge-pill badge-warning"><i>Menunggu</i></span>';
                        }else if($value->AJUKANUPL==2){
                            $status = '<span class="badge badge-pill badge-danger"><i>Ditolak LPPM</i></span>';
                            $del = '<a href="'.base_url('mhs/ver/upload/delete/').$value->KDUPL.'" data-message="'.$value->MSGUPL.'" onclick="return rejectisback(this,\''.$value->KDUPL.'\');" class="btn-link text-primary"><i>Apa yang salah?</i></a>';
                        }else if($value->AJUKANUPL==4){
                            $status = '<span class="badge badge-pill badge-success"><i>Diterima LPPM</i></span>';
                        }else if($value->AJUKANUPL==3){
                            $status = '<span class="badge badge-pill badge-danger"><i>Ditolak LPPM</i></span>';
                            $del = '<a href="#!" data-message="'.$value->MSGUPL.'" onclick="return rejectiswaiting(this);" class="btn-link text-primary"><i>Apa yang salah?</i></a>';
                        }
                    }else{
                        $del = '<a class="btn-link text-muted"><i>NO USER</i></a>';
                        if($value->AJUKANUPL==0 && $periode == true){
                            $status = '<span class="badge badge-pill badge-info"><i>Evaluasi DPL</i></span>';
                        }else if($value->AJUKANUPL==1){
                            $status = '<span class="badge badge-pill badge-warning"><i>Menunggu</i></span>';
                        }else if($value->AJUKANUPL==4){
                            $status = '<span class="badge badge-pill badge-success"><i>Diterima LPPM</i></span>';
                        }else if($value->AJUKANUPL==3 || $value->AJUKANUPL==2){
                            $status = '<span class="badge badge-pill badge-danger"><i>Ditolak LPPM</i></span>';
                            $del = '<a href="#!" data-anggota="'.$value->NAMAMHS.' ('.$value->NPM.')" data-message="'.$value->MSGUPL.'" onclick="return rejected(this);" class="btn-link text-primary"><i>Apa yang salah?</i></a>';
                        }
                    }
                    $ext = explode('.', $value->FILEUPL);
                    $ext = end($ext);
                        echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
                            <td><a '.(($ext=='pdf')? 'onclick="_cetak(this)"' : '' ).' href="'.base_url('mhs/ver/upload/view/').$value->KDUPL.'" class="btn-link" title="'.$value->FILEUPL.'">'.$this->mfungsi->minimalText($value->FILEUPL,15).'</a>&nbsp;.&nbsp;<a href="'.base_url('mhs/ver/upload/download/').$value->KDUPL.'">Download</a></td>
                            <td><span title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
                            <td>'.$this->mfungsi->koversiSize($value->SIZEUPL).'</td>
                            <td>'.$this->mfungsi->tgl($value->TGLUPL,true).'</td>
                            <td>'.ucwords($value->JENISUPL).'</td>
                            <td>'.$value->DESKUPL.'</td>
                            <td>'.$status.'</td>
                            <td>'.$del.'</td>
                            </tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    
  </div>
</div>

<div class="modal fade" id="again" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Mengapa ditolak oleh LPPM?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="alert alert-warning" role="alert">
            <span id="pesan-alasan"></span>
        </div>
        <div class="row">
            <div class="col text-center" id="tindakan-lanjutan">

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			searching:true,
			info:false,
			paging:false
		})
	});
	function hapus(t){
		_alert({
			mode:'confirm',
			title:'Apakah akan dihapus?',
			msg:'',
			yes:'Ya, Hapus!',
			no:'Tidak',
			isConfirm:function(){
				window.location.href=$(t).attr('href');;
			}
		})
		return false;
	}

    function rejectisback(t,kode){
        var msg= $(t).attr('data-message');
        $('#pesan-alasan').text(msg);
        $('#tindakan-lanjutan').html('<a href="<?php  echo base_url('mhs/ver/upload/delete/'); ?>'+kode+'" id="btlajukanulang" onclick="return hapus(this);" class="btn mt-3 btn-danger">Hapus dan perbaiki</a>');
        $('#again').modal('toggle');
        return false;
    }

    function rejectiswaiting(t){
        var msg= $(t).attr('data-message');
        $('#pesan-alasan').text(msg);
        $('#tindakan-lanjutan').html('<span class="badge mt-5 badge-pill badge-warning"><i>Menunggu tindakan dari DPL anda!</i></span>');
        $('#again').modal('toggle');
        return false;
    }

    function rejected(t){
        var msg= $(t).attr('data-message');
        var agt= $(t).attr('data-anggota');
        $('#pesan-alasan').text(msg);
        $('#tindakan-lanjutan').html('<span class="badge mt-5 badge-pill badge-warning"><i>Hanya <b>'+agt+'</b> yang dapat menangani</i></span>');
        $('#again').modal('toggle');
        return false;
    }

	function upload(t){
		$('#progressUpload').remove();
		var htm = '<tr id="progressUpload"><td colspan="8"><div class="progress">'+
				  '<div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>'+
				'</div></td></tr>';
		$('.xloadfile').prepend(htm);
		_upload({
			name:['file','jenis','deskripsi','nama'],
			progress:function(res){
				$('#progressUpload .progress-bar').css('width', res.percent+'%');
				$('#progressUpload .progress-bar').text(res.percent+'%');
			},
			complete:function(res){
				res = JSON.parse(res);
				if(res.status==true){
					$('#progressUpload').remove();
					$('.xloadfile').prepend(res.data);
					$('#file').val('');
                    $('#nama').val('');
				}else{
					$('#progressUpload').html(res.data);
				}
			},
			action:$(t).attr('action')
		})
		return false;
	}
</script>
<?php
}
?>