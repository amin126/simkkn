<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIM-KKN | UNIVERSITAS MADURA</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <style type="text/css">
#btn-Convert-Html2Image,#btn-print-sertifikat,#btn-Convert-pdf{
    display: inline-block;
    position: fixed;
    top: 3mm;
    background: #a1a1f9;
    color: white;
    text-decoration: none;
    padding: 3px;
    font-size: 18pt;
    box-shadow: -7px 5px 13px #00000038;
    z-index: 999999;
	}
	#btn-Convert-pdf{
		left: 3mm;
	}
	#btn-Convert-Html2Image{
		left: 19mm;
	}
	#btn-print-sertifikat{
		left: 34.7mm;
	}
 	#btn-Convert-Html2Image:hover,#btn-print-sertifikat:hover,#btn-Convert-pdf:hover{
 		background: #4141ce;
 		box-shadow: -7px 5px 13px #000000a8;
 	}
 	

 	/* Absolute Center Spinner */
.loading {
  position: fixed;
  display: none;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 999999999999;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0, 0.77);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 1500ms infinite linear;
  -moz-animation: spinner 1500ms infinite linear;
  -ms-animation: spinner 1500ms infinite linear;
  -o-animation: spinner 1500ms infinite linear;
  animation: spinner 1500ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.5) -1.5em 0 0 0, rgba(255,255,255, 0.5) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}


	@media print{
		@page {
			size: a4 landscape;
		}
		#btn-Convert-Html2Image,#btn-print-sertifikat,#btn-Convert-pdf{
			display: none;
		}
	}

	</style>
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/qr.js');  ?>"></script>
  <script src="<?php echo base_url('assets/js/html2canvas.js');  ?>"></script>
  <noscript>
    Aktifkan javascript pada browser
    <style>div { display:none; }</style>
  </noscript>
</head>
<body xonload="self.print();window.close();">
<div class="sertifikat" style="
    width:  296.5mm;
    height:  209.5mm;
    overflow:  hidden;
">
	<img src="<?php echo base_url('assets/images/sertifikat.jpg?'.rand(111,999)) ?>" style="
    width:  100%;
">
	<div class="nomor text-center" style="
    position:  absolute;
    width:  296.5mm;
    top: 68mm;
    font-size: 18pt;
"></div>
	<div class="nama font-weight-bold text-center" style="
    position:  absolute;
    top: 96mm;
    width:  296.5mm;
    left:  0;
    font-size: 24pt;
"></div>
	<div class="npm font-weight-bold text-center" style="
    position:  absolute;
    top: 107mm;
    width:   296.5mm;
    font-size: 17pt;
"></div>
		<div class="col-12 mt-3 text-center" style="
    position: relative;
    width:  296.5mm;
    top: -90mm;
    font-size: 14pt;
    font-weight: bold;
">Sebagai peserta pada kegiatan Kuliah Kerja Nyata (KKN) <br>Universitas Madura Tahun Akademik <span class="akademik"></span>
		</div>
	<div class="bqrcode" style="
    position:  absolute;
    top: 134mm;
    left: 22mm;
    padding:  5px;
    height: 32.5mm;
    z-index:  9999;
    text-align:  center;
">
		<div id="qrcode"></div>
		<div class="kodeqr" style="
    position:  absolute;
    left: 0;
    text-align:  center;
    margin-top: 0mm;
    width: 100%;
    font-size: 9pt;
"></div>
		<div class="kodematch" style="position: absolute;margin-top: 4mm;left: -5px;font-size: 24pt;color:  #ddd;">
			
			<?php 
			$a = 0;
			for ($i=0; $i <strlen($data[0]->KUNCIKODE) ; $i++) { 
				echo '<span style="font-size:'.(12+$a).'pt;">'.$data[0]->KUNCIKODE[$i].'</span>';
				$a +=3;
			}
			?>

		</div>
	</div>
	
	<div style="
    position:  absolute;
    top: 158mm;
    left: 198mm;
    width: 85mm;
    font-weight:  bold;
    font-size:  13pt;
    text-align:  center;
">
	Pamekasan, <?php echo $this->mfungsi->tglFull(date('Y-m-d')); ?></div>

<img src="<?php echo base_url('assets/images/signature.png?'.rand(111,999)) ?>" style="
    position:  absolute;
    width: 277px;
    top: 150mm;
    left: 193mm;
    opacity:  0.9;
">

	<div style="position:  absolute;top: 165mm;left: 195mm;font-weight: bold;width: 92mm;display:  table;text-align:  center;">
		<div style="
	    font-size: 13pt;
	">
			Ketua LPPM
		</div>
		<div style="
	    margin-top: 11mm;
	    text-align:  center;
	    font-size: 14pt;
	" class="pimpinan">
			-
		</div>
		<span class="nomorpimpinan" style="
	    font-size:  11pt;
	    font-weight:  bold;
	    border-top: solid 2px;
	    position: relative;
	    padding-top: 1mm;
	"></span>
	</div>

</div>
<a id="btn-print-sertifikat" href="#" style="display: none;">PRINT</a>
<a id="btn-Convert-Html2Image" href="#" style="display: none;">JPG</a>
<a id="btn-Convert-pdf" href="#" style="display: none;">PDF</a>
<div class="loading">Loading&#8230;</div>
<script type="text/javascript">
	var element = $(".sertifikat"); // global variable
	var getCanvas; // global variable
	var wqr = 0;
	$(document).ready(function() {
		overlayaktif();
		var data = <?php echo json_encode($data) ?>;
		createQr(data[0]);
	});



	function createQr(data){
		var plaintext = '';
    plaintext += 'KODE SERTIFIKAT = '+data.KODE+'-'+data.KUNCIKODE+'\n'
		plaintext += 'NPM = '+data.NPM+'\n';
		plaintext += 'NAMA = '+data.NAMAMHS+'\n';
		plaintext += 'PRODI = '+data.FAKPRODI+' / '+data.NAMAPRODI+'\n';
		plaintext += 'DIBUAT = '+data.TANGGAL+'\n';
		plaintext += 'TERDAFTAR = '+data.TGLREGMHS+'\n';
		// plaintext += 'TAHUN KKN = '+data.NAMATAHUN+'\n';
		// plaintext += 'ALAMAT KELOMPOK = '+data.ALAMATKEL+'\n';
		// plaintext += 'KAMPUS = UNIVERSITAS MADURA';
		$('#qrcode').empty();
		$('#qrcode').qrcode({
	    render: 'table',
	    minVersion: 1,
	    maxVersion: 40,
	    ecLevel: 'L',
	    left: 0,
	    top: 0,
	    size: 120,
	    fill: '#000',
	    background: '#fff',
	    text: plaintext,
	    radius: 0,
	    quiet: 0,
	    mode: 0,
	    mSize: 0.1,
	    mPosX: 0.5,
	    mPosY: 0.5,
	    label: 'no label',
	    fontname: 'sans',
	    fontcolor: '#000',
	    image: null
		});
		$('.sertifikat .nama').text(data.NAMAMHS);
		$('.sertifikat .npm').text(data.NPM);
		$('.sertifikat .kodeqr').text(data.KODE);
		$('.sertifikat .nomor').text(data.NOMOR);
		$('.sertifikat .akademik').text(data.AKADEMIK);
		$('.sertifikat .pimpinan').text(data.PIMPINAN);
		$('.sertifikat .nomorpimpinan').text(data.NOMORPIMPINAN);
		setTimeout(function(){
			html2canvas(element, {
        onrendered: function (canvas) {
          getCanvas = canvas;
          var imgageData = getCanvas.toDataURL("image/png");
    			var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
    			$("#btn-Convert-Html2Image").attr("download", data.NPM+".jpg").attr("href", newData).show(400);
    			$('#btn-print-sertifikat,#btn-Convert-pdf').show(400);
    			$('#btn-print-sertifikat').click(function(event) {
    				self.print();
    			});
    			$('#btn-Convert-pdf').click(function(event) {
    				overlayaktif()
    				$.ajax({
    					url:'<?php echo base_url('mhs/ver/nilai/sertifikat/topdf') ?>',
    					type:'post',
    					data:{
    						img:newData,
    						orang:'affan'
    					},
    					success:function(res){
                window.location.href = '<?php echo base_url('mhs/ver/nilai/sertifikat/pdf') ?>';
    						overlaytutup();
    					}
    				})
    			});
    			overlaytutup();
        }
      });
		},1000);
	}
	function overlayaktif(){
		$('.loading').fadeIn(400);
	}
	function overlaytutup(){
		$('.loading').fadeOut(400);
	}
</script>

</body>
</html>