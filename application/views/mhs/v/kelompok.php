<?php
$cek = intval($this->mfungsi->setting('hal_pstkelompok'));
$auto = intval($this->mfungsi->setting('auto_skedul'));
$tgl = $this->mfungsi->setting('tgl_skedul');

if($cek==1){
	$time = strtotime($this->mfungsi->timeNow(false));
	$nowt = date('Y-m-d',$time);
	$time = strtotime($tgl);
	$tgl_skd = date('Y-m-d',$time);
	if($auto==1 && $nowt>=$tgl_skd){
		$this->db->set('VALUESET',0);
		$this->db->where('JENISSET','hal_pstkelompok');
		$this->db->update('pengaturan');
		$cek = 0;
	}
}

function rahasia($n,$c){
	if($c==0){
		return $n;
	}else{
		return '<i class="text-muted">MASIH DALAM PROSES</i>';
	}
}

if($cek==1){
?>
<div class="alert alert-info">
	<b>Info</b> Seluruh informasi <?php echo ($auto==0) ? 'dimungkinkan' : ''; ?> akan ditampilkan pada tanggal <span class="text-danger"><?php echo $this->mfungsi->tgl($tgl); ?></span>
</div>
<?php } ?>
<div class="card">
  <div class="card-body">
    <center><h3>PROFIL KELOMPOK KKN</h3></center>
    <div class="row">
		<div class="col-md-3">
				<b>NAMA KELOMPOK KKN</b>
		</div>
		<div class="col-md-9">
			<?php echo $data->NAMAKEL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>
		<?php

		?>
		<div class="col-md-3">
				<b>ALAMAT KKN</b>
		</div>
		<div class="col-md-9">
			<?php echo rahasia($data->ALAMATKEL,$cek); ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>DPL KKN</b>
		</div>
		<div class="col-md-9">
			<a href="<?php echo base_url('mhs/ver/kelompok/dosen'); ?>" class="btn-link"><?php echo $data->NAMADPL; ?></a>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>ALAMAT DPL</b>
		</div>
		<div class="col-md-9">
			<?php echo ($data->ALAMATDPL==NULL) ? '-' : $data->ALAMATDPL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<div class="col-md-3">
				<b>KONTAK DPL</b>
		</div>
		<div class="col-md-9">
			<?php echo ($data->KONTAKDPL==NULL) ? '-' : $data->KONTAKDPL; ?>
		</div>
		<div class="dropdown-divider col-md-12">
			
		</div>

		<?php
		if($cek==0){
		?>
		<div class="col-md-12">
			<center class="mt-3"><h3>DAFTAR ANGGOTA KKN (PESERTA KKN)</h3></center>
			<table class="table datatable table-bordered table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Npm</th>
						<th>Nama</th>
						<th>Fak / Prodi</th>
						<th>No HP</th>
						<th>Alamat</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
						foreach ($anggota as $key => $value) {
							?>
							<tr>
								<td><?php echo $i++; ?></td>
								<td><a class="btn-link" href="<?php echo base_url('mhs/ver/kelompok/biodata/'.$value->NPM); ?>"><?php echo $value->NPM; ?></a></td>
								<td><?php echo $value->NAMAMHS; ?></td>
								<td><?php echo $value->FAKPRODI.' / '.$value->NAMAPRODI; ?></td>
								<td><?php echo $value->KONTAKMHS; ?></td>
								<td><?php echo $value->ALAMATMHS; ?></td>
							</tr>
							<?php
						}
					?>
				</tbody>
			</table>
		</div>
		<?php }else{ ?>

			<div class="col-md-12">
				<center class="mt-3"><h3 class="text-muted"><i>DAFTAR ANGGOTA KKN MASIH DALAM PROSES<i></h3></center>
			</div>

		<?php } ?>
	</div>
  </div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.datatable').DataTable({
			scrollX:true,
			paging:false,
			info:false
		})
	});
</script>