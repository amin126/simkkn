<style type="text/css">
</style>
<div class="row">
	<div class="col-12 mb-3 text-right">
		<a style="display: none;" href="#!" onclick="_cetak(this)" class="btn btn-warning mb-2 btnprint"><i class="fa fa-print"></i></a>
	</div>
	<div class="dropdown-divider"></div>
	<div class="col-12 mt-3  xloading">
		<div class="sertifikat" style="display: none;
    padding: 19px;
    border: 1px solid #ded6d6;
    background: white;
    box-shadow: -7px -6px 15px #86868626;
">
			<div class="row">
				<div class="col-12">
					<div id="qrcode"></div>
				</div>
				<div class="col-12">
					<div class="kodeqr" style="
    font-size: 80%;
    margin-left: 10px;
"></div>
				</div>
				<div class="col-12 text-muted" style="
    font-size: 80%;
    margin-left:  10px;
">
					<div class="kodematch"></div>
				</div>
				<div class="col-12 text-center mt-md-0 mt-3">
					<h2><u>SERTIFIKAT</u></h2>
				</div>
				<div class="col-12 text-center">
					<div class="nomor text-primary"></div>
				</div>
				<div class="col-12 text-center mt-5">
					Diberikan kepada :
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="nama font-weight-bold"></div>
				</div>
				<div class="col-12 text-center">
					<div class="npm font-weight-bold text-muted" style="
    font-size:  80%;
"></div>
				</div>
				<div class="col-12 mt-2 text-center">
					<div class="alert alert-info">Sebagai peserta pada kegiatan Kuliah Kerja Nyata (KKN) <br>Universitas Madura Tahun Akademik <span class="akademik"></span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var wqr = 0;
	var urlcetak = '<?php echo base_url('mhs/ver/nilai/sertifikat/cetak') ?>';
	$(document).ready(function() {
		setTimeout(function(){
     	generate()
    }, 300);
	});
	function generate(){
		_ajax({
			url:'<?php echo base_url('mhs/ver/nilai/ajax_sertifikat') ?>',
			loading:'.xloading',
			success:function(res){
				res = JSON.parse(res);
				if(res.status==true){
					var data = res.data[0];
					$('.sertifikat .nama').text(data.NAMAMHS);
					$('.sertifikat .npm').text(data.NPM);
					$('.sertifikat .kodeqr').text(data.KODE);
					$('.sertifikat .kodematch').text(data.KUNCIKODE);
					$('.sertifikat .nomor').text(data.NOMOR);
					$('.sertifikat .akademik').text(data.AKADEMIK);
					createQr(data);
				}else{
					$('.btnprint,.sertifikat').hide();
					_alert('Kesalaha',res.data,'error');
				}
			}
		})
	}

	function createQr(data){
		var plaintext = '';
		plaintext += 'KODE SERTIFIKAT = '+data.KODE+'-'+data.KUNCIKODE+'\n'
		plaintext += 'NPM = '+data.NPM+'-'+data.KUNCIKODE+'\n';
		plaintext += 'NAMA = '+data.NAMAMHS+'\n';
		plaintext += 'PRODI = '+data.FAKPRODI+' / '+data.NAMAPRODI+'\n';
		plaintext += 'DIBUAT = '+data.TANGGAL+'\n';
		plaintext += 'TERDAFTAR = '+data.TGLREGMHS+'\n';
		// plaintext += 'KELOMPOK = '+data.NAMAKEL+'\n';
		// plaintext += 'TAHUN KKN = '+data.NAMATAHUN+'\n';
		// plaintext += 'ALAMAT KELOMPOK = '+data.ALAMATKEL+'\n';
		// plaintext += 'KAMPUS = UNIVERSITAS MADURA';

		$('#qrcode').empty();
		$('#qrcode').qrcode({
	    render: 'table',
	    minVersion: 1,
	    maxVersion: 40,
	    ecLevel: 'L',
	    left: 0,
	    top: 0,
	    size: 120,
	    fill: '#000',
	    background: '#fff',
	    text: plaintext,
	    radius: 0,
	    quiet: 0,
	    mode: 0,
	    mSize: 0.1,
	    mPosX: 0.5,
	    mPosY: 0.5,
	    label: 'no label',
	    fontname: 'sans',
	    fontcolor: '#000',
	    image: null
		});
		$('.btnprint,.sertifikat').show();
		$('.btnprint').attr('href', urlcetak);
	}
</script>