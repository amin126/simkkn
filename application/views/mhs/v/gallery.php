<div class="card">
  <div class="card-body xloading">
    <h4 class="card-title">Album Foto Kegiatan Selama KKN</h4>
    
    <div class="row">
        <?php if($periode == true){ ?>

            <?php
        if($this->session->tidak_lulus!=true && $this->session->mhs_lulus!=true){
            ?>
         
        <div class="col-12">
            <!-- <div class="btn-group"style="margin-bottom: 15px;" role="group" aria-label="First group">
          <a class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Tambah manual data mahasiswa" href="<?php echo base_url('mhs/ver/upload/tambah'); ?>"><i class="fa fa-plus"></i></a>
        </div>   -->

        <h4>Tambah Foto</h4>
        <form action="<?php echo base_url('mhs/ver/album/proses') ?>" method="post" onsubmit="return upload(this)" enctype="multipart/form-data" id="js-upload-form">
          <div>
            <div class="form-group">
              <input autocomplete="off"  type="file" name="file" id="file">
            </div>
            <div class="form-group">
              <label for="namakel">* Deskripsi Foto</label>
              <textarea type="text" class="form-control" id="deskripsi" placeholder="Deskripsi" name="deskripsi"></textarea>
            </div>
            <button type="submit" oxnclick="upload()"; class="btn btn-sm btn-primary" id="js-upload-submit">Upload</button>
          </div>
        </form>
        </div>
        <?php  }
        } ?>
        <div class="col-12">
            <form class="form-inline float-right mb-2" action="<?php echo base_url('mhs/ver/upload/find') ?>" method="post" id="formcari">
                                
                  <div class="input-group  mr-sm-2">
                    <input autocomplete="off"  type="text" name="cari" value="<?php echo $cari; ?>" class="form-control" placeholder="Cari">
                  </div>


                  <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </form>
        </div>
    </div>

    <table class="table datatable table-hover xloadfile">
        <thead>
            <tr class="text-muted">
                <th></th>
                <th>File</th>
                <th>Upload User</th>
                <th>Deskripsi</th>
                <th>Del</th>
            </tr>
        </thead>
        <tbody>
            
        <?php
        foreach ($data as $key => $value){ 
            if($value->NPM==$this->session->usern){
                if($this->session->tidak_lulus!=true && $this->session->mhs_lulus!=true){
                    $del = '<a href="'.base_url('mhs/ver/album/delete/').$value->KDALB.'" onclick="return hapus(this);" class="btn-link text-danger"><i class="fa fa-close"></i></a>';
                }else{
                    $del = '';
                }
            }else{
                $del = '';
            }
                echo '<tr><td width="60px"><img src="'.base_url('assets/images/icons/'.$ext.'.png').'" class="img-fluid rounded"></td>
                    <td>'.$value->FOTOALB.'</td>
                    <td><span title="'.$value->NPM.'">'.$value->NAMAMHS.'</span></td>
                    <td>'.$value->SIZEALB.'Kb</td>
                    <td>'.$del.'</td>
                    </tr>';
        }
        ?>
        </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.datatable').DataTable({
            searching:false,
            info:false,
            paging:false,
            scrollX:false
        })
    });
    function hapus(t){
        _alert({
            mode:'confirm',
            title:'Apakah akan dihapus?',
            msg:'',
            yes:'Ya, Hapus!',
            no:'Tidak',
            isConfirm:function(){
                window.location.href=$(t).attr('href');;
            }
        })
        return false;
    }

    function upload(t){
        $('#progressUpload').remove();
        var htm = '<tr id="progressUpload"><td colspan="6"><div class="progress">'+
                  '<div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>'+
                '</div></td></tr>';
        $('.xloadfile').prepend(htm);
        _upload({
            name:'file',
            progress:function(res){
                $('#progressUpload .progress-bar').css('width', res.percent+'%');
                $('#progressUpload .progress-bar').text(res.percent+'%');
            },
            complete:function(res){
                //console.log(res);
                res = JSON.parse(res);
                if(res.status==true){
                    $('#progressUpload').remove();
                    $('.xloadfile').prepend(res.data);
                    $('#file').val('');
                    _ajax({
                        url:'<?php echo base_url('mhs/ver/album/deskripsi'); ?>',
                        data:{
                            kode:res.id,
                            deskripsi:$('#deskripsi').val()
                        },
                        loading:'.xloading',
                        success:function(){
                            $('#deskripsi').val('');
                        }
                    })
                }else{
                    $('#progressUpload').html(res.data);
                }
            },
            action:$(t).attr('action')
        })
        return false;
    }
</script>