
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin-free/jquery/pages/samples/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 07:02:26 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SIMPEMA KKN</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>node_modules/simple-line-icons/css/simple-line-icons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/style.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/mod.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>css/preloader.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/'); ?>images/favicon.png" />
  <style type="text/css">
  	.auth form .form-group i{
  		position: unset;
  	}
  </style>
</head>

<body>
  <div class="mod-middle bg-warning text-white">
    <div class="mod-submiddle">
      <div class="h1 font-weight-bold">
        DATA ANDA SUDAH KAMI TERIMA
      </div>
      <div class="h3 font-weight-bold">
        SILAHKAN MENUNGGU KEMBALI UNTUK PROSES PENEMPATAN KKN ANDA
      </div>
      <div class="mt-4">
        <a href="<?php echo base_url('keluar') ?>" class="btn btn-outline-primary btn-lg">KELUAR</a>
      </div>
    </div>
  </div>
  <script src="<?php echo base_url('assets/'); ?>plugins/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url('assets/'); ?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url('assets/'); ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/misc.js"></script>
  <script src="<?php echo base_url('assets/'); ?>js/fungsi.js?<?php echo rand(111,999); ?>"></script>

</body>
</html>
