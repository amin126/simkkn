<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BioModel extends CI_Model {

	public function get(){
		$this->db->where('USERN',$this->session->usern);
		$this->db->where('STATUS',2);
		return $this->db->get('users');
	}

	public function update($data){
		$this->db->set('NAMA',$data['nama']);
		if($data['pass']!='')
			$this->db->set('PASSN',MD5($data['pass']));
		$this->db->where('USERN',$this->session->usern);
		$this->db->where('STATUS',2);
		$this->db->update('users');
	}

}

/* End of file bioModel.php */
/* Location: ./application/models/dosen/bioModel.php */