<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mremaining  extends CI_Model {

	public function start($str=null){
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); 
		$this->idRemaining = 0;
		$this->running($str,'START');
	}

	public function running($a=[],$id=false){
		if($id==false)
			echo "id: $this->idRemaining" . PHP_EOL;
		else
			echo "id: $id" . PHP_EOL;
    echo "data: " . json_encode($a) . PHP_EOL;
    echo PHP_EOL;
    ob_flush();
    flush();
    $this->idRemaining++;
	}

	public function finish($n = []){
		$this->idRemaining = 'CLOSE';
		$this->running($n);
	}
}

/* End of file mremaining.php */
/* Location: ./application/models/mremaining.php */