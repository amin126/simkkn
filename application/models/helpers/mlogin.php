<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlogin extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')==false){
			$this->load->view('notsupport');
			//exit();
		}
	}
	
	// mengecek  pada semua controller, KECUALI pada controller login
	public function cek(){

		if($this->session->login!=true){
			// jika login selesai
			$this->session->error_login = true;
			redirect(base_url('login'),'refresh');
		}else{
			$this->session->unset_userdata('error_login');
			if(trim($this->uri->segment(1))!=trim($this->session->level)){
				redirect(base_url($this->session->level.'/beranda'),'refresh');
			}
		}

	}

	// mengecek pada controller login
	public function inLogin(){
		if($this->session->login==true){
			redirect(base_url($this->session->level.'/beranda'),'refresh');
		}
	}

	public function login_admin($n){
		$this->db->where('USERN',$n['username']);
		$this->db->where('PASSN',md5($n['password']));
		$query = $this->db->get('users');
		if($query->num_rows()>0){
			$query = $query->result()[0];
			if($query->STATUS)
			$sesi = array(
          'login'=>true,
          'usern'=>$query->USERN,
          'nama'=>$query->NAMA,
          'pass'=>$query->PASSN,
          'level'=>(($query->STATUS==1) ? 'admin' : (($query->STATUS==2) ? 'camat' : 'biro'))
      );
      $this->session->set_userdata($sesi);
      $this->remove_lock_system(false);
      redirect(base_url($this->session->level.'/beranda'),'refresh');
		}else{
			$this->set_lock_system();
			$this->login_error(1);
		}
	}

	public function set_lock_system(){
		$total_error = 5;
		if(!isset($this->session->lock_page))
			$this->session->lock_page = 0;
		$this->session->lock_page++;
		if($this->session->lock_page>=$total_error){
			$c = $this->mfungsi->dateAdd(array(
				'start'=>date('Y-m-d H:i:s'),
				'type'=>'S',
				'add'=>30,
				'format'=>'Y-m-d H:i:s'
			));
			$this->session->set_userdata(array(
				'start_time_lock'=>$c['start'],
				'end_time_lock'=>$c['end'],
				'time_lock_status'=>true
			));
		}
	}

	public function remove_lock_system($red=true){
		$this->session->unset_userdata(array('lock_page','start_time_lock','end_time_lock','time_lock_status'));
		if($red==true)
			redirect('login','refresh');
	}

	public function login_mhs($n,$saya,$pkkmb=false){
		// $kkn = $this->uniraModel->get_krs_kkn($saya->data->id);
		// if($kkn==TRUE){

						
			$prodi = $this->uniraModel->prodi($saya->data->attributes->prodi);
			$sesi = array(
	        'login'=>true,
	        'usern'=>$saya->data->id,
	        'nama'=>$saya->data->attributes->nama,
	        'alamat'=>$saya->data->attributes->alamat,
	        'telepon'=>$saya->data->attributes->telepon,
	        'email'=>$saya->data->attributes->email,
	        'level'=>'mhs',
	        'pkkmb'=>$pkkmb,
	        'foto'=>$this->ambilfoto($saya->data->attributes->thumbnail,$saya->data->id),
	        'fotoorigin'=>'https://api.unira.ac.id/'.$saya->data->attributes->thumbnail,
	        'prodi'=>$prodi->relationship->fakultas->data->attributes->nama.' / '.$prodi->attributes->nama,
	        'prodikode'=>$prodi->id,
	        'verified'=>$this->mfungsi->verifikasiMHS($saya->data->id),
	        'tunggu'=>$this->mfungsi->tungguMHS($saya->data->id),
	        'periode'=>$this->mfungsi->periodeMHS($saya->data->id)
	    );
	    $this->session->set_userdata($sesi);
	    $this->remove_lock_system(false);
			redirect(base_url('mhs/beranda'),'refresh');
		// }else{
		// 	$this->login_error(2);
		// }
	}

	public function pkkmb($n,$saya){
			$prodi = $this->uniraModel->prodi($saya->data->attributes->prodi);
			$sesi = array(
	        'login'=>true,
	        'usern'=>$saya->data->id,
	        'nama'=>$saya->data->attributes->nama,
	        'alamat'=>$saya->data->attributes->alamat,
	        'telepon'=>$saya->data->attributes->telepon,
	        'email'=>$saya->data->attributes->email,
	        'level'=>'pkkmb',
	        'fotoorigin'=>'https://api.unira.ac.id/'.$saya->data->attributes->thumbnail,
	        'prodi'=>$prodi->relationship->fakultas->data->attributes->nama.' / '.$prodi->attributes->nama,
	        'prodikode'=>$prodi->id,
	        'verified'=>$this->mfungsi->verifikasiMHS($saya->data->id),
	        'tunggu'=>$this->mfungsi->tungguMHS($saya->data->id),
	        'periode'=>$this->mfungsi->periodeMHS($saya->data->id)
	    );
	    $this->session->set_userdata($sesi);
	    $this->remove_lock_system(false);
			redirect(base_url('pkkmb/beranda'),'refresh');
	}

	// login mhs offline
	public function login_mhs_offline($n,$saya){
			$sesi = array(
	        'login'=>true,
	        'usern'=>$saya,
	        'nama'=>'anonymos',
	        'alamat'=>'anonymos',
	        'telepon'=>'anonymos',
	        'email'=>'anonymos',
	        'level'=>'mhs',
	        'foto'=>$this->ambilfoto(null,$saya) ,//|| 'null',
	        'prodi'=>'anonymos',
	        'prodikode'=>'52',
	        'verified'=>$this->mfungsi->verifikasiMHS($saya),
	        'tunggu'=>$this->mfungsi->tungguMHS($saya),
	        'periode'=>$this->mfungsi->periodeMHS($saya)
	    );
	    $this->session->set_userdata($sesi);
	    $this->remove_lock_system(false);
			redirect(base_url('mhs/beranda'),'refresh');
	}

	public function ambilfoto($pth,$npm){
		$this->db->where('NPM',$npm);
		$this->db->select('FOTOMHS');
		$foto = $this->db->get('mahasiswa')->result()[0];
		if($foto->FOTOMHS==NULL){
			$this->db->where('NPM',$npm);
			$this->db->set('FOTOMHS','https://api.unira.ac.id/'.$pth);
			$this->db->update('mahasiswa');
			return 'https://api.unira.ac.id/'.$pth;
		}else{
			if (strpos($foto->FOTOMHS, 'unira') !== false) {
			  return $foto->FOTOMHS;
			}else{
				return base_url($foto->FOTOMHS);
			}
		}
	}

	public function ambilfotodosen($pth,$dpl){
		$this->db->where('KDDPL',$dpl);
		$this->db->select('FOTODPL');
		$foto = $this->db->get('dosen')->result()[0];
		if($foto->FOTODPL==NULL){
			$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?filter[nis]='.$dpl));
			if($dmhs->data[0]->attributes->thumbnail==null){
				$foto = 'assets/images/faces/user.png';
			}
			else{
				$foto = 'https://api.unira.ac.id/'.$pth;
			}
			$this->db->where('KDDPL',$dpl);
			$this->db->set('FOTODPL',$foto);
			$this->db->update('dosen');
			if (strpos($foto, 'unira') !== false) {
			  return $foto;
			}else{
				return base_url($foto);
			}
		}else{
			if (strpos($foto->FOTODPL, 'unira') !== false) {
			  return $foto->FOTODPL;
			}else{
				return base_url($foto->FOTODPL);
			}
		}
	}

	public function login_dosen($saya){
		$sesi = array(
        'login'=>true,
        'usern'=>$saya->data->id,
        'nama'=>$saya->data->attributes->nama,
        'alamat'=>$saya->data->attributes->alamat,
        'telepon'=>$saya->data->attributes->telepon,
        'foto'=>$this->ambilfotodosen($saya->data->attributes->thumbnail,$saya->data->id),
        'fotoorigin'=>'https://api.unira.ac.id/'.$saya->data->attributes->thumbnail,
        'email'=>$saya->data->attributes->email,
        'level'=>'dosen'
    );
    $this->session->set_userdata($sesi);
    $this->remove_lock_system(false);
		redirect(base_url('dosen/beranda'),'refresh');
		return false;
	}

	// login dosen offline
	public function login_dosen_offline($saya){
		$dmhs = json_decode($this->mfungsi->file_get('https://api.unira.ac.id/v1/dokar?filter[nis]='.$saya));
		$sesi = array(
        'login'=>true,
        'usern'=>$saya,
        'nama'=>'anonymos',
        'alamat'=>'anonymos',
        'telepon'=>'anonymos',
        'email'=>'anonymos',
        'level'=>'dosen',
        'foto'=>$this->ambilfotodosen($dmhs->data[0]->attributes->thumbnail,$saya),
        'fotoorigin'=>'https://api.unira.ac.id/'.$dmhs->data[0]->attributes->thumbnail
    );
    $this->session->set_userdata($sesi);
    $this->remove_lock_system(false);
		redirect(base_url('dosen/beranda'),'refresh');
		return false;
	}

	// mengaktifkan autentikasi dengan cara login
	public function autentikasi($n){
		set_time_limit(0);
		if($n['username'] == '' || $n['password'] == ''){
			$this->login_error(5);
		}else{
			$this->load->model('admin/uniraModel');
			$cek = $this->uniraModel->get_Token(array('username'=>$n['username'],'password'=>$n['password']));

			if($cek->status==true){
				$array = array(
					'api_key_distance' => $this->mfungsi->setting('api_key')
				);
				$this->session->set_userdata($array);
				$saya = $this->uniraModel->saya();
				if(isset($saya->data->relationship->access)){
					$akses = $saya->data->relationship->access->data;
					foreach ($akses as $key => $value) {
						if($value->id==32){
							$usern = $saya->data->id;
							$cek = $this->cekdosen($usern);
							if($cek==true){
								$this->login_dosen($saya);
							}else{
								$this->login_error(3);
							}
							break;
						}
					}
					//$this->login_error(4);
				}else if(isset($saya->data->attributes->type)){
					if($saya->data->attributes->type=='mhs'){
						$cek_pernah_login = $this->cekpernahlogin($n['username']);
						if($cek_pernah_login==false){
							$cek_krs_kkn = $this->uniraModel->get_krs_kkn($n['username']);
							if($cek_krs_kkn==true){
								$cek = $this->mfungsi->setting('biro_login_mhs');
								if($cek==1){
									$this->db->where('NPMORD',$n['username']);
									$cek = $this->db->get('no_ordik');
									if($cek->num_rows()>0){ //tida dapat login
										$this->login_mhs($cek,$saya);
									}else{
										$this->login_mhs($cek,$saya,true); // ordik aktif
									}
								}else{
									$this->login_mhs($cek,$saya);
								}
							}else{
								$cek = $this->mfungsi->setting('biro_login_mhs');
								if($cek==1){
									$this->db->where('NPMORD',$n['username']);
									$cek = $this->db->get('no_ordik');
									if($cek->num_rows()>0){ //tida dapat login
										$this->login_error(6);
									}else{
										$this->pkkmb($cek,$saya);
									}
								}else{
									$this->login_error(7);
								}
							}
						}else{
							$cek = $this->mfungsi->setting('biro_login_mhs');
							if($cek==1){
								$this->db->where('NPMORD',$n['username']);
								$cek = $this->db->get('no_ordik');
								if($cek->num_rows()>0){ //tida dapat login
									$this->login_mhs($cek,$saya);
								}else{
									$this->login_mhs($cek,$saya,true); // ordik aktif
								}
							}else{
								$this->login_mhs($cek,$saya);
							}
						}
					}else{
						$this->login_admin($n);
					}
				}
			}else{
				$this->login_admin($n);
				//$this->login_dosen_offline('7104313507');
				//$this->login_mhs_offline(null,'2014610032');
			}
		}
	}

	public function cekpernahlogin($npm){
		$this->db->where('NPM',$npm);
		$a = $this->db->get('mahasiswa_mendaftar')->num_rows();
		if($a>0){
			$this->session->mhs_baru_daftar = true;
			return true;
		}else{
			$this->db->where('NPM',$npm);
			$a = $this->db->get('mahasiswa')->num_rows();
			if($a>0){
				return true;
			}else{
				$this->session->mhs_baru_daftar = true;
				return false;
			}
		}
	}

	public function cekdosen($kd){
		$cek = $this->db->query("SELECT * FROM `dosen` WHERE `KDDPL` = '$kd' AND `KDDPL` IN (SELECT `KDDPL` FROM `kelompok` WHERE `KDTAHUN` IN (SELECT `KDTAHUN` FROM `tahun` WHERE `STATUSTAHUN` = 1))");
		$data = $cek->result();
		if(count($data)>0)
			return true;
		else
			return false;
	}

	public function login_error($n=''){
		switch ($n) {
			case 1:
				$msg = 'Username atau password tidak benar!';
				break;
			case 2:
				$msg = 'Anda tidak memprogram matakuliah KKN';
				break;
			case 3:
				$msg = 'Anda tidak ada tugas dalam kegiatan kkn tahun akademik '.$this->mfungsi->tahun()->label;
				break;
			case 4:
				$msg = 'Anda bukan bagian dari kegiatan kuliah kerja nyata';
				break;
			case 5:
				$msg = 'Username atau password tidak boleh kosong';
				break;
			case 6:
				$msg = 'Anda tidak diperkenankan untuk login ke sistem, silahkan hubungi tim biro kemahasiswaan';
				break;
			case 7:
				$msg = 'Pengambilan sertifikat PKKMB sedang ditutup oleh tim biro kemahasiswaan';
				break;
			default:
				$msg = 'Kesalahan tidak ditemukan';
				break;
		}
		if($this->session->time_lock_status==true){
			$now = new DateTime(date('Y-m-d H:i:s'));
      $now = $now->getTimestamp();
      $end = new DateTime($this->session->end_time_lock);
      $end = $end->getTimestamp();
      $st = $end-$now;
      if($st>0){
          $this->load->view('locked',array(
              'detik'=>$st
          ));
      }else{
          $this->mlogin->remove_lock_system();
      }
		}else{
			$this->load->view('login',array(
				'auth'=>true,
				'msg'=>$msg
			));
		}
	}

	// keluar atau logout dari sistem
	public function logout(){
		$sesi = array(
      'login',
      'usern',
      'nama',
      'pass',
      'tokenUnira',
      'refTokenUnira',
      'alamat',
      'telepon',
      'email',
      'level',
      'foto',
      'fotoorigin',
      'prodi',
      'verifikasi',
      'prodikode',
      'dpl_kkn_aktif',
      'verified',
      'tunggu',
      'periode',
      'api_key_distance',
      'tidak_lulus',
      'mhs_lulus',
      'mhs_baru_daftar',
      'enable_edit_biodata',
      'pass_update',
      'dpl_kkn_nama',
      'subPageURL',
      'pageURL',
      'sertifikat_data',
      'sertifikat_npm',
      'pkkmb'
  	);
		$this->session->unset_userdata($sesi);
		$this->remove_lock_system(false);
		redirect(base_url('login'),'refresh');
	}

}

/* End of file login.php */
/* Location: ./application/models/login.php */