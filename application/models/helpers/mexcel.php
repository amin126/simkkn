<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mexcel extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	public function import($config=[]){
		$this->file = (!isset($config['filename']))?'data':$config['filename'];
		$this->field = (!isset($config['field']))?['a','b','c']:$config['field'];
		$this->tbl = (!isset($config['table']))?'myTable':$config['table'];
        $this->process = (!isset($config['process']))?false:$config['process'];
        $this->database = (!isset($config['database']))?true:$config['database'];
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	    $inputFileName = $this->file;
        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loadi file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
	    if($this->database!=false){
            $fields = '(';
            foreach ($this->field as $key => $value) {
          		$fields .= '`'.$value.'`,';

          	}
            if(isset($config['except'])){
                foreach ($config['except'] as $exc => $vexc) {
                   $fields .= '`'.$exc.'`,';
                }
            }
          	$fields = substr($fields,0, strlen($fields)-1).')';
          	$values = '';

            if($this->process==true)
                $this->mremaining->start($highestRow-1);
            for ($row = 2; $row <= $highestRow; $row++){
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                $value = '(';
                foreach ($this->field as $key => $val) {
                    if(isset($config['function'])){
                        if(isset($config['function'][$val])){
                            $fnc = $config['function'][$val];
                            $value .= str_replace("%s",'"'.$rowData[0][$key].'"',$fnc).',';
                        }else{
                            $value .= '"'.$rowData[0][$key].'",';
                        }
                    }else{
                        $value .= '"'.$rowData[0][$key].'",';
                    }            	
              	}
                if(isset($config['except'])){
                    foreach ($config['except'] as $exc => $vexc) {
                        eval('$except = '.$vexc.';');
                        $value .= '"'.$except.'",';
                    }
                }
              	$values .= ','.substr($value, 0,strlen($value)-1).')';
              	//array_push($datas, $data); 
                if($this->process==true)
                    $this->mremaining->running($row-1);    
            }
            $values = substr($values, 1);
            $sql = 'INSERT IGNORE INTO `'.$this->tbl.'`'.$fields.'VALUES'.$values;
            $this->db->query($sql);
            if($this->process==true)
                $this->mremaining->finish();
        }else{
            $values_arr = array();
            $fieldskip = null;
            $listskip = array();
            $indexexcept = 0;
            if(isset($config['skip'])){
                $fieldskip = $config['skip']['field'];
                $listskip = $config['skip']['list'];
                $indexexcept = array_search($fieldskip, $this->field);
            }
            $skip = (!isset($config['skip']))?false:$config['skip'];
            if($this->process==true)
                $this->mremaining->start($highestRow);
            for ($row = 2; $row <= $highestRow; $row++){
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                if(!in_array($rowData[0][$indexexcept], $listskip) || $fieldskip==null){
                    $value_arr = array();
                    foreach ($this->field as $key => $val) {
                        if(isset($config['function'])){
                            if(isset($config['function'][$val])){
                                $fnc = $config['function'][$val];
                                $value = str_replace("%s",$rowData[0][$key],$fnc);
                                array_push($value_arr, $value);
                            }else{
                                $value = $rowData[0][$key];
                                array_push($value_arr, $value);
                            }
                        }else{
                            $value = $rowData[0][$key];
                            array_push($value_arr, $value);
                        }
                    }
                    if(isset($config['except'])){
                        foreach ($config['except'] as $exc => $vexc) {
                            eval('$except = '.$vexc.';');
                            $value = $except;
                            array_push($value_arr, $value);
                        }
                    }
                    array_push($values_arr, $value_arr); 
                    if($this->process==true)
                        $this->mremaining->running($row);
                }
            }
            if($this->process==true)
                $this->mremaining->finish();
            return $values_arr;
        }
	}

    public function export($config = []){
        $data = (!isset($config['data']))?[[1,2,3]]:$config['data'];
        $header = (!isset($config['header']))?['No','Nama','Alamat']:$config['header'];
        $namafile = (!isset($config['filename']))? date('YmdHis').'.'.explode('|', $config['allowed_types'])[0] :$config['filename'].".xls";
        $width = (!isset($config['width']))?"100%":$config['width'];

        header("Content-type: application/vnd-ms-excel"); 
        header("Content-Disposition: attachment; filename='".urlencode($namafile)."'"); 
        header("Pragma: no-cache"); 
        header("Expires: 0");
        $html = '<table border="1" width="'.$width.'" cellpadding="0" cellspacing="0">';
        $html .= '<thead>';
        $html .= '<tr>';
        foreach ($header as $key => $value) {
            $html .= '<th>';
            $html .= $value;
            $html .= '</th>';
        }
        $html .= '</tr>';
        $html .= '<tbody>';
        foreach ($data as $key => $value) {
            $html .= '<tr>';
            foreach ($value as $key1 => $value1) {
                $html .= '<td>';
                $html .= $value1;
                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        $html .= '</table>';
        echo $html;
    }

}

/* End of file mexcel.php */
/* Location: ./application/models/mexcel.php */