<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpaging extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function set($n = array()){
		$this->load->library('pagination');
		$config = array();
		(!isset($n['filename']))?'data':$n['filename'];
		$config['base_url'] = (!isset($n['url']))?base_url():$n['url'];
		$config['total_rows'] = (!isset($n['count']))?100:$n['count'];
		$config['uri_segment'] = (!isset($n['uri']))?1:$n['uri'];
		$config['per_page'] = (!isset($n['perpage']))?5:$n['perpage'];;
		$config['num_links'] = (!isset($n['countlink']))?5:$n['countlink'];
		$config['use_page_numbers'] = (!isset($n['pagenumber']))?TRUE:$n['pagenumber'];

		$config['full_tag_open'] = (!isset($n['wrapopen']))?'<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">':$n['wrapopen'];
		$config['full_tag_close'] = (!isset($n['wrapclose']))?'</ul></nav>':$n['wrapclose'];
		$config['attributes'] = (!isset($n['attribute']))?array('class' => 'page-link'):$n['attribute'];
		$config['first_link'] = (!isset($n['first']))?'&laquo;':$n['first'];
		$config['first_tag_open'] = (!isset($n['firstopen']))?'<div first_tag_open>':$n['firstopen'];
		$config['first_tag_close'] = (!isset($n['firstclose']))?'</div first_tag_close>':$n['firstclose'];
		$config['first_url'] = (!isset($n['firsturl']))?1:$n['firsturl'];
		$config['last_link'] = (!isset($n['last']))?'&raquo;':$n['last'];
		$config['last_tag_open'] = (!isset($n['lastopen']))?'<div last_tag_open>':$n['lastopen'];
		$config['last_tag_close'] = (!isset($n['lastclose']))?'</div last_tag_close>':$n['lastclose'];
		$config['next_link'] = (!isset($n['next']))?'&gt;':$n['next'];
		$config['next_tag_open'] = (!isset($n['nextopen']))?'<li class="page-item">':$n['nextopen'];
		$config['next_tag_close'] = (!isset($n['nextclose']))?'</li>':$n['nextclose'];
		$config['prev_link'] = (!isset($n['prev']))?'&lt;':$n['prev'];
		$config['prev_tag_open'] = (!isset($n['prevopen']))?'<li class="page-item">':$n['prevopen'];
		$config['prev_tag_close'] = (!isset($n['prevclose']))?'</li>':$n['prevclose'];
		$config['cur_tag_open'] = (!isset($n['currentopen']))?'<li class="page-item active"><a class="page-link">':$n['currentopen'];
		$config['cur_tag_close'] = (!isset($n['currentclose']))?'</a></li>':$n['currentclose'];
		$config['num_tag_open'] = (!isset($n['numopen']))?'<li class="page-item">':$n['numopen'];
		$config['num_tag_close'] = (!isset($n['numclose']))?'</li>':$n['numclose'];

		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}

}

/* End of file mpaging.php */
/* Location: ./application/models/mpaging.php */