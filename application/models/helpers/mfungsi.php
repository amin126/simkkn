<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Mfungsi extends CI_Model {

	public function imgsize($pth){
		$size = getimagesize($pth);
		$size = explode('"',$size[3]);
		return array(
			'w'=>$size[1],
			'h'=>$size[3]
		);
	}
	public function kordinat($alamat){
		$key = $this->setting('api_key');
		$key = ($key==null) ? '' : '&key='.$key;
		error_reporting(0);
		$res = new stdClass();
		if(empty($alamat)){
			$res->status = false;
			$res->message = ADDRESS_NULL;
		}else{
			$url = 'https://maps.google.com/maps/api/geocode/json?address='.urlencode($alamat).'&language=id-ID';//.$key;
			$data = json_decode($this->file_get($url));
			$res->url = $url;
			$res->json = $data;
			if($data->status=='OK'){
				$res->status = true;
				$res->message = null;
				$res->alamat = $data->results[0]->formatted_address;
				$res->lat = $data->results[0]->geometry->location->lat;
				$res->lng = $data->results[0]->geometry->location->lng;
			}else{
				$res->status = false;
				$res->message = $data->status;
			}
		}
		return $res;
	}

	function getDistance($latitude1, $longitude1, $latitude2, $longitude2, $unit = null){ 
		$theta = $longitude1 - $longitude2; 
		$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)))  + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))); 
		$distance = acos($distance); 
		$distance = rad2deg($distance); 
		$distance = $distance * 60 * 1.1515; 
		switch($unit) 
		{ 
			case 'Mi': break; 
			case 'Km' : $distance = $distance * 1.609344; 
				break;
			default:
				break;
		} 
		return $distance;
		return (round($distance,2)); 
	}

	public function viewJson($data){
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}

	public function jarakTempuh($asal='',$tujuan=''){
		$key = $this->setting('api_key');
		$key = ($key==null) ? '' : '&key='.$key;
		error_reporting(0);
		$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.urlencode($asal).'&destinations='.urlencode($tujuan).'&language=id-ID'.$key;
		$data = $this->file_get($url);
		$distance = json_decode($data);
		$res = new stdClass();
		$res->asal = $distance->origin_addresses[0];
		$res->tujuan = $distance->destination_addresses;
		$res->skor_jarak = $distance->rows[0]->elements[0]->distance->value;
		$res->jarak = $distance->rows[0]->elements[0]->distance->text;
		$res->waktu = $distance->rows[0]->elements[0]->duration->text;
		$res->skor_waktu = $distance->rows[0]->elements[0]->duration->value;
		$res->status_lokasi = $distance->rows[0]->elements[0]->status;
		$res->status = $distance->status;
		$res->url = $url;
		return $res;
	}

	public function multijarakTempuh($asal='',$tujuan=[],$kunci=[]){
		$tmp = '';
		foreach ($tujuan as $key => $value) {
			$tmp .= '|'.join(' ',$this->filterKata($value));
		}
		$tujuan = substr($tmp, 1);
		$key = $this->setting('api_key');
		$key = ($key==null) ? '' : '&key='.$key;
		error_reporting(0);
		$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.urlencode(join(' ',$this->filterKata($asal))).'&destinations='.urlencode($tujuan).'&departure_time=1541202457&traffic_model=best_guess&language=id-ID'.$key;
		$data = $this->file_get($url);
		$distance = json_decode($data);
		$res = new stdClass();
		$res->asal = $distance->origin_addresses[0];
		$res->destinasi = array();
		for($i=0;$i<count($distance->destination_addresses);$i++){
			$data = new stdClass();
			$data->tujuan = $distance->destination_addresses[$i];
			$data->skor_jarak = $distance->rows[0]->elements[$i]->distance->value;
			$data->jarak = $distance->rows[0]->elements[$i]->distance->text;
			$data->waktu = $distance->rows[0]->elements[$i]->duration->text;
			$data->skor_waktu = $distance->rows[0]->elements[$i]->duration->value;
			$data->status_lokasi = $distance->rows[0]->elements[$i]->status;
			$data->kata_kunci = (isset($kunci[$i])) ? $kunci[$i] : null;
			array_push($res->destinasi, $data);
		}
		$res->status = $distance->status;
		$res->url = $url;
		return $res;
	}

	public function nilaihuruf($nilai1){
		if($nilai1==null) {
				$nilai1 = '-';
		}else if ($nilai1 ==0) {
        $nilai1 = 'E';
    } else if ($nilai1 >= 1 && $nilai1 <= 55) {
        $nilai1 = 'D';
    } else if ($nilai1 >= 56 && $nilai1 <= 62) {
        $nilai1 = 'C';
    } else if ($nilai1 >= 63 && $nilai1 <= 69) {
        $nilai1 = 'BC';
    } else if ($nilai1 >= 70 && $nilai1 <= 76) {
        $nilai1 = 'B';
    } else if ($nilai1 >= 77 && $nilai1 <= 84) {
        $nilai1 = 'AB';
    } else if ($nilai1 >= 85 && $nilai1 <= 100) {
        $nilai1 = 'A';
    } else {
        $nilai1 . '?';
    }
    return $nilai1;
	}

	public function random_color_part() {
	    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
	}

	public function random_color() {
	    return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
	}

	public function cekLayananGoogle($api,$alamat){
		$url = 'https://maps.google.com/maps/api/geocode/json?address='.urlencode($alamat).'&language=id-ID&key='.$api;
		$data = json_decode($this->file_get($url));
		$res = new stdClass();
		$res->url = $url;
		$res->json = $data;
		if($data->status=='OK'){
			$res->status = true;
			$res->message = null;
			$res->alamat = $data->results[0]->formatted_address;
			$res->lat = $data->results[0]->geometry->location->lat;
			$res->lng = $data->results[0]->geometry->location->lng;
		}else{
			$res->status = false;
			$res->message = $data->status;
		}
		return $res;
	}

	public function filterKata($n){
		$n = array_filter(preg_split('/ |_|\.|,|-|\+/i', strtolower($n)));
		$rm = array('ds','desa','dsn','dusun','kec','kecamatan','kab','kabupaten','kota','jalan','jln','jl','kelurahan','kel','rt','rw','gang','gg','0','1','2','3','4','5','6','7','8','9','\'');
		$newed = '';
		foreach ($n as $key => $value) {
			if(!in_array($value, $rm)){
				$newed .= ' '.$value;
			}
		}
		$xx = [];
		foreach (array_filter(explode(' ', $newed)) as $key => $value) {
			if(!in_array($value, $xx))
				array_push($xx, $value);
		}
		$xx = implode(' ', $xx);
		$xn = [];
		foreach (array_filter(explode(' ', str_replace(['0','1','2','3','4','5','6','7','8','9'], '', $xx))) as $key => $value) {
				array_push($xn, $value);
		}
		return $xn;
	}

	public function set_ini(){
		ini_set('upload_max_filesize', '60M');
		ini_set('memory_limit', '128M');
		ini_set('post_max_size', '60M'); 
	}

	public function file_get($n){
		$arrContextOptions=array(
		  "ssl"=>array(
		      "verify_peer"=>false,
		      "verify_peer_name"=>false,
		  ),
		);
		return file_get_contents($n, false, stream_context_create($arrContextOptions));
	}
	
	public function tahun(){
		$this->db->where('STATUSTAHUN',1);
		$data = $this->db->get('tahun');
		$rows = $data->result();
		if(count($rows)>0){
			$dt = new stdClass();
			$dt->label = $rows[0]->NAMATAHUN;
			$dt->kode = $rows[0]->KDTAHUN;
			return $dt;
		}
		else{
			$dt = new stdClass();
			$dt->label = '[TAHUN PELAKSANAAN TIDAK ADA YANG AKTIF]';
			$dt->kode = false;
			return $dt;
		}
	}

	public function periodeMHS($npm){
		$thn = $this->tahun()->kode;
		$this->db->where("NPM",$npm);
		$this->db->where("KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN = '$thn')");
		$this->db->from('kelompok_peserta');
		return ($this->db->count_all_results()>0) ? true : false;
	}

	public function tungguMHS($npm){
		$this->db->where("NPM",$npm);
		$this->db->where("STATUSMHS",0);
		$this->db->from('mahasiswa');
		return ($this->db->count_all_results()>0) ? true : false;
	}

	public function verifikasiMHS($npm){
		$this->db->where("NPM",$npm);
		$this->db->where("STATUSMHS != ",0);
		$this->db->from('mahasiswa');
		return ($this->db->count_all_results()>0) ? true : false;
	}

	public function setting($n){
		$this->db->where('JENISSET',$n);
		$data = $this->db->get('pengaturan');
		$rows = $data->result();
		if(count($rows)>0){
			return $rows[0]->VALUESET;
		}else{
			return null;
		}
	}

	public function idTime($n=null){
		return date('ynjHis').(($n===null)?rand(0,999):$n);
	}

	public function timeNow($n=true){
		date_default_timezone_set('Asia/Jakarta');
		$j = ($n==false) ? '' : ' H:i:s';
		return date('Y-m-d'.$j);
	}

	public function dateAdd($config){
		$f = (empty($config['start'])) ? date('Y-m-d H:i:s') : $config['start'];
		$t = (empty($config['add'])) ? 1 : $config['add'];
		$v = (empty($config['format'])) ? 'Y-m-d H:i:s' : $config['format'];
		$p = (empty($config['type'])) ? 'S' : $config['type'];
		$time = new DateTime($f);
		$time->add(new DateInterval('PT' . $t . $p));
		$stamp = $time->format($v);
		return array(
			'start'=>$f,
			'end'=>$stamp
		);
	}

	function cekLampau($config){
		$a = new DateTime((empty($config['now'])) ? date('Y-m-d H:i:s') : $config['now']);
		$b = new DateTime((empty($config['date'])) ? '2018-4-21 12:44:25' : $config['date']);

		if($b>$a){
			return true;
		}else{
			return false;
		}
	}

	public function bulan($b,$m=false){
		if($m==false)
			$x12 = array('Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des');
		else
			$x12 = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		return $x12[$b-1];
	}

	public function tgl($date,$time=false){
		if($date!='0000-00-00 00:00:00'){
			$a = explode(' ', $date);
			$a[1] = (!isset($a[1]))?'':$a[1];
			$timex = explode(':', $a[1]);
			$t = ($time==true)?' '.$timex[0].':'.$timex[1]:'';
			$tgl = explode('-', $a[0]);
			return $tgl[2].' '.$this->bulan($tgl[1]).', '.$tgl[0].$t;
		}else{
			return '-';
		}
	}

	public function tglFull($date,$time=false){
		if($date!='0000-00-00 00:00:00'){
			$a = explode(' ', $date);
			$a[1] = (!isset($a[1]))?'':$a[1];
			$timex = explode(':', $a[1]);
			$t = ($time==true)?' '.$timex[0].':'.$timex[1]:'';
			$tgl = explode('-', $a[0]);
			return $tgl[2].' '.$this->bulan($tgl[1],true).', '.$tgl[0].$t;
		}else{
			return '-';
		}
	}

	public function tglsql($date,$f=false){
		//dari 12/28/2017
		$a = explode('/', $date);
		$b = ($f==true) ? ' '.date('H:i:s') : '' ;
		return $a[2].'-'.$a[0].'-'.$a[1].$b;
	}

	public function tglpicker($date){
		//dari 2017-12-28
		$a = explode('-', $date);
		return $a[1].'/'.$a[2].'/'.$a[0];
	}

	public function waktu_lalu($timestamp){
		$selisih = time() - strtotime($timestamp) ;
 
    $detik = $selisih ;
    $menit = round($selisih / 60 );
    $jam = round($selisih / 3600 );
    $hari = round($selisih / 86400 );
    $minggu = round($selisih / 604800 );
    $bulan = round($selisih / 2419200 );
    $tahun = round($selisih / 29030400 );
 
    if ($detik <= 60) {
        $waktu = $detik.' detik yang lalu';
    } else if ($menit <= 60) {
        $waktu = $menit.' menit yang lalu';
    } else if ($jam <= 24) {
        $waktu = $jam.' jam yang lalu';
    } else if ($hari <= 7) {
        $waktu = $hari.' hari yang lalu';
    } else if ($minggu <= 4) {
        $waktu = $minggu.' minggu yang lalu';
    } else if ($bulan <= 12) {
        $waktu = $bulan.' bulan yang lalu';
    } else {
        $waktu = $tahun.' tahun yang lalu';
    }
    
    return $waktu;
	}

	public function koversiSize($n,$j='kb'){
		switch ($j) {
			case 'kb':
				return round($n/1000,2).'Kb';
				break;
			case 'mb':
				return round($n/1000000,2).'Mb';
				break;
		}
	}

	public function minimalText($teks,$maks){
		$new_teks = '';
		$opacity = 9;
		if(strlen($teks)>$maks){
			for ($i= 0; $i <$maks; $i++) { 
				if($i>$maks-9){
					$new_teks .= '<span style="opacity:0.'.$opacity.' !important;">'.$teks[$i].'</span>';
					$opacity--;
				}else{
					$new_teks .= $teks[$i];
				}
			}
			return $new_teks;
		}else{
			return $teks;
		}
	}

}

/* End of file mfungsi.php */
/* Location: ./application/models/mfungsi.php */