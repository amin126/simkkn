<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BioModel extends CI_Model {

	public function get(){
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('mahasiswa')->result();
	}

	public function update($n){
		$this->db->set('KONTAKMHS', $n['telp']);
		$this->db->set('SERAGAMMHS', $n['seragam']);
		$this->db->set('SKSMHS', $n['sks']);
		$this->db->set('PEKERJAANMHS', $n['kerja']);
		$this->db->where('NPM',$this->session->usern);
		$this->db->update('mahasiswa');
	}

	public function updatefoto($foto){
		$this->db->set('FOTOMHS',$foto);
		$this->db->where('NPM',$this->session->usern);
		$this->db->update('mahasiswa');
	}

	public function defaultfoto(){
		$this->db->set('FOTOMHS',$this->session->fotoorigin);
		$this->db->where('NPM',$this->session->usern);
		$this->db->update('mahasiswa');
	}

	public function ambilfoto(){
		$this->db->select('FOTOMHS');
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('mahasiswa')->result()[0]->FOTOMHS;
	}

}

/* End of file bioModel.php */
/* Location: ./application/models/mhs/v/bioModel.php */