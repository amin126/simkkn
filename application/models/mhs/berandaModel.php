<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BerandaModel extends CI_Model {

	public function mahasiswa(){
		$this->db->where('NPM',$this->session->usern);
		$data = $this->db->get('mahasiswa_mendaftar');
		return $data->result();
	}

	public function cek_lulus(){
		$this->db->where('NPM',$this->session->usern);
		$this->db->where('STATUSMHS',2);
		$data = $this->db->get('mahasiswa');
		return $data->result();
	}

	public function mahasiswa_diterima(){
		$this->db->where('NPM',$this->session->usern);
		$this->db->where('STATUSMHS',0);
		$data = $this->db->get('mahasiswa');
		return $data->num_rows();
	}

	public function ordik(){
		$this->db->where('NPMORD',$this->session->usern);
		$data = $this->db->get('no_ordik');
		return $data->num_rows();
	}

}

/* End of file berandaModel.php */
/* Location: ./application/models/mhs/berandaModel.php */