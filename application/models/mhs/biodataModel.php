<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BiodataModel extends CI_Model {

	public function mahasiswa(){
		if($this->session->enable_edit_biodata==true){
			$db = 'mahasiswa';
		}else{
			$db = 'mahasiswa_mendaftar';
		}
		$this->db->where($db.'.NPM',$this->session->usern);
		$this->db->join('prodi','prodi.KDPRODI = '.$db.'.KDPRODI','LEFT');
		$data = $this->db->get($db);
		return $data->result();
	}

	public function update($n){
		$this->db->set('ALAMATMHS', $n['alamat']);
		$this->db->set('KONTAKMHS', $n['telp']);
		$this->db->set('SERAGAMMHS', $n['seragam']);
		$this->db->set('SKSMHS', $n['sks']);
		$this->db->set('PEKERJAANMHS', $n['kerja']);
		$this->db->where('NPM',$this->session->usern);
		if($this->session->enable_edit_biodata==true){
			$this->db->where('STATUSMHS',0);
			$this->db->set('KORDXMHS', $n['kordx']);
			$this->db->set('KORDYMHS', $n['kordy']);
			$this->db->update('mahasiswa');
		}else{
			$this->db->set('KORDXMHSD', $n['kordx']);
			$this->db->set('KORDYMHSD', $n['kordy']);
			$this->db->update('mahasiswa_mendaftar');
		}
	}

}

/* End of file biodataModel.php */
/* Location: ./application/models/mhs/biodataModel.php */