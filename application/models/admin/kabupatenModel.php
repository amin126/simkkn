<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KabupatenModel extends CI_Model {

	public function view(){
		$kd = $this->mfungsi->tahun()->kode;
		$this->db->order_by('');
		$data = $this->db->get('kabupaten');
		return $data->result();
	}

	public function simpan($n){
		$data['name'] = $n['nama'];
		$this->db->insert('kabupaten',$data);
	}

	public function update($n,$k){
		$this->db->set('name', $n['nama']);
		$this->db->where('id', $k);
		$this->db->update('kabupaten');
	}

	public function load($kode){
		$this->db->where('id', $kode);
		$data = $this->db->get('kabupaten');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('id', $kode);
		$this->db->delete('kabupaten');
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */