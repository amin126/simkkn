<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdiModel extends CI_Model {
	public function cek(){
		return $this->db->count_all('prodi');
	}

	public function simpan($n){
		$this->db->insert_batch('prodi', $n);
	}

	public function update($n){
		$this->db->set('FAKPRODI',$n['fakultas']);
		$this->db->set('NAMAPRODI',$n['prodi']);
		$this->db->where('KDPRODI',$n['kode']);
		$this->db->update('prodi');
	}

	public function simpan1($n){
		$data = array(
			'KDPRODI'=>$n['kode'],
			'FAKPRODI'=>$n['fakultas'],
			'NAMAPRODI'=>$n['prodi']
		);
		$this->db->insert('prodi', $data);
	}

	public function cek1($n){
		$this->db->where('KDPRODI',$n);
		$this->db->from('prodi');
		return $this->db->count_all_results();
	}

	public function select(){
		return $this->db->get('prodi')->result();
	}
}

/* End of file prodiModel.php */
/* Location: ./application/models/admin/prodiModel.php */