<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BackupModel extends CI_Model
{
   
  // backup files in directory
  function files()
  {
     $opt = array(
       'src' => '../www', // dir name to backup
       'dst' => 'backup/files' // dir name backup output destination
     );
     
     // Codeigniter v3x
     $this->load->library('recurseZip_lib', $opt);
     $download = $this->recursezip_lib->compress();
     
     /* Codeigniter v2x
     $zip    = $this->load->library('recurseZip_lib', $opt);     
     $download = $zip->compress();
     */
     
     redirect(base_url($download));
  }
   
  // backup database.sql
  public function db()
  {
     $this->load->dbutil();
   
     $prefs = array(
       'format' => 'zip',
       'filename' => 'simkkn_backup.sql'
     );
     //$tabel = array('upload','kelompok_peserta','kapasitas_prodi','prodi','tahun','mahasiswa','kelompok','dosen');
     $tabel = array('tahun','prodi','mahasiswa','dosen','kelompok','kelompok_peserta','kapasitas_prodi','upload','pengaturan','users','notifikasi','mahasiswa_mendaftar','no_ordik','lembar_monitoring');
     $backup =& $this->dbutil->backup($prefs,$tabel);
   
     $db_name = 'backup-simkkn-' . date("Y-m-d-H-i-s") . '.zip'; // file name
     $save  = 'backup/db/' . $db_name; // dir name backup output destination
   
     $this->load->helper('file');
     //write_file($save, $backup);
   
     $this->load->helper('download');
     force_download($db_name, $backup);
  }

  //restore
  function restoredb($path='')
  {
    $isi_file = $this->mfungsi->file_get($path);
    $string_query = rtrim( $isi_file, "\n;" );
    $array_query = explode(";", $string_query);
    $ttl = 100/count($array_query);
    $i=0;
    $prc = 0;
    $fullquery='';
    $tabel = array('lembar_monitoring','upload','kelompok_peserta','kapasitas_prodi','prodi','mahasiswa','kelompok','dosen','tahun','pengaturan','users','notifikasi','mahasiswa_mendaftar','no_ordik');
    $this->db->query('SET FOREIGN_KEY_CHECKS = 0;');
    $this->db->query('DROP TABLE IF EXISTS '.implode(',', $tabel).';');
    $this->db->query('SET FOREIGN_KEY_CHECKS = 1;');
    foreach($array_query as $query)
    {
      $i++;
      if(floor($ttl*$i)!=$prc)
        if($prc<95)
          $this->mremaining->running(array('proses'=>$prc,'total'=>100));
      $this->db->query($query);
      $prc = floor($ttl*$i);
    }
    $this->db->select('upload.NPM AS NPM, upload.FILEUPL AS FILE, kelompok.KDKEL AS KEL');
    $this->db->join('kelompok_peserta','kelompok_peserta.NPM = upload.NPM');
    $this->db->join('kelompok','kelompok_peserta.KDKEL = kelompok.KDKEL');
    $listfile = $this->db->get('upload');

    $this->db->join('kelompok','kelompok.KDKEL = lembar_monitoring.KDKEL');
    $listfile2 = $this->db->get('lembar_monitoring');

    $ttl = 100/count($listfile->num_rows());
    $i=0;
    $this->mfile->empty_dir('./monitoring');
    $this->mfile->empty_dir('./dokumen');
    $this->mfile->empty_dir('./assets/images/peserta');
    $this->mfile->empty_dir('./assets/images/dosen');
    $this->mfile->empty_dir('./jarak');
    foreach ($listfile->result() as $key => $value) {
      $i++;
      @mkdir('./dokumen/'.$value->KEL);
      $this->mfile->copy_file('./upload/backup/dokumen/'.$value->KEL.'/'.$value->FILE,'./dokumen/'.$value->KEL.'/'.$value->FILE);
      $this->mremaining->running(array('proses'=>95,'total'=>100),'file');
    }

    foreach ($listfile2->result() as $key => $value) {
      $i++;
      @mkdir('./monitoring/'.$value->KDKEL);
      $this->mfile->copy_file('./upload/backup/monitoring/'.$value->KDKEL.'/'.$value->FILELM,'./monitoring/'.$value->KDKEL.'/'.$value->FILELM);
      $this->mremaining->running(array('proses'=>96,'total'=>100),'file');
    }

    if ($this->mfile->exists('./upload/backup/foto/') && $handle = opendir('./upload/backup/foto/')) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $this->mfile->copy_file('./upload/backup/foto/'.$entry,'./assets/images/peserta/'.$entry);
        }
      }
      $this->mremaining->running(array('proses'=>97,'total'=>100),'file');
      closedir($handle);
    }

    if ($this->mfile->exists('./upload/backup/ph_dosen/') && $handle = opendir('./upload/backup/ph_dosen/')) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $this->mfile->copy_file('./upload/backup/ph_dosen/'.$entry,'./assets/images/dosen/'.$entry);
        }
      }
      $this->mremaining->running(array('proses'=>98,'total'=>100),'file');
      closedir($handle);
    }

    if ($this->mfile->exists('./upload/backup/jarak/') && $handle = opendir('./upload/backup/jarak/')) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
            $this->mfile->copy_file('./upload/backup/jarak/'.$entry,'./jarak/'.$entry);
        }
      }
      $this->mremaining->running(array('proses'=>99,'total'=>100),'file');
      closedir($handle);
    }

    $this->mfile->empty_dir('./upload');

    $m = $this->mfungsi->setting('model_pembentukan');
    $res = $this->mfile->copy_file('./application/controllers/admin/metode/Metode_'.$m.'.php','./application/controllers/admin/metode/Gen.php');
  }
   
}