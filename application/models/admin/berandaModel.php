<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BerandaModel extends CI_Model {

	public function totalkkn(){
		$thn = $this->mfungsi->tahun();
		$this->db->where('KDTAHUN',$thn->kode);
		return $this->db->get('kelompok')->num_rows();
	}

	public function totalpeserta(){
		$thn = $this->mfungsi->tahun();
		return $this->db->query("SELECT * FROM `mahasiswa` WHERE NPM not in (SELECT NPM from kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN = '$thn->kode') ) AND STATUSMHS != '0'")->num_rows();
	}

	public function loadMhs(){
		$data =$this->db->get('mahasiswa')->result();
		$res = array();
		foreach ($data as $key => $value) {
			array_push($res, array(
				$value->NPM,
				$value->NAMAMHS,
				$this->atribut_mhs($value->NPM)->kode,
				$value->ALAMATMHS
			));
		}
		return $res;
	}

	private function atribut_mhs($nim){
		$data = $this->db->query("SELECT KDPRODI AS kode, ALAMATMHS AS alamat FROM mahasiswa WHERE NPM = '$nim'")->result();
		return $data[0];
	}

}


/* End of file berandaModel.php */
/* Location: ./application/models/admin/berandaModel.php */