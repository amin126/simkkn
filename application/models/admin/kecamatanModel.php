<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KecamatanModel extends CI_Model {

	public function view(){
		$this->db->order_by('kecamatan.KDKAB,kecamatan.NAMAKEC');
		$this->db->join('kabupaten','kabupaten.KDKAB = kecamatan.KDKAB');
		$data = $this->db->get('kecamatan');
		return $data->result();
	}

	public function simpan($n){
		$data['NAMAKEC'] = $n['kecamatan'];
		$data['KDKAB'] = $n['kabupaten'];
		$this->db->insert('kecamatan',$data);
	}

	public function update($n,$k){
		$this->db->set('NAMAKEC', $n['kecamatan']);
		$this->db->set('KDKAB', $n['kabupaten']);
		$this->db->where('KDKEC', $k);
		$this->db->update('kecamatan');
	}

	public function load($kode){
		$this->db->where('KDKEC', $kode);
		$data = $this->db->get('kecamatan');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('KDKEC', $kode);
		$this->db->delete('kecamatan');
	}

	public function kabupaten(){
		$this->db->order_by('KDKAB');
		$data = $this->db->get('kabupaten');
		return $data->result();
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */