<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatistikaModel extends CI_Model {

	
	public function prodi(){
		return $this->db->get('prodi')->result();
	}

	public function cek_kapasitasProdi(){
		return $this->db->query('SELECT IFNULL(sum(KAPRODI),0) AS TOTAL FROM `kapasitas_prodi` WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))')->row();
	}

	public function setKapasitasprodi($kel,$pro,$val){
		$this->db->where('KDKEL',$kel);
		$this->db->where('KDPRODI',$pro);
		$this->db->set('KAPRODI',$val);
		$this->db->update('kapasitas_prodi');
	}

	public function setKapasitas($kel,$val){
		$this->db->where('KDKEL',$kel);
		$this->db->set('KAPASITASKEL',$val);
		$this->db->update('kelompok');
	}

	// statistika

	public function monitoring($kel){
		$this->db->where('KDKEL',$kel);
		return $this->db->get('lembar_monitoring')->result();
	}

	public function select(){
		$kel = $this->kelompok();
		$data = [];
		foreach ($kel as $key => $value) {
			$kapasitas = $this->kapasitasprodi($value->KDKEL);
			$perprodi = [];
			foreach ($kapasitas as $k => $v) {
				$isi = $this->terisi($v->KDPRODI,$value->KDKEL);
				$perprodi[$v->KDPRODI] = array(
					'kode'=>$v->KDPRODI,
					'prodi'=>$v->NAMAPRODI,
					'kapasitas'=>$v->KAPRODI,
					'isi'=>$isi
				);
				//array_push($perprodi, );
			}
			$data[$value->KDKEL] = array(
				'kode'=>$value->KDKEL,
				'kelompok'=>$value->NAMAKEL,
				'alamat'=>$value->ALAMATKEL,
				'prodi'=>$perprodi
			);
			//array_push($data,);
		}
		//return $this->order($data);
		return $data;
	}

	private function order($data){
		$total = [];
		$jml = [];
		if(count($data)==0)
			return $data;
		foreach ($data as $i => $iv) {
			foreach ($iv['prodi'] as $j => $jv) {
				if(!isset($jml[$jv['kode']]))
					$jml[$jv['kode']] = 0; // prodi
				$jml[$jv['kode']] += $jv['isi'];

				if(!isset($total[$iv['kode']]))
					$total[$iv['kode']] = 0; //kel
				$total[$iv['kode']] += $jv['isi'];
			}
		}

		for ($i=0; $i < count($data)-1 ; $i++) { 
			for ($j=$i+1; $j < count($data) ; $j++) { 
				$a = $data[$i]['kode'];
				$b = $data[$j]['kode'];
				if($total[$b]<$total[$a]){
					$temp = $data[$i];
					$data[$i] = $data[$j];
					$data[$j] = $temp;
				}
			}
		}

		for ($i=0; $i < count($data[0]['prodi'])-1; $i++) { 
			for ($j=$i+1; $j < count($data[0]['prodi']); $j++) { 
				$a = $data[0]['prodi'][$i]['kode'];
				$b = $data[0]['prodi'][$j]['kode'];
				if($jml[$b]>$jml[$a]){
					for($k=0; $k < count($data); $k++) {
						$temp = $data[$k]['prodi'][$i];
						$data[$k]['prodi'][$i] = $data[$k]['prodi'][$j];
						$data[$k]['prodi'][$j] = $temp;
					}
				}
			}
		}

		return $data;
	}

	public function pengajuan($kdkel){
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where("upload.AJUKANUPL",1);
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		return $this->db->get('upload')->result();
	}

	public function diterima($kdkel){
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where("upload.AJUKANUPL",4);
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		return $this->db->get('upload')->result();
	}

	public function setAjuan($t,$k,$i,$a){
		if($a!=null)
			$this->db->set('MSGUPL',$a);	
		$this->db->set('AJUKANUPL',$t);
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$k'))");
		$this->db->where('KDUPL',$i);
		$this->db->update('upload');
	}

	public function cancelAjuan($k,$i){
		$this->db->set('AJUKANUPL',1);
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$k'))");
		$this->db->where('KDUPL',$i);
		$this->db->update('upload');
	}

	public function nilai($kdkel){
		$this->db->where("(mahasiswa.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->join('prodi','mahasiswa.KDPRODI = prodi.KDPRODI');
		$this->db->join('kelompok_peserta',"mahasiswa.NPM = kelompok_peserta.NPM AND kelompok_peserta.KDKEL = $kdkel",'LEFT');
		$this->db->order_by('mahasiswa.NAMAMHS,mahasiswa.NPM');
		return $this->db->get('mahasiswa')->result();
	}

	public function upload($kdkel){
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		if(isset($_POST['cari'])){
			$this->db->like('upload.NPM', $_POST['cari']);
			$this->db->or_like('upload.FILEUPL', $_POST['cari']);
			$this->db->or_like('mahasiswa.NAMAMHS', $_POST['cari']);
		}
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		$this->db->order_by('upload.TGLUPL','DESC');
		return $this->db->get('upload')->result();
	}

	public function kelompok($kode = ''){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('kelompok.KDTAHUN',$thn);
		if($kode!='')
			$this->db->where('kelompok.KDKEL',$kode);
		$this->db->order_by('kelompok.NAMAKEL');
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		return $this->db->get('kelompok')->result();
	}

	private function kapasitasprodi($kel){
		$this->db->where('KDKEL',$kel);
		$this->db->join('prodi','prodi.KDPRODI = kapasitas_prodi.KDPRODI','LEFT');
		return $this->db->get('kapasitas_prodi')->result();
	}

	private function terisi($prodi,$kel){
		$isi = $this->db->query("SELECT COUNT(*) AS ISI FROM mahasiswa WHERE NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kel') AND KDPRODI = '$prodi'")->result();
		return $isi[0]->ISI;
	}

	/* statis kemajuan kinerja kkn*/

	public function count_upload($kdkel){
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->from('upload');
		return $this->db->count_all_results();
	}

	public function count_monitoring($kdkel){
		$this->db->where('KDKEL',$kdkel);
		$this->db->from('lembar_monitoring');
		return $this->db->count_all_results();
	}

	public function count_size_upload($kdkel){
		$this->db->select_sum('SIZEUPL');
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$x = $this->db->get('upload')->result();
		$x = ($x[0]->SIZEUPL==null) ? 0 : $x[0]->SIZEUPL;
		$this->db->select_sum('SIZELM');
		$this->db->where('KDKEL',$kdkel);
		$y = $this->db->get('lembar_monitoring')->result();
		$y = ($y[0]->SIZELM==null) ? 0 : $y[0]->SIZELM;
		return $x+$y;
	}

	public function count_pengajuan($kdkel){
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where("AJUKANUPL",1);
		$this->db->from('upload');
		return $this->db->count_all_results();
	}

	public function count_pengajuan_diterima($kdkel){
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where("AJUKANUPL",4);
		$this->db->from('upload');
		return $this->db->count_all_results();
	}


	public function count_anggota($kdkel){
		$this->db->where("KDKEL",$kdkel);
		$this->db->from('kelompok_peserta');
		return $this->db->count_all_results();
	}

}

/* End of file statistikaModel.php */
/* Location: ./application/models/admin/statistikaModel.php */