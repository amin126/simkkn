<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengaturanModel extends CI_Model {

	public function baru(){
		$data = array();
		array_push($data, array(
			'JENISSET'=>'edit_biodata_mhs',
			'VALUESET'=>'1'
		));
		array_push($data, array(
			'JENISSET'=>'kabupaten_asal',
			'VALUESET'=>'sampang|pamekasan'
		));
		array_push($data, array(
			'JENISSET'=>'minimal_prodi',
			'VALUESET'=>'3'
		));
		array_push($data, array(
			'JENISSET'=>'model_pembentukan',
			'VALUESET'=>'a'
		));
		$this->db->insert_batch('pengaturan', $data);
	}

	public function minimal_prodi($kap){
		$this->db->set('VALUESET',$kap);
		$this->db->where('JENISSET','minimal_prodi');
		$this->db->update('pengaturan');
	}

	public function model_pembentukan($model){
		$this->db->set('VALUESET',$model);
		$this->db->where('JENISSET','model_pembentukan');
		$this->db->update('pengaturan');
	}

	public function setNew($n,$v=''){
		$this->db->where('JENISSET',$n);
		$this->db->from('pengaturan');
		$cek = $this->db->count_all_results(); 
		if($cek==0)
			$this->created($n,$v);
	}

	public function created($n,$v){
		$this->db->insert('pengaturan',array('JENISSET'=>$n,'VALUESET'=>$v));
	}

	public function api_key($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','api_key');
		$this->db->update('pengaturan');
	}

	public function filter_kab($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','kabupaten_asal');
		$this->db->update('pengaturan');
	}

	public function edit_biodata($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','edit_biodata_mhs');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_dpl($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_dpl');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_camat($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_camat');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_biro($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_biro');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_peserta($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_peserta');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_daftar($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_daftar');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_peserta_kelompok($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','hal_pstkelompok');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_uploadp($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_uploadp');
		$this->db->update('pengaturan');
	}

	public function edit_halaman_uploadd($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','halaman_uploadd');
		$this->db->update('pengaturan');
	}

	public function auto_skedul($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','auto_skedul');
		$this->db->update('pengaturan');
	}

	public function tgl_skedul($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','tgl_skedul');
		$this->db->update('pengaturan');
	}

	public function pimpinan_lppm($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','pimpinan_lppm');
		$this->db->update('pengaturan');
	}

	public function nomor_sertifikat($n){
		$this->db->set('VALUESET',$n);
		$this->db->where('JENISSET','nosertifikat');
		$this->db->update('pengaturan');
	}
}

/* End of file pengaturanModel.php */
/* Location: ./application/models/admin/pengaturanModel.php */