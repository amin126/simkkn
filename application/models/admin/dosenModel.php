<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosenModel extends CI_Model {

	public function view(){
		$this->db->order_by('NAMADPL');
		$data = $this->db->get('dosen');
		return $data->result();
	}

	public function allusername(){
		$this->db->order_by('NAMADPL');
		$data = $this->db->get('dosen');
		$d1 = $data->result();
		$usernames = array();
		foreach ($d1 as $key => $value) {
			array_push($usernames, $value->KDDPL);
		}
		return $usernames;
	}

	public function maxId(){
		$this->db->select_max('KDDPL','kode');
		$data = $this->db->get('dosen');
		return $data->result();
	}

	public function totaldata(){
		return $this->db->count_all('dosen');
	}

	public function simpan($n){
		$data['KDDPL'] = $n['kode'];
		$data['NAMADPL'] = $n['nama'];
		$data['KONTAKDPL'] = $n['kontak'];
		$data['ALAMATDPL'] = $n['alamat'];
		$this->db->insert('dosen',$data);
	}

	public function update($n,$k){
		$this->db->set('NAMADPL', $n['nama']);
		$this->db->set('KONTAKDPL', $n['kontak']);
		$this->db->set('ALAMATDPL', $n['alamat']);
		$this->db->where('KDDPL', $k);
		$this->db->update('dosen');
	}

	public function load($kode){
		$this->db->where('KDDPL', $kode);
		$data = $this->db->get('dosen');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('KDDPL', $kode);
		$this->db->delete('dosen');
	}

	public function kabupaten(){
		$this->db->order_by('KDKAB');
		$data = $this->db->get('kabupaten');
		return $data->result();
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */