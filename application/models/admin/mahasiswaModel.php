<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MahasiswaModel extends CI_Model {

	public function arsipkan(){

		//$this->db->query("UPDATE `mahasiswa` SET `STATUSMHS` = 0 WHERE `STATUSMHS` = 1 AND NPM IN (SELECT NPM FROM kelompok_peserta WHERE NILAIMHS <= 55 OR `NILAIMHS` IS NULL AND `KDKEL` IN (SELECT KDKEL from kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))");


		//tambah where `STATUSMHS` = 1 AND || `STATUSMHS` = 1 AND 

		$this->db->query("UPDATE `mahasiswa` SET `STATUSMHS` = 0 WHERE NPM IN (SELECT NPM FROM kelompok_peserta WHERE NILAIMHS <= 55 AND `NILAIMHS` IS NOT NULL AND `KDKEL` IN (SELECT KDKEL from kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))");

		$this->db->query("UPDATE `mahasiswa` SET `STATUSMHS` = 2 WHERE NPM IN (SELECT NPM FROM kelompok_peserta WHERE NILAIMHS > 55 AND `NILAIMHS` IS NOT NULL AND `KDKEL` IN (SELECT KDKEL from kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))");
	}

	public function null_nilai(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM AND mahasiswa.STATUSMHS = 1');
		$this->db->join('kelompok','kelompok_peserta.KDKEL = kelompok.KDKEL AND kelompok.KDTAHUN =  '.$thn);
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL');
		$this->db->where('kelompok_peserta.NILAIMHS',NULL);
		$this->db->order_by('kelompok.NAMAKEL');
		return $this->db->get('kelompok_peserta')->result();
	}

	public function keluaranggota($npm){
		$this->db->select('kelompok_peserta.KDKEL');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM AND mahasiswa.STATUSMHS = 1');
		$this->db->where('kelompok_peserta.NPM',$npm);
		$kel = $this->db->get('kelompok_peserta')->result();

		if(count($kel)>0){
			$this->db->where('NPM',$npm);
			$this->db->where('KDKEL',$kel[0]->KDKEL);
			$this->db->delete('kelompok_peserta');

			$this->db->where('NPM',$npm);
			$this->db->set('STATUSMHS',0);
			$this->db->update('mahasiswa');
		}
	}

	public function verifikasi($npm){
		$this->db->set('STATUSMHS',1);
		$this->db->where('NPM',$npm);
		$this->db->where('STATUSMHS',0);
		$this->db->update('mahasiswa');
	}

	public function getAllUsername(){
		$this->db->order_by('NPM');
		$data = $this->db->get('mahasiswa');
		$d1 = $data->result();
		$usernames = array();
		foreach ($d1 as $key => $value) {
			array_push($usernames, $value->NPM);
		}
		return $usernames;
	}

	public function btlverifikasi($npm){
		$this->db->set('STATUSMHS',0);
		$this->db->where('NPM',$npm);
		$this->db->where('STATUSMHS',1);
		$this->db->update('mahasiswa');
	}

	public function kelompok_peserta_hapus($npm){
		$this->db->where('NPM', $npm);
		$this->db->delete('kelompok_peserta');
	}

	public function kelompok_peserta($kel,$npm){
		$this->db->insert('kelompok_peserta',array(
			'KDKEL'=>$kel,
			'NPM'=>$npm
		));
	}

	public function view($p = 0,$c = []){
		if(count($c)>0){
			if($c[0]!=''){
				$this->db->like('(mahasiswa.NPM', $c[0]);
				$this->db->or_like('mahasiswa.NAMAMHS', $c[0]);
				$this->db->or_like('mahasiswa.KONTAKMHS', $c[0]);
				$this->db->or_where("mahasiswa.ALAMATMHS LIKE '%$c[0]%' ESCAPE '!')");
			}

			if($c[1]!=''){
				if($c[1]==3){
					$this->db->where('(mahasiswa.STATUSMHS = 0)');
					$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM AND kelompok_peserta.NILAIMHS IS NOT NULL');
					$this->db->join('(SELECT NPM,KDKEL, COUNT(*) AS LULUS FROM kelompok_peserta b GROUP BY b.NPM)b','b.NPM = mahasiswa.NPM ');
				}else if($c[1]==0){
					$this->db->where('(mahasiswa.STATUSMHS = 0)');
					$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM AND kelompok_peserta.NILAIMHS IS NULL');
					$this->db->join('(SELECT NPM,KDKEL, COUNT(*) AS LULUS FROM kelompok_peserta b GROUP BY b.NPM)b','b.NPM = mahasiswa.NPM ');
				}else{
					$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM','LEFT');
					$this->db->where('(mahasiswa.STATUSMHS = '.$c[1].')');
					$this->db->join('(SELECT NPM,KDKEL, COUNT(*) AS LULUS FROM kelompok_peserta b GROUP BY b.NPM)b','b.NPM = mahasiswa.NPM','LEFT');
				}
			}else{
				$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM','LEFT');
				$this->db->join('(SELECT NPM,KDKEL, COUNT(*) AS LULUS FROM kelompok_peserta b GROUP BY b.NPM)b','b.NPM = mahasiswa.NPM','LEFT');
			}
		}
		$this->db->select('mahasiswa.*,prodi.*,IFNULL(b.LULUS,0) AS LULUS,kelompok_peserta.NPM AS EXISTS');
		$this->db->join('prodi','mahasiswa.KDPRODI = prodi.KDPRODI','LEFT');
		$this->db->order_by('prodi.NAMAPRODI,mahasiswa.NAMAMHS');
		$this->db->limit(5,$p);
		$data = $this->db->get('mahasiswa');
		return $data->result();
	}

	public function cek($c){
		$this->db->where('NPM',$c);
		$this->db->from('mahasiswa');
		return $this->db->count_all_results();
	}

	public function getMahasiswa(){
		$this->db->where('STATUSMHS', 0);
		$this->db->or_where('STATUSMHS', 1);
		$data = $this->db->get('mahasiswa');
		return $data->result();
	}

	public function getJmlKapasitas(){
		$this->db->select_sum('KAPRODI');
		$this->db->where('KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1))');
		$query = $this->db->get('kapasitas_prodi')->result(); 
		return $query[0]->KAPRODI;
	}

	public function getkapasistakelompokkabupaten($n){
		$this->db->select_sum('KAPRODI');
		foreach ($n as $a => $b) {
			$this->db->or_where('KDKEL',$b);
		}
		$query = $this->db->get('kapasitas_prodi');
		return $query->result()[0]->KAPRODI;
	}

	public function getkapasistakelompokkabupatenisi($n){
		foreach ($n as $a => $b) {
			$this->db->or_where('KDKEL',$b);
		}
		$this->db->from('kelompok_peserta');
		return $this->db->count_all_results();
	}

	public function totaldata($c){
		if(count($c)>0){
			if($c[0]!=''){
				$this->db->like('(NPM', $c[0]);
				$this->db->or_like('NAMAMHS', $c[0]);
				$this->db->or_like('KONTAKMHS', $c[0]);
				$this->db->or_where("ALAMATMHS LIKE '%$c[0]%' ESCAPE '!')");
			}

			if($c[1]!=''){
				if($c[1]=='3'){
					$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM AND kelompok_peserta.NILAIMHS IS NOT NULL');
					$c[1] = 0;
				}else if($c[1]=='0'){
					$this->db->join('kelompok_peserta','kelompok_peserta.NPM = mahasiswa.NPM AND kelompok_peserta.NILAIMHS IS NULL');
				}
				$this->db->where('(STATUSMHS = '.$c[1].')');
			}
		}
		
		$this->db->select('SERAGAMMHS');
		return $this->db->get('mahasiswa')->result();
	}

	public function allusername(){
		$this->db->order_by('NPM');
		$data = $this->db->get('mahasiswa');
		$d1 = $data->result();
		$usernames = array();
		foreach ($d1 as $key => $value) {
			array_push($usernames, $value->NPM);
		}
		return $usernames;
	}

	public function maxId(){
		$this->db->select_max('NPM','kode');
		$data = $this->db->get('mahasiswa');
		return $data->result();
	}

	public function simpan($n){
		$data = array(
			'NPM'=>$n['nim'],
			'NAMAMHS'=>$n['nama'],
			'ALAMATMHS'=>$n['alamat'],
			'KONTAKMHS'=>$n['kontak'],
			'SERAGAMMHS'=>$n['seragam'],
			'KDPRODI'=>$n['prodi'],
			'SKSMHS'=>$n['sks'],
			'KORDXMHS'=>$n['kordx'],
			'KORDYMHS'=>$n['kordy'],
			'PEKERJAANMHS'=>$n['kerja'],
			'STATUSMHS'=>$n['status'],
			'TGLREGMHS'=>$this->mfungsi->timeNow()
		);
		$this->db->insert('mahasiswa',$data);

		$this->db->where('NPM',$n['nim']);
		$this->db->delete('mahasiswa_mendaftar');
	}

	public function update($n,$k){
		$this->db->set('NPM', $n['nim']);
		$this->db->set('NAMAMHS', $n['nama']);
		$this->db->set('KONTAKMHS', $n['kontak']);
		$this->db->set('KDPRODI', $n['prodi']);
		$this->db->set('ALAMATMHS', $n['alamat']);
		$this->db->set('SERAGAMMHS', $n['seragam']);
		$this->db->set('KORDXMHS', $n['kordx']);
		$this->db->set('KORDYMHS', $n['kordy']);
		$this->db->set('SKSMHS', $n['sks']);
		$this->db->set('PEKERJAANMHS', $n['kerja']);
		$this->db->where('NPM', $k);
		$this->db->update('mahasiswa');
	}

	public function load($kode){
		$this->db->where('mahasiswa.NPM', $kode);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('mahasiswa');
		return $data->result()[0];
	}

	public function mahasiswastatus($n){
		$this->db->where('mahasiswa.STATUSMHS', $n);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('mahasiswa');
		return $data->result();
	}

	public function semuaMhs(){
		$this->db->where("mahasiswa.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN IN (SELECT KDTAHUN FROM tahun WHERE STATUSTAHUN = 1)))");
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('xmahasiswa');
		return $data->result();
	}

	public function hapus($kode){
		$this->db->where('NPM', $kode);
		$this->db->delete('mahasiswa');
	}

	public function prodi(){
		return $this->db->get('prodi')->result();
	}

	public function prodikapasitas(){
		$dprod = $this->prodi();
		$thn = $this->mfungsi->tahun()->kode;
		$data = array();
		foreach ($dprod as $key => $value) {
			$res = $this->kapprodi($value->KDPRODI,$thn);
			array_push($data,array(
				'JML'=> $res,
				'NAMAPRODI'=>$value->NAMAPRODI,
				'KDPRODI'=>$value->KDPRODI
			));
		}
		return $data;
	}

	private function kapprodi($prodi,$thn){
		$this->db->where("NPM IN (SELECT NPM FROM mahasiswa WHERE KDPRODI = '$prodi') AND KDKEL IN (SELECT KDKEL FROM kelompok WHERE KDTAHUN = '$thn')");
		$this->db->get('kelompok_peserta');
		return $this->db->count_all_results();
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */