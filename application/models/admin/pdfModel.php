<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PdfModel extends CI_Model {

	public function file($kode){
		$this->db->where('KDUPL',$kode);
		return $this->db->get('upload')->result();
	}

	public function file2($kode){
		$this->db->where('KDLM',$kode);
		return $this->db->get('lembar_monitoring')->result();
	}

}

/* End of file pdfModel.php */
/* Location: ./application/models/admin/pdfModel.php */