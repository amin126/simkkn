<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BiroModel extends CI_Model {

	public function view(){
		$this->db->where('STATUS',3);
		$this->db->order_by('USERN');
		$data = $this->db->get('users');
		return $data->result();
	}

	public function simpan($n){
		$data['USERN'] = $n['username'];
		$data['NAMA'] = $n['nama'];
		$data['PASSN'] = MD5($n['pass']);
		$data['STATUS'] = 3;
		$this->db->insert('users',$data);
	}

	public function update($n,$k){
		$this->db->set('USERN', $n['username']);
		$this->db->set('NAMA', $n['nama']);
		if($n['pass']!=''){
			$this->db->set('PASSN', MD5($n['pass']));
			if($this->session->usern == $k){
				$this->session->set_userdata('pass',MD5($n['pass']));
			}
		}
		$this->db->where('STATUS',3);
		$this->db->where('USERN', $k);
		$this->db->update('users');
	}

	public function load($kode){
		$this->db->where('USERN', $kode);
		//$this->db->where('USERN != ', $this->session->usern);
		$data = $this->db->get('users');
		return $data->result()[0];
	}

	public function hapus($kode){
		$this->db->where('STATUS',3);
		$this->db->where('USERN', $kode);
		$this->db->where('USERN != ', $this->session->usern);
		$this->db->delete('users');
	}

	public function kabupaten(){
		$this->db->order_by('KDKAB');
		$data = $this->db->get('kabupaten');
		return $data->result();
	}

}

/* End of file tahunModel.php */
/* Location: ./application/models/admin/tahunModel.php */