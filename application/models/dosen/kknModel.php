<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KknModel extends CI_Model {

	public function get(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->where('KDTAHUN',$thn);
		return $this->db->get('kelompok')->result();
	}

	public function cek($kode){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('KDDPL',$this->session->usern);
		$this->db->where('KDTAHUN',$thn);
		$this->db->where('KDKEL',$kode);
		$this->db->from('kelompok');
		return $this->db->count_all_results(); 
	}

	public function set($kode){
		$cek = $this->cek($kode);
		if($cek>0){
			$this->session->set_userdata('dpl_kkn_aktif', $kode);
		}
	}

	public function aktif(){
		if(isset($this->session->dpl_kkn_aktif)){
			return $this->session->dpl_kkn_aktif;
		}else{
			$this->session->set_userdata('dpl_kkn_aktif', $this->get()[0]->KDKEL);
			return $this->session->dpl_kkn_aktif;
		}
	}

}

/* End of file kknModel.php */
/* Location: ./application/models/dosen/kknModel.php */