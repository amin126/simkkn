<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadModel extends CI_Model {

	public function view(){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->where("(upload.NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		if(isset($_POST['cari'])){
			$this->db->like('upload.NPM', $_POST['cari']);
			$this->db->or_like('upload.FILEUPL', $_POST['cari']);
			$this->db->or_like('mahasiswa.NAMAMHS', $_POST['cari']);
		}
		$this->db->join('mahasiswa','mahasiswa.NPM = upload.NPM');
		$this->db->order_by('upload.TGLUPL','DESC');
		return $this->db->get('upload')->result();
	}

	public function cek_arsip(){
		$this->db->where('kelompok_peserta.NILAIMHS is NOT NULL', NULL, FALSE);
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM AND mahasiswa.STATUSMHS = 2');
		$this->db->join('kelompok','kelompok.KDKEL = kelompok_peserta.KDKEL');
		$this->db->join('tahun','tahun.KDTAHUN = kelompok.KDTAHUN AND tahun.STATUSTAHUN = 1');
		return $this->db->get('kelompok_peserta')->num_rows();
	}

	public function lmonit(){
		$this->db->where('KDKEL',$this->session->dpl_kkn_aktif);
		return $this->db->get('lembar_monitoring')->result();
	}

	public function kodekel(){
		$this->db->where('NPM',$this->session->usern);
		return $this->db->get('kelompok_peserta')->result();
	}

	public function cek($kode){
		$this->db->where('KDUPL',$kode);
		$this->db->where('NPM',$this->session->usern);
		$this->db->from('upload');
		return $this->db->count_all_results();
	}

	public function file($kode){
		$this->db->where('KDUPL',$kode);
		return $this->db->get('upload')->result();
	}

	public function ajukan($kode){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->set('AJUKANUPL',1);
		//$this->db->where('AJUKANUPL',0);
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where('KDUPL',$kode);
		$this->db->update('upload');
	}

	public function batalajukan($kode){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->set('AJUKANUPL',0);
		$this->db->set('MSGUPL',null);
		//$this->db->where('AJUKANUPL',1);
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where('KDUPL',$kode);
		$this->db->update('upload');
	}

	public function batalajukanulang($kode){
		$kdkel = $this->session->dpl_kkn_aktif;
		$this->db->set('AJUKANUPL',2);
		$this->db->where("(NPM IN (SELECT NPM FROM kelompok_peserta WHERE KDKEL = '$kdkel'))");
		$this->db->where('KDUPL',$kode);
		$this->db->update('upload');
	}

	public function infofile($kode=null){
		$this->db->join('kelompok','kelompok.KDDPL = dosen.KDDPL');
		$this->db->join('kelompok_peserta','kelompok.KDKEL = kelompok_peserta.KDKEL');
		if($kode!=null)
			$this->db->join('upload','upload.NPM = kelompok_peserta.NPM AND upload.KDUPL = '.$kode);
		$this->db->where('dosen.KDDPL',$this->session->usern);
		$data = $this->db->get('dosen')->result();
		return $data[0];
	}

	public function infofile2($kode=null){
		$this->db->join('kelompok','kelompok.KDDPL = dosen.KDDPL');
		$this->db->join('kelompok_peserta','kelompok.KDKEL = kelompok_peserta.KDKEL');
		if($kode!=null)
			$this->db->join('lembar_monitoring','lembar_monitoring.KDKEL = kelompok.KDKEL AND lembar_monitoring.KDLM = '.$kode);
		$this->db->where('dosen.KDDPL',$this->session->usern);
		$data = $this->db->get('dosen')->result();
		return $data[0];
	}

	public function simpan($n){
		$data = array(
			'FILELM'=>$n['file'],
			'SIZELM'=>$n['ukuran'],
			'TGLLM'=>$n['tgl'],
			'KDKEL'=>$n['kel'],
			'KDLM'=>$n['id'],
			'HASHLM'=>$n['hash']
		);
		$this->db->insert('lembar_monitoring',$data);
	}

	public function delete($kode){
		$this->db->where('KDLM',$kode);
		$this->db->where('KDKEL',$this->session->dpl_kkn_aktif);
		$this->db->delete('lembar_monitoring');
	}

	public function file2($kode){
		$this->db->where('KDLM',$kode);
		return $this->db->get('lembar_monitoring')->result();
	}

	public function cek2($kode){
		$this->db->where('KDLM',$kode);
		$this->db->where('KDKEL',$this->session->dpl_kkn_aktif);
		$this->db->from('lembar_monitoring');
		return $this->db->count_all_results();
	}

	public function maxId(){
		$this->db->select_max('KDLM','kode');
		$data = $this->db->get('lembar_monitoring');
		return ($data->result()[0]->kode)+1;
	}

}

/* End of file uploadModel.php */
/* Location: ./application/models/dosen/uploadModel.php */