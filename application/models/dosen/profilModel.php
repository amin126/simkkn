<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilModel extends CI_Model {

	public function get(){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('kelompok.KDDPL',$this->session->usern);
		$this->db->where('kelompok.KDTAHUN',$thn);
		$this->db->where('kelompok.KDKEL',$this->session->dpl_kkn_aktif);
		$this->db->join('dosen','dosen.KDDPL = kelompok.KDDPL','LEFT');
		return $this->db->get('kelompok')->result()[0];
	}

	public function anggota(){
		$this->db->where('kelompok_peserta.KDKEL',$this->session->dpl_kkn_aktif);
		$this->db->join('mahasiswa','mahasiswa.NPM = kelompok_peserta.NPM','LEFT');
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		return $this->db->get('kelompok_peserta')->result();
	}

	public function biodata($kode){
		$thn = $this->mfungsi->tahun()->kode;
		$this->db->where('mahasiswa.NPM', $kode);
		$this->db->where_in('NPM','(SELECT NPM FROM kelompok_peserta where KDKEL ="'.$this->session->dpl_kkn_aktif.'")',false);
		$this->db->join('prodi','prodi.KDPRODI = mahasiswa.KDPRODI','LEFT');
		$data = $this->db->get('mahasiswa');
		return $data->result()[0];
	}
	
}

/* End of file profilModel.php */
/* Location: ./application/models/dosen/profilModel.php */